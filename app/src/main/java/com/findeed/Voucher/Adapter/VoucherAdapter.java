package com.findeed.Voucher.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.findeed.Bot.ViewHolder.AddChildVH;
import com.findeed.Bot.ViewHolder.BaseViewHolder;
import com.findeed.Bot.ViewHolder.BotDocImageVH;
import com.findeed.Bot.ViewHolder.BotImageResponseVH;
import com.findeed.Bot.ViewHolder.ChatInputVH;
import com.findeed.Bot.ViewHolder.ChatObject;
import com.findeed.Bot.ViewHolder.ChatResponseVH;
import com.findeed.Bot.ViewHolder.ImageResponseVH;
import com.findeed.Bot.ViewHolder.SendVoterImageVH;
import com.findeed.Bot.ViewHolder.SendVouchersVH;
import com.findeed.Bot.ViewHolder.UserLocationVH;
import com.findeed.R;

import java.util.ArrayList;

public class VoucherAdapter extends RecyclerView. Adapter<BaseViewHolder> {

    private ArrayList<ChatObject> chatObjects;
    public VoucherAdapter(ArrayList<ChatObject> chatObjects) {
        this.chatObjects = chatObjects;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());


        // Create the ViewHolder based on the viewType
        final View itemView;

            if(i==ChatObject.RESPONSE_OBJECT) {
                itemView = inflater.inflate(R.layout.chat_response_layout, viewGroup, false);
                return new ChatResponseVH(itemView);
            }else if(i==ChatObject.INPUT_OBJECT){
                itemView = inflater.inflate(R.layout.chat_input_layout, viewGroup, false);
                return new ChatInputVH(itemView);
            }
            else if(i==ChatObject.IMAGE_OBJECT) {
                itemView=inflater.inflate(R.layout.image_chat_input,viewGroup,false);
                return  new ImageResponseVH(itemView);
            }else if(i==ChatObject.DOC_IMAGE){
                itemView=inflater.inflate(R.layout.withoutimage_chatinput,viewGroup,false);
                return new BotDocImageVH(itemView);

            }else if(i==ChatObject.SEND_CONTACT){

                itemView=inflater.inflate(R.layout.send_voucher,viewGroup,false);
                return  new SendVouchersVH(itemView);
            }else if(i==ChatObject.SEND_LOCATION){
                itemView=inflater.inflate(R.layout.send_current_location_with_address,viewGroup,false);
                return new UserLocationVH(itemView);
            }else if(i==ChatObject.ADD_CHILD){
                itemView=inflater.inflate(R.layout.add_child_deatil_layout,viewGroup,false);
                return new AddChildVH(itemView);
            } else if(i==ChatObject.VOTER_IMAGE){
                itemView=inflater.inflate(R.layout.voterid_img_chatinput,viewGroup,false);
                return new SendVoterImageVH(itemView);
            }
            else {
                itemView=inflater.inflate(R.layout.bot_image_response_layout,viewGroup,false);
                return  new BotImageResponseVH(itemView);
            }


        }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder baseViewHolder, int i) {
        baseViewHolder.onBindView(chatObjects.get(i));
    }


    @Override
    public int getItemViewType(int position) {
        return chatObjects.get(position).getType();
    }

    @Override
    public int getItemCount() {
        return chatObjects.size();
    }
}
