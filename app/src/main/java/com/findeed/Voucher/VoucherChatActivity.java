package com.findeed.Voucher;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintSet;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.findeed.API.AadhaarAndPAN.RequestData.SaveDocument;
import com.findeed.API.AadhaarAndPAN.ResponseData.SaveDocumentResponse;
import com.findeed.API.Customer;
import com.findeed.API.DocumentData;
import com.findeed.API.LocationService;
import com.findeed.API.MsiteFetchDataRequest;
import com.findeed.API.PanDetails;
import com.findeed.API.PanRequestPayload;
import com.findeed.API.SaveDocumentDetailsRequest;
import com.findeed.API.SaveVoucherDetailsRequest;
import com.findeed.API.VData;
import com.findeed.API.ValidateOTPResponse;
import com.findeed.API.VerifyUserIdDocRequestPayload;
import com.findeed.API.Voucher;
import com.findeed.API.VoucherFlags;
import com.findeed.API.VoucherResponseData;
import com.findeed.ApplicationData;
import com.findeed.Bot.ChatActivity;
import com.findeed.Bot.ChatContract;
import com.findeed.Bot.ChatPresenter;
import com.findeed.Bot.ViewHolder.ChatObject;
import com.findeed.Bot.WebViewActivity;
import com.findeed.CameraActivity;
import com.findeed.KYC.VideoIdKYCREq.MsiteFetchRequest;
import com.findeed.KYC.VideoIdKYCREq.headers;
import com.findeed.KYC.VideoIdKycRes.KYCInfo;
import com.findeed.KYC.VideoIdKycRes.OriginalKYCInfo;
import com.findeed.KYC.VideoIdKycRes.ResponseData;
import com.findeed.PrivacyActivity;
import com.findeed.R;
import com.findeed.TermsActivity;
import com.findeed.Voucher.Adapter.VoucherAdapter;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.findeed.ApplicationData.CLIENT_CODE;
import static com.findeed.ApplicationData.KHOSALA_URL;
import static com.findeed.ApplicationData.validateOTPResponse;
import static com.findeed.ApplicationData.voucherResponseData;
import static com.findeed.Bot.ChatActivity.electricityUri;
import static com.findeed.Bot.ChatActivity.isNullOrBlank;
import static com.findeed.Bot.ChatActivity.sha256;


public class VoucherChatActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, ChatContract.View {


    public BroadcastReceiver locationUpdateReceiver;

    public BroadcastReceiver predictedLocationReceiver;
    ProgressDialog pd;
    boolean doubleBackToExitPressedOnce;


    public static VData VData_Persistance;
    public static VData VData_Temp=new VData();
    public static ArrayList<DocumentData> documents=new ArrayList<>();
    public static DocumentData documentData;

    public static Bitmap PanFrontBitmap;
    public static Bitmap PanBackBitmap;
    public static Bitmap AadhaarFrontBitmap;
    public static Bitmap VoterFrontBitmap;
    public static Bitmap VoterBackBitmap;

    HashMap<String,Integer> docStatus=new HashMap<>();


    RecyclerView voucherRecyclerView;
    public static   VoucherAdapter voucherAdapter;
    private ChatPresenter presenter;
    public static int selectedIndex;
    public static boolean isNumberPickerValueChanged = false;

    private HashMap<String ,String > voucherResponse = new HashMap<>();

    int REQUEST_TAKE_PHOTO_PAN=1;
    public int PAN_PICK_PHOTO=2;
    public  int REQUEST_TAKE_PHOTO_VOTER_FRONT=3,REQUEST_PICK_PHOTO_VOTER_FRONT=5;
    public  int REQUEST_TAKE_PHOTO_VOTER_BACK=6;
    public  int REQUEST_PICK_PHOTO_VOTER_BACK=7;
    public int PROCEED_TO_MSITE=9;
    public static final int LOCATION_PERMISSION=15;
    public  static  final  int CLICK_PAN=16;
    public static final int REQUEST_CODE_ASK_PERMISSIONS=17;
    public static final int REQUEST_PICK_PHOTO_PAN_BACK=18;
    public  static final int REQUEST_TAKE_PHOTO_PAN_BACK=19;
    public  static  final  int PAN_BACK_CLICK_PERMISSION=20;
    public static final int REQUEST_LOCATION=21;
    public static final int VOTER_BACK_PERMISSION=22;

    String DOC_TYPE;
    String cityName, addressLine,pin,loc;
    public  static String  userLocation,USER_FACE;
    Double latitude=null;
    Double longitude=null;
    public  static Uri panURI, voterBackURI, voterFrontURI,panBackUri;
    ImageView imgyess, imgnoo;
    ImageView imgaadhartick;

    String base64String;

    String phone;
    int CUSTOMER_ID;
    int customerVoucherId;
    Customer c1 ;

   public static VoucherResponseData VOUCHER_DATA;

    boolean next=true;
    boolean isProceedClicked;
    boolean isAcquantanceClicked;
    boolean isRelationShipLengthClicked;
    boolean isNumberOfChildrenClicked;
    boolean isPanCardClicked;
    boolean isVoterIdClicked;
    boolean isBureauAgreeClicked;
    boolean isAddressClicked;
    boolean isCcsPermissionClicked;

    private DrawerLayout dl;
    private ActionBar actionBar;
    private NavigationView nv;
    ImageView navicon;


    LinearLayout voucherWelcome,voucherRelation;
    Button voucherProceed,saveRelationShipAge,selectAndProceedChildren,pickPanPhoto, clickPanPhoto, btnPanContinue,panEdit,panConfirm;
    LinearLayout voucherFamily,voucherColleague,voucherFriend,voucherIdontKnow,noPanCard,lLayoutProcessingPan;
    LinearLayout relationShipAge,numberOfChildren,panCardLayout,iHavePan,iDontHavePan,panOption1,lLayoutEditPanInfo,voterOrAadharLayout;
    LinearLayout editVoterIdLayout;
    NumberPicker relationShipAgePicker;
    RelativeLayout selectChildTextView;
    LinearLayout llayoutFillAddressManually,useMyCurrentLocationLayout,selectOptiionForVoterId;
    RelativeLayout llUpdateVoterInfo;
    BottomSheetDialog childList;
    TextView selectedChild;
    EditText PanDob, PanId, PanName,currentAddressLineOne,currentCity,currentPincode,currentLocality,addressLineOne, locality, city, pinCode;
    EditText edtVoterId, edtVoterName, edtVoterDOB, edtVoterAddress;
    ScrollView updatePanDetail;
    ImageView panEditCross,useCurrentLocationCross;
    LinearLayout clickPickForVoterBack,processingVoterId,agreeOrcancelLayout,yesOrNo,no,yes;
    Button pickVoterFront,clickVoterFront,clickVoterBack,continueVoterId,pickVoterBack,voterConfirm,clickPanBack,pickPanBack,editVoterId;
    Button btnagree,btnCancelBureau,btnCurrentLocation,useMyLocationSave,btnManually,btnAddManuallyAdd,WillUploadPan,getBackLater,iHavePanNow,btnVoterInfoUpdate,iDecline;
    LinearLayout proceedLayoutAadhaar,llayoutAddressManually,llayoutAdditionalInfo,btnMoreInfoYes, btnMoreInfoNo,llayoutNoPanCard,clickPickPanBack;
    ImageView imageNo, imageYes,clearVoterEdit;
    RadioGroup radioGroupSex;
    DatePickerDialog picker;
    ImageView  numberPickerDownImagePink, numberPickerDownImageGray,numberpickerUpimageGray, numberPickerUpimagePink;
    ImageView imgFamily, imgColleague, imgFriend, imgUnknown;
    ImageView imagepan, imagenopan,startOver;



    Bitmap bitmap;
HashMap<String ,String > voucherFlag=new HashMap<>();



    public static String selectedAge;
    private String currentPhotoPath;
    public  String VOTER_ADDRESS;
    public  String VOTER_DOCUMENT_ID;
    public  String VOTER_FIRST_NAME;
    public  String VOTER_LAST_NAME;
    public  String VOTER_DOB;
    public  String VOTER_GENDER;
    public  String AADHAR_ADDRESS;
    public  String AADHAR_DOC_ID;
    public  String AADHAR_FIRST_NAME;
    public  String AADHAR_LAST_NAME;
    public  String AADHAR_DOB;
    public  String AADHAR_GENDER;
    public  OriginalKYCInfo UserAddressData;
    String panNumber;
    String  panName,sex;
    String  panDob;
    Bitmap VOTER_FRONT,VOTER_BACK;
    String END_POINT = "http://157.245.102.194:8080/los-qa/";
    Gson gson = new Gson();
    boolean isChoosed=false;
    boolean isAadhaarChoosed=false,isCurrentAddressSame=false;


    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            view.clearFocus();


            InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.voucher_activity);

        VOUCHER_DATA=voucherResponseData;
        ApplicationData.isItVoucher=true;


       /* Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable ex) {
                //inform yourself that the activity unexpectedly stopped
                //or
                Log.d("Exception",ex.toString());
            }
        });*/


        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);


        voucherRecyclerView=findViewById(R.id.voucher_chat_recycler_view);
        voucherWelcome=findViewById(R.id.ll_welcomevouch);
        iDecline=findViewById(R.id.btn_voucherdeclined);
        voucherProceed = findViewById(R.id.btn_voucherproceed);
        voucherRelation=findViewById(R.id.voucher_relation);
        voucherFamily=findViewById(R.id.btn_layout_family);
        voucherColleague=findViewById(R.id.btn_layout_colleague);
        voucherFriend=findViewById(R.id.btn_layout_friend);
        voucherIdontKnow=findViewById(R.id.btn_layout_do_not_know);
        relationShipAge=findViewById(R.id.llIncomeRange);
        relationShipAgePicker=findViewById(R.id.numberPicker);
        saveRelationShipAge=findViewById(R.id.btn_addRange);
        numberOfChildren=findViewById(R.id.number_of_children);
        selectChildTextView =findViewById(R.id.select_child);
        selectedChild=findViewById(R.id.selected_child);
        selectAndProceedChildren=findViewById(R.id.select_and_proceed_children);
        panCardLayout=findViewById(R.id.pan_card_layout);
        iHavePan=findViewById(R.id.i_have_pan_card);
        iDontHavePan=findViewById(R.id.i_do_not_have_pan_card);
        noPanCard=findViewById(R.id.llNoPanCard);
        panOption1=findViewById(R.id.llpanoptionselect);
        pickPanPhoto=findViewById(R.id.btn_pickpanphoto);
        clickPanPhoto=findViewById(R.id.btn_takepanphoto);
        btnPanContinue = findViewById(R.id.btn_pan_continue);
        lLayoutProcessingPan=findViewById(R.id.llprocessingpan);
        lLayoutEditPanInfo = findViewById(R.id.edit_pan_detail_layout);
        editVoterIdLayout = findViewById(R.id.llvoterdetailedit);
        panEdit=findViewById(R.id.edit_pan_info);
        panConfirm=findViewById(R.id.confirm_pan);
        updatePanDetail=findViewById(R.id.update_pan_info);
        PanId=findViewById(R.id.v_pan_number);
        PanName=findViewById(R.id.v_pan_name);
        PanDob=findViewById(R.id.v_pan_dob);
        panEditCross=findViewById(R.id.pan_cross);
        voterOrAadharLayout = findViewById(R.id.select_voter_or_aadhar);
        Button savePanDetail = findViewById(R.id.save_pan_detail);
        LinearLayout voterId=findViewById(R.id.voter_id);
        LinearLayout aadhaarCard=findViewById(R.id.aadhaar_card);
        pickVoterFront=findViewById(R.id.pick_voter_front);
        clickVoterFront = findViewById(R.id.click_voter_front);
         selectOptiionForVoterId=findViewById(R.id.select_option_for_voter_id);
        clickPickForVoterBack = findViewById(R.id.pick_click_voter_back);
        clickVoterBack=findViewById(R.id.click_voter_back);
        continueVoterId = findViewById(R.id.continue_voter);
        pickVoterBack=findViewById(R.id.pick_voter_back);
        processingVoterId = findViewById(R.id.processing_voter_id);
        agreeOrcancelLayout = findViewById(R.id.bureau_agree_cancel);
        btnagree = findViewById(R.id.bureau_agree);
        btnCancelBureau = findViewById(R.id.bureau_cancel);
        yesOrNo = findViewById(R.id.yes_or_no);
        voterConfirm = findViewById(R.id.btn_idconfirmvoterid);
        no=findViewById(R.id.btn_no);
        yes=findViewById(R.id.btn_yes);
        proceedLayoutAadhaar = findViewById(R.id.aadhaar_proceed_layout);
        Button aadhaarProceed=findViewById(R.id.aadhaar_proceed_button);
        llayoutAddressManually = findViewById(R.id.ll_Address_Manually);
        btnCurrentLocation = findViewById(R.id.btn_currentlocation);
        currentAddressLineOne=findViewById(R.id.use_my_location_address_line_one);
        currentCity=findViewById(R.id.use_my_location_city_data);
        currentPincode=findViewById(R.id.use_my_location_pin_code_data);
        useMyLocationSave=findViewById(R.id.use_my_location_save);
        useCurrentLocationCross=findViewById(R.id.use_current_location_cross);
        currentLocality=findViewById(R.id.use_my_location_locality_data);
        useMyCurrentLocationLayout=findViewById(R.id.user_my_current_location);
        useCurrentLocationCross=findViewById(R.id.use_current_location_cross);
        btnManually = findViewById(R.id.btn_manually);
        llayoutFillAddressManually = findViewById(R.id.llfilladdressmanually);
        btnAddManuallyAdd = findViewById(R.id.btn_add_address_manually);
        addressLineOne = findViewById(R.id.address_line_one);
        locality = findViewById(R.id.locality);
        pinCode = findViewById(R.id.pin_code);
        city = findViewById(R.id.city);
        llayoutAdditionalInfo = findViewById(R.id.ll_Additional_Info);
        btnMoreInfoYes = findViewById(R.id.btn_moreinfo_yes);
        btnMoreInfoNo = findViewById(R.id.btn_moreinfo_no);
        imageNo = findViewById(R.id.imageno);
        imageYes = findViewById(R.id.imageyes);
        btnMoreInfoNo = findViewById(R.id.btn_moreinfo_no);
        llayoutNoPanCard = findViewById(R.id.llNoPanCard);
        WillUploadPan = findViewById(R.id.I_Will_Back);
        getBackLater=findViewById(R.id.get_back_later);
        iHavePanNow=findViewById(R.id.i_have_pan_now);
        clickPickPanBack=findViewById(R.id.pick_click_pan_back);
        pickPanBack=findViewById(R.id.pick_pan_back);
        clickPanBack=findViewById(R.id.click_pan_back);
        llUpdateVoterInfo = findViewById(R.id.ll_UpdateVoterInfo);
        clearVoterEdit = findViewById(R.id.img_cancel_voteredit);
        editVoterId = findViewById(R.id.btn_editinfovoterid);
        edtVoterId = findViewById(R.id.id_number);
        edtVoterName = findViewById(R.id.voter_name);
        edtVoterAddress = findViewById(R.id.voteraddress);
        edtVoterDOB = findViewById(R.id.voterdob);
        btnVoterInfoUpdate = findViewById(R.id.btn_VoterInfoUpdate);
        radioGroupSex=findViewById(R.id.voter_sex);
        Button aadhaarCancel = findViewById(R.id.btn_cancel_aadhar);

        numberPickerDownImagePink = findViewById(R.id.numberpickerdownimagepink);
        numberpickerUpimageGray= findViewById(R.id.numberpickerUpimagegray);
        numberPickerDownImageGray = findViewById(R.id.numberpickerdownimagegray);
        numberPickerUpimagePink = findViewById(R.id.numberpickerUpimagepink);
        imgFamily = findViewById(R.id.img_family);
        imgColleague = findViewById(R.id.img_colleague);
        imgFriend = findViewById(R.id.img_friend);
        imgUnknown = findViewById(R.id.img_notknow);
        imagepan = findViewById(R.id.imagehavepan);
        imagenopan = findViewById(R.id.imagenopan);
        imgyess = findViewById(R.id.imageyess);
        imgnoo = findViewById(R.id.imagenoo);
        startOver=findViewById(R.id.icon_restart);
        navicon = findViewById(R.id.nav_icon);


        SharedPreferences sf = getSharedPreferences("mobileNumber", MODE_PRIVATE);
        phone = sf.getString("phone", null);




        locationUpdateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Location newLocation = intent.getParcelableExtra("location");
                ApplicationData.mapLocation = newLocation;


                try {
                    latitude = ApplicationData.mapLocation.getLatitude();
                    longitude = ApplicationData.mapLocation.getLongitude();
                    Geocoder gcd = new Geocoder(getBaseContext(), Locale.getDefault());
                    List<Address> addresses;

                    addresses = gcd.getFromLocation(latitude,
                            longitude, 1);
                    if (addresses.size() > 0) {
                        cityName = addresses.get(0).getLocality();
                        addressLine = addresses.get(0).getAddressLine(0);
                        pin = addresses.get(0).getPostalCode();
                        loc = addresses.get(0).getSubLocality();
                    }

                    if(pd.isShowing()){
                        pd.dismiss();
                        currentAddressLineOne.setText(addressLine);
                        currentCity.setText(cityName);
                        currentPincode.setText(pin);
                        currentLocality.setText(loc);

                        presenter.botMessage("Please confirm your address",useMyCurrentLocationLayout,ChatPresenter.chatObjects.size()+1);

                    }


                } catch (Exception e) {
                    e.printStackTrace();

                }
            }

        };



        predictedLocationReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Location newLocation = intent.getParcelableExtra("location");
                ApplicationData.mapLocation = newLocation;
            }
        };

        LocalBroadcastManager.getInstance(this).registerReceiver(
                locationUpdateReceiver,
                new IntentFilter("LocationUpdated"));



        LocalBroadcastManager.getInstance(this).registerReceiver(
                predictedLocationReceiver,
                new IntentFilter("PredictLocation"));





        startOver.setOnClickListener(v -> {


            final Dialog dialog = new Dialog(VoucherChatActivity.this);
            dialog.setContentView(R.layout.custom_dialog_startover);

            TextView txtYesStartOver = dialog.findViewById(R.id.txt_yesStartOver);
            TextView txtNoStartOver = dialog.findViewById(R.id.txt_cancelStartOver);

            dialog.getWindow().setBackgroundDrawable(new
                    ColorDrawable(Color.TRANSPARENT));

            txtYesStartOver.setOnClickListener(v1 -> {
                Intent intent = getIntent();
                new StartOver(intent).execute();
                dialog.dismiss();


            });


            txtNoStartOver.setOnClickListener(v1 -> {
                dialog.dismiss();
            });

            dialog.show();
        });


        try{
            CUSTOMER_ID= voucherResponseData.getData().get(0).getCustomerId();
            customerVoucherId=voucherResponseData.getData().get(0).getCustomerVoucherId();
            c1 = new Customer();
            c1.setCustomerId(CUSTOMER_ID);
            Log.i("CUSTOMER_ID",String.valueOf(CUSTOMER_ID));

        imgaadhartick = findViewById(R.id.imageaadharvoucher);



        }catch (Exception e){
            e.printStackTrace();

        }


        dl = findViewById(R.id.activity_main);

        nv = findViewById(R.id.nv);
        nv.setNavigationItemSelectedListener(this);
        nv.setItemIconTintList(null);

        View headerView = nv.getHeaderView(0);
        TextView navUsername =  headerView.findViewById(R.id.user_name);
        ImageView navOpenProfile = headerView.findViewById(R.id.open_profile);

     //   navUsername.setText(validateOTPResponse.getData().getFirstName()+" "+validateOTPResponse.getData().getLastName());



        aadhaarCancel.setOnClickListener(v -> {

            isAadhaarChoosed=false;
            proceedLayoutAadhaar.setVisibility(View.GONE);
            ChatPresenter.chatObjects.subList(ChatPresenter.chatObjects.size()-2,ChatPresenter.chatObjects.size()).clear();
            voucherAdapter.notifyDataSetChanged();
        });



        iDecline.setOnClickListener(v -> {
            final Dialog declineDialog = new Dialog(VoucherChatActivity.this);
            declineDialog.setContentView(R.layout.dialog_declineconfirmation);

            TextView goBack = declineDialog.findViewById(R.id.decline_goback);
            TextView yesIdecline = declineDialog.findViewById(R.id.decline_yesdecline);

            Objects.requireNonNull(declineDialog.getWindow()).setBackgroundDrawable(new
                    ColorDrawable(Color.TRANSPARENT));
            declineDialog.show();


            goBack.setOnClickListener(v1 -> declineDialog.dismiss());

                yesIdecline.setOnClickListener(v110 -> {

                });
        });

        voucherFamily.setOnTouchListener((v, event) -> {
            imgFamily.setVisibility(View.VISIBLE);
            imgColleague.setVisibility(View.INVISIBLE);
            imgFriend.setVisibility(View.INVISIBLE);
            imgUnknown.setVisibility(View.INVISIBLE);


            return false;
        });

        voucherColleague.setOnTouchListener((v, event) -> {

            imgFamily.setVisibility(View.INVISIBLE);
            imgColleague.setVisibility(View.VISIBLE);
            imgFriend.setVisibility(View.INVISIBLE);
            imgUnknown.setVisibility(View.INVISIBLE);

            return false;
        });

        voucherFriend.setOnTouchListener((v, event) -> {

            imgFamily.setVisibility(View.INVISIBLE);
            imgColleague.setVisibility(View.INVISIBLE);
            imgFriend.setVisibility(View.VISIBLE);
            imgUnknown.setVisibility(View.INVISIBLE);

            return false;
        });

        voucherIdontKnow.setOnTouchListener((v, event) -> {

                imgFamily.setVisibility(View.INVISIBLE);
                imgColleague.setVisibility(View.INVISIBLE);
                imgFriend.setVisibility(View.INVISIBLE);
                imgUnknown.setVisibility(View.VISIBLE);
                return false;

        });

        iHavePan.setOnTouchListener((v, event) -> {
            imagepan.setVisibility(View.VISIBLE);
            imagenopan.setVisibility(View.INVISIBLE);
            return false;
        });

        iDontHavePan.setOnTouchListener((v, event) -> {

            imagenopan.setVisibility(View.VISIBLE);
            imagepan.setVisibility(View.INVISIBLE);
            return false;
        });



        btnVoterInfoUpdate.setOnClickListener(v -> {


            try {

                if (isNullOrBlank(edtVoterName.getText().toString()) || isNullOrBlank(edtVoterId.getText().toString()) ||  isNullOrBlank(edtVoterDOB.getText().toString()) ||  isNullOrBlank(edtVoterAddress.getText().toString())) {

                    if(isNullOrBlank(edtVoterName.getText().toString())){
                        Toast.makeText(VoucherChatActivity.this, "Please enter name", Toast.LENGTH_SHORT).show();
                    }else  if(isNullOrBlank(edtVoterId.getText().toString())){
                        Toast.makeText(VoucherChatActivity.this, "Please enter document Id", Toast.LENGTH_SHORT).show();
                    }else if(isNullOrBlank(edtVoterDOB.getText().toString())){
                        Toast.makeText(VoucherChatActivity.this, "Please enter valid date of birth", Toast.LENGTH_SHORT).show();
                    }else if(isNullOrBlank(edtVoterAddress.getText().toString())){
                        Toast.makeText(VoucherChatActivity.this, "Please enter Address", Toast.LENGTH_SHORT).show();
                    }


                } else {

                    if(!isAlpha(edtVoterName.getText().toString())){
                        Toast.makeText(VoucherChatActivity.this, "Name can not contain numbers", Toast.LENGTH_SHORT).show();
                    }else if(!isValidId(edtVoterId.getText().toString())){
                        Toast.makeText(VoucherChatActivity.this, "Inavlid voter id details", Toast.LENGTH_SHORT).show();
                    }

                    else {

                        int id= radioGroupSex.getCheckedRadioButtonId();
                        if(id==findViewById(R.id.rb_male).getId()){
                            sex="Male";
                        }else  if(id==findViewById(R.id.rb_female).getId()){
                            sex="Female";
                        }else
                            sex="Other";
                        presenter.onEditTextActionDone("Voter Id:" + edtVoterId.getText().toString() + "\n" + "Name: " + edtVoterName.getText().toString() + "\n" + "Date of birth:" + edtVoterDOB.getText().toString() + "\n" + "Address: " + edtVoterAddress.getText().toString()+ "\n" + edtVoterAddress.getText().toString()+"\n"+"Gender: "+sex, null, ChatPresenter.chatObjects.size() + 1);
                        presenter.botMessage("Thank you  for providing the ID and Address proofs.", null, ChatPresenter.chatObjects.size() + 1);
                        presenter.botMessage("Please click \"I Agree\" to allow Findeed to share the details with the lending partners and check your credit information with the credit bureau. Click here for the detailed Terms & Conditions.", agreeOrcancelLayout, ChatPresenter.chatObjects.size() + 1);
                        llUpdateVoterInfo.setVisibility(View.GONE);
                        DOC_TYPE="EDITED_VOTER_ID";
                        InputMethodManager imm =(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(findViewById(android.R.id.content).getRootView()
                                .getWindowToken(), 0);
                        new SaveDocumentDetails().execute();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();

            }


        });



        edtVoterDOB.setOnTouchListener((v, event) -> {

            final int DRAWABLE_LEFT = 0;
            final int DRAWABLE_TOP = 1;
            final int DRAWABLE_RIGHT = 2;
            final int DRAWABLE_BOTTOM = 3;

            if (event.getAction() == MotionEvent.ACTION_UP) {

                final Calendar cldr = Calendar.getInstance();
                cldr.add(Calendar.YEAR, -23);

                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker



                picker = new DatePickerDialog(VoucherChatActivity.this, R.style.MyDatePickerDialogStyle, (view, year1, month1, dayOfMonth) -> edtVoterDOB.setText(dayOfMonth + "/" + (month1 + 1) + "/" + year1),year, month, day);

                picker.show();
                picker.getButton(DatePickerDialog.BUTTON_NEGATIVE).setTextColor(getColor(R.color.dark_gray));
                picker.getButton(DatePickerDialog.BUTTON_POSITIVE).setTextColor(getColor(R.color.dark_pink));
                picker.getDatePicker().setMaxDate(cldr.getTimeInMillis());

                return true;

            }
            return false;
        });


        editVoterId.setOnClickListener(v -> {
            editVoterIdLayout.setVisibility(View.GONE);
            edtVoterId.setText(VOTER_DOCUMENT_ID);
            edtVoterName.setText(VOTER_FIRST_NAME);
            edtVoterDOB.setText(VOTER_DOB);
            edtVoterAddress.setText(VOTER_ADDRESS);


            presenter.botMessage("Please edit VOTER details in the following window", llUpdateVoterInfo, ChatPresenter.chatObjects.size() + 1);

        });



        clearVoterEdit.setOnClickListener(v -> {
            llUpdateVoterInfo.setVisibility(View.GONE);
            ChatPresenter.chatObjects.subList(ChatPresenter.chatObjects.size() - 1, ChatPresenter.chatObjects.size()).clear();
            voucherAdapter.notifyDataSetChanged();

        });


        pickPanBack.setOnClickListener(v -> {

            try {
                clickPickPanBack.setVisibility(View.GONE);
                Intent galleryIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
                galleryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, false);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent, REQUEST_PICK_PHOTO_PAN_BACK);

            } catch (Exception e) {
                e.printStackTrace();
            }

        });

        clickPanBack.setOnClickListener(v -> {
            clickPickPanBack.setVisibility(View.GONE);
            if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Intent takePictureIntent = new Intent(this, CameraActivity.class);
                //Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                        final File file=new File(storageDir, "pic.jpg");
                        photoFile=file;
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    if (photoFile != null) {
                        panBackUri = FileProvider.getUriForFile(this,
                                "com.findeed.fileprovider",
                                photoFile);
                        takePictureIntent.putExtra("image_side","BACK SIDE");
                        takePictureIntent.putExtra("doc_type","PAN");
                        startActivityForResult(takePictureIntent,REQUEST_TAKE_PHOTO_PAN_BACK);
                    }
                }

            } else {
                requestPermissions(new String[]{
                                Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE
                        }

                        , PAN_BACK_CLICK_PERMISSION);

            }

        });






        iDontHavePan.setOnClickListener(view -> {
            presenter.onEditTextActionDone("I don't have PAN card", null, ChatPresenter.chatObjects.size() + 1);
            presenter.botMessage("We are sorry, PAN card is mandatory to process the application. You can upload the PAN card if you do have one or get back to us when you get the card details. Your information will be saved with us", llayoutNoPanCard, ChatPresenter.chatObjects.size() + 1);
            panCardLayout.setVisibility(View.GONE);
        });

        WillUploadPan.setOnClickListener(view -> {
            llayoutNoPanCard.setVisibility(View.GONE);
            presenter.onEditTextActionDone("I will upload the PAN card", null, ChatPresenter.chatObjects.size() + 1);
            presenter.botMessage("Great, Please choose from the options below to upload your PAN card", panOption1, ChatPresenter.chatObjects.size() + 1);
            //  currentPos=ChatPresenter.chatObjects.size()+1;

        });

        getBackLater.setOnClickListener(v -> {
            llayoutNoPanCard.setVisibility(View.GONE);
            presenter.botMessage("Please get back to us when you have the PAN card details and we will certainly assist you with your loan.", iHavePanNow, ChatPresenter.chatObjects.size()+1);


        });


        iHavePanNow.setOnClickListener(v -> {
            iHavePanNow.setVisibility(View.GONE);
            presenter.botMessage("Great, Please choose from the options below to upload your PAN card", panOption1, ChatPresenter.chatObjects.size() + 1);

        });

        PanDob.setOnTouchListener((v, event) -> {

            final int DRAWABLE_LEFT = 0;
            final int DRAWABLE_TOP = 1;
            final int DRAWABLE_RIGHT = 2;
            final int DRAWABLE_BOTTOM = 3;

            if (event.getAction() == MotionEvent.ACTION_UP) {


                final Calendar cldr = Calendar.getInstance();
                cldr.add(Calendar.YEAR, -23);

                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog

                picker = new DatePickerDialog(VoucherChatActivity.this, R.style.MyDatePickerDialogStyle, (view, year12, month12, dayOfMonth) -> PanDob.setText(dayOfMonth + "/" + (month12 + 1) + "/" + year12),year, month, day);

                picker.show();
                picker.getButton(DatePickerDialog.BUTTON_NEGATIVE).setTextColor(getColor(R.color.dark_gray));
                picker.getButton(DatePickerDialog.BUTTON_POSITIVE).setTextColor(getColor(R.color.dark_pink));
                picker.getDatePicker().setMaxDate(cldr.getTimeInMillis());

                return true;

            }
            return false;
        });




        btnMoreInfoNo.setOnClickListener(v -> {

            final Dialog dialog = new Dialog(VoucherChatActivity.this);
            dialog.setContentView(R.layout.custom_dialog_nolocation);
            TextView textView = dialog.findViewById(R.id.txt_goback);


            Window window = dialog.getWindow();
            assert window != null;
            window.setGravity(Gravity.CENTER);

            Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new
                    ColorDrawable(Color.TRANSPARENT));
            textView.setOnClickListener(v17 -> {

                dialog.dismiss();

                llayoutAdditionalInfo.setVisibility(View.VISIBLE);

            });

            dialog.show();

        });



        btnAddManuallyAdd.setOnClickListener(v -> {

            if (isNullOrBlank(addressLineOne.getText().toString())|| isNullOrBlank(pinCode.getText().toString()) || isNullOrBlank(locality.getText().toString())|| isNullOrBlank(city.getText().toString())) {
                if(isNullOrBlank(addressLineOne.getText().toString())){
                    Toast.makeText(VoucherChatActivity.this, "Please enter Address line 1", Toast.LENGTH_SHORT).show();
                }else if(isNullOrBlank(locality.getText().toString())){
                    Toast.makeText(VoucherChatActivity.this, "Please enter Locality", Toast.LENGTH_SHORT).show();

                }else if(isNullOrBlank(city.getText().toString())){
                    Toast.makeText(VoucherChatActivity.this, "Please enter city", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(VoucherChatActivity.this, "Please enter pin code", Toast.LENGTH_SHORT).show();

                }
            } else {

                if(!isValidPin(pinCode.getText().toString())){
                    Toast.makeText(VoucherChatActivity.this, "Please enter correct pin code", Toast.LENGTH_SHORT).show();
                }else {
                    llayoutFillAddressManually.setVisibility(View.GONE);
                    userLocation = addressLineOne.getText().toString() + "," + pinCode.getText().toString();
                    Bitmap staticmage = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                            R.drawable.school_static_image);
                    presenter.sendUserLocation(staticmage,useMyCurrentLocationLayout,ChatPresenter.chatObjects.size()+1);
                    presenter.botMessage("Thank you for completing the process. Your reference will help us and "+VOUCHER_DATA.getData().get(0).getCustomerName()+" with their loan application",null,ChatPresenter.chatObjects.size()+1);
                    String address=addressLineOne.getText().toString()+", "+pinCode.getText().toString()+","+locality.getText().toString()+","+city.getText().toString();
                    new SaveVoucherStatus(phone,"1").execute();
                    new SendVoucherAddress(address).execute();

                }
            }


        });

        btnManually.setOnClickListener(v -> {
            llayoutAddressManually.setVisibility(View.GONE);
            presenter.botMessage("Please enter your current address", llayoutFillAddressManually, ChatPresenter.chatObjects.size() + 1);

        });


        useMyLocationSave.setOnClickListener(v -> {

            if(isNullOrBlank(currentAddressLineOne.getText().toString())|| isNullOrBlank(currentLocality.getText().toString())|| isNullOrBlank(currentCity.getText().toString()) || isNullOrBlank(currentPincode.getText().toString())){
                if(isNullOrBlank(currentAddressLineOne.getText().toString())){
                    Toast.makeText(VoucherChatActivity.this, "Please enter Address line 1", Toast.LENGTH_SHORT).show();
                }else if(isNullOrBlank(currentLocality.getText().toString())){
                    Toast.makeText(VoucherChatActivity.this, "Please enter Locality", Toast.LENGTH_SHORT).show();

                }else if(isNullOrBlank(currentCity.getText().toString())){
                    Toast.makeText(VoucherChatActivity.this, "Please enter city", Toast.LENGTH_SHORT).show();


                }else {
                    Toast.makeText(VoucherChatActivity.this, "Please enter pin code", Toast.LENGTH_SHORT).show();

                }
            }else {
                if(!isValidPin(currentPincode.getText().toString())){
                    Toast.makeText(VoucherChatActivity.this, "Please enter correct pin code", Toast.LENGTH_SHORT).show();


                }else {
                    new SendCurrentLocation().execute();
                    String  address=currentAddressLineOne.getText().toString()+", "+currentLocality.getText().toString()+" ,"+currentCity.getText().toString()+" ,"+currentPincode.getText().toString();
                    new SendVoucherAddress(address).execute();
                }
            }

        });


        btnMoreInfoYes.setOnClickListener(v -> {
            llayoutAdditionalInfo.setVisibility(View.GONE);
            requestPermissions(new String[]{
                            Manifest.permission.READ_CONTACTS,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.READ_SMS},
                    REQUEST_CODE_ASK_PERMISSIONS);

        });

        btnMoreInfoNo.setOnTouchListener((v, event) -> {
            imageNo.setVisibility(View.INVISIBLE);
            imageYes.setVisibility(View.INVISIBLE);
            return false;
        });
        btnMoreInfoYes.setOnTouchListener((v, event) -> {

            imageYes.setVisibility(View.VISIBLE);
            imageNo.setVisibility(View.GONE);
            return false;
        });



        useCurrentLocationCross.setOnClickListener(v -> {
            useMyCurrentLocationLayout.setVisibility(View.GONE);
            ChatPresenter.chatObjects.subList(ChatPresenter.chatObjects.size()-1,ChatPresenter.chatObjects.size()).clear();
            voucherAdapter.notifyDataSetChanged();
        });

        btnCurrentLocation.setOnClickListener(v -> {
            //Toast.makeText(getApplicationContext(), "Sorry!, I'm in development phase", Toast.LENGTH_SHORT).show();

//            llayoutAddressManually.setVisibility(View.GONE);
//            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
//                new UseMyCurrentLocation().execute();
//
//            }else{
//                requestPermissions(new String[]{
//                        Manifest.permission.ACCESS_FINE_LOCATION,
//                        Manifest.permission.ACCESS_COARSE_LOCATION
//                }, LOCATION_PERMISSION);
//            }

            llayoutAddressManually.setVisibility(View.GONE);

            boolean isGps=hasGPSDevice(VoucherChatActivity.this);
            if(isGps){

                final LocationManager manager = (LocationManager) VoucherChatActivity.this.getSystemService(Context.LOCATION_SERVICE);
                if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER) && hasGPSDevice(getApplicationContext())) {
                    enableLoc();

                }else {


                    if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        final Intent locationService = new Intent(VoucherChatActivity.this, LocationService.class);
                        VoucherChatActivity.this.startService(locationService);
                        VoucherChatActivity.this.bindService(locationService, serviceConnection, Context.BIND_AUTO_CREATE);

                        callForLocation();

                    }else{
                        requestPermissions(new String[]{
                                Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION
                        }, LOCATION_PERMISSION);
                    }


                }




            }else {
                Toast.makeText(VoucherChatActivity.this, "Phone does not have GPS", Toast.LENGTH_SHORT).show();
            }


        });





        aadhaarProceed.setOnClickListener(v -> {

            proceedLayoutAadhaar.setVisibility(View.GONE);
            Intent i = new Intent(VoucherChatActivity.this, WebViewActivity.class);
            startActivityForResult(i, PROCEED_TO_MSITE);
        });


        btnMoreInfoYes.setOnClickListener(v -> {
            llayoutAdditionalInfo.setVisibility(View.GONE);


            requestPermissions(new String[]{
                            Manifest.permission.READ_CONTACTS,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.READ_SMS},
                    REQUEST_CODE_ASK_PERMISSIONS);

        });

        no.setOnTouchListener((v, event) -> {
            imgnoo.setVisibility(View.VISIBLE);
            imgyess.setVisibility(View.GONE);

            return false;
        });

        yes.setOnTouchListener((v, event) -> {

            imgnoo.setVisibility(View.GONE);
            imgyess.setVisibility(View.VISIBLE);

            return false;
        });


        no.setOnClickListener(v -> {
            yesOrNo.setVisibility(View.GONE);
            if (ContextCompat.checkSelfPermission(VoucherChatActivity.this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
                presenter.onEditTextActionDone("No", null, ChatPresenter.chatObjects.size() + 1);
                presenter.botMessage("Do you want to use your current location or fill it by yourself", llayoutAddressManually, ChatPresenter.chatObjects.size() + 1);


            } else {
                presenter.botMessage("We would require some additional information from you for smooth processing of your loan application", llayoutAdditionalInfo, ChatPresenter.chatObjects.size() + 1);
            }
            voucherFlag.put("isLocationSame","2");
            new SaveVoucherFlags().execute();
            new SaveVoucherStatus(phone,"1").execute();

        });



        yes.setOnClickListener(v -> {

            isCurrentAddressSame=true;
            yesOrNo.setVisibility(View.GONE);
            presenter.onEditTextActionDone("Yes", null, ChatPresenter.chatObjects.size() + 1);

            if (ContextCompat.checkSelfPermission(VoucherChatActivity.this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(VoucherChatActivity.this, Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED ) {
                presenter.botMessage("Thank you for completing the process. Your reference will help us and "+VOUCHER_DATA.getData().get(0).getCustomerName()+" with their loan application",null,ChatPresenter.chatObjects.size()+1);
            new SaveVoucherStatus(phone,"1").execute();

            } else {
                presenter.botMessage("We need some additonal information to verify how you are connected to "+voucherResponseData.getData().get(0).getCustomerName()+" and help process her loan.", llayoutAdditionalInfo, ChatPresenter.chatObjects.size() + 1);
            }

            voucherFlag.put("isLocationSame","1");
            new SaveVoucherFlags().execute();
            new SaveVoucherStatus(phone,"1").execute();



            // new SaveUserAddress().execute();
//            new ChatActivity.SaveCustomerFlag().execute();

        });

        btnagree.setOnClickListener(v -> {

            agreeOrcancelLayout.setVisibility(View.GONE);
            if (isAadhaarChoosed) {
                voucherFlag.put("bureauPermission","1");
                new SaveVoucherFlags().execute();
                presenter.botMessage("Is the address in your Aadhar card your current address?", yesOrNo, ChatPresenter.chatObjects.size() + 1);
            } else {
                voucherFlag.put("bureauPermission","1");
                new SaveVoucherFlags().execute();
                presenter.botMessage("Is the address in your VoterId card your current address?", yesOrNo, ChatPresenter.chatObjects.size() + 1);
            }


        });

        btnCancelBureau.setOnClickListener(v -> {

            final Dialog dialog = new Dialog(VoucherChatActivity.this);
            dialog.setContentView(R.layout.custom_dialog_nolocation);
            TextView textView = dialog.findViewById(R.id.txt_goback);
            TextView text = dialog.findViewById(R.id.bureau_check_no_text);
            text.setText("Please click 'I Agree', without your permission we can not proceed further in this loan process");
            voucherFlag.put("bureauPermission","2");
            new SaveVoucherFlags().execute();

            Window window = dialog.getWindow();
            assert window != null;
            window.setGravity(Gravity.CENTER);

            dialog.getWindow().setBackgroundDrawable(new
                    ColorDrawable(Color.TRANSPARENT));
            textView.setOnClickListener(v17 -> {

                dialog.dismiss();

                agreeOrcancelLayout.setVisibility(View.VISIBLE);

            });

            dialog.show();



        });


        voterConfirm.setOnClickListener(v -> {

            editVoterIdLayout.setVisibility(View.GONE);
            presenter.botMessage("Thank you  for providing the ID and Address proofs.", null, ChatPresenter.chatObjects.size() + 1);
            presenter.botMessage("Please click \"I Agree\" to allow Findeed to share the details with the lending partners and check your credit information with the credit bureau. Click here for the detailed Terms & Conditions.", agreeOrcancelLayout, ChatPresenter.chatObjects.size() + 1);
            DOC_TYPE="VOTER_ID";
            new SaveDocumentDetails().execute();


        });



        continueVoterId.setOnClickListener(v -> {
            continueVoterId.setVisibility(View.GONE);
            processingVoterId.setVisibility(View.VISIBLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            DOC_TYPE = "VOTER_ID";
            new GetDocumentData().execute();


        });


        aadhaarCard.setOnClickListener(v -> {

            presenter.onEditTextActionDone("Aadhaar Card", null, ChatPresenter.chatObjects.size() + 1);
            voterOrAadharLayout.setVisibility(View.GONE);
            presenter.botMessage("Steps to follow:" + getResources().getString(R.string.aadhaar_steps), proceedLayoutAadhaar, ChatPresenter.chatObjects.size() + 1);
            isAadhaarChoosed = true;
            DOC_TYPE = "AADHAAR";
        });

        aadhaarCard.setOnTouchListener((v, event) -> {
            imgaadhartick.setVisibility(View.VISIBLE);
            return false;
        });

        pickVoterBack.setOnClickListener(v -> {
            Intent galleryIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
            galleryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            galleryIntent.setType("image/*");
            startActivityForResult(galleryIntent, REQUEST_PICK_PHOTO_VOTER_BACK);
        });




        clickVoterBack.setOnClickListener(v -> {
            clickPickForVoterBack.setVisibility(View.GONE);
            if ( checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
                Intent takePictureIntent = new Intent(this, CameraActivity.class);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                        final File file=new File(storageDir, "pic.jpg");
                        photoFile=file;
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    if (photoFile != null) {
                        voterBackURI = FileProvider.getUriForFile(getApplicationContext(),
                                "com.findeed.fileprovider",
                                photoFile);
                        takePictureIntent.putExtra("image_side","BACK SIDE");
                        takePictureIntent.putExtra("doc_type","VOTER");
                        startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO_VOTER_BACK);
                    }
                }
            }else {
                requestPermissions(new String[]{
                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                }, VOTER_BACK_PERMISSION);
            }


        });



        pickVoterFront.setOnClickListener(v -> {
            selectOptiionForVoterId.setVisibility(View.GONE);
            Intent galleryIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
            galleryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            galleryIntent.setType("image/*");
            startActivityForResult(galleryIntent, REQUEST_PICK_PHOTO_VOTER_FRONT);
        });





        clickVoterFront.setOnClickListener(v -> {
            selectOptiionForVoterId.setVisibility(View.GONE);
            if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Intent takePictureIntent = new Intent(this, CameraActivity.class);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                        final File file=new File(storageDir, "pic.jpg");
                        photoFile=file;
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    if (photoFile != null) {
                        voterFrontURI = FileProvider.getUriForFile(this,
                                "com.findeed.fileprovider",
                                photoFile);
                        Log.i("Voter_front",String.valueOf(voterFrontURI));
                        takePictureIntent.putExtra("image_side","FRONT SIDE");
                        takePictureIntent.putExtra("doc_type","VOTER");
                        startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO_VOTER_FRONT);
                    }
                }

            } else {
                requestPermissions(new String[]{
                                Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE
                        }

                        , 4);

            }


        });



        voterId.setOnClickListener(v -> {

            voterOrAadharLayout.setVisibility(View.GONE);
            presenter.onEditTextActionDone("Voter Id", null, ChatPresenter.chatObjects.size() + 1);
            presenter.botMessage("Can you provide your Voter's ID for verification?", selectOptiionForVoterId, ChatPresenter.chatObjects.size() + 1);

        });



        savePanDetail.setOnClickListener(v -> {
            if(isNullOrBlank(PanId.getText().toString()) || isNullOrBlank(PanName.getText().toString())){
                if(isNullOrBlank(PanId.getText().toString())){
                    Toast.makeText(VoucherChatActivity.this, "Please enter document id", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(VoucherChatActivity.this, "Please enter name", Toast.LENGTH_SHORT).show();
                }
            }else {
                if(!isValidPanId(PanId.getText().toString())){
                    Toast.makeText(VoucherChatActivity.this, "Invalid PAN Id", Toast.LENGTH_SHORT).show();
                }else if(!isAlpha(PanName.getText().toString())) {
                    Toast.makeText(VoucherChatActivity.this, "Please enter valid name", Toast.LENGTH_SHORT).show();
                }else {
                    hideKeyboard();
                    lLayoutEditPanInfo.setVisibility(View.GONE);
                    updatePanDetail.setVisibility(View.GONE);
                    presenter.onEditTextActionDone("PAN ID: " + PanId.getText().toString() + "\n" + "Name: " + PanName.getText().toString() + "\n" + "Date of birth:" + PanDob.getText().toString(), null, ChatPresenter.chatObjects.size() + 1);
                    presenter.botMessage("Awesome now we have your ID proof.", null, ChatPresenter.chatObjects.size() + 1);
                    presenter.botMessage("Can you provide your address proof? You can give us Your Aadhaar or Voter's ID card. Which one would you like to use?", voterOrAadharLayout, ChatPresenter.chatObjects.size() + 1);

                    DOC_TYPE="PAN";
                    panNumber="";
                    panDob="";
                    panName="";
                    InputMethodManager imm =(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(findViewById(android.R.id.content).getRootView()
                            .getWindowToken(), 0);

                    new SaveDocumentDetails().execute();

                }
            }
        });
        panConfirm.setOnClickListener(v -> {
            lLayoutEditPanInfo.setVisibility(View.GONE);
            presenter.botMessage("Awesome now we have your ID proof.", null, ChatPresenter.chatObjects.size() + 1);
//            new Handler().postDelayed(() -> presenter.botMessage("Can you provide your address proof? You can give us Your Aadhar or Voter's ID card. Which one would you like to use.", voterOrAadharLayout, ChatPresenter.chatObjects.size() + 1),
//                    1000);
            presenter.botMessage("Can you provide your address proof? You can give us Your Aadhar or Voter's ID card. Which one would you like to use.", voterOrAadharLayout, ChatPresenter.chatObjects.size() + 1);

        });


        panEditCross.setOnClickListener(v -> {
            updatePanDetail.setVisibility(View.GONE);
            ChatPresenter.chatObjects.subList(ChatPresenter.chatObjects.size() - 1, ChatPresenter.chatObjects.size()).clear();
            voucherAdapter.notifyDataSetChanged();

        });

        panEdit.setOnClickListener(v -> {
            lLayoutEditPanInfo.setVisibility(View.GONE);
            PanId.setText(panNumber);
            PanName.setText(panName);
            PanDob.setText(panDob);
            presenter.botMessage("Please edit PAN details in the following window", updatePanDetail, ChatPresenter.chatObjects.size() + 1);

        });



        btnPanContinue.setOnClickListener(v -> {

            btnPanContinue.setVisibility(View.GONE);
            lLayoutProcessingPan.setVisibility(View.VISIBLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            new GetDocumentData().execute();
            DOC_TYPE = "PAN";

        });



        clickPanPhoto.setOnClickListener(v -> {
            if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Intent takePictureIntent = new Intent(this, CameraActivity.class);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                        final File file=new File(storageDir, "pic.jpg");
                        photoFile=file;
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    if (photoFile != null) {
                        panURI = FileProvider.getUriForFile(this,
                                "com.findeed.fileprovider",
                                photoFile);
                        // takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, panURI);
                        // takePictureIntent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                        // startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO_PAN);
                        takePictureIntent.putExtra("image_side","FRONT SIDE");
                        takePictureIntent.putExtra("doc_type","PAN");
                        startActivityForResult(takePictureIntent,REQUEST_TAKE_PHOTO_PAN);
                    }
                }
            } else {
                requestPermissions(new String[]{
                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                }, 121);
            }


        });



        pickPanPhoto.setOnClickListener(v -> {
            panOption1.setVisibility(View.GONE);
            try {
                Intent galleryIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
                galleryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent, PAN_PICK_PHOTO);

            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        navicon.setOnClickListener(v -> {
            if (!dl.isDrawerOpen(GravityCompat.START)) {
                dl.openDrawer(GravityCompat.START);
            } else {
                dl.closeDrawer(GravityCompat.START);
            }
        });



        iDontHavePan.setOnClickListener(v -> {
            panCardLayout.setVisibility(View.GONE);
            presenter.onEditTextActionDone("I don't have pan card ",null,ChatPresenter.chatObjects.size()+1);
            presenter.botMessage("We are sorry, Pan Card is mandatory to proceed further. You can upload a pan card if you have or get back to us when you have one.",noPanCard,ChatPresenter.chatObjects.size()+1);
        });


        iHavePan.setOnClickListener(v -> {
            panCardLayout.setVisibility(View.GONE);
            presenter.onEditTextActionDone("I have pan card ",null,ChatPresenter.chatObjects.size()+1);
            presenter.botMessage("Please share your PAN card for verification",null,ChatPresenter.chatObjects.size()+1);
            presenter.botMessage("Please upload or click frontside image of the PAN card",panOption1,ChatPresenter.chatObjects.size()+1);

        });



        selectAndProceedChildren.setOnClickListener(v -> {
            numberOfChildren.setVisibility(View.GONE);
            selectedChild.getText().toString();
            if(selectedChild.getText().toString().equals("") || selectedChild.getText().toString().equals("Select the number of children")){
                voucherAdapter.notifyDataSetChanged();
                Toast.makeText(this, "Please select number of children", Toast.LENGTH_SHORT).show();
            }else {
                voucherResponse.put("numberOfChildren",selectedChild.getText().toString());
                presenter.onEditTextActionDone(selectedChild.getText().toString()+" Children",null,ChatPresenter.chatObjects.size()+1,VOUCHER_DATA.getData().get(0));
                presenter.botMessage("To proceed with the verification, can you please provide your PAN Card?",null,ChatPresenter.chatObjects.size()+1);
                presenter.botMessage("To assure you again, you will not have any financial obligation. However, your reference is very important for us. We may also reach out to in the furture to help reach "+VOUCHER_DATA.getData().get(0).getCustomerName(),panCardLayout,ChatPresenter.chatObjects.size()+1);
                new SaveVoucherDetails().execute();
            }

        });


        final String[] values={"<1 Year","1 Year - 3 Year","3 Year - 5 Year",">5 Year"};
        relationShipAgePicker.setMinValue(0);
        relationShipAgePicker.setMaxValue(values.length - 1);
        relationShipAgePicker.setDisplayedValues(values);
        relationShipAgePicker.setWrapSelectorWheel(false);

        relationShipAgePicker.setOnValueChangedListener((picker, oldVal, newVal) -> {
            selectedAge = values[newVal];
            selectedIndex=newVal;
            isNumberPickerValueChanged = true;
            numberPickerDownImagePink.setVisibility(View.VISIBLE);
            numberPickerDownImageGray.setVisibility(View.GONE);
            numberPickerUpimagePink.setVisibility(View.GONE);
            numberpickerUpimageGray.setVisibility(View.VISIBLE);

            if(selectedAge.equals(values[values.length - 1])) {
                numberPickerDownImageGray.setVisibility(View.VISIBLE);
                numberPickerUpimagePink.setVisibility(View.VISIBLE);
                numberPickerDownImagePink.setVisibility(View.GONE);
                numberpickerUpimageGray.setVisibility(View.GONE);

            } else if(selectedAge.equals(values[0])) {
                numberPickerDownImageGray.setVisibility(View.GONE);
                numberPickerUpimagePink.setVisibility(View.GONE);
                numberPickerDownImagePink.setVisibility(View.VISIBLE);
                numberpickerUpimageGray.setVisibility(View.VISIBLE);
            }
            else
            {
                numberPickerDownImageGray.setVisibility(View.INVISIBLE);
                numberPickerUpimagePink.setVisibility(View.VISIBLE);
                numberPickerDownImagePink.setVisibility(View.VISIBLE);
                numberpickerUpimageGray.setVisibility(View.INVISIBLE);
            }
        });

        numberPickerDownImagePink.setOnClickListener(v -> {
            relationShipAgePicker.setValue(selectedIndex+1);
            selectedIndex=selectedIndex+1;
            selectedAge=values[selectedIndex];

            isNumberPickerValueChanged = true;
            if(selectedAge.equals(values[values.length - 1])) {
                numberPickerDownImageGray.setVisibility(View.VISIBLE);
                numberPickerUpimagePink.setVisibility(View.VISIBLE);
                numberPickerDownImagePink.setVisibility(View.GONE);
                numberpickerUpimageGray.setVisibility(View.GONE);

            } else if(selectedAge.equals(values[0])) {
                numberPickerDownImageGray.setVisibility(View.GONE);
                numberPickerUpimagePink.setVisibility(View.GONE);
                numberPickerDownImagePink.setVisibility(View.VISIBLE);
                numberpickerUpimageGray.setVisibility(View.VISIBLE);
            }
            else
            {
                numberPickerDownImageGray.setVisibility(View.GONE);
                numberPickerUpimagePink.setVisibility(View.VISIBLE);
                numberPickerDownImagePink.setVisibility(View.VISIBLE);
                numberpickerUpimageGray.setVisibility(View.GONE);
            }




        });

        numberPickerUpimagePink.setOnClickListener(v -> {
            relationShipAgePicker.setValue(selectedIndex-1);
            selectedIndex=selectedIndex-1;
            selectedAge=values[selectedIndex];
            isNumberPickerValueChanged = true;
            if(selectedAge.equals(values[values.length - 1])) {
                numberPickerDownImageGray.setVisibility(View.VISIBLE);
                numberPickerUpimagePink.setVisibility(View.VISIBLE);
                numberPickerDownImagePink.setVisibility(View.GONE);
                numberpickerUpimageGray.setVisibility(View.GONE);

            } else if(selectedAge.equals(values[0])) {
                numberPickerDownImageGray.setVisibility(View.GONE);
                numberPickerUpimagePink.setVisibility(View.GONE);
                numberPickerDownImagePink.setVisibility(View.VISIBLE);
                numberpickerUpimageGray.setVisibility(View.VISIBLE);
            }
            else
            {
                numberPickerDownImageGray.setVisibility(View.GONE);
                numberPickerUpimagePink.setVisibility(View.VISIBLE);
                numberPickerDownImagePink.setVisibility(View.VISIBLE);
                numberpickerUpimageGray.setVisibility(View.GONE);
            }





        });


        selectChildTextView.setOnClickListener(v -> {



            View view = View.inflate(this, R.layout.custom_bottomsheet_voucherchildren, null);
            if (childList == null) {
                childList = new BottomSheetDialog(VoucherChatActivity.this);
            }
            childList.setContentView(view);

            BottomSheetBehavior bottomSheetBehaviorloan = BottomSheetBehavior.from(((View) view.getParent()));
            bottomSheetBehaviorloan.setState(BottomSheetBehavior.STATE_EXPANDED);
            childList=new BottomSheetDialog(VoucherChatActivity.this);
            childList.setContentView(R.layout.custom_bottomsheet_voucherchildren);
            RelativeLayout childOne=childList.findViewById(R.id.one_child);
            RelativeLayout childTwo=childList.findViewById(R.id.second_child);
            RelativeLayout childThree=childList.findViewById(R.id.three_child);
            RelativeLayout moreThanThree=childList.findViewById(R.id.more_than_three);
            RelativeLayout iDontKnow=childList.findViewById(R.id.i_do_not_know);
            TextView tv1Child = childList.findViewById(R.id.tv_1child);
            TextView tv2Child = childList.findViewById(R.id.tv_2child);
            TextView tv3Child = childList.findViewById(R.id.tv_3child);
            TextView tvMoreChild = childList.findViewById(R.id.tv_morechild);
            TextView tvNoChild =   childList.findViewById(R.id.tv_nochild);
            ImageView img1Child =  childList.findViewById(R.id.tick_onechild);
            ImageView img2Child = childList.findViewById(R.id.tick_twochild);
            ImageView img3Child = childList.findViewById(R.id.tick_threechild);
            ImageView imgMoreChild = childList.findViewById(R.id.tick_morechild);
            ImageView imgNoChild  = childList.findViewById(R.id.tick_dontno);
            ImageView  imgClearChild = childList.findViewById(R.id.img_voucher_child_clear);
            Button btnSelectChild = childList.findViewById(R.id.btn_voucher_child);


            assert imgClearChild != null;
            if (imgClearChild != null) {
                Objects.requireNonNull(imgClearChild).setOnClickListener(v12 -> childList.dismiss());
            }


            childList.show();
            /*childOne.setOnClickListener(v1 -> {
                childList.dismiss();
                selectAndProceedChildren.setBackgroundResource(R.drawable.shape_btn_addchilddark);
                selectedChild.setText("1 children");


            });

            childTwo.setOnClickListener(v1 -> {
                childList.dismiss();
                selectedChild.setText("2 children");
                selectAndProceedChildren.setBackgroundResource(R.drawable.shape_btn_addchilddark);


            });

            childThree.setOnClickListener(v1 -> {
                childList.dismiss();
                selectedChild.setText("3 children");
                selectAndProceedChildren.setBackgroundResource(R.drawable.shape_btn_addchilddark);

            });

            moreThanThree.setOnClickListener(v1 -> {
                childList.dismiss();
                selectedChild.setText("More than 3 children");
                selectAndProceedChildren.setBackgroundResource(R.drawable.shape_btn_addchilddark);

            });
            iDontKnow.setOnClickListener(v1 -> {
                childList.dismiss();
                selectedChild.setText("I don't know");
                selectAndProceedChildren.setBackgroundResource(R.drawable.shape_btn_addchilddark);

            });
*/

            assert tv1Child != null;
            if (tv1Child != null) {
                Objects.requireNonNull(tv1Child).setOnClickListener(v13 -> {
                    img1Child.setVisibility(View.VISIBLE);
                    img2Child.setVisibility(View.INVISIBLE);
                    img3Child.setVisibility(View.INVISIBLE);
                    imgMoreChild.setVisibility(View.INVISIBLE);
                    imgNoChild.setVisibility(View.INVISIBLE);
    
                    selectedChild.setText("1");
                    btnSelectChild.setBackgroundResource(R.drawable.shape_btn_addloandark);
                });
            }

            tv2Child.setOnClickListener(v14 -> {
                img1Child.setVisibility(View.INVISIBLE);
                img2Child.setVisibility(View.VISIBLE);
                img3Child.setVisibility(View.INVISIBLE);
                imgMoreChild.setVisibility(View.INVISIBLE);
                imgNoChild.setVisibility(View.INVISIBLE);

                selectedChild.setText("2");
                btnSelectChild.setBackgroundResource(R.drawable.shape_btn_addloandark);
            });

            assert tv3Child != null;
            if (tv3Child != null) {
                Objects.requireNonNull(tv3Child).setOnClickListener(v15 -> {
                    img1Child.setVisibility(View.INVISIBLE);
                    img2Child.setVisibility(View.INVISIBLE);
                    img3Child.setVisibility(View.VISIBLE);
                    imgMoreChild.setVisibility(View.INVISIBLE);
                    imgNoChild.setVisibility(View.INVISIBLE);
    
                    selectedChild.setText("3");
                    btnSelectChild.setBackgroundResource(R.drawable.shape_btn_addloandark);
                });
            }

            assert tvMoreChild != null;
            Objects.requireNonNull(tvMoreChild).setOnClickListener(v16 -> {
                img1Child.setVisibility(View.INVISIBLE);
                img2Child.setVisibility(View.INVISIBLE);
                img3Child.setVisibility(View.INVISIBLE);
                imgMoreChild.setVisibility(View.VISIBLE);
                imgNoChild.setVisibility(View.INVISIBLE);

                selectedChild.setText("More than 3");
                btnSelectChild.setBackgroundResource(R.drawable.shape_btn_addloandark);
            });

            assert tvNoChild != null;
            Objects.requireNonNull(tvNoChild).setOnClickListener(v18 -> {
                img1Child.setVisibility(View.INVISIBLE);
                img2Child.setVisibility(View.INVISIBLE);
                img3Child.setVisibility(View.INVISIBLE);
                imgMoreChild.setVisibility(View.INVISIBLE);
                imgNoChild.setVisibility(View.VISIBLE);
                selectedChild.setText("Zero or None");
                btnSelectChild.setBackgroundResource(R.drawable.shape_btn_addloandark);

            });

            assert btnSelectChild != null;
            Objects.requireNonNull(btnSelectChild).setOnClickListener(v19 -> {
                childList.dismiss();
                selectAndProceedChildren.setBackgroundResource(R.drawable.shape_btn_addchilddark);
            });








        });

        saveRelationShipAge.setOnClickListener(v -> {
            relationShipAge.setVisibility(View.GONE);
            if (!isNumberPickerValueChanged) {
                selectedAge = values[0];
            }
            voucherResponse.put("relationshipLength",selectedAge);
            presenter.onEditTextActionDone(selectedAge,null,ChatPresenter.chatObjects.size()+1,VOUCHER_DATA.getData().get(0));
            presenter.botMessage("How many children does "+voucherResponseData.getData().get(0).getCustomerName()+"have ?",numberOfChildren,ChatPresenter.chatObjects.size()+1);
            new SaveVoucherDetails().execute();
        });

        voucherIdontKnow.setOnClickListener(v -> {
            voucherRelation.setVisibility(View.GONE);
            voucherResponse.put("customerAcquantance","I don't know him/her");
            presenter.onEditTextActionDone("I don't know him/her",null,ChatPresenter.chatObjects.size()+1,VOUCHER_DATA.getData().get(0));
            presenter.botMessage("Sorry to hear this, we will contact "+VOUCHER_DATA.getData().get(0).getCustomerName()+"for further information",null,ChatPresenter.chatObjects.size()+1);
            new SaveVoucherDetails().execute();
        });


        voucherFriend.setOnClickListener(v -> {
            voucherRelation.setVisibility(View.GONE);
            voucherResponse.put("customerAcquantance","Friend");
            presenter.onEditTextActionDone("Friend",null,ChatPresenter.chatObjects.size()+1,VOUCHER_DATA.getData().get(0));
            presenter.botMessage("How long have you known "+VOUCHER_DATA.getData().get(0).getCustomerName()+"?",relationShipAge,ChatPresenter.chatObjects.size()+1);
            new SaveVoucherDetails().execute();
        });

        voucherColleague.setOnClickListener(v -> {
            voucherRelation.setVisibility(View.GONE);
            voucherResponse.put("customerAcquantance","Colleague");
            presenter.onEditTextActionDone("Colleague",null,ChatPresenter.chatObjects.size()+1,VOUCHER_DATA.getData().get(0));
            presenter.botMessage("How long have you known "+VOUCHER_DATA.getData().get(0).getCustomerName()+"?",relationShipAge,ChatPresenter.chatObjects.size()+1);
            new SaveVoucherDetails().execute();
        });

        voucherFamily.setOnClickListener(v -> {
            voucherRelation.setVisibility(View.GONE);
            voucherResponse.put("customerAcquantance","Family");
            presenter.onEditTextActionDone("Family",null,ChatPresenter.chatObjects.size()+1,VOUCHER_DATA.getData().get(0));
            presenter.botMessage("How long have you known "+VOUCHER_DATA.getData().get(0).getCustomerName()+"?",relationShipAge,ChatPresenter.chatObjects.size()+1);
            new SaveVoucherDetails().execute();
        });


        voucherProceed.setOnClickListener(v -> {
            voucherWelcome.setVisibility(View.GONE);
            presenter.botMessage("Thank you"+getResources().getString(R.string.smiley),null,ChatPresenter.chatObjects.size()+1);
            presenter.botMessage("We would like to ask you a few questions.",null,ChatPresenter.chatObjects.size()+1);

           // presenter.botMessage("How do you know "+data.getCustomerName()+"?",voucherRelation,ChatPresenter.chatObjects.size()+1);
            presenter.botMessage("How do you know ? "+VOUCHER_DATA.getData().get(0).getCustomerName(),voucherRelation,ChatPresenter.chatObjects.size()+1);
            voucherFlag.put("hasAcceptedInvitation","1");
            new SaveVoucherFlags().execute();
        });


        ArrayList<ChatObject> chatObjects = new ArrayList<>();
        this.presenter = new ChatPresenter();
        presenter.attachView(this);
        voucherAdapter= new VoucherAdapter(presenter.getChatObjects());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        linearLayoutManager.setItemPrefetchEnabled(true);
        voucherRecyclerView.setAdapter(voucherAdapter);
        voucherRecyclerView.setLayoutManager(linearLayoutManager);
        voucherRecyclerView.setItemAnimator(new DefaultItemAnimator());

        presenter.botMessage("Hello. Welcome to Findeed",null,ChatPresenter.chatObjects.size()+1);
        presenter.botMessage(VOUCHER_DATA.getData().get(0).getCustomerName()+" has requested you to be a reference for her. This will help her with her loan approval",voucherWelcome,ChatPresenter.chatObjects.size()+1);

        createChat(voucherResponseData.getData().get(0));

        voucherRecyclerView.addOnLayoutChangeListener((v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom) -> {
            scrollChatDown();


            if (bottom < oldBottom) {
//                voucherRecyclerView.postDelayed(() -> voucherRecyclerView.smoothScrollToPosition(
//                        voucherRecyclerView.getAdapter().getItemCount() - 1), 100);

                voucherRecyclerView.smoothScrollToPosition(Objects.requireNonNull(voucherRecyclerView.getAdapter()).getItemCount() - 1);
            }


        });


    }

    private void callForLocation() {
        pd = new ProgressDialog(VoucherChatActivity.this);
        pd.setMessage("Getting Your Location .....");
        pd.setCancelable(false);
        pd.show();
    }

    private GoogleApiClient googleApiClient;

    private void enableLoc() {
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(Bundle bundle) {
                            Log.d("Connected ","APi");

                        }

                        @Override
                        public void onConnectionSuspended(int i) {
                            googleApiClient.connect();
                        }







                    })
                    .addOnConnectionFailedListener(connectionResult -> Log.d("Location error","Location error " + connectionResult.getErrorCode())).build();
            googleApiClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(10);
            locationRequest.setFastestInterval(50);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            builder.setAlwaysShow(true);

            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and checkVideoEnd the result in onActivityResult().
                                status.startResolutionForResult(VoucherChatActivity.this, REQUEST_LOCATION);
//                                    finish();
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                    }
                }
            });
        }else {
            googleApiClient=null;
            enableLoc();
        }

    }

    private boolean hasGPSDevice(Context context) {
        final LocationManager mgr = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);
        if (mgr == null)
            return false;
        final List<String> providers = mgr.getAllProviders();
        if (providers == null)
            return false;
        return providers.contains(LocationManager.GPS_PROVIDER);

    }

    private void createChat(VData data) {
        try {
            VData_Persistance=new VData();
            VData_Temp.setCustomerId(data.getCustomerId());
            VData_Temp.setCustomerName(data.getCustomerName());
            VData_Temp.setCustomerVoucherId(data.getCustomerVoucherId());
            VData_Temp.setPhoneNumber(data.getPhoneNumber());
            VData_Temp.setVoucherName(data.getVoucherName());
            VData_Temp.setVoucherResponseId(data.getVoucherResponseId());
            VData_Temp.setVoucherFlags(data.getVoucherFlags());
            VData_Temp.setVoucherAddress("");

            VData_Persistance= (VData) VData_Temp.clone();

            if(data.getVoucherFlags()!=null){
                setFlags(data.getVoucherFlags());
                if(voucherFlag.get("hasAcceptedInvitation").equals("1")){
                    presenter.botMessage("Thank you"+getResources().getString(R.string.smiley),null,ChatPresenter.chatObjects.size()+1);
                    voucherWelcome.setVisibility(View.GONE);
                    isProceedClicked=true;
                }else {
                    next=false;
                }


            }

            if(data.getCustomerAcquantance()!=null && next){
                voucherWelcome.setVisibility(View.GONE);
                presenter.botMessage("We would like to ask you a few questions.",null,ChatPresenter.chatObjects.size()+1);
                 presenter.botMessage("How do you know "+data.getCustomerName()+"?",voucherRelation,ChatPresenter.chatObjects.size()+1);

                presenter.onEditTextActionDone(data.getCustomerAcquantance(),null,ChatPresenter.chatObjects.size()+1,VData_Persistance);
                VData_Persistance= (VData) getJson(data,"ask_questions").clone();
                Log.i("vchr",new Gson().toJson(VData_Persistance));

                voucherResponse.put("customerAcquantance",data.getCustomerAcquantance());
                isAcquantanceClicked=true;

            }else if(isProceedClicked){
                presenter.botMessage("We would like to ask you a few questions.",null,ChatPresenter.chatObjects.size()+1);
                // presenter.botMessage("How do you know "+voucherResponseData.getData().get(0).getCustomerName()+"?",voucherRelation,ChatPresenter.chatObjects.size()+1);
                presenter.botMessage("How do you know  ?"+VOUCHER_DATA.getData().get(0).getCustomerName(),voucherRelation,ChatPresenter.chatObjects.size()+1);

                next=false;

            }



            if(data.getRelationshipLength()!=null && next){
                presenter.botMessage("How long have you known "+data.getCustomerName()+"?",relationShipAge,ChatPresenter.chatObjects.size()+1);
                presenter.onEditTextActionDone(data.getRelationshipLength(),null,ChatPresenter.chatObjects.size()+1,VData_Persistance);
                VData_Persistance= (VData) getJson(data,"relation_duration").clone();


                isRelationShipLengthClicked=true;
                voucherResponse.put("relationshipLength",data.getRelationshipLength());
            }else if(isAcquantanceClicked){
                presenter.botMessage("How long have you known "+data.getCustomerName()+"?",relationShipAge,ChatPresenter.chatObjects.size()+1);
                next=false;
            }



            if(data.getNumberOfChildren()!=null && next){
                presenter.botMessage("How many children does "+data.getCustomerName()+" have ?",numberOfChildren,ChatPresenter.chatObjects.size()+1);
                presenter.onEditTextActionDone(data.getNumberOfChildren()+"Children",null,ChatPresenter.chatObjects.size()+1,VData_Persistance);
                VData_Persistance.setNumberOfChildren("0");
                VData_Persistance= (VData) getJson(data,"num_children").clone();


                isNumberOfChildrenClicked=true;
                voucherResponse.put("numberOfChildren",data.getNumberOfChildren());


            }else if(isRelationShipLengthClicked){
                presenter.botMessage("How many children does "+data.getCustomerName()+" have ?",numberOfChildren,ChatPresenter.chatObjects.size()+1);
                next=false;
            }


            if(data.getDocuments()!=null && next){
                if(data.getDocuments().size()!=0){
                    presenter.botMessage("To proceed with the verification, can you please provide your PAN Card?",null,ChatPresenter.chatObjects.size()+1);
                    presenter.botMessage("To assure you again, you will not have any financial obligation. However, your reference is very important for us. We may also reach out to in the furture to help reach"+VOUCHER_DATA.getData().get(0).getCustomerName(),panCardLayout,ChatPresenter.chatObjects.size()+1);

                    presenter.onEditTextActionDone("I have a PAN Card",null,ChatPresenter.chatObjects.size()+1,VData_Persistance);
                    VData_Persistance= (VData) getJson(data,"pan_data").clone();

                    for(int i=0;i<data.getDocuments().size();i++){
                        String docType=data.getDocuments().get(i).getDocumentType();
                        switch (docType){
                            case "PAN":
                                docStatus.put("PAN",i);
                                presenter.botMessage("Please share your PAN card for verification",null,ChatPresenter.chatObjects.size()+1);
                                presenter.botMessage("Please upload or click frontside image of the PAN card",panOption1,ChatPresenter.chatObjects.size()+1);
//                                Bitmap icon = BitmapFactory.decodeResource(getApplicationContext().getResources(),
//                                        R.drawable.dummy_pan);

                                break;

                        }
                    }

                    if(docStatus.get("PAN")!=null){
                        new GetImageFromUrl(data.getDocuments().get(docStatus.get("PAN")).getFrontImageURL(), "PAN", "FRONT").execute().get();

                        presenter.sendDocsImage(PanFrontBitmap,null,ChatPresenter.chatObjects.size()+1);


                        presenter.botMessage("Please continue for us to process your PAN",btnPanContinue,ChatPresenter.chatObjects.size()+1);
                        presenter.botMessage("Here are your details in the PAN card",null,ChatPresenter.chatObjects.size()+1);
                        presenter.botMessage("PAN ID: "+data.getDocuments().get(docStatus.get("PAN")).getDocumentId()+"\n"+"Name: "+data.getDocuments().get(docStatus.get("PAN")).getFirstName()+"\n"+"Date of Birth: "+data.getDocuments().get(docStatus.get("PAN")).getDateOfBirth(),lLayoutEditPanInfo,ChatPresenter.chatObjects.size()+1);
                        isPanCardClicked=true;
                    }
                    else{
                        presenter.botMessage("To proceed with the verification, can you please provide your PAN Card?",null,ChatPresenter.chatObjects.size()+1);
                        presenter.botMessage("Please be assured that vouching doesn't mean you'll be held as a guarantor or obligated to anything.",panCardLayout,ChatPresenter.chatObjects.size()+1);
                        next=false;
                    }
                }else if(isNumberOfChildrenClicked){
                    presenter.botMessage("To proceed with the verification, can you please provide your PAN Card?",null,ChatPresenter.chatObjects.size()+1);
                    presenter.botMessage("To assure you again, you will not have any financial obligation. However, your reference is very important for us. We may also reach out to in the furture to help reach "+VOUCHER_DATA.getData().get(0).getCustomerName(),panCardLayout,ChatPresenter.chatObjects.size()+1);
                    next=false;
                }

            }else if(isNumberOfChildrenClicked){
                presenter.botMessage("To proceed with the verification, can you please provide your PAN Card?",null,ChatPresenter.chatObjects.size()+1);
                presenter.botMessage("To assure you again, you will not have any financial obligation. However, your reference is very important for us. We may also reach out to in the furture to help reach "+VOUCHER_DATA.getData().get(0).getCustomerName(),panCardLayout,ChatPresenter.chatObjects.size()+1);
                next=false;
            }


            if(data.getDocuments()!=null && next){
                if(data.getDocuments().size()!=0){
                    presenter.botMessage("Awesome now we have your ID proof.", null, ChatPresenter.chatObjects.size() + 1);
                    presenter.botMessage("Can you provide your address proof? You can give us Your Aadhaar or Voter's ID card. Which one would you like to use?", voterOrAadharLayout, ChatPresenter.chatObjects.size() + 1);
                    for(int i=0;i<data.getDocuments().size();i++){
                        String docType=data.getDocuments().get(i).getDocumentType();
                        Log.i("DOC",docType);
                        if(docType.equals("VOTER_ID") || docType.equals("AADHAAR")){
                            if(docType.equals("VOTER_ID")){
                                docStatus.put("VOTER_ID",i);
                                isVoterIdClicked=true;
                                isAadhaarChoosed=false;
                                VOTER_ADDRESS= data.getDocuments().get(i).getAddress();
                                VOTER_DOCUMENT_ID=data.getDocuments().get(i).getDocumentId();
                                VOTER_FIRST_NAME=data.getDocuments().get(i).getFirstName();
                                VOTER_DOB=data.getDocuments().get(i).getDateOfBirth();
                                VOTER_GENDER=data.getDocuments().get(i).getGender();
                                presenter.onEditTextActionDone("Voter Id",null,ChatPresenter.chatObjects.size()+1,VData_Persistance);
                                VData_Persistance= (VData) getJson(data,"voter_data").clone();

                                presenter.botMessage("Can you provide your Voter's ID for verification?", selectOptiionForVoterId, ChatPresenter.chatObjects.size() + 1);
//                                Bitmap icon = BitmapFactory.decodeResource(getApplicationContext().getResources(),
//                                        R.drawable.dummy_voter_front);
                               break;

                            }else {
                                docStatus.put("AADHAAR",i);
                                isVoterIdClicked=true;
                                isAadhaarChoosed=true;
                                AADHAR_ADDRESS= data.getDocuments().get(i).getAddress();
                                AADHAR_DOC_ID=data.getDocuments().get(i).getDocumentId();
                                AADHAR_FIRST_NAME=data.getDocuments().get(i).getFirstName();
                                AADHAR_DOB=data.getDocuments().get(i).getDateOfBirth();
                                AADHAR_GENDER=data.getDocuments().get(i).getGender();
                                presenter.onEditTextActionDone("Aadhaar Card", null, ChatPresenter.chatObjects.size() + 1,VData_Persistance);
                                VData_Persistance= (VData) getJson(data,"aadhaar_data").clone();

                                presenter.botMessage("Steps to follow:" + getResources().getString(R.string.aadhaar_steps), proceedLayoutAadhaar, ChatPresenter.chatObjects.size() + 1);
//                                Bitmap icon2 = BitmapFactory.decodeResource(getApplicationContext().getResources(),
//                                        R.drawable.dummy_user);
                               break;

                            }
                        }else if(i==data.getDocuments().size() && isPanCardClicked){
                            presenter.botMessage("Awesome now we have your ID proof.", null, ChatPresenter.chatObjects.size() + 1);
                            presenter.botMessage("Can you provide your address proof? You can give us Your Aadhaar or Voter's ID card. Which one would you like to use?", voterOrAadharLayout, ChatPresenter.chatObjects.size() + 1);
                            next=false;
                        }

                    }
                    if(docStatus.get("VOTER_ID")!=null){
                        new GetImageFromUrl(data.getDocuments().get(docStatus.get("VOTER_ID")).getFrontImageURL(), "VOTER", "FRONT").execute().get();
                        VOTER_FRONT=VoterFrontBitmap;
                        presenter.sendVoterIdImage(VoterFrontBitmap,null,ChatPresenter.chatObjects.size()+1);
                        presenter.botMessage("Please upload backside image of the Voter's ID",clickPickForVoterBack,ChatPresenter.chatObjects.size()+1);
//                                Bitmap icon2 = BitmapFactory.decodeResource(getApplicationContext().getResources(),
//                                        R.drawable.voterid_back);
                        new GetImageFromUrl(data.getDocuments().get(docStatus.get("VOTER_ID")).getBackImageURL(), "VOTER", "BACK").execute().get();
                        VOTER_BACK=VoterBackBitmap;
                        presenter.sendVoterIdImage(VoterBackBitmap,null,ChatPresenter.chatObjects.size()+1);
                        presenter.botMessage("Please continue for us to process your Voter's ID",continueVoterId,ChatPresenter.chatObjects.size()+1);
                        presenter.botMessage("Here are your details in the Voter's ID ",null,ChatPresenter.chatObjects.size()+1);
                        presenter.botMessage("Full Name: "+data.getDocuments().get(docStatus.get("VOTER_ID")).getFirstName()+"\n"+"Gender-"+data.getDocuments().get(docStatus.get("VOTER_ID")).getGender()+"\n"+"Date of Birth - "+data.getDocuments().get(docStatus.get("VOTER_ID")).getDateOfBirth()+"\n"+"Address -"+data.getDocuments().get(docStatus.get("VOTER_ID")).getAddress(),editVoterIdLayout,ChatPresenter.chatObjects.size()+1);

                    }
                   else if(docStatus.get("AADHAAR")!=null){
                        new GetImageFromUrl(data.getDocuments().get(docStatus.get("AADHAAR")).getFrontImageURL(), "AADHAAR", "FRONT").execute().get();

                        presenter.setBotMessageImage(AadhaarFrontBitmap,null,ChatPresenter.chatObjects.size()+1);
                        presenter.botMessage("Aadhaar Number: "+data.getDocuments().get(docStatus.get("AADHAAR")).getDocumentId()+"\n" +
                                "Name: "+data.getDocuments().get(docStatus.get("AADHAAR")).getFirstName()+"\n" +
                                "Date of Birth: "+data.getDocuments().get(docStatus.get("AADHAAR")).getDateOfBirth()+"\n" +
                                "Address: "+data.getDocuments().get(docStatus.get("AADHAAR")).getAddress(),null,ChatPresenter.chatObjects.size()+1);
                        presenter.botMessage("Thank you for providing the ID and address proofs.",null,ChatPresenter.chatObjects.size()+1);

                    }
                    else{
                       next=false;
                    }

                }else if(isPanCardClicked){
                    presenter.botMessage("Awesome now we have your ID proof.", null, ChatPresenter.chatObjects.size() + 1);
                    presenter.botMessage("Can you provide your address proof? You can give us Your Aadhaar or Voter's ID card. Which one would you like to use?", voterOrAadharLayout, ChatPresenter.chatObjects.size() + 1);
                    next=false;
                }
            }else if(isPanCardClicked){
                presenter.botMessage("Awesome now we have your ID proof.", null, ChatPresenter.chatObjects.size() + 1);
                presenter.botMessage("Can you provide your address proof? You can give us Your Aadhaar or Voter's ID card. Which one would you like to use?", voterOrAadharLayout, ChatPresenter.chatObjects.size() + 1);
                next=false;
            }



            if(voucherFlag.get("bureauPermission").equals("1") && next){
                presenter.botMessage("Please click \"I Agree\" to allow Findeed to share the details with the lending partners and check your credit information with the credit bureau. Click here for the detailed Terms & Conditions.",agreeOrcancelLayout,ChatPresenter.chatObjects.size()+1);
                isBureauAgreeClicked=true;

            }else if(isVoterIdClicked){
                presenter.botMessage("Please click \"I Agree\" to allow Findeed to share the details with the lending partners and check your credit information with the credit bureau. Click here for the detailed Terms & Conditions.",agreeOrcancelLayout,ChatPresenter.chatObjects.size()+1);
                next=false;
            }


            if(voucherFlag.get("isLocationSame").equals("1") ||voucherFlag.get("isLocationSame").equals("2")&&next ){
                if(Objects.requireNonNull(voucherFlag.get("isLocationSame")).equals("1")){
                    if (isAadhaarChoosed) {
                        presenter.botMessage("Is the address in your Aadhar card your current address?", yesOrNo, ChatPresenter.chatObjects.size() + 1);
                    } else {
                        presenter.botMessage("Is the address in your VoterId card your current address?", yesOrNo, ChatPresenter.chatObjects.size() + 1);
                    }
                    presenter.onEditTextActionDone("Yes", null, ChatPresenter.chatObjects.size() + 1,VData_Persistance);
                    VData_Persistance= (VData) getJson(data,"same_address").clone();
                    presenter.botMessage("Thank you for completing the process. Your reference will help us and "+VOUCHER_DATA.getData().get(0).getCustomerName()+" with their loan application",null,ChatPresenter.chatObjects.size()+1);


                }else if(Objects.requireNonNull(voucherFlag.get("isLocationSame")).equals("2")) {
                    if (isAadhaarChoosed) {
                        presenter.botMessage("Is the address in your Aadhar card your current address?", yesOrNo, ChatPresenter.chatObjects.size() + 1);
                    } else {
                        presenter.botMessage("Is the address in your VoterId card your current address?", yesOrNo, ChatPresenter.chatObjects.size() + 1);
                    }

                    presenter.onEditTextActionDone("No", null, ChatPresenter.chatObjects.size() + 1,VData_Persistance);
                    VData_Persistance= (VData) getJson(data,"same_address").clone();

                        if(data.getVoucherAddress()!=null){
                            presenter.botMessage("Do you want to use your current location or fill it by yourself", llayoutAddressManually, ChatPresenter.chatObjects.size() + 1);

                            presenter.botMessage("Please confirm your address",useMyCurrentLocationLayout,ChatPresenter.chatObjects.size()+1);
                            userLocation = data.getVoucherAddress();
                            Bitmap staticmage = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                                    R.drawable.school_static_image);
                            presenter.sendUserLocation(staticmage,useMyCurrentLocationLayout,ChatPresenter.chatObjects.size()+1);
                            presenter.botMessage("Thank you for completing the process. Your reference will help us and "+VOUCHER_DATA.getData().get(0).getCustomerName()+" with their loan application",null,ChatPresenter.chatObjects.size()+1);

                        }
                        else{
                            presenter.botMessage("Do you want to use your current location or fill it by yourself", llayoutAddressManually, ChatPresenter.chatObjects.size() + 1);
                            next=false;
                        }
                }else {
                    if (isAadhaarChoosed) {
                        presenter.botMessage("Is the address in your Aadhar card your current address?", yesOrNo, ChatPresenter.chatObjects.size() + 1);
                    } else {
                        presenter.botMessage("Is the address in your VoterId card your current address?", yesOrNo, ChatPresenter.chatObjects.size() + 1);
                    }
                    presenter.onEditTextActionDone("No", null, ChatPresenter.chatObjects.size() + 1,VData_Persistance);
                    VData_Persistance= (VData) getJson(data,"same_address").clone();

                    presenter.botMessage("Do you want to use your current location or fill it by yourself", llayoutAddressManually, ChatPresenter.chatObjects.size() + 1);

                }


            }else if(isBureauAgreeClicked){
                if (isAadhaarChoosed) {
                    presenter.botMessage("Is the address in your Aadhar card your current address?", yesOrNo, ChatPresenter.chatObjects.size() + 1);
                } else {
                    presenter.botMessage("Is the address in your VoterId card your current address?", yesOrNo, ChatPresenter.chatObjects.size() + 1);
                }
                next=false;

            }


        }catch (Exception e){
            e.printStackTrace();
        }





    }

    private VData getJson(VData data, String cases) {
        switch(cases){
            case "ask_questions":
                VData_Temp.setCustomerAcquantance(data.getCustomerAcquantance());
                return VData_Temp;

            case "relation_duration":
                VData_Temp.setRelationshipLength(data.getRelationshipLength());
                return VData_Temp;

            case "num_children":
                VData_Temp.setNumberOfChildren(data.getNumberOfChildren());
                return VData_Temp;
            case "pan_data":
                DocumentData documentPan = new DocumentData();
                for(int i=0;i<data.getDocuments().size();i++){
                    if(data.getDocuments().get(i).getDocumentType().equals("PAN")){
                        documentPan.setDocumentType(data.getDocuments().get(i).getDocumentType());//.get(i).getClass().clone();
                        documentPan.setAddress(data.getDocuments().get(i).getAddress());
                        documentPan.setId(data.getDocuments().get(i).getId());
                        documentPan.setDateOfBirth(data.getDocuments().get(i).getDateOfBirth());
                        documentPan.setDocSide(data.getDocuments().get(i).getDocSide());
                        documentPan.setBackImage(data.getDocuments().get(i).getBackImage());
                        documentPan.setFrontImage(data.getDocuments().get(i).getFrontImage());
                        documentPan.setFrontImageURL(data.getDocuments().get(i).getFrontImageURL());
                        documentPan.setBackImageURL(data.getDocuments().get(i).getBackImageURL());

                        documentPan.setFirstName(data.getDocuments().get(i).getFirstName());
                        documentPan.setLastName(data.getDocuments().get(i).getLastName());
                        documentPan.setGender(data.getDocuments().get(i).getGender());
                        documentPan.setQuality(data.getDocuments().get(i).getQuality());
                        documentPan.setVerified(data.getDocuments().get(i).isVerified());

                        documents.add(documentPan);
                        ArrayList<DocumentData> docArrayP=new ArrayList<>();
                        docArrayP= (ArrayList<DocumentData>) documents.clone();
                        VData_Temp.setDocuments(docArrayP);
                        break;

                    }
                }
                return VData_Temp;
            case "voter_data":
                DocumentData documentVoter = new DocumentData();
                for(int i=0;i<data.getDocuments().size();i++){
                    if(data.getDocuments().get(i).getDocumentType().equals("VOTER_ID")){
                        documentVoter.setDocumentType(data.getDocuments().get(i).getDocumentType());//.get(i).getClass().clone();
                        documentVoter.setAddress(data.getDocuments().get(i).getAddress());
                        documentVoter.setId(data.getDocuments().get(i).getId());
                        documentVoter.setDateOfBirth(data.getDocuments().get(i).getDateOfBirth());
                        documentVoter.setDocSide(data.getDocuments().get(i).getDocSide());
                        documentVoter.setBackImage(data.getDocuments().get(i).getBackImage());
                        documentVoter.setFrontImage(data.getDocuments().get(i).getFrontImage());
                        documentVoter.setFrontImageURL(data.getDocuments().get(i).getFrontImageURL());
                        documentVoter.setBackImageURL(data.getDocuments().get(i).getBackImageURL());

                        documentVoter.setFirstName(data.getDocuments().get(i).getFirstName());
                        documentVoter.setLastName(data.getDocuments().get(i).getLastName());
                        documentVoter.setGender(data.getDocuments().get(i).getGender());
                        documentVoter.setQuality(data.getDocuments().get(i).getQuality());
                        documentVoter.setVerified(data.getDocuments().get(i).isVerified());

                        documents.add(documentVoter);
//                        ArrayList<DocumentData> docArrayV=new ArrayList<>();
//                        docArrayV= (ArrayList<DocumentData>) documents.clone();

                        VData_Temp.setDocuments(documents);
                        break;

                    }
                }

                return VData_Temp;
            case "aadhaar_data":
                DocumentData documentAdhaar = new DocumentData();
                for(int i=0;i<data.getDocuments().size();i++){
                    if(data.getDocuments().get(i).getDocumentType().equals("AADHAAR")){
                        documentAdhaar.setDocumentType(data.getDocuments().get(i).getDocumentType());//.get(i).getClass().clone();
                        documentAdhaar.setAddress(data.getDocuments().get(i).getAddress());
                        documentAdhaar.setId(data.getDocuments().get(i).getId());
                        documentAdhaar.setDateOfBirth(data.getDocuments().get(i).getDateOfBirth());
                        documentAdhaar.setDocSide(data.getDocuments().get(i).getDocSide());
                        documentAdhaar.setBackImage(data.getDocuments().get(i).getBackImage());
                        documentAdhaar.setFrontImage(data.getDocuments().get(i).getFrontImage()   );
                        documentAdhaar.setFrontImageURL(data.getDocuments().get(i).getFrontImageURL());
                        documentAdhaar.setBackImageURL(data.getDocuments().get(i).getBackImageURL());

                        documentAdhaar.setFirstName(data.getDocuments().get(i).getFirstName());
                        documentAdhaar.setLastName(data.getDocuments().get(i).getLastName());
                        documentAdhaar.setGender(data.getDocuments().get(i).getGender());
                        documentAdhaar.setQuality(data.getDocuments().get(i).getQuality());
                        documentAdhaar.setVerified(data.getDocuments().get(i).isVerified());

                        documents.add(documentAdhaar);
                        ArrayList<DocumentData> docArrayA=new ArrayList<>(documents);
                        VData_Temp.setDocuments(docArrayA);
                        break;

                    }
                }
                return VData_Temp;
            case "same_address":



                VData_Temp.setVoucherAddress(data.getVoucherAddress());
                return VData_Temp;

            default:
                return VData_Temp;


        }
    }


    private void setFlags(VoucherFlags voucherFlags) {
        Log.i("FLAGS",gson.toJson(voucherFlags));
        voucherFlag.put("ccsPermission", String.valueOf(voucherFlags.getCcsPermission()));
        voucherFlag.put("isLocationSame",String.valueOf(voucherFlags.getIsLocationSame()));
        voucherFlag.put("bureauPermission",String.valueOf(voucherFlags.getBureauPermission()));
        voucherFlag.put("hasAcceptedInvitation",String.valueOf(voucherFlags.getHasAcceptedInvitation()));
    }


    public boolean isValidPanId (String panId){
        return panId.matches("^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$");
    }

    public boolean isAlpha(String name) {
        return name.matches("^[ a-zA-Z]*$");
    }
    public boolean isValidPin (String pinCode){
        return pinCode.matches("^([0-9]){6}?$");
    }
    public boolean isValidId(String voterId){
        return voterId.matches("^([a-zA-Z]){3}([0-9]){7}?$");
    }




    @Override
    public void notifyAdapterObjectAdded(int position) {

        voucherAdapter.notifyItemInserted(position);
    }

    @Override
    public void scrollChatDown() {
        try{
            this.voucherRecyclerView.smoothScrollToPosition(presenter.getChatObjects().size() - 1);

        }catch(IllegalArgumentException e){
            e.printStackTrace();
        }

    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );

        currentPhotoPath = image.getAbsolutePath();
        return image;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){



            case  PAN_BACK_CLICK_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)
                {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        File photoFile = null;
                        try {
                            photoFile = createImageFile();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                        if (photoFile != null) {
                            panBackUri = FileProvider.getUriForFile(this,
                                    "com.findeed.fileprovider",
                                    photoFile);
                            takePictureIntent.putExtra("image_side","BACK_SIDE");
                            takePictureIntent.putExtra("doc_type","PAN");
                            startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO_PAN_BACK);
                        }
                    }
                }
                else
                {
                    voucherAdapter.notifyDataSetChanged();
                }
                break;

            case VOTER_BACK_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    Intent takePictureIntent = new Intent(this, CameraActivity.class);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        File photoFile = null;
                        try {
                            photoFile = createImageFile();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                        if (photoFile != null) {
                            voterBackURI = FileProvider.getUriForFile(getApplicationContext(),
                                    "com.findeed.fileprovider",
                                    photoFile);
                            takePictureIntent.putExtra("image_side", "BACK SIDE");
                            takePictureIntent.putExtra("doc_type", "VOTER");
                            startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO_VOTER_BACK);
                        }
                    }
                }else {
                    voucherAdapter.notifyDataSetChanged();

                }
                break;

            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED &&grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {


                    if(isCurrentAddressSame){
                        presenter.botMessage("Thank you for completing the process. Your reference will help us and "+VOUCHER_DATA.getData().get(0).getCustomerName()+" with their loan application",null,ChatPresenter.chatObjects.size()+1);
                        new SaveVoucherStatus(phone,"1").execute();

                    }else {
                        presenter.onEditTextActionDone("No", null, ChatPresenter.chatObjects.size() + 1);
                        presenter.botMessage("Do you want to use your current location or fill it by yourself", llayoutAddressManually, ChatPresenter.chatObjects.size() + 1);

                    }


                } else {
                    voucherAdapter.notifyDataSetChanged();
                    Toast.makeText(VoucherChatActivity.this, "We can not proceed further without your permission", Toast.LENGTH_SHORT)
                            .show();
                }
                break;

            case  4:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)
                {
                    Intent takePictureIntent = new Intent(this, CameraActivity.class);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        File photoFile = null;
                        try {
                            photoFile = createImageFile();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                        if (photoFile != null) {
                            voterFrontURI = FileProvider.getUriForFile(this,
                                    "com.findeed.fileprovider",
                                    photoFile);
                            Log.i("Voter_front",String.valueOf(voterFrontURI));
                            takePictureIntent.putExtra("image_side","FRONT SIDE");
                            takePictureIntent.putExtra("doc_type","VOTER");
                            startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO_VOTER_FRONT);
                        }
                    }
                }
                else
                {
                    voucherAdapter.notifyDataSetChanged();
                    Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show();
                }
                break;

            case 10:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED &&grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED && grantResults[3] == PackageManager.PERMISSION_GRANTED && grantResults[4] == PackageManager.PERMISSION_GRANTED) {


                    presenter.botMessage("Thank you for completing the process. Your reference will help us and "+VOUCHER_DATA.getData().get(0).getCustomerName()+" with their loan application",null,ChatPresenter.chatObjects.size()+1);
                    new SaveVoucherStatus(phone,"1").execute();


                } else {
                    voucherAdapter.notifyDataSetChanged();
                    Toast.makeText(VoucherChatActivity.this, "We can not proceed further without your permission", Toast.LENGTH_SHORT)
                            .show();
                }
                break;


            case  CLICK_PAN:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    try{
                        Intent takePictureIntent = new Intent(this, CameraActivity.class);
                        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                            takePictureIntent.setPackage("android.hardware.camera");
                            File photoFile = null;
                            try {
                                photoFile = createImageFile();
                            } catch (IOException ex) {
                                ex.printStackTrace();
                            }
                            if (photoFile != null) {
                                panURI = FileProvider.getUriForFile(this,
                                        "com.findeed.fileprovider",
                                        photoFile);
                                takePictureIntent.putExtra("image_side","FRONT SIDE");
                                takePictureIntent.putExtra("doc_type","PAN");
                                Log.i("panUriValue",String.valueOf(panURI));
                                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO_PAN);
                            }
                        }
                    }catch(Exception e){
                        Log.d("Pan permission block",e.toString());
                    }
                }
                else
                {
                    voucherAdapter.notifyDataSetChanged();
                    Toast.makeText(this, "We can not proceed without camera permission", Toast.LENGTH_LONG).show();
                }
                break;

            case(LOCATION_PERMISSION):


                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1]==PackageManager.PERMISSION_GRANTED) {
                    final Intent locationService = new Intent(VoucherChatActivity.this, LocationService.class);
                    VoucherChatActivity.this.startService(locationService);
                    VoucherChatActivity.this.bindService(locationService, serviceConnection, Context.BIND_AUTO_CREATE);
                    //new UseMyCurrentLocation().execute();
                    callForLocation();

                } else {
                    ChatActivity.chatAdapter.notifyDataSetChanged();
                    Toast.makeText(VoucherChatActivity.this, "We can not proceed without your permission", Toast.LENGTH_SHORT)
                            .show();
                }
                break;

//                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1]==PackageManager.PERMISSION_GRANTED) {
//
//                    new UseMyCurrentLocation().execute();
//
//                } else {
//                   voucherAdapter.notifyDataSetChanged();
//                    Toast.makeText(VoucherChatActivity.this, "We can not proceed without your permission", Toast.LENGTH_SHORT)
//                            .show();
//                }
//                break;


            default:
                break;
        }
        
        
        
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    if(requestCode==REQUEST_LOCATION){
        if(resultCode  == RESULT_OK){

            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                final Intent locationService = new Intent(VoucherChatActivity.this, LocationService.class);
                VoucherChatActivity.this.startService(locationService);
                VoucherChatActivity.this.bindService(locationService, serviceConnection, Context.BIND_AUTO_CREATE);
                //new UseMyCurrentLocation().execute();
                callForLocation();
            } else {
                requestPermissions(new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                }, LOCATION_PERMISSION);


            }

        }else {
            Toast.makeText(getApplicationContext(),"Location service is mandatory",Toast.LENGTH_SHORT).show();
            voucherAdapter.notifyDataSetChanged();
        }
    }


       else if(requestCode==REQUEST_PICK_PHOTO_PAN_BACK){
            if(resultCode==RESULT_OK){
                assert data != null;
                final String action = data != null ? data.getAction() : null;
                if (action == null) {

                    Uri imageUri = data.getData();

                    try {
                        bitmap= MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                        presenter.sendDocsImage(bitmap,null,ChatPresenter.chatObjects.size()+1);
//                        presenter.botMessage("Please continue for us to process your PAN",btnPanContinue,ChatPresenter.chatObjects.size()+1);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }else {
                voucherAdapter.notifyDataSetChanged();
            }
        }


        else if(requestCode==REQUEST_TAKE_PHOTO_PAN_BACK){

            if(resultCode==RESULT_OK){
                final Bitmap  bm;
                try {

                    bm = MediaStore.Images.Media.getBitmap(this.getContentResolver(), panBackUri);
                    presenter.sendDocsImage(bm,null,ChatPresenter.chatObjects.size()+1);
//                    presenter.botMessage("Please continue for us to process your PAN",btnPanContinue,ChatPresenter.chatObjects.size()+1);
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }else {
               voucherAdapter.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(),"Could not capture  image",Toast.LENGTH_SHORT).show();
            }


        }

        else if(requestCode==REQUEST_TAKE_PHOTO_PAN){


                if(resultCode==RESULT_OK){

                    try {
                        panOption1.setVisibility(View.GONE);
                        bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), panURI);
                        Log.i("panUriValue",String.valueOf(bitmap));
                        presenter.sendDocsImage(bitmap,null,ChatPresenter.chatObjects.size()+1);
                        presenter.botMessage("Please continue for us to process your PAN",btnPanContinue,ChatPresenter.chatObjects.size()+1);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }



                }else if(resultCode==RESULT_CANCELED){
                    voucherAdapter.notifyDataSetChanged();
                    //  Toast.makeText(getApplicationContext(),"Could not capture PAN image",Toast.LENGTH_SHORT).show();
                }

        }
          else if(requestCode==PAN_PICK_PHOTO){

            if(data!=null){
                final String action = data.getAction();
                if (action == null) {

                    Uri imageUri = data.getData();

                    try {
                        bitmap= MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                        Log.i("bitMapValue",String.valueOf(bitmap));
                        presenter.sendDocsImage(bitmap,null, ChatPresenter.chatObjects.size()+1);
//                        presenter.botMessage("Please upload backside image of the PAN card.",clickPickPanBack,ChatPresenter.chatObjects.size()+1);
                        presenter.botMessage("Please continue for us to process your PAN",btnPanContinue,ChatPresenter.chatObjects.size()+1);


                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }else {
               voucherAdapter.notifyDataSetChanged();
            }

        }else if(requestCode==REQUEST_TAKE_PHOTO_VOTER_FRONT){
                if(resultCode==RESULT_OK){

                    try {
                        VOTER_FRONT= MediaStore.Images.Media.getBitmap(this.getContentResolver(), voterFrontURI);
                        presenter.sendVoterIdImage(VOTER_FRONT,null,ChatPresenter.chatObjects.size()+1);
                        presenter.botMessage("Please upload backside image of the Voter's ID",clickPickForVoterBack,ChatPresenter.chatObjects.size()+1);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }else {
                    ChatActivity.chatAdapter.notifyDataSetChanged();
                    Toast.makeText(getApplicationContext(),"Could not capture  image",Toast.LENGTH_SHORT).show();
                }


        }

        else if(requestCode==REQUEST_PICK_PHOTO_VOTER_FRONT){
            if(resultCode==RESULT_OK){
                assert data != null;
                final String action = data.getAction();
                if (action == null) {

                    Uri imageUri = data.getData();

                    try {
                        VOTER_FRONT= MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                        presenter.sendVoterIdImage(VOTER_FRONT,null, ChatPresenter.chatObjects.size()+1);
                        voterOrAadharLayout.setVisibility(View.GONE);
                        clickVoterFront.setText("CLICK A PICTURE");
                        pickVoterFront.setText("PICK FROM PHOTOS");
                        presenter.botMessage("Please upload backside image of the Voter's ID",clickPickForVoterBack,ChatPresenter.chatObjects.size()+1);


                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }else {
                voucherAdapter.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(), "Did  not choose the image", Toast.LENGTH_SHORT).show();
            }
        }

        else if(requestCode==REQUEST_TAKE_PHOTO_VOTER_BACK){
            if(resultCode==RESULT_OK){
                try {

                    VOTER_BACK = MediaStore.Images.Media.getBitmap(this.getContentResolver(), voterBackURI);
                    presenter.sendVoterIdImage(VOTER_BACK,null, ChatPresenter.chatObjects.size()+1);
                    presenter.botMessage("Please continue for us to process your Voter's ID",continueVoterId,ChatPresenter.chatObjects.size()+1);
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }else {
                voucherAdapter.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(),"Could not capture  image",Toast.LENGTH_SHORT).show();
            }
        }

        else if(requestCode==REQUEST_PICK_PHOTO_VOTER_BACK){
            if(resultCode==RESULT_OK){
                final String action = Objects.requireNonNull(data).getAction();
                if (action == null) {

                    Uri imageUri = data.getData();

                    try {
                        VOTER_BACK = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                        presenter.sendVoterIdImage(VOTER_BACK,null, ChatPresenter.chatObjects.size()+1);
                        voterOrAadharLayout.setVisibility(View.GONE);
                        clickPickForVoterBack.setVisibility(View.GONE);
                        presenter.botMessage("Please continue for us to process your Voter's Id",continueVoterId,ChatPresenter.chatObjects.size()+1);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }else {
                    Toast.makeText(getApplicationContext(), "Did  not choose the image", Toast.LENGTH_SHORT).show();
                }
            }else {
                voucherAdapter.notifyDataSetChanged();
            }
        }
        else if(requestCode==PROCEED_TO_MSITE){

            new ExtractProcessedInformation().execute();



        }

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        int id = menuItem.getItemId();


        if (id == R.id.my_loan) {

            new GetMyLoan(validateOTPResponse.getData().getPhoneNumber()).execute();

        }




        if (id == R.id.loan_apply) {

            new GetMyLoan(validateOTPResponse.getData().getPhoneNumber()).execute();

        } else if (id == R.id.vouch_someone) {

            // Toast.makeText(chatActivity, "You Select Vouch for someone", Toast.LENGTH_SHORT).show();

            new ReturnAfterVouching(validateOTPResponse.getData().getPhoneNumber()).execute();

        } else if (id == R.id.notification) {
            Toast.makeText(VoucherChatActivity.this, "You Select Notification", Toast.LENGTH_SHORT).show();

        } else if (id == R.id.helpcontact) {
            Toast.makeText(VoucherChatActivity.this, "You Select Contact us", Toast.LENGTH_SHORT).show();

        } else if (id == R.id.invitefriend) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, "http://findeed.in/");
            sendIntent.setType("text/plain");

            Intent shareIntent = Intent.createChooser(sendIntent, null);
            startActivity(shareIntent);

        } else if (id == R.id.rate) {
            final Dialog dialog = new Dialog(VoucherChatActivity.this);
            dialog.setContentView(R.layout.dialog_rating);
            RatingBar ratingBar = dialog.findViewById(R.id.ratingbar);
            TextView tvRating = dialog.findViewById(R.id.tv_rating);
            dialog.show();

            dialog.getWindow().setBackgroundDrawable(new
                    ColorDrawable(Color.TRANSPARENT));


            ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                    Toast.makeText(VoucherChatActivity.this,
                            "Rating is :  "+ ratingBar.getRating(),
                            Toast.LENGTH_SHORT).show();
                    tvRating.setTextColor(getResources().getColor(R.color.dark_pink));

                    tvRating.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                }
            });



        } else if (id == R.id.terms) {
            Intent i = new Intent(this, TermsActivity.class);
            String voucherActivity = "Voucher Activity";
            i.putExtra("VOUCHER_ACTIVITY", voucherActivity);
            this.startActivity(i);

        } else if (id == R.id.privacy){
            Intent i = new Intent(this, PrivacyActivity.class);
            this.startActivity(i);

        }

        dl.closeDrawer(GravityCompat.START,true);
        return false;
    }

    public class GetImageFromUrl extends AsyncTask<String, Void, Bitmap>{
        ProgressDialog progressDialog = new ProgressDialog(VoucherChatActivity.this);

        ImageView imageView;
        String stringUrl;
        String DocType;
        String DocSide;
        public GetImageFromUrl(String URL, String DocType,String DocSide){
            this.stringUrl=URL;
            this.DocType=DocType;
            this.DocSide=DocSide;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


//            progressDialog.setCancelable(false);
//            progressDialog.show();
        }

        @Override
        protected Bitmap doInBackground(String... url) {

            bitmap = null;
            InputStream inputStream;
            try {
                inputStream = new java.net.URL(stringUrl).openStream();
                bitmap = BitmapFactory.decodeStream(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (DocType.equals("PAN") && DocSide.equals("FRONT")) {
                PanFrontBitmap=bitmap;
                return PanFrontBitmap;

            }
            else if (DocType.equals("PAN") && DocSide.equals("BACK")) {
                PanBackBitmap=bitmap;
                return PanBackBitmap;

            }
            else if (DocType.equals("VOTER") && DocSide.equals("FRONT")) {
                VoterFrontBitmap=bitmap;
                return VoterFrontBitmap;
            }
            else if (DocType.equals("VOTER") && DocSide.equals("BACK")) {
                VoterBackBitmap=bitmap;
                return VoterBackBitmap;

            }
            else if (DocType.equals("AADHAAR") && DocSide.equals("FRONT")) {

                AadhaarFrontBitmap=bitmap;
                return AadhaarFrontBitmap;
            }


            return bitmap;
        }
        @Override
        protected void onPostExecute(Bitmap bitmap){
            super.onPostExecute(bitmap);


        }
    }





    public class SendVoucherAddress extends AsyncTask<String, String ,String>{


       String  address;

        public SendVoucherAddress(String  address) {
            this.address = address;
        }


        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"voucher/saveVoucherAddress/"+customerVoucherId+"/"+address)
                    .build();
            try{
                Response response = client.newCall(request).execute();
                String  a= response.body().string();
                return a;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }

    public class StartOver extends AsyncTask<String, String ,String>{


        Intent intent;

        public StartOver(Intent intent) {
            this.intent = intent;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            finish();
            startActivity(intent);
        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"voucher/startOverVoucher/"+phone)
                    .build();
            try{
                Response response = client.newCall(request).execute();
                String  a= response.body().string();
                voucherResponseData=gson.fromJson(a, VoucherResponseData.class);
                Log.d("StratOver",a);
                CUSTOMER_ID=voucherResponseData.getData().get(0).getCustomerId();
                return a;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }


    public class SaveVoucherStatus extends AsyncTask<String, String ,String> {

        String  pNumber,value;

        public SaveVoucherStatus(String pNumber,String value) {
            this.pNumber = pNumber;
            this.value=value;

        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            Log.i("VOUCHER:",s);

        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"voucher/saveIsVoucherApproved/"+pNumber+"/"+value)
                    .build();
            try{
                Response response = client.newCall(request).execute();
                return response.body().string();
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }



    private class ExtractProcessedInformation extends AsyncTask<String,String ,String >{
        String userInfo;



        @Override
        protected void onPostExecute(String s) {


           try{
               if(s==null){
                   Toast.makeText(getApplicationContext(),"Could not finish the aadhaar verification, please try again",Toast.LENGTH_SHORT).show();
                   ChatPresenter.chatObjects.subList(ChatPresenter.chatObjects.size()-2,ChatPresenter.chatObjects.size()).clear();
                   voucherAdapter.notifyDataSetChanged();
               }else {
                   presenter.botMessage("Thank you!,",null,ChatPresenter.chatObjects.size()+1);
                   presenter.botMessage("Here are your details in aadhaar",null,ChatPresenter.chatObjects.size()+1);
                   byte[] decodedString = Base64.decode(USER_FACE, Base64.DEFAULT);
                   Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                   presenter.setBotMessageImage(decodedByte,null,ChatPresenter.chatObjects.size()+1);
                   presenter.botMessage(userInfo,null,ChatPresenter.chatObjects.size()+1);
                   presenter.botMessage(" Thank you for providing the ID and Address proofs.",null,ChatPresenter.chatObjects.size()+1);
                   presenter.botMessage("Please click \"I Agree\" to allow Findeed to share the details with the lending partners and check your credit information with the credit bureau. Click here for the detailed Terms & Conditions.",agreeOrcancelLayout,ChatPresenter.chatObjects.size()+1);
                   DOC_TYPE="AADHAAR";
                   new SaveDocumentDetails().execute();
               }

           }catch (Exception e){
               e.printStackTrace();


           }


        }

        @Override
        protected String doInBackground(String... strings) {
            if(WebViewActivity.USER_ID==null || WebViewActivity.USER_ID.equals("")){
                return null;

            }else {
                String seq= CLIENT_CODE+"|"+WebViewActivity.USER_ID+"|"+ApplicationData.API_KEY+"|"+ApplicationData.SALT;
                String h=sha256(seq);
                MsiteFetchDataRequest msiteFetchDataRequest= new MsiteFetchDataRequest();
                headers h1 = new headers();
                h1.setClient_code(ApplicationData.CLIENT_CODE);
                h1.setSub_client_code(ApplicationData.CLIENT_CODE);
                h1.setActor_type("NA");
                h1.setChannel_code("MSITE");
                h1.setStan(WebViewActivity.STAN);
                h1.setRun_mode("REAL");
                h1.setFunction_code("REVISED");
                h1.setFunction_sub_code("DEFAULT");
                MsiteFetchRequest msiteFetchRequest = new MsiteFetchRequest();
                msiteFetchRequest.setApi_key(ApplicationData.API_KEY);
                msiteFetchRequest.setUser_id(WebViewActivity.USER_ID);
                msiteFetchRequest.setHash(h);
                msiteFetchDataRequest.setHeaders(h1);
                msiteFetchDataRequest.setRequest(msiteFetchRequest);

                try {
                    String jsonData = gson.toJson(msiteFetchDataRequest);
                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                    RequestBody body = RequestBody.create(JSON, jsonData);
                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .build();

                    Request request = new Request.Builder()
                            .url(KHOSALA_URL+"video-id-kyc/api/1.0/fetchKYCInfo")
                            .post(body)
                            .build();


                    Response response = client.newCall(request).execute();
                    String resData= Objects.requireNonNull(response.body()).string();
                    ResponseData responseData=gson.fromJson(resData,ResponseData.class);
                    String decoded = new String(Base64.decode(responseData.getResponse_data().getKyc_info(),0));
                    KYCInfo kyc_info =gson.fromJson(decoded,KYCInfo.class);
                    UserAddressData = kyc_info.getOriginal_kyc_info();
                    userInfo= "Aadhar Number: "+kyc_info.getOriginal_kyc_info().getDocument_id()+"\n"+"Name: "+kyc_info.getOriginal_kyc_info().getName()+"\n"+"Date of Birth:"+kyc_info.getOriginal_kyc_info().getDob()+"\n"+"Address:"+kyc_info.getOriginal_kyc_info().getAddress();
                    AADHAR_ADDRESS=kyc_info.getOriginal_kyc_info().getAddress();
                    AADHAR_DOC_ID=kyc_info.getOriginal_kyc_info().getDocument_id();
                    AADHAR_FIRST_NAME=kyc_info.getOriginal_kyc_info().getName();
                    AADHAR_LAST_NAME=AADHAR_FIRST_NAME.split(" ")[1];
                    AADHAR_DOB=kyc_info.getOriginal_kyc_info().getDob();
                    AADHAR_GENDER=kyc_info.getOriginal_kyc_info().getGender();
                    USER_FACE=kyc_info.getPhoto().getDocument_image().replaceAll("\n","");
                    return  "Sucess";
                }catch (Exception e){
                    e.printStackTrace();
                    return null;

                }

            }

        }
    }


    public  class GetDocumentData extends AsyncTask<String,String ,String > {


        String voterFront,voterBack;
        SaveDocumentResponse sdr;


        @Override
        protected void onPostExecute(String s) {

            if(s.equals("Passed")){
                if(DOC_TYPE.equals("PAN")){

                    panNumber=sdr.getData().getDocumentId();
                    panName=sdr.getData().getFirstName();
                    panDob=sdr.getData().getDateOfBirth();
                    lLayoutProcessingPan.setVisibility(View.GONE);
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    presenter.botMessage("Here are your details in the PAN card",null,ChatPresenter.chatObjects.size()+1);
                    presenter.botMessage("PAN ID: "+panNumber+"\n"+"Name: "+panName+"\n"+"Date of Birth: "+panDob,lLayoutEditPanInfo,ChatPresenter.chatObjects.size()+1);

                }
                else {
                    processingVoterId.setVisibility(View.GONE);
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    presenter.botMessage("Here are your details in the Voter's ID ",null,ChatPresenter.chatObjects.size()+1);
                    VOTER_ADDRESS=sdr.getData().getAddress();
                    VOTER_DOCUMENT_ID=sdr.getData().getDocumentId();
                    VOTER_FIRST_NAME=sdr.getData().getFirstName();
                    VOTER_LAST_NAME=sdr.getData().getLastName();
                    VOTER_DOB=sdr.getData().getDateOfBirth();
                    VOTER_GENDER=sdr.getData().getGender();
                    presenter.botMessage("Full Name: "+VOTER_FIRST_NAME+"\n"+"Gender-"+VOTER_GENDER+"\n"+"Date of Birth - "+VOTER_DOB+"\n"+"Address -"+VOTER_ADDRESS,editVoterIdLayout,ChatPresenter.chatObjects.size()+1);

                }



            }else {
                if(DOC_TYPE.equals("PAN")){
                    lLayoutProcessingPan.setVisibility(View.GONE);
                    panEditCross.setVisibility(View.GONE);
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    Toast.makeText(VoucherChatActivity.this,"Document OCR failed!",Toast.LENGTH_LONG).show();
                    // presenter.botMessage("Here are your details in the PAN card",null,ChatPresenter.chatObjects.size()+1);
                    presenter.botMessage("We were not able to read data from the image you provided, Please fill your PAN details manually",updatePanDetail,ChatPresenter.chatObjects.size()+1);


                }else {

                    clearVoterEdit.setVisibility(View.GONE);
                    processingVoterId.setVisibility(View.GONE);
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    Toast.makeText(VoucherChatActivity.this,"Document OCR failed!",Toast.LENGTH_LONG).show();
                    // presenter.botMessage("Here are your details in the Voter's card",null,ChatPresenter.chatObjects.size()+1);
                    presenter.botMessage("We were not able to read data from the image you provided, Please fill your Voter's Id details manually",llUpdateVoterInfo,ChatPresenter.chatObjects.size()+1);
                }
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try{
            if(DOC_TYPE.equals("PAN")){
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream .toByteArray();
                base64String = Base64.encodeToString(byteArray,Base64.NO_WRAP);
            }
            else{
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                VOTER_FRONT.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream .toByteArray();
                voterFront = Base64.encodeToString(byteArray,Base64.NO_WRAP);

                ByteArrayOutputStream byteArrayOutputStream1 = new ByteArrayOutputStream();
                VOTER_BACK.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream1);
                byte[] byteArray1 = byteArrayOutputStream1 .toByteArray();
                voterBack = Base64.encodeToString(byteArray1,Base64.NO_WRAP);

            }

            SaveDocument sd =new SaveDocument();
            if(DOC_TYPE.equals("PAN")){
                sd.setId(customerVoucherId);
                Log.i("CID",String.valueOf(customerVoucherId));
                sd.setDocumentType("PAN");
                sd.setDocSide("FRONT");
                sd.setFrontImage(base64String);
                sd.setBackImage("");
            }else {
                sd.setId(customerVoucherId);
                sd.setDocumentType("VOTER_ID");
                sd.setDocSide("BOTH");
                sd.setFrontImage(voterFront);
                sd.setBackImage(voterBack);

            }
            String jsonData= gson.toJson(sd);
            Log.i("VDATA",jsonData);
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            RequestBody body = RequestBody.create(JSON,jsonData);
            OkHttpClient client =new OkHttpClient.Builder()
                    .connectTimeout(10, TimeUnit.SECONDS)
                    .writeTimeout(10, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .build();

            Request request = new Request.Builder()
                    .url(END_POINT+"voucher/saveDocument")
                    .post(body)
                    .build();


                Response response = client.newCall(request).execute();
                String resData= Objects.requireNonNull(response.body()).string();
                Log.i("RESPONSE",resData);
                sdr =gson.fromJson(resData,SaveDocumentResponse.class);
                if(sdr.getStatusCode().equals("500")){
                    return "Fail";
                }else{
                    return "Passed";
                }

            }catch (Exception e){
                e.printStackTrace();
                return "Fail";
            }

        }
    }


    private class SendCurrentLocation extends AsyncTask<String,String ,String >{



        ProgressDialog pd = new ProgressDialog(VoucherChatActivity.this);
        Bitmap staticMapImage;
        final String url_map = "https://maps.google.com/maps/api/staticmap?center=" + latitude + "," + longitude + "&zoom=17&markers=" + latitude + "," + longitude + "&size=250x250&sensor=false&key=AIzaSyD0dxGm6-bsvSvZv0Slk5pMKk7jFJq77Ms";


        @Override
        protected void onPreExecute() {
            pd.setMessage("Please wait...");
            pd.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(s!=null){
                if(pd.isShowing()){
                    pd.dismiss();
                    userLocation=cityName+","+pin;
                    presenter.sendUserLocation(staticMapImage,useMyCurrentLocationLayout,ChatPresenter.chatObjects.size()+1);
                    useMyCurrentLocationLayout.setVisibility(View.GONE);
                    presenter.botMessage("Thank you for completing the process. Your reference will help us and "+VOUCHER_DATA.getData().get(0).getCustomerName()+" with their loan application",null,ChatPresenter.chatObjects.size()+1);
                    new SaveVoucherStatus(phone,"1").execute();

                }
            }else {
                pd.dismiss();
                Bitmap icon2 = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.location_placeholder);
                presenter.sendUserLocation(icon2,useMyCurrentLocationLayout,ChatPresenter.chatObjects.size()+1);
                userLocation=cityName+","+pin;
                useMyCurrentLocationLayout.setVisibility(View.GONE);
                presenter.botMessage("Thank you for completing the process. Your reference will help us and "+VOUCHER_DATA.getData().get(0).getCustomerName()+" with their loan application",null,ChatPresenter.chatObjects.size()+1);
                new SaveVoucherStatus(phone,"1").execute();
            }


        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                URL url = new URL(url_map);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                staticMapImage = BitmapFactory.decodeStream(input);
                return "Sucess";

            } catch (IOException e) {


                e.printStackTrace();
                return null;
            }
        }
    }

    private class SaveVoucherFlags extends AsyncTask<String ,String ,String >{


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            new GetVoucherDetails().execute();
        }

        @Override
        protected String doInBackground(String... strings) {

           VoucherFlags voucherFlags = new VoucherFlags();

            voucherFlags.setCustomerVoucherId(VOUCHER_DATA.getData().get(0).getCustomerVoucherId());
            for (Map.Entry<String, String > entry : voucherFlag.entrySet()) {
                String  key = entry.getKey();
                String  value = entry.getValue();
                switch (key){
                    case "hasAcceptedInvitation":
                        voucherFlags.setHasAcceptedInvitation(Integer.parseInt(value));
                        break;
                    case "bureauPermission":
                        voucherFlags.setBureauPermission(Integer.parseInt(value));
                        break;
                    case "isLocationSame":
                        voucherFlags.setIsLocationSame(Integer.parseInt(value));
                        break;
                    case "ccsPermission":
                        voucherFlags.setCcsPermission(Integer.parseInt(value));
                        break;
                }
            }


            String  jsonData= gson.toJson(voucherFlags);
            Log.i("payLoad",jsonData);
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            OkHttpClient client = new OkHttpClient();
            RequestBody body = RequestBody.create(JSON, jsonData);
            Request request = new Request.Builder()
                    .url(END_POINT+"voucher/saveVoucherFlags")
                    .post(body)
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String  aa=response.body().string();
                Log.i("VoucherData",aa);
                return aa;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }

        }
    }


    private class SaveVoucherDetails extends AsyncTask<String ,String ,String >{


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            new GetVoucherDetails().execute();
        }

        @Override
        protected String doInBackground(String... strings) {

            SaveVoucherDetailsRequest  svdr = new SaveVoucherDetailsRequest();


            ArrayList<Customer> customer = new ArrayList<>();
            customer.add(c1);
            svdr.setCustomers(customer);
            svdr.setPhoneNumber(voucherResponseData.getData().get(0).getPhoneNumber());
            svdr.setCustomerVoucherId(voucherResponseData.getData().get(0).getCustomerVoucherId());



            for (Map.Entry<String, String > entry : voucherResponse.entrySet()) {
                String key = entry.getKey();
                String  value = entry.getValue();
                switch (key){
                    case "customerAcquantance":
                        svdr.setCustomerAcquantance(value);
                        break;
                    case "customerRelationship":
                        svdr.setCustomerRelationship(value);
                        break;
                    case "relationshipLength":
                        svdr.setRelationshipLength(value);
                        break;
                    case "maritalStatus":
                        svdr.setMaritalStatus(value);
                        break;
                    case "numberOfChildren":
                        svdr.setNumberOfChildren(value);
                        break;
                    case "customerEmployment":
                       svdr.setCustomerEmployment(value);
                        break;
                    case "customerBusiness":
                       svdr.setCustomerBusiness(value);
                        break;
                    case "customerVoucherArea":
                       svdr.setCustomerVoucherArea(value);
                        break;
                    case "customerStayArea":
                       svdr.setCustomerStayArea(value);
                        break;
                    case "customerVoucherWork":
                        svdr.setCustomerVoucherWork(value);
                        break;
                    default:
                        break;
                }
            }


            String  jsonData= gson.toJson(svdr);
            Log.i("payLoad",jsonData);
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            OkHttpClient client = new OkHttpClient();
            RequestBody body = RequestBody.create(JSON, jsonData);
            Request request = new Request.Builder()
                    .url(END_POINT+"voucher/saveVoucherDetails")
                    .post(body)
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String  aa=response.body().string();
                Log.i("VoucherData",aa);
                return aa;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }

        }
    }

    private class SaveDocumentDetails extends AsyncTask<String ,String ,String>{
        String jsonData;



        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);



            if(DOC_TYPE.equals("PAN")){
                new VerifyUserDocumentPAN(PanId.getText().toString(),PanName.getText().toString(),PanDob.getText().toString()).execute();
            }
//            VOTER_ADDRESS="";
//            VOTER_DOB="";
//            VOTER_FIRST_NAME="";
//            VOTER_LAST_NAME="";
//            VOTER_GENDER="";
//            VOTER_DOCUMENT_ID="";

            panEditCross.setVisibility(View.VISIBLE);
            clearVoterEdit.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... strings) {
            try {


                switch (DOC_TYPE) {
                    case "EDITED_VOTER_ID": {

                        SaveDocumentDetailsRequest documentDetailsRequest = new SaveDocumentDetailsRequest();
                        documentDetailsRequest.setAddress(edtVoterAddress.getText().toString());
                        documentDetailsRequest.setId(customerVoucherId);
                        documentDetailsRequest.setDateOfBirth(edtVoterDOB.getText().toString());
                        documentDetailsRequest.setDocumentType("VOTER_ID");
                        documentDetailsRequest.setFirstName(edtVoterName.getText().toString());
                        documentDetailsRequest.setLastName("");
                        documentDetailsRequest.setGender(sex);
                        documentDetailsRequest.setDocumentId(edtVoterId.getText().toString());
                        jsonData = gson.toJson(documentDetailsRequest);
                        break;
                    }
                    case "VOTER_ID": {
                        SaveDocumentDetailsRequest documentDetailsRequest = new SaveDocumentDetailsRequest();
                        documentDetailsRequest.setAddress(VOTER_ADDRESS);
                        documentDetailsRequest.setId(customerVoucherId);
                        documentDetailsRequest.setDateOfBirth(VOTER_DOB);
                        documentDetailsRequest.setDocumentType("VOTER_ID");
                        documentDetailsRequest.setFirstName(VOTER_FIRST_NAME);
                        documentDetailsRequest.setLastName(VOTER_LAST_NAME);
                        documentDetailsRequest.setGender(VOTER_GENDER);
                        documentDetailsRequest.setDocumentId(VOTER_DOCUMENT_ID);
                        jsonData = gson.toJson(documentDetailsRequest);
                        break;
                    }
                    case "AADHAAR": {
                        SaveDocumentDetailsRequest documentDetailsRequest = new SaveDocumentDetailsRequest();
                        documentDetailsRequest.setAddress(AADHAR_ADDRESS);
                        documentDetailsRequest.setId(customerVoucherId);
                        documentDetailsRequest.setDateOfBirth(AADHAR_DOB);
                        documentDetailsRequest.setFrontImage(USER_FACE);
                        documentDetailsRequest.setDocumentType("AADHAAR");
                        documentDetailsRequest.setFirstName(AADHAR_FIRST_NAME);
                        documentDetailsRequest.setLastName(AADHAR_LAST_NAME);
                        documentDetailsRequest.setGender(AADHAR_GENDER);
                        documentDetailsRequest.setDocumentId(AADHAR_DOC_ID);
                        jsonData = gson.toJson(documentDetailsRequest);

                        break;
                    }
                    case "PAN": {

                        SaveDocumentDetailsRequest documentDetailsRequest = new SaveDocumentDetailsRequest();
                        documentDetailsRequest.setAddress("");
                        documentDetailsRequest.setId(customerVoucherId);
                        documentDetailsRequest.setDateOfBirth(PanDob.getText().toString());
                        documentDetailsRequest.setDocumentType("PAN");
                        documentDetailsRequest.setFirstName(PanName.getText().toString());
                        documentDetailsRequest.setLastName("");
                        documentDetailsRequest.setGender("");
                        documentDetailsRequest.setDocumentId(PanId.getText().toString());
                        jsonData = gson.toJson(documentDetailsRequest);

                        break;
                    }
                }

                Log.i("SAVEDOCUMENTDETILS",jsonData);
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                OkHttpClient client = new OkHttpClient();
                RequestBody body = RequestBody.create(JSON, jsonData);
                Request request = new Request.Builder()
                        .url(END_POINT+"/voucher/saveDocumentDetails")
                        .post(body)
                        .build();

                Response response = client.newCall(request).execute();
                Log.i("RES",response.body().string());
                return "Sucess";


            } catch (IOException | RuntimeException e) {
                e.printStackTrace();
                return null;

            }
        }
    }


    private  class VerifyUserDocumentPAN extends  AsyncTask<String,String,String >{
        String pId,pname,pDob;

        public VerifyUserDocumentPAN(String pId, String pname, String pDob) {
            this.pId = pId;
            this.pname = pname;
            this.pDob = pDob;
        }

        @Override
        protected String doInBackground(String... strings) {

            headers h = new headers();
            h.setClient_code("FIND9043");
            h.setSub_client_code("dummy");
            h.setChannel_code("ANDROID_SDK");
            h.setChannel_version("1.0.0");
            h.setTransmission_datetime("1428393481435");
            h.setOperation_mode("SELF");
            h.setRun_mode("TRIAL");
            h.setActor_type("OTHER");
            h.setUser_handle_type("EMAIL");
            h.setUser_handle_value("a@b.com");
            h.setFunction_code("VERIFY_PAN");
            h.setFunction_sub_code("DOCUMENT");
            h.setStan(getRequestId());

            PanDetails pd = new PanDetails();
            pd.setDob(pDob);
            pd.setName(pname);
            pd.setPan_number(pId);
            pd.setDocument(base64String);
            pd.setPan_type("IND");

            PanRequestPayload prp= new PanRequestPayload();
            prp.setPan_details(pd);

            VerifyUserIdDocRequestPayload payload = new VerifyUserIdDocRequestPayload();
            payload.setHeaders(h);
            payload.setRequest(prp);

            try{

                String jsonData=gson.toJson(payload);
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                OkHttpClient client = new OkHttpClient();
                RequestBody body = RequestBody.create(JSON, jsonData);
                Request request = new Request.Builder()
                        .url(KHOSALA_URL+"service/api/2.0/verifyUserIdDoc")
                        .post(body)
                        .build();

                Response response = client.newCall(request).execute();
                return  response.body().string();



            }catch (Exception e){
                e.printStackTrace();
                return null;
            }

        }
    }

    private String getRequestId() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 10) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }

        return salt.toString();
    }



    private class GetVoucherDetails extends AsyncTask<String,String ,String >{
        Gson gson=new Gson();
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (s != null) {

            }
        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"voucher/getVoucherDetails/"+voucherResponseData.getData().get(0).getPhoneNumber())
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String a=response.body().string();
                VOUCHER_DATA=gson.fromJson(a,VoucherResponseData.class);
                return a;

            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }


    private ServiceConnection serviceConnection = new ServiceConnection() {
        LocationService locationService;
        public void onServiceConnected(ComponentName className, IBinder service) {
            // This is called when the connection with the service has been
            // established, giving us the service object we can use to
            // interact with the service.  Because we have bound to a explicit
            // service that we know is running in our own process, we can
            // cast its IBinder to a concrete class and directly access it.
            String name = className.getClassName();

            if (name.endsWith("LocationService")) {
                locationService = ((LocationService.LocationServiceBinder) service).getService();

                locationService.startUpdatingLocation();
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            // Because it is running in our same process, we should never
            // see this happen.
            if (className.getClassName().equals("LocationService")) {
                locationService.stopUpdatingLocation();
                locationService = null;
            }
        }
    };


    class ReturnAfterVouching extends AsyncTask<String,String ,String > {

        String phone;
        ProgressDialog vouch;

        public ReturnAfterVouching(String phone) {
            this.phone = phone;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            vouch = new ProgressDialog(VoucherChatActivity.this);
            vouch.setTitle("Please Wait...");
            vouch.setCancelable(false);
            vouch.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (s != null) {
                vouch.dismiss();
                if (ApplicationData.voucherResponseData.getData().size() == 0) {
                    Toast.makeText(VoucherChatActivity.this, "You are not a voucher", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(VoucherChatActivity.this, VoucherChatActivity.class);
                    finish();
                    startActivity(intent);
                }

            }

        }


        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"voucher/getVoucherDetails/"+phone)
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String a=response.body().string();
                ApplicationData.voucherResponseData=gson.fromJson(a, VoucherResponseData.class);
                return ApplicationData.voucherResponseData.getMessage() ;

            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }




         class GetMyLoan extends AsyncTask<String, String, String> {

            String phone;
            ProgressDialog loanUI;



            public GetMyLoan(String phoneNumber) {
                this.phone = phoneNumber;
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                loanUI = new ProgressDialog(VoucherChatActivity.this);
                loanUI.setTitle("Please Wait...");
                loanUI.setCancelable(false);
                loanUI.show();
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                if (s != null) {
                    loanUI.dismiss();
                    if (ApplicationData.validateOTPResponse.getData().getCustomerId() == 0) {
                        Toast.makeText(VoucherChatActivity.this, "No Active Loan Found", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent intent = new Intent(VoucherChatActivity.this, ChatActivity.class);
                        finish();
                        startActivity(intent);
                    }
                }
            }

            @Override
            protected String doInBackground(String... strings) {
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url(END_POINT + "customer/getCustomerDetails/" + phone)
                        .build();

                try {
                    Response response = client.newCall(request).execute();
                    String a = response.body().string();
                    validateOTPResponse = gson.fromJson(a, ValidateOTPResponse.class);
                    Log.i("RETUREDdATAfROMvOUCHING",gson.toJson(validateOTPResponse));
                    return ApplicationData.validateOTPResponse.getMessage();

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }





    @Override
    public void onBackPressed() {
        if(doubleBackToExitPressedOnce){
            // super.onBackPressed();

            Intent a = new Intent(Intent.ACTION_MAIN);
            a.addCategory(Intent.CATEGORY_HOME);
            finish();
            //.setFlags(Inten
            // t.FLAG_ACTIVITY_NEW_TASK);
            // a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(a);
            ApplicationData.isItVoucher=false;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please press BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 500);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ApplicationData.isItVoucher=false;
    }
}
