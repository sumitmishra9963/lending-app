package com.findeed;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.findeed.Bot.ChatActivity;
import com.findeed.Voucher.VoucherChatActivity;

public class PhoneRegisterActivity2 extends AppCompatActivity {


    Button voucher;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.voucher_or_user);

        voucher=findViewById(R.id.button);
        Button us = findViewById(R.id.button2);
        voucher.setOnClickListener(v -> {

        Intent vo= new Intent(PhoneRegisterActivity2.this, VoucherChatActivity.class);
        startActivity(vo);

        });


        us.setOnClickListener(v -> {
            Intent vo= new Intent(PhoneRegisterActivity2.this, ChatActivity.class);
            startActivity(vo);
        });



    }
}
