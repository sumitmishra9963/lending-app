package com.findeed;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;
import com.synnapps.carouselview.ViewListener;

public class OnboardingOneActivity extends AppCompatActivity {

    CarouselView carouselView;
    int NUMBER_OF_PAGES = 5;
    TextView textnext, textskip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarding_one);

        carouselView = findViewById(R.id.carouselView);
        textskip = findViewById(R.id.text_skip);

        carouselView.setPageCount(NUMBER_OF_PAGES);
        // set ViewListener for custom view
        carouselView.setViewListener(viewListener);
        textnext = findViewById(R.id.text_next);

        textnext.setOnClickListener(v -> {
            Intent intent = new Intent(OnboardingOneActivity.this, SelectLanguage.class);
            startActivity(intent);

        });

        textskip.setOnClickListener(v -> {
            Intent intent = new Intent(OnboardingOneActivity.this, SelectLanguage.class);
            startActivity(intent);

        });

    }
    ViewListener viewListener = position -> getLayoutInflater().inflate(R.layout.custome_view, null);

}
