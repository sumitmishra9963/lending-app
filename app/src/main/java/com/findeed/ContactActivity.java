package com.findeed;



import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class ContactActivity extends AppCompatActivity {
    ArrayList<com.findeed.Contacts> ContactList=new ArrayList<>();
    public static RecyclerView recyclerView, recyclerViewSelected;
    public RecyclerView.LayoutManager layoutManager;
    Cursor phones;
    ImageView crossImage;
    CircleImageView circleImageView;
    TextView selectedContact;
    Intent data;
    LinearLayout ss;
    public static Context ctx;
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;
    public static ContactActivity cca;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        ctx = getApplicationContext();
        cca=this;

        recyclerView =  findViewById(R.id.contacts_list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);



        showContacts();





    }

    private void showContacts() {
        // Check the SDK version and whether the permission is already granted or not.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
        } else {
            // Android version is lesser than 6.0 or the permission is already granted.
            phones = getApplicationContext().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
            LoadContact loadContact = new LoadContact();
            loadContact.execute();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                showContacts();
            } else {
                Toast.makeText(this, "Until you grant the permission, we canot display the names", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void onClickCalled(String your_argument_here) {

    }


    class LoadContact extends AsyncTask<Void, Void, Void> {

        ProgressDialog pd = new ProgressDialog(ContactActivity.this);
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pd.setMessage("Please Wait...");
            pd.show();



        }

        @Override
        protected Void doInBackground(Void... voids) {
            // Get Contact list from Phone

            if (phones != null) {
                Log.e("count", "" + phones.getCount());
                if (phones.getCount() == 0) {

                }

                int i=0;
                //data =new Intent();
                Bundle extra=getIntent().getExtras();
                String voucher1=extra.getString("voucher1");
                Log.i("voucherN",voucher1);
                while (phones.moveToNext()) {
                    String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    String phoneNumber2 = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    StringBuilder phoneNumber1= new StringBuilder();
                    if(phoneNumber2.contains(" ") ) {
                        for (int j = 0; j < phoneNumber2.split(" ").length; j++) {
                            phoneNumber1.append(phoneNumber2.split(" ")[j]);
                        }
                        phoneNumber2= phoneNumber1.toString();
                    }
                     if(phoneNumber2.contains("+91")){
                        phoneNumber2=phoneNumber2.substring(3);
                    }

                    try{
                        if(!name.matches("\\d+(?:\\.\\d+)?") && !phoneNumber2.contains("*") && !phoneNumber2.contains("#") && !phoneNumber2.contains("@") && phoneNumber2.charAt(0)!=1 && phoneNumber2.length()>=10 && !phoneNumber2.equals(voucher1) && !phoneNumber2.equals(ApplicationData.validateOTPResponse.getData().getPhoneNumber()) ){
                            Log.i("phoneNo2",phoneNumber2);
                            com.findeed.Contacts selectUser = new com.findeed.Contacts();

                            selectUser.setName(name);

                            selectUser.setPhone(phoneNumber2);
                            selectUser.setAvatar(name.substring(0,1));

                            Log.d("as",selectUser.toString());
                            ContactList.add(i,selectUser);
                            //    }

                            i++;

                        }else {
                          //  if(){
                            Log.i("phoneNo",phoneNumber2);
                            continue;
                        }


                    }catch (Exception e){
                        e.printStackTrace();

                    }



                }
            } else {
                Log.e("Cursor close 1", "----------------");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            // sortContacts();



            RecyclerAdapter ra = new RecyclerAdapter(ContactList);
            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
            recyclerView.setAdapter(ra);

            AutoCompleteTextView actvSearch=findViewById(R.id.actvSearch);
            actvSearch.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


                }
                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    //  lvlay.setVisibility(View.VISIBLE);
//                btnActvClear.setVisibility(View.VISIBLE);
                    ra.filter(charSequence.toString());
                }
                @Override
                public void afterTextChanged(Editable editable) {

                  /*  if(editable.length() > 2) {
                        if (!actv.isPopupShowing()) {
                            Toast.makeText(getApplicationContext(),"No results", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }*/

                }
            });




            if (pd.isShowing())
            {
                pd.dismiss();
            }

        }
    }



}
