package com.findeed;


import android.view.View;
import android.widget.ImageView;

public class Contacts {

    String name;
    String phone;
    String avatar;

    public void setView(View view) {
        this.view = view;
    }

    public View getView() {
        return view;
    }

    View view;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

}
