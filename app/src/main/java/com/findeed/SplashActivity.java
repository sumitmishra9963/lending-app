package com.findeed;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.findeed.API.ValidateOTPResponse;
import com.findeed.API.VoucherResponseData;
import com.findeed.Bot.ChatActivity;
import com.findeed.Voucher.VoucherChatActivity;
import com.google.gson.Gson;

import io.branch.referral.Branch;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;



@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP_MR1)
public class SplashActivity extends AppCompatActivity {

    String END_POINT = "http://157.245.102.194:8080/los-qa/";
    static  String phone;
    Intent intent;



    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);



                boolean isInternetAvailable=haveNetworkConnection();
                if(isInternetAvailable){

                    SharedPreferences sf = getSharedPreferences("mobileNumber", MODE_PRIVATE);
                    phone = sf.getString("phone", null);
                    if (phone== null || phone =="") {
                        final Intent intent = new Intent(SplashActivity.this, PhoneRegisterActivity.class);
                        Thread timer = new Thread() {
                            public void run() {
                                try {

                                    sleep(2000);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    finish();
                                    startActivity(intent);
                                }
                            }
                        };
                        timer.start();
                    } else {
                        new getCustomerDetails().execute();
                    }
                }else {
                    Toast.makeText(getApplicationContext(),"Kindly check your internet connection",Toast.LENGTH_LONG).show();
                }
    }



    private class GetVoucherDetails extends AsyncTask<String,String ,String >{
        Gson gson=new Gson();
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(s!=null){

                Intent intent=new Intent(SplashActivity.this,VoucherChatActivity.class);
                Thread timer = new Thread()
                {
                    public void run()
                    {
                        try {

                            sleep(2000);
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                        finally {
                            startActivity(intent);
                            finish();
                        }
                    }
                };
                timer.start();

            }else {
                Toast.makeText(SplashActivity.this,"Server Error",Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"voucher/getVoucherDetails/"+phone)
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String a=response.body().string();
                ApplicationData.voucherResponseData=gson.fromJson(a,VoucherResponseData.class);

                return a;

            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }

    private class getCustomerDetails extends AsyncTask<String,String ,String >{
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(s!=null){
                SharedPreferences sf = getSharedPreferences("deepLink", MODE_PRIVATE);
                String isVoucher = sf.getString("isVoucher", null);
                if(isVoucher!=null){

                        Log.i("Came Here","Yes");
                        new GetVoucherDetails().execute();

                }else if(ApplicationData.isCameFromDeepLink){
                    intent = new Intent(SplashActivity.this, PhoneRegisterActivity.class);
                    Thread timer = new Thread()
                    {
                        public void run()
                        {
                            try {

                                sleep(2000);
                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                            }
                            finally {
                                finish();
                                startActivity(intent);

                            }
                        }
                    };
                    timer.start();
                }

                else {
                      Gson gson = new Gson();
                      ApplicationData.validateOTPResponse = gson.fromJson(s, ValidateOTPResponse.class);
                      intent = new Intent(SplashActivity.this, ChatActivity.class);
                    Thread timer = new Thread()
                    {
                        public void run()
                        {
                            try {

                                sleep(2000);
                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                            }
                            finally {
                                startActivity(intent);
                                finish();
                            }
                        }
                    };
                    timer.start();
                }

            }else {
                Toast.makeText(SplashActivity.this,"Server Error",Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"customer/getCustomerDetails/"+phone)
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String a=response.body().string();
                return a;

            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }

    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    @Override
    public void onStart() {
        super.onStart();
        Branch branch = Branch.getInstance();

        // Branch init
        branch.initSession((referringParams, error) -> {
            if (error == null) {
                // params are the deep linked params associated with the link that the user clicked -> was re-directed to this app
                // params will be empty if no data found
                // ... insert custom logic here ...

                try {

                    if(referringParams.has("+clicked_branch_link")){
                        ApplicationData.isCameFromDeepLink= referringParams.getBoolean("+clicked_branch_link");
                        Log.i("SUMIT:",String.valueOf(ApplicationData.isCameFromDeepLink));
                    }
                    else{
                        ApplicationData.isCameFromDeepLink= false;
                        Log.i("SUMIT:","Do not have Link");
                    }

                } catch (Exception e) {
                    e.printStackTrace();

                }
                Log.i("BRANCH SDK", referringParams.toString());
            } else {
                Log.i("V SDK", error.getMessage());
            }
        }, this.getIntent().getData(), this);
    }

    @Override
    public void onNewIntent(Intent intent) {
        this.setIntent(intent);
    }
}

