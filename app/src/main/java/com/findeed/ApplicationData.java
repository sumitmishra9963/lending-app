package com.findeed;

import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.location.Location;

import com.findeed.API.ValidateOTPResponse;
import com.findeed.API.VoucherResponse;
import com.findeed.API.VoucherResponseData;
import com.google.firebase.analytics.FirebaseAnalytics;

import io.branch.referral.Branch;
import io.branch.referral.BranchApp;

public class ApplicationData extends BranchApp {
    public static boolean isCameFromDeepLink;
    public static ValidateOTPResponse validateOTPResponse;
    public static VoucherResponseData voucherResponseData;
    public static   boolean isStartOverClicked=false;
    public  static VoucherResponse voucherResponse;
    public static Location mapLocation;
    public  static ProgressDialog loadChat;
    public static String END_POINT = "http://157.245.102.194:8080/los-qa/";
    public static String KHOSALA_URL="https://prod.veri5digital.com/";
    public static  boolean isItVoucher;
    public static final String CLIENT_CODE="FIND9402";
    public static final String API_KEY="192832718240580";
    public  static  final  String SALT="Wwa7sWnc28kfsx";
    FirebaseAnalytics mFirebaseAnalytics;

    @Override
    public void onCreate() {
        super.onCreate();
        // Initialize the Branch object
        Branch.getAutoInstance(this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);

    }

    @Override
    public void registerActivityLifecycleCallbacks(ActivityLifecycleCallbacks callback) {
        super.registerActivityLifecycleCallbacks(callback);
    }




}
