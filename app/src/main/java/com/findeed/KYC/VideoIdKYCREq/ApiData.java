package com.findeed.KYC.VideoIdKYCREq;

public class ApiData {
 com.findeed.KYC.VideoIdKYCREq.headers headers;
 com.findeed.KYC.VideoIdKYCREq.request request;

    public headers getHeaders() {
        return headers;
    }

    public void setHeaders(headers headers) {
        this.headers = headers;
    }

    public request getRequest() {
        return request;
    }

    public void setRequest(request request) {
        this.request = request;
    }
}
