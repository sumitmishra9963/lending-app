package com.findeed.KYC.VideoIdKycRes;

public class KYCInfo {
    DeclaredKycInfo declared_kyc_info;
    Photo photo;
    DocImage doc_image;
    OriginalKYCInfo original_kyc_info;
    VerifiedAgent verified_agent;

    public DeclaredKycInfo getDeclared_kyc_info() {
        return declared_kyc_info;
    }

    public void setDeclared_kyc_info(DeclaredKycInfo declared_kyc_info) {
        this.declared_kyc_info = declared_kyc_info;
    }

    public Photo getPhoto() {
        return photo;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }

    public DocImage getDoc_image() {
        return doc_image;
    }

    public void setDoc_image(DocImage doc_image) {
        this.doc_image = doc_image;
    }

    public OriginalKYCInfo getOriginal_kyc_info() {
        return original_kyc_info;
    }

    public void setOriginal_kyc_info(OriginalKYCInfo original_kyc_info) {
        this.original_kyc_info = original_kyc_info;
    }

    public VerifiedAgent getVerified_agent() {
        return verified_agent;
    }

    public void setVerified_agent(VerifiedAgent verified_agent) {
        this.verified_agent = verified_agent;
    }
}
