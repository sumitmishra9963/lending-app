package com.findeed;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.findeed.Bot.ChatActivity;

public class TermsActivity extends AppCompatActivity {

    ImageView backArrowTerms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);

        backArrowTerms = findViewById(R.id.back_terms);
        Intent intent = getIntent();
        String chatActivity = intent.getStringExtra("CHAT_ACTIVITY");
        String voucherActivity = intent.getStringExtra("VOUCHER_ACTIVITY");

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);


        backArrowTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*Intent intent = new Intent(TermsActivity.this, ChatActivity.class);
                startActivity(intent);*/
                finish();
            }
        });

    }
}
