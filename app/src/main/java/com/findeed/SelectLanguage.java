package com.findeed;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.findeed.Bot.ChatActivity;


public class SelectLanguage extends AppCompatActivity {

    ImageView englishImg, hindiImg, kannadaImg;
  public static String USER_LANGUAGE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_language);




        englishImg = findViewById(R.id.english);
        hindiImg  = findViewById(R.id.hindi);
        kannadaImg = findViewById(R.id.kannada);





        englishImg.setOnClickListener(v -> {
            try{
                Intent intent = new Intent(SelectLanguage.this, PhoneRegisterActivity.class);
                startActivity(intent);
                finish();
                USER_LANGUAGE="English";


            }catch (Exception e){
                e.printStackTrace();
            }
        });
        hindiImg.setOnClickListener(v -> {
            Intent intent = new Intent(SelectLanguage.this, PhoneRegisterActivity.class);
            startActivity(intent);
            finish();
            USER_LANGUAGE="Hindi";


        });
        kannadaImg.setOnClickListener(v -> {
            Intent intent = new Intent(SelectLanguage.this, PhoneRegisterActivity.class);
            startActivity(intent);
            finish();
            USER_LANGUAGE="Kannad";

        });

    }
}
