package com.findeed;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.findeed.API.InvallidOTPData;
import com.findeed.API.OtpResponse;
import com.findeed.API.ValidateOTPResponse;
import com.findeed.API.VoucherResponseData;
import com.findeed.Bot.ChatActivity;
import com.findeed.Voucher.VoucherChatActivity;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.Circle;
import com.github.ybq.android.spinkit.style.DoubleBounce;
import com.goodiebag.pinview.Pinview;
import com.google.gson.Gson;

import java.util.Objects;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.findeed.ApplicationData.validateOTPResponse;

public class VerifyOTPActivity extends AppCompatActivity {

    TextView textcount, editnumber,number, otpexpireTV, resendOTP;
    EditText edtone, edttwo, edtthree, edtfour;
    Button btnstart;
    String otp ,phoneNumber;
    ImageView back;
    CountDownTimer countDownTimer;
    ProgressBar progressBar;
    int timer=45000;
    boolean isCounterRunning  = false;
    String END_POINT=  "http://157.245.102.194:8080/los-qa/";
   public static Pinview pn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_otp);


        Intent i =getIntent();
        phoneNumber=i.getStringExtra("phone");

        number=findViewById(R.id.number);
        textcount = findViewById(R.id.counttime);
        resendOTP = findViewById(R.id.tv_resendOTP);
        editnumber = findViewById(R.id.edit_number);
        pn = findViewById(R.id.myview);
        pn.requestFocus();

//        edtone = findViewById(R.id.edt1);
//        edttwo = findViewById(R.id.edt2);
//        edtthree = findViewById(R.id.edt3);
//        edtfour = findViewById(R.id.edt4);
        btnstart = findViewById(R.id.btnstart);
        back=findViewById(R.id.backarrow);
        otpexpireTV = findViewById(R.id.otpexpireTV);

//         edtone.addTextChangedListener(otpTextWatcher);
//         edttwo.addTextChangedListener(otpTextWatcher);
//         edtthree.addTextChangedListener(otpTextWatcher);
//         edtfour.addTextChangedListener(otpTextWatcher);


        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        number.setText("+91XXXXXX"+phoneNumber.substring(6));

        back.setOnClickListener(view -> {
            Intent intent1 = new Intent(getApplicationContext(), PhoneRegisterActivity.class);
            startActivity(intent1);
        });






        new CountDownTimer(timer, 1000) {

            public void onTick(long millisUntilFinished) {
                long seconds=millisUntilFinished/1000;
                long minutes=seconds/60;
                long secondsLeft=seconds%60;
                textcount.setGravity(View.TEXT_ALIGNMENT_CENTER);
                textcount.setGravity(Gravity.CENTER);
                if(minutes==0){
                    textcount.setText("00: "+secondsLeft);
                }else
                    textcount.setText( "00: "+secondsLeft+ "");



            }

            public void onFinish() {
                resendOTP.setVisibility(View.VISIBLE);
                otpexpireTV.setVisibility(View.GONE);
                textcount.setVisibility(View.GONE);
                resendOTP.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                resendOTP.setGravity(Gravity.CENTER);
               /* textcount.setText("OTP has expired. Resend OTP");
                textcount.setTextColor(getResources().getColor(R.color.dark_pink));*/


            }
        }.start();


//        new CountDownTimer(50000,1000) {
//            @Override
//            public void onTick(long millisUntilFinished) {
//
//            }
//            @Override
//            public void onFinish() {
//               resendOTP.setVisibility(View.VISIBLE);
//            }
//        }.start();



        resendOTP.setOnClickListener(view -> new ResendOtp().execute());


        editnumber.setOnClickListener(v -> {

            Intent intent1 = new Intent(getApplicationContext(), PhoneRegisterActivity.class);
            intent1.putExtra("phone",phoneNumber);
            startActivity(intent1);
        });

        btnstart.setOnClickListener(v -> {
            btnstart.setEnabled(false);
//            progressBar = findViewById(R.id.spin_kit);
//            Sprite doubleBounce = new Circle();
//            progressBar.setIndeterminateDrawable(doubleBounce);
//            progressBar.setVisibility(View.VISIBLE);
            ApplicationData.loadChat=new ProgressDialog(VerifyOTPActivity.this);
            ApplicationData.loadChat.setMessage("Verifying OTP..");
            ApplicationData.loadChat.setCancelable(false);
            ApplicationData.loadChat.show();

            new verifyOTP().execute();
             //new GetCustomerDetails().execute();


        });


        pn.setPinViewEventListener((pinview, fromUser) -> {


            otp = pinview.getValue();

            if (otp.length() == 4) {
                btnstart.setEnabled(true);
            }
        });

//        edtone.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//                Integer textlength1 = edtone.getText().length();
//
//                if (textlength1 >= 1) {
//                    edttwo.requestFocus();
//                }
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
//
//        edttwo.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//                Integer textlength2 = edttwo.getText().length();
//
//                if (textlength2 >= 1) {
//                    edtthree.requestFocus();
//                }
//                else if(textlength2==0)
//                {
//                    edtone.requestFocus();
//                }
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
//
//        edtthree.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//                Integer textlength3 = edtthree.getText().length();
//
//                if (textlength3 >= 1) {
//                    edtfour.requestFocus();
//                }
//                else if(textlength3==0)
//                {
//                    edttwo.requestFocus();
//                }
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
//        edtfour.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//                Integer textlength4 = edtfour.getText().length();
//                if (textlength4 >= 1) {
//                    edtfour.requestFocus();
//                    /*btnstart.requestFocus();*/
//                }
//                else if(textlength4==0)
//                {
//                    edtthree.requestFocus();
//                }
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
    }

//    private TextWatcher otpTextWatcher = new TextWatcher() {
//        @Override
//        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//        }
//
//        @SuppressLint("ResourceType")
//        @Override
//        public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            String editOneInput = edtone.getText().toString().trim();
//            String editTwoInput = edttwo.getText().toString().trim();
//            String editThirdInput = edtthree.getText().toString().trim();
//            String editFourInput = edtfour.getText().toString().trim();
//            if(!editFourInput.isEmpty()){
//                hideKeyboard();
//            }
//            btnstart.setEnabled(!editOneInput.isEmpty() && !editTwoInput.isEmpty() && !editThirdInput.isEmpty() && !editFourInput.isEmpty());
//           if(!editOneInput.isEmpty() && !editTwoInput.isEmpty() && !editThirdInput.isEmpty() && !editFourInput.isEmpty())
//           {
//               btnstart.setBackgroundColor(Color.BLACK);
//               btnstart.setBackgroundResource(R.drawable.btn_gradient_style);
//
//           }
//           else if(editOneInput.isEmpty() || editTwoInput.isEmpty() || editThirdInput.isEmpty() || editFourInput.isEmpty())
//           {
//               btnstart.setEnabled(false);
//               btnstart.setBackgroundColor(getResources().getColor(R.color.silverDark));
//               btnstart.setBackgroundResource(R.drawable.btn_gradient_style);
//           }
//
//        }
//
//        @Override
//        public void afterTextChanged(Editable s) {
//            otp=edtone.getText().toString()+edttwo.getText().toString()+edtthree.getText().toString()+edtfour.getText().toString();
//
//        }
//    };

    private   void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            view.clearFocus();
            this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            //InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            //inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }


    public  class ResendOtp extends AsyncTask<String,String,String>{
        ProgressDialog pd = new ProgressDialog(VerifyOTPActivity.this);

        @Override
        protected void onPreExecute() {

            resendOTP.setVisibility(View.GONE);
            otpexpireTV.setVisibility(View.VISIBLE);
            textcount.setVisibility(View.VISIBLE);
            pd.setMessage("Re-sending OTP.....");
            pd.show();
        }

        @Override
        protected void onPostExecute(String s) {
            if (pd.isShowing()) {
                pd.dismiss();
                btnstart.setEnabled(true);

                new CountDownTimer(timer, 1000) {

                    public void onTick(long millisUntilFinished) {
                        long seconds=millisUntilFinished/1000;
                        long minutes=seconds/60;
                        long secondsLeft=seconds%60;
                        textcount.setGravity(View.TEXT_ALIGNMENT_CENTER);
                        textcount.setGravity(Gravity.CENTER);
                        if(minutes==0){
                            textcount.setText("00: "+secondsLeft);
                        }else
                            textcount.setText( "00: "+secondsLeft+ "");



                    }

                    public void onFinish() {
                        resendOTP.setVisibility(View.VISIBLE);
                        otpexpireTV.setVisibility(View.GONE);
                        textcount.setVisibility(View.GONE);
                        resendOTP.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                        resendOTP.setGravity(Gravity.CENTER);
               /* textcount.setText("OTP has expired. Resend OTP");
                textcount.setTextColor(getResources().getColor(R.color.dark_pink));*/
                        btnstart.setEnabled(false);

                    }
                }.start();


            }
        }

        @Override
        protected String doInBackground(String... strings) {


            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"otp/generateOTP/"+phoneNumber)
                    .get()
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String resData= Objects.requireNonNull(response.body()).string();
                Gson gson = new Gson();
                OtpResponse res = gson.fromJson(resData,OtpResponse.class);
                return res.toString();

            }catch (Exception e){
                e.printStackTrace();
                return null;
            }

        }
    }


    public  class GetCustomerDetails extends AsyncTask<String,String ,String >{



        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            ApplicationData.loadChat.dismiss();
            Intent intentwelcome = new Intent(VerifyOTPActivity.this, ChatActivity.class);
            finish();
            startActivity(intentwelcome);

        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(ApplicationData.END_POINT+"customer/getCustomerDetails/"+phoneNumber)
                    .build();
            try{
                Response response = client.newCall(request).execute();
                validateOTPResponse=new Gson().fromJson(response.body().string(),ValidateOTPResponse.class);
                return response.body().string();

            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }

    public class verifyOTP extends AsyncTask<String,String,String>{

        @Override
        protected void onPostExecute(String s) {

            try{
                if(s.equals("Success")){
                    //hideKeyboard();
                    ApplicationData.loadChat.setMessage("Loading...");
                    Toast.makeText(getApplicationContext(),"OTP Matched",Toast.LENGTH_LONG).show();
                    Gson gson = new Gson();
                    SharedPreferences sf = getSharedPreferences("userData",MODE_PRIVATE);
                    ApplicationData.validateOTPResponse = gson.fromJson(sf.getString("details", ""),ValidateOTPResponse.class);

                    if(ApplicationData.isCameFromDeepLink){
                        SharedPreferences.Editor editor2 = getSharedPreferences("deepLink", MODE_PRIVATE).edit();
                        editor2.putString("isVoucher","1");
                        editor2.apply();
                        new GetVoucherDetails().execute();


                    }else {
                        Intent intentwelcome = new Intent(VerifyOTPActivity.this, ChatActivity.class);
                        finish();
                        startActivity(intentwelcome);
                    }

                    InputMethodManager imm =(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(findViewById(android.R.id.content).getRootView()
                            .getWindowToken(), 0);
                    finish();
                }else {
                    ApplicationData.loadChat.dismiss();
                    Toast.makeText(getApplicationContext(),"Invalid OTP",Toast.LENGTH_LONG).show();
                }
            }catch (NullPointerException e){
                e.printStackTrace();

            }

        }

        Gson gson = new Gson();
        String resData;

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            //.url(END_POINT+"otp/validateOTP/"+phoneNumber+"/"+otp+"/"+SelectLanguage.USER_LANGUAGE)
            Request request = new Request.Builder()
                    .url(END_POINT+"otp/validateOTP/"+phoneNumber+"/"+otp+"/"+"English")
                    .get()
                    .build();

            try{
                Response response = client.newCall(request).execute();
                resData  = Objects.requireNonNull(response.body()).string();
                ApplicationData.validateOTPResponse  = gson.fromJson(resData,ValidateOTPResponse.class);
                SharedPreferences.Editor editor = getSharedPreferences("userData", MODE_PRIVATE).edit();
                editor.putString("details", gson.toJson(ApplicationData.validateOTPResponse));
                editor.apply();
                SharedPreferences.Editor editor2 = getSharedPreferences("mobileNumber", MODE_PRIVATE).edit();
                editor2.putString("phone",ApplicationData.validateOTPResponse.getData().getPhoneNumber());
                editor2.apply();
                return ApplicationData.validateOTPResponse.getMessage() ;

            }catch (Exception e){
                try{
                    InvallidOTPData id = gson.fromJson(resData,InvallidOTPData.class);
                    return id.getMessage();
                }catch (Exception e2){
                    e2.printStackTrace();
                    runOnUiThread(() -> Toast.makeText(VerifyOTPActivity.this, "500 Internal Server Error!", Toast.LENGTH_SHORT).show());
                    return "Failed";
                }

            }
        }
    }

    private class GetVoucherDetails extends AsyncTask<String,String ,String >{
        Gson gson=new Gson();
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(s!=null){

                Intent intentwelcome = new Intent(VerifyOTPActivity.this, VoucherChatActivity.class);
                finish();
                startActivity(intentwelcome);

            }else {
                Toast.makeText(VerifyOTPActivity.this,"Server Error",Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"voucher/getVoucherDetails/"+phoneNumber)
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String a=response.body().string();
                ApplicationData.voucherResponseData=gson.fromJson(a, VoucherResponseData.class);
                return a;

            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }

    @Override
    public void onBackPressed() {

        Intent intent1 = new Intent(getApplicationContext(), PhoneRegisterActivity.class);
        finish();
        startActivity(intent1);
        super.onBackPressed();
    }
}



