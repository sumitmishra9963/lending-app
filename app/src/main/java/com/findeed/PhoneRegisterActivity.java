package com.findeed;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.TypefaceSpan;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.findeed.API.InvallidOTPData;
import com.findeed.API.OtpResponse;
import com.findeed.API.ValidateOTPResponse;
import com.findeed.Bot.ChatActivity;
import com.findeed.Bot.ChatContract;
import com.findeed.Bot.PrivacyPolicy;
import com.google.gson.Gson;

import java.util.Objects;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class PhoneRegisterActivity extends AppCompatActivity {
    LinearLayout linearLayout, llPhoneView;
    EditText phoneNumber;
    String mobNo=null;
    TextView txtWelcome, txtotpexpire;
    TextView txtTerms, tvInvalidPhone;
    String END_POINT=  "http://157.245.102.194:8080/los-qa/";
    InputMethodManager methodManager;

    private   void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            view.clearFocus();
            InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_register);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        // txtotpexpire = findViewById(R.id.otpexpireTV);


        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        methodManager = (InputMethodManager) this.getSystemService(INPUT_METHOD_SERVICE);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        linearLayout = findViewById(R.id.llnextbutton);
        phoneNumber=findViewById(R.id.phoneNumber);
        txtWelcome = findViewById(R.id.txt_welcomeone);
        txtTerms = findViewById(R.id.txt_terms);
        llPhoneView = findViewById(R.id.llphonetextview);
        tvInvalidPhone = findViewById(R.id.tv_error_invalidphone);
        try{
            Intent i=getIntent();
            mobNo=i.getStringExtra("phone");
            phoneNumber.setText(mobNo);
            phoneNumber.requestFocus();
            phoneNumber.setSelection(mobNo.length()-1);


        }catch (RuntimeException e){
            e.printStackTrace();
        }









        Typeface font1 = ResourcesCompat.getFont(this, R.font.montserratsemibold);
        Typeface font2 = ResourcesCompat.getFont(this, R.font.montserrat_regular);

        SpannableStringBuilder spanString = new SpannableStringBuilder("Welcome to Findeed");
        ForegroundColorSpan fcs = new ForegroundColorSpan(getResources().getColor(R.color.darkslate));

        spanString.setSpan(new CustomTypefaceSpan("", font2), 0, 9,Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        spanString.setSpan(new CustomTypefaceSpan("", font1), 10, 18,Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        spanString.setSpan(fcs, 10, 18, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        spanString.setSpan(new RelativeSizeSpan(1.2f), 10, 18, 0);
        // spanString.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 10, 18, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        txtWelcome.setText(spanString);




        SpannableStringBuilder spanTerms = new SpannableStringBuilder("By continuing, you agree to our Terms and Conditions and");
        spanTerms.setSpan(new CustomTypefaceSpan("", font2), 0, 31, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        spanTerms.setSpan(new CustomTypefaceSpan("", font1), 32, 52,Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        spanTerms.setSpan(new CustomTypefaceSpan("", font2), 53, 56,Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        ForegroundColorSpan fcss = new ForegroundColorSpan(getResources().getColor(R.color.dark_pink));
        spanTerms.setSpan(fcss, 31, 52, Spannable.SPAN_INCLUSIVE_INCLUSIVE );
        ClickableSpan terms = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                Toast.makeText(PhoneRegisterActivity.this, "Terms", Toast.LENGTH_SHORT).show();
            }
        };

        spanTerms.setSpan(terms, 31, 52, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        txtTerms.setText(spanTerms);
        txtTerms.setMovementMethod(LinkMovementMethod.getInstance());


        Spannable privacypolicy = new SpannableString(" Privacy Policy");
        privacypolicy.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.dark_pink)), 0, 15, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        privacypolicy.setSpan(new CustomTypefaceSpan("", font1), 0, 15, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

        ClickableSpan privacy = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {

                Toast.makeText(PhoneRegisterActivity.this, "Privacy", Toast.LENGTH_SHORT).show();
            }
        };

        privacypolicy.setSpan(privacy, 0, 15, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        txtTerms.append(privacypolicy);
        txtTerms.setMovementMethod(LinkMovementMethod.getInstance());
        txtTerms.setHighlightColor(Color.TRANSPARENT);


        llPhoneView.setOnClickListener(v -> startColorAnimation(phoneNumber));

        phoneNumber.setOnClickListener(v -> startColorAnimation(phoneNumber));



        txtTerms.setOnClickListener(v -> {
            Intent t=new Intent(PhoneRegisterActivity.this, PrivacyPolicy.class);
            startActivity(t);
        });



        methodManager.toggleSoftInputFromWindow(
                phoneNumber.getApplicationWindowToken(),
                InputMethodManager.SHOW_FORCED, 0);

        if(mobNo==null) {

            phoneNumber.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }else {

            phoneNumber.setText(mobNo);
            phoneNumber.setSelection(mobNo.length());
        }


        linearLayout.setOnClickListener(v -> {
            if(phoneNumber.getText().toString().length()!=10)  {
                tvInvalidPhone.setVisibility(View.VISIBLE);
            }
            else{
                tvInvalidPhone.setVisibility(View.GONE);
//                Intent intent = new Intent(PhoneRegisterActivity.this, VerifyOTPActivity.class);
//                intent.putExtra("phone",phoneNumber.getText().toString());
//                startActivity(intent);
//                finish();
                new getOtp().execute();

            }

        });

        phoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tvInvalidPhone.setVisibility(View.GONE);

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    private  void startColorAnimation(View v)
    {
        int colorStart = v.getSolidColor();
        int endColor =   0xF4F5F6;

        ValueAnimator colorAnim = ObjectAnimator.ofInt(v, "backgroundColor", colorStart, endColor);
        colorAnim.setDuration(1000);
        colorAnim.setEvaluator(new ArgbEvaluator());
        colorAnim.setRepeatCount(1);
        colorAnim.setRepeatMode(ValueAnimator.REVERSE);
        colorAnim.start();
    }



//   public class BypassOtp extends AsyncTask<String,String,String>{
//
//        @Override
//        protected void onPostExecute(String s) {
//            try{
//
//                Gson gson = new Gson();
//                SharedPreferences sf = getSharedPreferences("userData",MODE_PRIVATE);
//                ApplicationData.validateOTPResponse = gson.fromJson(sf.getString("details", ""), ValidateOTPResponse.class);
//                SharedPreferences.Editor editor2 = getSharedPreferences("mobileNumber", MODE_PRIVATE).edit();
//                editor2.putString("phone",ApplicationData.validateOTPResponse.getData().getPhoneNumber());
//                editor2.apply();
//                Intent intentwelcome = new Intent(PhoneRegisterActivity.this, ChatActivity.class);
//                startActivity(intentwelcome);
//
//
//            }catch (NullPointerException e){
//                e.printStackTrace();
//            }
//
//        }
//
//        Gson gson = new Gson();
//        String resData;
//
//        @Override
//        protected String doInBackground(String... strings) {
//            OkHttpClient client = new OkHttpClient();
//            Request request = new Request.Builder()
//                    .url("http://157.245.102.194:8080/los-build/customer/create/"+phoneNumber.getText().toString())
//                    .get()
//                    .build();
//
//            try{
//                Response response = client.newCall(request).execute();
//                resData  =response.body().string();
//                SharedPreferences.Editor editor = getSharedPreferences("userData", MODE_PRIVATE).edit();
//                editor.putString("details", resData);
//                editor.apply();
//                return "Success" ;
//
//            }catch (Exception e){
//                InvallidOTPData id = gson.fromJson(resData,InvallidOTPData.class);
//                return id.getMessage();
//            }
//        }
//    }

    public  class getOtp extends AsyncTask<String,String,String>{
        ProgressDialog pd = new ProgressDialog(PhoneRegisterActivity.this);

        @Override
        protected void onPreExecute() {
            pd.setMessage("Sending OTP...");
            pd.show();
        }

        @Override
        protected void onPostExecute(String s) {
            if (pd.isShowing()) {
                pd.dismiss();
                Intent intent = new Intent(PhoneRegisterActivity.this, VerifyOTPActivity.class);
                intent.putExtra("phone",phoneNumber.getText().toString());
                finish();
                startActivity(intent);
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"otp/generateOTP/"+phoneNumber.getText().toString())
                    .get()
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String resData= Objects.requireNonNull(response.body()).string();
                Gson gson = new Gson();
                OtpResponse res = gson.fromJson(resData,OtpResponse.class);
                return res.getMessage();

            }catch (Exception e){
                e.printStackTrace();
                return null;
            }

        }
    }
}