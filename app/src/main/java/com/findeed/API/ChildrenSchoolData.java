package com.findeed.API;

public class ChildrenSchoolData {

    String schoolId;
    String schoolName;
    String contactName;
    String addressLine1;
    String addressLine2;
    String area;
    String city;
    String zipcode;
    String phoneNumber;
    String email;
    Float latitude;
    Float longitude;

    public String getSchoolId() {
        return schoolId;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public String getContactName() {
        return contactName;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public String getArea() {
        return area;
    }

    public String getCity() {
        return city;
    }

    public String getZipcode() {
        return zipcode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public Float getLatitude() {
        return latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public int getIsEmpanelled() {
        return isEmpanelled;
    }

    public String getClasses() {
        return classes;
    }

    int isEmpanelled;
    String classes;
}
