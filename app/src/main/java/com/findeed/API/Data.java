package com.findeed.API;

import java.util.ArrayList;

public class Data implements Cloneable{



    public  int isSaveHaveKids;

    public String employerName;
    public int isSaveLoanAggrement;
    public SchoolData school;


    public void setSchool(SchoolData school) {
        this.school = school;
    }

    public SchoolData getSchool() {
        return school;
    }

    public void setIsSaveHaveKids(int isSaveHaveKids) {
        this.isSaveHaveKids = isSaveHaveKids;
    }

    public void setEmployerName(String employerName) {
        this.employerName = employerName;
    }

    public void setIsSaveLoanAggrement(int isSaveLoanAggrement) {
        this.isSaveLoanAggrement = isSaveLoanAggrement;
    }

    public void setIsCurrentPermanentAddressSame(int isCurrentPermanentAddressSame) {
        this.isCurrentPermanentAddressSame = isCurrentPermanentAddressSame;
    }

    public void setIsSaveLoanSummary(int isSaveLoanSummary) {
        this.isSaveLoanSummary = isSaveLoanSummary;
    }

    public void setIsSaveEmiSummary(int isSaveEmiSummary) {
        this.isSaveEmiSummary = isSaveEmiSummary;
    }

    public void setIsFaceAuth(int isFaceAuth) {
        this.isFaceAuth = isFaceAuth;
    }

    public void setPanUploadCount(int panUploadCount) {
        this.panUploadCount = panUploadCount;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public void setWorkingYears(String workingYears) {
        this.workingYears = workingYears;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public void setWhatsappNumber(String whatsappNumber) {
        this.whatsappNumber = whatsappNumber;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public void setVoucherOrCustomer(String voucherOrCustomer) {
        this.voucherOrCustomer = voucherOrCustomer;
    }

    public void setDocuments(ArrayList<DocumentData> documents) {
        this.documents = documents;
    }

    public void setUnempanelledSchools(ArrayList<UnEmpanelledSchool> unempanelledSchools) {
        this.unempanelledSchools = unempanelledSchools;
    }

    public int getIsSaveLoanAggrement() {
        return isSaveLoanAggrement;
    }

    public String getEmployerName() {
        return employerName;
    }


    public int getIsSaveHaveKids() {
        return isSaveHaveKids;
    }

    public int isCurrentPermanentAddressSame;

    public int getIsSaveLoanSummary() {
        return isSaveLoanSummary;
    }

    public int getIsSaveEmiSummary() {
        return isSaveEmiSummary;
    }



    public int getIsFaceAuth() {
        return isFaceAuth;
    }

    public int isSaveLoanSummary;
   public int isSaveEmiSummary;
   public int isFaceAuth;
   public int panUploadCount;

    public int getPanUploadCount() {
        return panUploadCount;
    }

    public int getIsCurrentPermanentAddressSame() {
        return isCurrentPermanentAddressSame;
    }


    public String employeeId;
    public String workingYears;
    public String businessType;
    public String businessName;
    public String emailAddress;
    public String whatsappNumber;
    public String maritalStatus;

    public String getEmployeeId() {
        return employeeId;
    }

    public String getWorkingYears() {
        return workingYears;
    }

    public String getBusinessType() {
        return businessType;
    }

    public String getBusinessName() {
        return businessName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getWhatsappNumber() {
        return whatsappNumber;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public String getVoucherOrCustomer() {
        return voucherOrCustomer;
    }

    public String voucherOrCustomer;

    public int customerId;
    public String firstName;
    public String lastName;
    public String phoneNumber;
    public int numberOfChildren;
    public UserAddress address;
    public String language;
    public String employmentType;
    public String salaryRange;
    public ArrayList<Voucher> vouchers;
    public ArrayList< Children > children = new ArrayList ();
    public ArrayList < Loan > loans = new ArrayList ();
    public ArrayList<DocumentData> documents=new ArrayList<>();
    public  ArrayList<UnEmpanelledSchool> unempanelledSchools = new ArrayList<>();
    public ArrayList<DocumentData> getDocuments() {
        return documents;
    }
    public ArrayList<UnEmpanelledSchool> getUnempanelledSchools() {
        return unempanelledSchools;
    }
    public int getNumberOfChildren() {
        return numberOfChildren;
    }
    public void setNumberOfChildren(int numberOfChildren) { this.numberOfChildren = numberOfChildren; }
    public ArrayList<Voucher> getVouchers() { return vouchers; }
    public void setVouchers(ArrayList<Voucher> vouchers) { this.vouchers = vouchers; }
    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getEmploymentType() {
        return employmentType;
    }

    public void setEmploymentType(String employmentType) {
        this.employmentType = employmentType;
    }

    public String getSalaryRange() {
        return salaryRange;
    }

    public void setSalaryRange(String salaryRange) {
        this.salaryRange = salaryRange;
    }

    public ArrayList<Children> getChildren() {
        return children;
    }

    public void setChildren(ArrayList<Children> children) {
        this.children = children;
    }

    public ArrayList<Loan> getLoans() {
        return loans;
    }

    public void setLoans(ArrayList<Loan> loans) {
        this.loans = loans;
    }


    // Getter Methods

    public int getCustomerId() {
        return customerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public UserAddress getAddress() {
        return address;
    }

    public void setAddress(UserAddress address) {
        this.address = address;
    }


// Setter Methods

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

}
