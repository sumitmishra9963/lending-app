package com.findeed.API;

public class LivelinessReq {
    int customerId;
    String image;
    float matchValue;

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setMatchValue(float matchValue) {
        this.matchValue = matchValue;
    }
}
