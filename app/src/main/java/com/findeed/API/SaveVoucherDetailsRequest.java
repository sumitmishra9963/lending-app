package com.findeed.API;

import java.util.List;

public class SaveVoucherDetailsRequest {


    int customerVoucherId;

    String voucherAddress;

    public void setVoucherAddress(String voucherAddress) {
        this.voucherAddress = voucherAddress;
    }

    public void setCustomerVoucherId(int customerVoucherId) {
        this.customerVoucherId = customerVoucherId;
    }

    List<Customer> customers;

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setVoucherName(String voucherName) {
        this.voucherName = voucherName;
    }

    public void setCustomerAcquantance(String customerAcquantance) {
        this.customerAcquantance = customerAcquantance;
    }

    public void setCustomerRelationship(String customerRelationship) {
        this.customerRelationship = customerRelationship;
    }

    public void setRelationshipLength(String relationshipLength) {
        this.relationshipLength = relationshipLength;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public void setNumberOfChildren(String numberOfChildren) {
        this.numberOfChildren = numberOfChildren;
    }

    public void setCustomerEmployment(String customerEmployment) {
        this.customerEmployment = customerEmployment;
    }

    public void setCustomerEmployer(String customerEmployer) {
        this.customerEmployer = customerEmployer;
    }

    public void setCustomerBusiness(String customerBusiness) {
        this.customerBusiness = customerBusiness;
    }

    public void setCustomerVoucherArea(String customerVoucherArea) {
        this.customerVoucherArea = customerVoucherArea;
    }

    public void setCustomerStayArea(String customerStayArea) {
        this.customerStayArea = customerStayArea;
    }

    public void setCustomerVoucherWork(String customerVoucherWork) {
        this.customerVoucherWork = customerVoucherWork;
    }

    String  phoneNumber;
    String  voucherName;
    String  customerAcquantance;
    String  customerRelationship;
    String  relationshipLength;
    String  maritalStatus;
    String numberOfChildren;
    String  customerEmployment;
    String customerEmployer;
    String customerBusiness;
    String customerVoucherArea;
    String customerStayArea;
    String customerVoucherWork;




}
