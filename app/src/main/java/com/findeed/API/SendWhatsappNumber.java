package com.findeed.API;

public class SendWhatsappNumber {
    int customerId;
    String whatsappNumber;

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public void setWhatsappNumber(String whatsappNumber) {
        this.whatsappNumber = whatsappNumber;
    }
}
