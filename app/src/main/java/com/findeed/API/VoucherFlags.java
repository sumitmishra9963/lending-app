package com.findeed.API;

public class VoucherFlags {
    int customerVoucherId;
    int ccsPermission;
    int isLocationSame;
    int bureauPermission;
    int hasAcceptedInvitation;

    public int getCustomerVoucherId() {
        return customerVoucherId;
    }

    public int getCcsPermission() {
        return ccsPermission;
    }

    public void setCustomerVoucherId(int customerVoucherId) {
        this.customerVoucherId = customerVoucherId;
    }

    public void setCcsPermission(int ccsPermission) {
        this.ccsPermission = ccsPermission;
    }

    public void setIsLocationSame(int isLocationSame) {
        this.isLocationSame = isLocationSame;
    }

    public void setBureauPermission(int bureauPermission) {
        this.bureauPermission = bureauPermission;
    }

    public void setHasAcceptedInvitation(int hasAcceptedInvitation) {
        this.hasAcceptedInvitation = hasAcceptedInvitation;
    }

    public int getIsLocationSame() {
        return isLocationSame;
    }

    public int getBureauPermission() {
        return bureauPermission;
    }

    public int getHasAcceptedInvitation() {
        return hasAcceptedInvitation;
    }
}
