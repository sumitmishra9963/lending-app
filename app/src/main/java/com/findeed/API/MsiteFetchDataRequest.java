package com.findeed.API;

import com.findeed.KYC.VideoIdKYCREq.MsiteFetchRequest;
import com.findeed.KYC.VideoIdKYCREq.headers;

public class MsiteFetchDataRequest {
    headers headers;
    MsiteFetchRequest request;

    public com.findeed.KYC.VideoIdKYCREq.headers getHeaders() {
        return headers;
    }

    public void setHeaders(com.findeed.KYC.VideoIdKYCREq.headers headers) {
        this.headers = headers;
    }

    public MsiteFetchRequest getRequest() {
        return request;
    }

    public void setRequest(MsiteFetchRequest request) {
        this.request = request;
    }
}
