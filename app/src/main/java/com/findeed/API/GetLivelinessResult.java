package com.findeed.API;

public class GetLivelinessResult {
    LivelinessData livelinessData;
    String statusCode;
    int recordCount;
    String  message;
    boolean status;

    public LivelinessData getLivelinessData() {
        return livelinessData;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public int getRecordCount() {
        return recordCount;
    }

    public String getMessage() {
        return message;
    }

    public boolean isStatus() {
        return status;
    }
}
