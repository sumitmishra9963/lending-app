package com.findeed.API;

import java.util.ArrayList;

public class GetSchoolList {
    ArrayList<SchoolData> data ;
    private float statusCode;
    private float recordCount;
    String message;
    String status;

    public ArrayList<SchoolData> getSchoolData() {
        return data;
    }

    public void setSchoolData(ArrayList<SchoolData> schoolData) {
        this.data = schoolData;
    }

    public float getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(float statusCode) {
        this.statusCode = statusCode;
    }

    public float getRecordCount() {
        return recordCount;
    }

    public void setRecordCount(float recordCount) {
        this.recordCount = recordCount;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
