package com.findeed.API;

public class NumberOfChildren {

    Data data;
    private float statusCode;
    private float recordCount;
    private String message;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public float getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(float statusCode) {
        this.statusCode = statusCode;
    }

    public float getRecordCount() {
        return recordCount;
    }

    public void setRecordCount(float recordCount) {
        this.recordCount = recordCount;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    private boolean status;

}
