package com.findeed.API;

public class ErrorList implements Cloneable{
    String errorName;
    String errorMessage;
    int errorStatus;

    public int getErrorStatus() {
        return errorStatus;
    }

    public String getErrorName() {
        return errorName;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorStatus(int errorStatus) {
        this.errorStatus=errorStatus;
    }

    public void setErrorName(String errorName) {
        this.errorName=errorName;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage=errorMessage;
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
