package com.findeed.API;

import java.util.ArrayList;

public class VoucherResponse {
    ArrayList<VoucherResponseData> data;
    int statusCode;
    int recordCount;
    String  message;
    boolean status;

    public ArrayList<VoucherResponseData> getData() {
        return data;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public int getRecordCount() {
        return recordCount;
    }

    public String getMessage() {
        return message;
    }

    public boolean isStatus() {
        return status;
    }
}
