package com.findeed.API.AadhaarAndPAN.RequestData;

public class SaveDocument {
    int id;
    String  documentType;
    String  docSide;

    public void setId(int id) {
        this.id = id;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDocSide() {
        return docSide;
    }

    public String getFrontImage() {
        return frontImage;
    }

    public void setFrontImage(String frontImage) {
        this.frontImage = frontImage;
    }

    public String getBackImage() {
        return backImage;
    }

    public void setBackImage(String backImage) {
        this.backImage = backImage;
    }

    public void setDocSide(String docSide) {
        this.docSide = docSide;
    }



    String  frontImage;
    String backImage;

}
