package com.findeed.API.AadhaarAndPAN.ResponseData;

public class QualityCheckPan {
    String  imageAspectRatio;
    String  imageExposure;
    String  imageBlur;
    String  imageColour;
    String  imageReflection;

    public String getImageAspectRatio() {
        return imageAspectRatio;
    }

    public void setImageAspectRatio(String imageAspectRatio) {
        this.imageAspectRatio = imageAspectRatio;
    }

    public String getImageExposure() {
        return imageExposure;
    }

    public void setImageExposure(String imageExposure) {
        this.imageExposure = imageExposure;
    }

    public String getImageBlur() {
        return imageBlur;
    }

    public void setImageBlur(String imageBlur) {
        this.imageBlur = imageBlur;
    }

    public String getImageColour() {
        return imageColour;
    }

    public void setImageColour(String imageColour) {
        this.imageColour = imageColour;
    }

    public String getImageReflection() {
        return imageReflection;
    }

    public void setImageReflection(String imageReflection) {
        this.imageReflection = imageReflection;
    }
}




