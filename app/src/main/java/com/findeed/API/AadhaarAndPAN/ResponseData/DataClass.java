package com.findeed.API.AadhaarAndPAN.ResponseData;

public class DataClass {
    String  customerId;
    String  documentType;
    String docSide;
    String documentFrontImage;
    String documentBackImage;
    String  firstName;
    String  lastName;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDocSide() {
        return docSide;
    }

    public void setDocSide(String docSide) {
        this.docSide = docSide;
    }

    public String getDocumentFrontImage() {
        return documentFrontImage;
    }

    public void setDocumentFrontImage(String documentFrontImage) {
        this.documentFrontImage = documentFrontImage;
    }

    public String getDocumentBackImage() {
        return documentBackImage;
    }

    public void setDocumentBackImage(String documentBackImage) {
        this.documentBackImage = documentBackImage;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public QualityCheckPan getQuality() {
        return quality;
    }

    public void setQuality(QualityCheckPan quality) {
        this.quality = quality;
    }

    String  gender;
    String  dateOfBirth;
    String  address;
    String  documentId;
    QualityCheckPan quality;




}
