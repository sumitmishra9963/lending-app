package com.findeed.API;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SmsBroadCastReciever extends BroadcastReceiver {
    Gson gson = new Gson();
    @Override
    public void onReceive(Context context, Intent intent) {
        final Bundle bundle = intent.getExtras();
        try {
            if (bundle != null) {
                final Object[] pdusObj = (Object[]) bundle.get("pdus");
                for (Object o : pdusObj) {

                    SmsMessage currentMessage;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        String format = bundle.getString("format");
                        currentMessage = SmsMessage.createFromPdu((byte[]) o, format);
                        Log.e("Current Message", format + " : " + currentMessage.getDisplayOriginatingAddress());
                    } else {
                        currentMessage = SmsMessage.createFromPdu((byte[]) o);
                    }
                    Pattern regEx =
                            Pattern.compile("[a-zA-Z0-9]{2}-[a-zA-Z0-9]{6}");
                    Matcher m = regEx.matcher(currentMessage.getOriginatingAddress());
                    if (m.find()) {
                        try {
                            String phoneNumber = m.group(0);
                            int cId = intent.getIntExtra("customerId", 0);
                            Long date = currentMessage.getTimestampMillis();
                            String message = currentMessage.getDisplayMessageBody();
                            new SendTransactionSMS(message, phoneNumber, date, cId).execute();
                            Log.e("SmsReceiver Mine", "senderNum: " + phoneNumber + "; message: " + message);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.e("Mismatch", "Mismatch value");
                    }
                }
            }
        } catch (Exception e) {
            Log.e("SmsReceiver", "Exception smsReceiver" + e);
        }
    }






    public class SendTransactionSMS extends AsyncTask<String,String ,String >{
        String msg,phNo;
        Long timeStamp;
        int cId;

        String END_POINT = "http://157.245.102.194:8080/los-build/";

        public SendTransactionSMS(String msg, String phNo, Long timeStamp, int cId) {
            this.msg = msg;
            this.phNo = phNo;
            this.timeStamp = timeStamp;
            this.cId = cId;
        }

        @Override
        protected String doInBackground(String... strings) {


        SendSMSPayload ssp = new SendSMSPayload();
        ssp.setCustomerId(cId);
        ssp.setReceiverNumber("");
        ssp.setSenderNumber(phNo);
        ssp.setSmsMessage(msg);
        ssp.setSmsTimestamp(String.valueOf(timeStamp));



            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            OkHttpClient client = new OkHttpClient();
            String jsonData=gson.toJson(ssp);
            RequestBody body = RequestBody.create(JSON, jsonData);
            Request request = new Request.Builder()
                    .url(END_POINT+"customer/saveCustomerSmss")
                    .post(body)
                    .build();
            try {
                Response response = client.newCall(request).execute();

                return   response.body().string();


            } catch (IOException | RuntimeException e) {
                e.printStackTrace();
                return null;

            }

        }
    }
}
