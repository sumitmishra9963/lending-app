package com.findeed.API;

public  class Voucher {
    int voucherResponseId;
    int customerVoucherId;
    String  voucherName;
    String phoneNumber;
    String  customers;
    String  customerAcquantance;
    String  customerRelationship;
    String  relationshipLength;
    String  maritalStatus;
    String  numberOfChildren;
    String  customerEmployment;
    String  customerEmployer;
    String  customerBusiness;
    String  customerVoucherArea;
    String  customerStayArea;
    String  customerVoucherWork;
    int isVoucherApproved;

    public int getIsVoucherApproved() {
        return isVoucherApproved;
    }

    public int getVoucherResponseId() {
        return voucherResponseId;
    }

    public void setVoucherResponseId(int voucherResponseId) {
        this.voucherResponseId = voucherResponseId;
    }

    public int getCustomerVoucherId() {
        return customerVoucherId;
    }

    public void setCustomerVoucherId(int customerVoucherId) {
        this.customerVoucherId = customerVoucherId;
    }

    public String getVoucherName() {
        return voucherName;
    }

    public void setVoucherName(String voucherName) {
        this.voucherName = voucherName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCustomers() {
        return customers;
    }

    public void setCustomers(String customers) {
        this.customers = customers;
    }

    public String getCustomerAcquantance() {
        return customerAcquantance;
    }

    public void setCustomerAcquantance(String customerAcquantance) {
        this.customerAcquantance = customerAcquantance;
    }

    public String getCustomerRelationship() {
        return customerRelationship;
    }

    public void setCustomerRelationship(String customerRelationship) {
        this.customerRelationship = customerRelationship;
    }

    public String getRelationshipLength() {
        return relationshipLength;
    }

    public void setRelationshipLength(String relationshipLength) {
        this.relationshipLength = relationshipLength;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getNumberOfChildren() {
        return numberOfChildren;
    }

    public void setNumberOfChildren(String numberOfChildren) {
        this.numberOfChildren = numberOfChildren;
    }

    public String getCustomerEmployment() {
        return customerEmployment;
    }

    public void setCustomerEmployment(String customerEmployment) {
        this.customerEmployment = customerEmployment;
    }

    public String getCustomerEmployer() {
        return customerEmployer;
    }

    public void setCustomerEmployer(String customerEmployer) {
        this.customerEmployer = customerEmployer;
    }

    public String getCustomerBusiness() {
        return customerBusiness;
    }

    public void setCustomerBusiness(String customerBusiness) {
        this.customerBusiness = customerBusiness;
    }

    public String getCustomerVoucherArea() {
        return customerVoucherArea;
    }

    public void setCustomerVoucherArea(String customerVoucherArea) {
        this.customerVoucherArea = customerVoucherArea;
    }

    public String getCustomerStayArea() {
        return customerStayArea;
    }

    public void setCustomerStayArea(String customerStayArea) {
        this.customerStayArea = customerStayArea;
    }

    public String getCustomerVoucherWork() {
        return customerVoucherWork;
    }

    public void setCustomerVoucherWork(String customerVoucherWork) {
        this.customerVoucherWork = customerVoucherWork;
    }
}
