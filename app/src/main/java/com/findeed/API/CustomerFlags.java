package com.findeed.API;

public class CustomerFlags {

    int customerApplicationId;
    int customerId;
    String  applicationNumber;
    int isPanAvailable;
    int isPanEdited;
    int panOcrFailed;
    int voterIdConfirmed;
    int voterIdEdited;
    int voterOcrFailed;
    int shareCreditBureau;
    int aadhaarCorrectAddress;
    int saveLoanSummary;
    int agreeLoanTenure;

    public void setCustomerApplicationId(int customerApplicationId) {
        this.customerApplicationId = customerApplicationId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public void setApplicationNumber(String applicationNumber) {
        this.applicationNumber = applicationNumber;
    }

    public void setIsPanAvailable(int isPanAvailable) {
        this.isPanAvailable = isPanAvailable;
    }

    public void setIsPanEdited(int isPanEdited) {
        this.isPanEdited = isPanEdited;
    }

    public void setPanOcrFailed(int panOcrFailed) {
        this.panOcrFailed = panOcrFailed;
    }

    public void setVoterIdConfirmed(int voterIdConfirmed) {
        this.voterIdConfirmed = voterIdConfirmed;
    }

    public void setVoterIdEdited(int voterIdEdited) {
        this.voterIdEdited = voterIdEdited;
    }

    public void setVoterOcrFailed(int voterOcrFailed) {
        this.voterOcrFailed = voterOcrFailed;
    }

    public void setShareCreditBureau(int shareCreditBureau) {
        this.shareCreditBureau = shareCreditBureau;
    }

    public void setAadhaarCorrectAddress(int aadhaarCorrectAddress) {
        this.aadhaarCorrectAddress = aadhaarCorrectAddress;
    }

    public void setSaveLoanSummary(int saveLoanSummary) {
        this.saveLoanSummary = saveLoanSummary;
    }

    public void setAgreeLoanTenure(int agreeLoanTenure) {
        this.agreeLoanTenure = agreeLoanTenure;
    }
}
