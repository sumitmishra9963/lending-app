package com.findeed.API;

public class AddNewEmployerRequest {
    String  employerName;
    String  contactName;
    String  addressLine1;
    String addressLine2;
    String area;
    String  city;
    String zipcode;

    public void setEmployerName(String employerName) {
        this.employerName = employerName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }
}
