package com.findeed.API;

 public class LoanFlags {

    int customerApplicationId;
    int customerId;
    String  applicationNumber;
    int isPanAvailable;
    int isPanEdited;
    int panOcrFailed;
    int voterIdConfirmed;
    int voterIdEdited;
    int voterOcrFailed;
    int shareCreditBureau;
    int aadhaarCorrectAddress;
    int saveLoanSummary;
    int agreeLoanTenure;

    public int getCustomerApplicationId() {
        return customerApplicationId;
    }

    public void setCustomerApplicationId(int customerApplicationId) {
        this.customerApplicationId = customerApplicationId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getApplicationNumber() {
        return applicationNumber;
    }

    public void setApplicationNumber(String applicationNumber) {
        this.applicationNumber = applicationNumber;
    }

    public int getIsPanAvailable() {
        return isPanAvailable;
    }

    public void setIsPanAvailable(int isPanAvailable) {
        this.isPanAvailable = isPanAvailable;
    }

    public int getIsPanEdited() {
        return isPanEdited;
    }

    public void setIsPanEdited(int isPanEdited) {
        this.isPanEdited = isPanEdited;
    }

    public int getPanOcrFailed() {
        return panOcrFailed;
    }

    public void setPanOcrFailed(int panOcrFailed) {
        this.panOcrFailed = panOcrFailed;
    }

    public int getVoterIdConfirmed() {
        return voterIdConfirmed;
    }

    public void setVoterIdConfirmed(int voterIdConfirmed) {
        this.voterIdConfirmed = voterIdConfirmed;
    }

    public int getVoterIdEdited() {
        return voterIdEdited;
    }

    public void setVoterIdEdited(int voterIdEdited) {
        this.voterIdEdited = voterIdEdited;
    }

    public int getVoterOcrFailed() {
        return voterOcrFailed;
    }

    public void setVoterOcrFailed(int voterOcrFailed) {
        this.voterOcrFailed = voterOcrFailed;
    }

    public int getShareCreditBureau() {
        return shareCreditBureau;
    }

    public void setShareCreditBureau(int shareCreditBureau) {
        this.shareCreditBureau = shareCreditBureau;
    }

    public int getAadhaarCorrectAddress() {
        return aadhaarCorrectAddress;
    }

    public void setAadhaarCorrectAddress(int aadhaarCorrectAddress) {
        this.aadhaarCorrectAddress = aadhaarCorrectAddress;
    }

    public int getSaveLoanSummary() {
        return saveLoanSummary;
    }

    public void setSaveLoanSummary(int saveLoanSummary) {
        this.saveLoanSummary = saveLoanSummary;
    }

    public int getAgreeLoanTenure() {
        return agreeLoanTenure;
    }

    public void setAgreeLoanTenure(int agreeLoanTenure) {
        this.agreeLoanTenure = agreeLoanTenure;
    }
}
