package com.findeed.API;

public class LoanRequestData {

    int customerApplicationId;
    String  applicationNumber;
    String  loanType;
    String loanStatus;
    String  maxLoanEligible;
    double requestAmount;
    double approvedAmount;
    String  emiRate;
    int   tenure;
    int paymentFrequency;

    public int getCustomerApplicationId() {
        return customerApplicationId;
    }

    public void setCustomerApplicationId(int customerApplicationId) {
        this.customerApplicationId = customerApplicationId;
    }

    public String getApplicationNumber() {
        return applicationNumber;
    }

    public void setApplicationNumber(String applicationNumber) {
        this.applicationNumber = applicationNumber;
    }

    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    public String getLoanStatus() {
        return loanStatus;
    }

    public void setLoanStatus(String loanStatus) {
        this.loanStatus = loanStatus;
    }

    public String getMaxLoanEligible() {
        return maxLoanEligible;
    }

    public void setMaxLoanEligible(String maxLoanEligible) {
        this.maxLoanEligible = maxLoanEligible;
    }

    public double getRequestAmount() {
        return requestAmount;
    }

    public void setRequestAmount(double requestAmount) {
        this.requestAmount = requestAmount;
    }

    public double getApprovedAmount() {
        return approvedAmount;
    }

    public void setApprovedAmount(double approvedAmount) {
        this.approvedAmount = approvedAmount;
    }

    public String getEmiRate() {
        return emiRate;
    }

    public void setEmiRate(String emiRate) {
        this.emiRate = emiRate;
    }

    public int getTenure() {
        return tenure;
    }

    public void setTenure(int tenure) {
        this.tenure = tenure;
    }

    public int getPaymentFrequency() {
        return paymentFrequency;
    }

    public void setPaymentFrequency(int paymentFrequency) {
        this.paymentFrequency = paymentFrequency;
    }
}
