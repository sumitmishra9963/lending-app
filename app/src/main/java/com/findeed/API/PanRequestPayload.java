package com.findeed.API;

public class PanRequestPayload {
        String consent;
        String  consent_message;
        PanDetails pan_details;

    public void setConsent(String consent) {
        this.consent = consent;
    }

    public void setConsent_message(String consent_message) {
        this.consent_message = consent_message;
    }

    public void setPan_details(PanDetails pan_details) {
        this.pan_details = pan_details;
    }
}
