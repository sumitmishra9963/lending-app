package com.findeed.API;

import java.util.ArrayList;

public class Loan implements Cloneable
{

    String customerApplicationId;
    String applicationNumber;
    String  loanStatus;
    String  requestAmount;
    String  approvedAmount;

    public void setLoanType(LoanType loanType) {
        this.loanType = loanType;
    }

    public void setLoanFlags(LoanFlags loanFlags) {
        this.loanFlags = loanFlags;
    }

    public void setErrorList(ArrayList<ErrorList> errorList) {
        this.errorList = errorList;
    }

    String  emiRate;
    String  tenure;
    LoanType loanType;
    String paymentFrequency;
    float maxLoanEligible;

    public float getMaxLoanEligible() {
        return maxLoanEligible;
    }

    public String getPaymentFrequency() {
        return paymentFrequency;
    }

    LoanFlags loanFlags;
    ArrayList<ErrorList> errorList;

    public ArrayList<ErrorList> getErrorList() {
        return errorList;
    }

    public LoanFlags getLoanFlags() {
        return loanFlags;
    }

    public LoanType getLoanType() {
        return loanType;
    }

    public String getCustomerApplicationId() {
        return customerApplicationId;
    }

    public void setCustomerApplicationId(String customerApplicationId) {
        this.customerApplicationId = customerApplicationId;
    }

    public String getApplicationNumber() {
        return applicationNumber;
    }

    public void setApplicationNumber(String applicationNumber) {
        this.applicationNumber = applicationNumber;
    }



    public String getLoanStatus() {
        return loanStatus;
    }

    public void setLoanStatus(String loanStatus) {
        this.loanStatus = loanStatus;
    }

    public String getRequestAmount() {
        return requestAmount;
    }

    public void setRequestAmount(String requestAmount) {
        this.requestAmount = requestAmount;
    }

    public String getApprovedAmount() {
        return approvedAmount;
    }

    public void setApprovedAmount(String approvedAmount) {
        this.approvedAmount = approvedAmount;
    }

    public String getEmiRate() {
        return emiRate;
    }

    public void setEmiRate(String emiRate) {
        this.emiRate = emiRate;
    }

    public String getTenure() {
        return tenure;
    }

    public void setTenure(String tenure) {
        this.tenure = tenure;
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

}
