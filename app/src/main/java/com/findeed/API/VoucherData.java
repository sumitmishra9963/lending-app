package com.findeed.API;

public class VoucherData {
    int customerVoucherId;
    String  voucherName;
    String phoneNumber;

    public int getCustomerVoucherId() {
        return customerVoucherId;
    }

    public String getVoucherName() {
        return voucherName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
}
