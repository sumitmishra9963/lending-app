package com.findeed.API;

import android.view.View;

public class ClassData2 implements Cloneable {

    int  classId;
    String  className;
    float fees;

    View view;

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }

    public int getClassId() {
        return classId;
    }

    public void setClassId(int classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Float getFees() {
        return fees;
    }

    public void setFees(Float fees) {
        this.fees = fees;
    }
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
