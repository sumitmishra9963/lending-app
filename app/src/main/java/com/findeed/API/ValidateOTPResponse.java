package com.findeed.API;

public class ValidateOTPResponse {
    Data data;
    private float statusCode;
    private float recordCount;
    private String message;
    private String status;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public float getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(float statusCode) {
        this.statusCode = statusCode;
    }

    public float getRecordCount() {
        return recordCount;
    }

    public void setRecordCount(float recordCount) {
        this.recordCount = recordCount;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String  isStatus() {
        return status;
    }

    public void setStatus(String  status) {
        this.status = status;
    }



}
