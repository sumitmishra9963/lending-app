package com.findeed.API;

public class SaveEmailRequest {
    int customerId;
    String  emailAddress;

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
}
