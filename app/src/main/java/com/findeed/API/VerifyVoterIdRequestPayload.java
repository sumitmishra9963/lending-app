package com.findeed.API;

import com.findeed.API.DocInfoExtract.VoterIdPayload;
import com.findeed.KYC.VideoIdKYCREq.headers;

public class VerifyVoterIdRequestPayload {
  headers headers;
  VoterIdPayload request;

    public void setHeaders(com.findeed.KYC.VideoIdKYCREq.headers headers) {
        this.headers = headers;
    }

    public void setRequest(VoterIdPayload request) {
        this.request = request;
    }
}
