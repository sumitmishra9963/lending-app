package com.findeed.API.DocInfoExtract;

import com.findeed.API.RequestVoterIdDetail;

public class VoterIdPayload {
    String consent;
    String  consent_message;
    RequestVoterIdDetail voter_id_details;

    public void setConsent(String consent) {
        this.consent = consent;
    }

    public void setConsent_message(String consent_message) {
        this.consent_message = consent_message;
    }

    public void setVoter_id_details(RequestVoterIdDetail voter_id_details) {
        this.voter_id_details = voter_id_details;
    }
}
