package com.findeed.API.DocInfoExtract;

public class DocInfoRequest {

    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    public String getRequest_id() {
        return request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getDocument_type() {
        return document_type;
    }

    public void setDocument_type(String document_type) {
        this.document_type = document_type;
    }

    public String getDocument_side() {
        return document_side;
    }

    public void setDocument_side(String document_side) {
        this.document_side = document_side;
    }

    public String getExtraction_type() {
        return extraction_type;
    }

    public void setExtraction_type(String extraction_type) {
        this.extraction_type = extraction_type;
    }

    public String getDocument_front_image() {
        return document_front_image;
    }

    public void setDocument_front_image(String document_front_image) {
        this.document_front_image = document_front_image;
    }

    public String getDocument_back_image() {
        return document_back_image;
    }

    public void setDocument_back_image(String document_back_image) {
        this.document_back_image = document_back_image;
    }

    private String api_key;
    private String request_id;
    String  purpose;
    String hash;
    String document_type;
    String  document_side;
    String extraction_type;
    String document_front_image;
     String document_back_image;


}
