package com.findeed.API.DocInfoExtract;

public class FrontImage {

    String image_aspect_ratio;
    String  image_exposure;
    String  image_blur;
    String  image_colour;

    public String getImage_aspect_ratio() {
        return image_aspect_ratio;
    }

    public String getImage_exposure() {
        return image_exposure;
    }

    public String getImage_blur() {
        return image_blur;
    }

    public String getImage_colour() {
        return image_colour;
    }

    public String getImage_reflection() {
        return image_reflection;
    }

    String  image_reflection;

}
