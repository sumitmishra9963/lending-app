package com.findeed.API.DocInfoExtract;

public class DocInfoResponseData {
    String encrypted;
    String document_data;
    String hash;

    public String getEncrypted() {
        return encrypted;
    }

    public String getDocument_data() {
        return document_data;
    }

    public String getHash() {
        return hash;
    }
}

