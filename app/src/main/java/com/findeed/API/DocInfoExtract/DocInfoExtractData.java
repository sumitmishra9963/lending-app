package com.findeed.API.DocInfoExtract;

public class DocInfoExtractData {
    com.findeed.KYC.VideoIdKYCREq.headers headers;
    DocInfoRequest request;

    public void setHeaders(com.findeed.KYC.VideoIdKYCREq.headers headers) {
        this.headers = headers;
    }

    public void setRequest(DocInfoRequest request) {
        this.request = request;
    }
}
