package com.findeed.API.DocInfoExtract;


import com.khosla.faceauthapi.api.ApiResponse;

public class DocInfoExtractResponse {
    ApiResponse.ResponseStatus response_status;
    DocInfoResponseData response_data;

    public DocInfoResponseData getResponse_data() {
        return response_data;
    }

    public ApiResponse.ResponseStatus getResponse_status() {
        return response_status;
    }



}
