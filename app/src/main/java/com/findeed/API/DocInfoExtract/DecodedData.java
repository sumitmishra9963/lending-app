package com.findeed.API.DocInfoExtract;

import com.findeed.API.AadhaarAndPAN.ResponseData.QualityCheckAadhaar;

public class DecodedData {
    DocPhoto photo;
    DocKycInfo original_kyc_info;
    DocImageGot doc_image;
    QualityCheckAadhaar quality_check;

    public DocPhoto getPhoto() {
        return photo;
    }

    public DocKycInfo getOriginal_kyc_info() {
        return original_kyc_info;
    }

    public DocImageGot getDoc_image() {
        return doc_image;
    }

    public QualityCheckAadhaar getQuality_check() {
        return quality_check;
    }

}
