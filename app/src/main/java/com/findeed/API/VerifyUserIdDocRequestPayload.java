package com.findeed.API;

import com.findeed.KYC.VideoIdKYCREq.headers;

public class VerifyUserIdDocRequestPayload {
    headers headers;
    PanRequestPayload request;

    public void setHeaders(com.findeed.KYC.VideoIdKYCREq.headers headers) {
        this.headers = headers;
    }

    public void setRequest(PanRequestPayload request) {
        this.request = request;
    }
}
