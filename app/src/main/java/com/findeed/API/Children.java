package com.findeed.API;

public class Children implements Cloneable {
    String customerDependentId;
    String firstName;
    String lastName;
    SchoolData school;
    ClassInformation inClass;

    public SchoolData getSchool() {
        return school;
    }

    public ClassInformation getInClass() {
        return inClass;
    }

    public String getCustomerDependentId() {
        return customerDependentId;
    }

    public void setCustomerDependentId(String customerDependentId) {
        this.customerDependentId = customerDependentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setSchool(SchoolData school) {
        this.school = school;
    }

    public void setInClass(ClassInformation inClass) {
        this.inClass = inClass;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }


}
