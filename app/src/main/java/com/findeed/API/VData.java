package com.findeed.API;

import java.util.ArrayList;

public class VData implements Cloneable {
    int voucherResponseId;
    int customerVoucherId;
    int customerId;
    String customerName;
    String  voucherName;
    String  phoneNumber;
    String  customers;
    String  customerAcquantance;
    String  customerRelationship;
    String  relationshipLength;
    String maritalStatus;
    String  numberOfChildren;
    String  customerEmployment;
    String  customerEmployer;
    String  customerBusiness;
    String  customerVoucherArea;
    String  customerStayArea;
    String  customerVoucherWork;
    int isVoucherApproved;
    VoucherFlags voucherFlags;
    String voucherAddress;

    public String getVoucherAddress() {
        return voucherAddress;
    }

    public VoucherFlags getVoucherFlags() {
        return voucherFlags;
    }

    ArrayList<DocumentData> documents;

    public ArrayList<DocumentData> getDocuments() {
        return documents;
    }

    public int getCustomerId() {
        return customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public int getIsVoucherApproved() {
        return isVoucherApproved;
    }

    public int getVoucherResponseId() {
        return voucherResponseId;
    }

    public int getCustomerVoucherId() {
        return customerVoucherId;
    }

    public String getVoucherName() {
        return voucherName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getCustomers() {
        return customers;
    }

    public String getCustomerAcquantance() {
        return customerAcquantance;
    }

    public String getCustomerRelationship() {
        return customerRelationship;
    }

    public String getRelationshipLength() {
        return relationshipLength;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public String getNumberOfChildren() {
        return numberOfChildren;
    }

    public String getCustomerEmployment() {
        return customerEmployment;
    }

    public String getCustomerEmployer() {
        return customerEmployer;
    }

    public String getCustomerBusiness() {
        return customerBusiness;
    }

    public String getCustomerVoucherArea() {
        return customerVoucherArea;
    }

    public String getCustomerStayArea() {
        return customerStayArea;
    }

    public String getCustomerVoucherWork() {
        return customerVoucherWork;
    }

    public void setVoucherResponseId(int voucherResponseId) {
        this.voucherResponseId = voucherResponseId;
    }

    public void setCustomerVoucherId(int customerVoucherId) {
        this.customerVoucherId = customerVoucherId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public void setVoucherName(String voucherName) {
        this.voucherName = voucherName;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setCustomers(String customers) {
        this.customers = customers;
    }

    public void setCustomerAcquantance(String customerAcquantance) {
        this.customerAcquantance = customerAcquantance;
    }

    public void setCustomerRelationship(String customerRelationship) {
        this.customerRelationship = customerRelationship;
    }

    public void setRelationshipLength(String relationshipLength) {
        this.relationshipLength = relationshipLength;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public void setNumberOfChildren(String numberOfChildren) {
        this.numberOfChildren = numberOfChildren;
    }

    public void setCustomerEmployment(String customerEmployment) {
        this.customerEmployment = customerEmployment;
    }

    public void setCustomerEmployer(String customerEmployer) {
        this.customerEmployer = customerEmployer;
    }

    public void setCustomerBusiness(String customerBusiness) {
        this.customerBusiness = customerBusiness;
    }

    public void setCustomerVoucherArea(String customerVoucherArea) {
        this.customerVoucherArea = customerVoucherArea;
    }

    public void setCustomerStayArea(String customerStayArea) {
        this.customerStayArea = customerStayArea;
    }

    public void setCustomerVoucherWork(String customerVoucherWork) {
        this.customerVoucherWork = customerVoucherWork;
    }

    public void setIsVoucherApproved(int isVoucherApproved) {
        this.isVoucherApproved = isVoucherApproved;
    }

    public void setVoucherFlags(VoucherFlags voucherFlags) {
        this.voucherFlags = voucherFlags;
    }

    public void setVoucherAddress(String voucherAddress) {
        this.voucherAddress = voucherAddress;
    }

    public void setDocuments(ArrayList<DocumentData> documents) {
        this.documents = documents;
    }
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
