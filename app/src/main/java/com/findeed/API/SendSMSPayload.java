package com.findeed.API;

public class SendSMSPayload {
    int customerId;
    String  senderNumber;
    String  receiverNumber;
    String  smsMessage;
    String  smsTimestamp;

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public void setSenderNumber(String senderNumber) {
        this.senderNumber = senderNumber;
    }

    public void setReceiverNumber(String receiverNumber) {
        this.receiverNumber = receiverNumber;
    }

    public void setSmsMessage(String smsMessage) {
        this.smsMessage = smsMessage;
    }

    public void setSmsTimestamp(String smsTimestamp) {
        this.smsTimestamp = smsTimestamp;
    }
}
