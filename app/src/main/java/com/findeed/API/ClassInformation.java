package com.findeed.API;

public class ClassInformation implements Cloneable {

     String classId;
     String className;
     String fees;

    public String getClassId() {
        return classId;
    }

    public String getClassName() {
        return className;
    }

    public String getFees() {
        return fees;
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
