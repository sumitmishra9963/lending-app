package com.findeed.API;

import java.util.ArrayList;

public class ClassInSchool {

     ArrayList < ClassData >classList= new ArrayList <>();
     int statusCode;
     int recordCount;
     String message;
     boolean status;


    public String getMessage() {
        return message;
    }

    public ArrayList<ClassData> getClassList() {
        return classList;
    }

    public void setClassList(ArrayList<ClassData> classList) {
        this.classList = classList;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public int getRecordCount() {
        return recordCount;
    }

    public void setRecordCount(int recordCount) {
        this.recordCount = recordCount;
    }

    public boolean isStatus() {
        return status;
    }

    public boolean getStatus() {
        return status;
    }



    public void setMessage(String message) {
        this.message = message;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}

