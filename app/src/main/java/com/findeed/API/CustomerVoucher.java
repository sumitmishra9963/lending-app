package com.findeed.API;

import java.util.ArrayList;

public class CustomerVoucher {
    ArrayList<VoucherData> data = new ArrayList<>();
    String statusCode;
    int recordCount;
    String  message;
    boolean status;

    public ArrayList<VoucherData> getData() {
        return data;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public int getRecordCount() {
        return recordCount;
    }

    public String getMessage() {
        return message;
    }

    public boolean isStatus() {
        return status;
    }
}
