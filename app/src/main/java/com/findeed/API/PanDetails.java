package com.findeed.API;

public class PanDetails {

    String pan_number;
    String name;
    String  dob;
    String pan_type;
    String  document;

    public void setPan_number(String pan_number) {
        this.pan_number = pan_number;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public void setPan_type(String pan_type) {
        this.pan_type = pan_type;
    }

    public void setDocument(String document) {
        this.document = document;
    }
}
