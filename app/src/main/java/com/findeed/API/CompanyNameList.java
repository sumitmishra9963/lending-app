package com.findeed.API;

public class CompanyNameList {

    int employerId;
    String  employerName;
    String  contactName;
    String  addressLine1;
    String  addressLine2;
    String  area;
    String  city;
    String  zipcode;
    String  phoneNumber;
    String  email;
    long latitude;
    long longitude;
    int isEmpanelled;
    int isVerified;
    int requestedBy;

    public int getEmployerId() {
        return employerId;
    }

    public String getEmployerName() {
        return employerName;
    }

    public String getContactName() {
        return contactName;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public String getArea() {
        return area;
    }

    public String getCity() {
        return city;
    }

    public String getZipcode() {
        return zipcode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public long getLatitude() {
        return latitude;
    }

    public long getLongitude() {
        return longitude;
    }

    public int getIsEmpanelled() {
        return isEmpanelled;
    }

    public int getIsVerified() {
        return isVerified;
    }

    public int getRequestedBy() {
        return requestedBy;
    }
}
