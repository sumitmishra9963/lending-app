package com.findeed.API;

import java.util.ArrayList;

public class customerAllLoanDetailsData {

   public ArrayList< Object > data = new ArrayList<>();
    public float statusCode;
    public float recordCount;
    public String message;
    public boolean status;


    // Getter Methods

    public float getStatusCode() {
        return statusCode;
    }

    public float getRecordCount() {
        return recordCount;
    }

    public String getMessage() {
        return message;
    }

    public boolean getStatus() {
        return status;
    }

    // Setter Methods

    public void setStatusCode(float statusCode) {
        this.statusCode = statusCode;
    }

    public void setRecordCount(float recordCount) {
        this.recordCount = recordCount;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

}
