package com.findeed.API;

public class SchoolData implements Cloneable{

    int schoolId;
    String schoolName;
    String contactName;
    String addressLine1;
    String addressLine2;
    String area;
    String city;
    float zipcode;
    String  phoneNumber;
    String email;
    float latitude;
    float longitude;
    float isEmpanelled;

    public void setIsVerified(int isVerified) {
        this.isVerified = isVerified;
    }

    public void setRequestedBy(String requestedBy) {
        this.requestedBy = requestedBy;
    }

    public void setClasses(String classes) {
        this.classes = classes;
    }

    public int getIsVerified() {
        return isVerified;
    }

    public String getRequestedBy() {
        return requestedBy;
    }

    public String getClasses() {
        return classes;
    }

    int isVerified;
    String requestedBy;
    String classes;

    public int getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(int schoolId) {
        this.schoolId = schoolId;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public float getZipcode() {
        return zipcode;
    }

    public void setZipcode(float zipcode) {
        this.zipcode = zipcode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public float getIsEmpanelled() {
        return isEmpanelled;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public float isEmpanelled() {
        return isEmpanelled;
    }

    public void setEmpanelled(float empanelled) {
        isEmpanelled = empanelled;
    }


    public void setIsEmpanelled(float isEmpanelled) {
        this.isEmpanelled = isEmpanelled;
    }
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
