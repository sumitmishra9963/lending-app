package com.findeed.API;

public class SaveChildResponse {
    ChildData data;
    int statusCode;
    int recordCount;

    public ChildData getData() {
        return data;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public int getRecordCount() {
        return recordCount;
    }

    public boolean isStatus() {
        return status;
    }

    boolean status;

}
