package com.findeed.API;

public class ChildSchoolResponseData {
   public int customerDependentId;
   public String firstName = null;
    public String lastName = null;

    public int getCustomerDependentId() {
        return customerDependentId;
    }

    public void setCustomerDependentId(int customerDependentId) {
        this.customerDependentId = customerDependentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getInClass() {
        return inClass;
    }

    public void setInClass(String inClass) {
        this.inClass = inClass;
    }

    String inClass = null;
}
