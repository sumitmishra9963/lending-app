package com.findeed.API;

public class ChildData {
    int customerDependentId;
    String  firstName;
    String  lastName;
    String  school;
    String  inClass;

    public int getCustomerDependentId() {
        return customerDependentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getSchool() {
        return school;
    }

    public String getInClass() {
        return inClass;
    }
}
