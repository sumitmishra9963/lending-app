package com.findeed.API;

public class UnEmpanelledSchool {

    int schoolId;
    String  schoolName;
    String  contactName;
    String  addressLine1;
    String  addressLine2;
    String  area;
    String  city;
    String  zipcode;
    String  phoneNumber;
    String  email;
    Float  latitude;
    Float longitude;
    int isEmpanelled;
    int isVerified;
    int requestedBy;
    Object classes;

    public int getSchoolId() {
        return schoolId;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public String getContactName() {
        return contactName;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public String getArea() {
        return area;
    }

    public String getCity() {
        return city;
    }

    public String getZipcode() {
        return zipcode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public Float getLatitude() {
        return latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public int getIsEmpanelled() {
        return isEmpanelled;
    }

    public int getIsVerified() {
        return isVerified;
    }

    public int getRequestedBy() {
        return requestedBy;
    }

    public Object getClasses() {
        return classes;
    }
}
