package com.findeed.API;

public class DocumentData {
    int id;
    String  documentType;
    String  docSide;
    String frontImage;
    String backImage;
    String frontImageURL;
    String backImageURL;
    String firstName;

    public void setFrontImageURL(String frontImageURL) {
        this.frontImageURL = frontImageURL;
    }

    public void setBackImageURL(String backImageURL) {
        this.backImageURL = backImageURL;
    }

    public void setUploadCount(int uploadCount) {
        this.uploadCount = uploadCount;
    }

    public String getFrontImageURL() {
        return frontImageURL;
    }

    public String getBackImageURL() {
        return backImageURL;
    }

    String  lastName;
    String gender;
    String dateOfBirth;
    String address;
    String documentId;
    String quality;
    boolean verified;
    int uploadCount;

    public int getUploadCount() {
        return uploadCount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getDocumentType() {
        return documentType;
    }
    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }
    public String getDocSide() {
        return docSide;
    }
    public void setDocSide(String docSide) {
        this.docSide = docSide;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }
    public String getDateOfBirth() {
        return dateOfBirth;
    }
    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getDocumentId() {
        return documentId;
    }
    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }
    public String getQuality() {
        return quality;
    }
    public void setQuality(String quality) {
        this.quality = quality;
    }
    public boolean isVerified() {
        return verified;
    }

    public String getFrontImage() {
        return frontImage;
    }

    public void setFrontImage(String frontImage) {
        this.frontImage = frontImage;
    }

    public String getBackImage() {
        return backImage;
    }

    public void setBackImage(String backImage) {
        this.backImage = backImage;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }
}
