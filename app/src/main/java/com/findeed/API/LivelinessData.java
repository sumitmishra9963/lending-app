package com.findeed.API;

public class LivelinessData {
    int customerId;
    String image;
    float matchValue;

    public int getCustomerId() {
        return customerId;
    }

    public String getImage() {
        return image;
    }

    public float getMatchValue() {
        return matchValue;
    }
}

