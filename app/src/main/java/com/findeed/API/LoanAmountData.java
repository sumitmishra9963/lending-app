package com.findeed.API;

public class LoanAmountData {
    String customerApplicationId;
    String applicationNumber;
    String loanType;
    String loanStatus;
    String requestAmount;
    String approvedAmount;
    String emiRate;
    String tenure;

    public String getCustomerApplicationId() {
        return customerApplicationId;
    }

    public void setCustomerApplicationId(String customerApplicationId) {
        this.customerApplicationId = customerApplicationId;
    }

    public String getApplicationNumber() {
        return applicationNumber;
    }

    public void setApplicationNumber(String applicationNumber) {
        this.applicationNumber = applicationNumber;
    }

    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    public String getLoanStatus() {
        return loanStatus;
    }

    public void setLoanStatus(String loanStatus) {
        this.loanStatus = loanStatus;
    }

    public String getRequestAmount() {
        return requestAmount;
    }

    public void setRequestAmount(String requestAmount) {
        this.requestAmount = requestAmount;
    }

    public String getApprovedAmount() {
        return approvedAmount;
    }

    public void setApprovedAmount(String approvedAmount) {
        this.approvedAmount = approvedAmount;
    }

    public String getEmiRate() {
        return emiRate;
    }

    public void setEmiRate(String emiRate) {
        this.emiRate = emiRate;
    }

    public String getTenure() {
        return tenure;
    }

    public void setTenure(String tenure) {
        this.tenure = tenure;
    }




}
