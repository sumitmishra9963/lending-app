package com.findeed;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.findeed.Bot.ChatActivity;
import com.findeed.Bot.ChatPresenter;
import com.findeed.Bot.EditLoanInfo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.findeed.ApplicationData.validateOTPResponse;
import static com.findeed.Bot.ChatActivity.CUSTOMER_DETAILS;
import static com.findeed.Bot.ChatActivity.CUSTOMER_ID;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoanAgreementFragment extends Fragment {

    public static boolean isLoanEdited;

    public LoanAgreementFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_loan_agreement, container, false);
        Button agreeAndContinue=view.findViewById(R.id.save_and_continue_loan_agreement);
        TextView editLoanAgreement = view.findViewById(R.id.tv_editloanAgreement);
        TextView editLoan=view.findViewById(R.id.tv_editloanAgreement);

        TextView loanAggrementText=view.findViewById(R.id.loan_aggrement_text);
        TextView nameAddress= view.findViewById(R.id.tv_borrower_name);
        TextView typeOfLoan= view.findViewById(R.id.tv_type_of_loan);
        TextView purpose= view.findViewById(R.id.tv_loan_purpose);
        TextView loanAMountAgreement= view.findViewById(R.id.tv_loan_amount);
        TextView lTenure= view.findViewById(R.id.tv_loan_tenure);
        TextView feesa= view.findViewById(R.id.tv_fees);


        nameAddress.setText(validateOTPResponse.getData().getFirstName()+","+validateOTPResponse.getData().getAddress().getAddressLine1());
        typeOfLoan.setText(validateOTPResponse.getData().getLoans().get(0).getLoanType().getLoanTypeName());
        purpose.setText(validateOTPResponse.getData().getLoans().get(0).getLoanType().getLoanTypeName());
        loanAMountAgreement.setText(validateOTPResponse.getData().getLoans().get(0).getRequestAmount());
        lTenure.setText(validateOTPResponse.getData().getLoans().get(0).getTenure()+"Months");
        feesa.setText(LoanSummaryFragment.fee);


        String aggrement=getString();
        loanAggrementText.setText(aggrement);

        editLoan.setOnClickListener(v -> {
            ChatActivity.TOTAL_INTEREST=0.0;
            TabLayoutActivity.cIntenet.finish();
            Intent editLoanInfo = new Intent(TabLayoutActivity.ctx, EditLoanInfo.class);
            startActivity(editLoanInfo);

        });



        agreeAndContinue.setOnClickListener(v -> {
            ChatActivity.presenter.botMessage("Thank you!",null, ChatPresenter.chatObjects.size()+1);
            ChatActivity.presenter.botMessage("Loan Amount: Rs "+Math.round(Float.valueOf(validateOTPResponse.getData().getLoans().get(0).getRequestAmount()))+"\n"+
                    "Loan Tenure: "+validateOTPResponse.getData().getLoans().get(0).getTenure()+"Months"+"\n"+"Payment Frequency: Monthly"+"\n"
                    +"Interest Rate: 2% Per Month"+"\n"+"Payment Due: 10th of every month"+"\n"+"Late fee: Rs "+LoanSummaryFragment.lateFee+"\n"+
                    "Processing Fee: Rs"+LoanSummaryFragment.fee,null,ChatPresenter.chatObjects.size()+1);
            //ChatActivity.presenter.botMessage("Please proceed to the next step. Let’s complete a video call first.",ChatActivity.videoRecording,ChatPresenter.chatObjects.size()+1);
            ChatActivity.presenter.botMessage("Are you using the same mobile no. "+validateOTPResponse.getData().getPhoneNumber()+" on Whatsapp?",ChatActivity.whatsApp,ChatPresenter.chatObjects.size()+1);
            Objects.requireNonNull(getActivity()).onBackPressed();
            new SaveIsLoanAgreement("1").execute();
        });



        return view;

    }

    public String getString() {
        StringBuilder termsString = new StringBuilder();
        BufferedReader reader;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(getActivity().getAssets().open("loan_agreement.txt")));

            String str;
            while ((str = reader.readLine()) != null) {
                termsString.append(str);
                termsString.append(System.getProperty("line.separator"));
            }

            reader.close();
            return termsString.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public class SaveIsLoanAgreement extends AsyncTask<String, String ,String> {

        String oneOrZero;

        public SaveIsLoanAgreement(String oneOrZero) {
            this.oneOrZero = oneOrZero;
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            new ChatActivity.GetCustomerDetails().execute();
        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url("http://157.245.102.194:8080/los-qa/customer/saveIsSaveLoanAggreement/"+CUSTOMER_ID+"/"+oneOrZero)
                    .build();
            try{
                Response response = client.newCall(request).execute();
                return response.body().string();
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }

}
