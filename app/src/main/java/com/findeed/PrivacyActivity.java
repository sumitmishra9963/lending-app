package com.findeed;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class PrivacyActivity extends AppCompatActivity {

    ImageView backArrowPrivacy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        backArrowPrivacy = findViewById(R.id.backbuttn_privacy);


        backArrowPrivacy.setOnClickListener(v -> {

            finish();
        });
    }
}
