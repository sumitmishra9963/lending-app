package com.findeed;

import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ListView;

import com.findeed.Bot.Adapters.RepaymentAdapter;
import com.findeed.Bot.Adapters.TabAdapter;
import com.findeed.Bot.ChatActivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TabLayoutActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager viewPager;
    public  static Context ctx;
    public static TabLayoutActivity cIntenet;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tablayout_activity);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.viewPager);
        ctx=getApplicationContext();
        cIntenet=this;

        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);


        tabLayout.addTab(tabLayout.newTab().setText("LOAN \n SUMMARY"));
        tabLayout.addTab(tabLayout.newTab().setText("DETAILED \n SCHEDULE"));
        tabLayout.addTab(tabLayout.newTab().setText("LOAN \n AGREEMENT"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#fbbb02"));

        //                listv = findViewById(R.id.listview);
//                for (Map.Entry<String, String> entry : map.entrySet()) {
//                    arraylist.add(new SchoolNames(entry.getKey(), entry.getValue()));
//
//                }
//
//                final ListViewAdapter adapter = new ListViewAdapter(ctx, arraylist);
//                listv.setAdapter(adapter);




        final TabAdapter adapter = new TabAdapter(this, getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });




    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ChatActivity.chatAdapter.notifyDataSetChanged();
    }

    public void navigateFragment(int position){
        viewPager.setCurrentItem(position, true);

    }



}
