package com.findeed;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.findeed.Bot.Adapters.RepaymentAdapter;
import com.findeed.Bot.ChatActivity;
import com.findeed.Bot.ChatPresenter;
import com.findeed.Bot.EditLoanInfo;

import java.util.Objects;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.findeed.ApplicationData.validateOTPResponse;
import static com.findeed.Bot.ChatActivity.CUSTOMER_DETAILS;
import static com.findeed.Bot.ChatActivity.CUSTOMER_ID;


/**
 * A simple {@link Fragment} subclass.
 */
public class DetailedScheduleFragment extends Fragment {
    public static boolean isLoanEdited;


    public DetailedScheduleFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment



        View view =inflater.inflate(R.layout.fragment_detailed_schedule, container, false);
        TextView editDetailedLoan = view.findViewById(R.id.tv_edit_detailedloan);
        Button agreeAndContinue=view.findViewById(R.id.agree_detailed_schedule);
        TextView loanAmount=view.findViewById(R.id.fragment_total_loan_amount);
        TextView totalInterest=view.findViewById(R.id.fragment_total__interest);
        TextView editLoan=view.findViewById(R.id.tv_edit_detailedloan);

        ListView repaymentListView=view.findViewById(R.id.lvschedule);
        final RepaymentAdapter rAdapter = new RepaymentAdapter(TabLayoutActivity.ctx, ChatActivity.repaymentList);
        repaymentListView.setAdapter(rAdapter);
        loanAmount.setText("\u20B9 "+validateOTPResponse.getData().getLoans().get(0).getRequestAmount());
        totalInterest.setText("\u20B9 "+ChatActivity.TOTAL_INTEREST);


        editLoan.setOnClickListener(v -> {

            ChatActivity.TOTAL_INTEREST=0.0;
            TabLayoutActivity.cIntenet.finish();
            Intent editLoanInfo = new Intent(TabLayoutActivity.ctx, EditLoanInfo.class);
            startActivity(editLoanInfo);



        });

        agreeAndContinue.setOnClickListener(v -> { ((TabLayoutActivity)getActivity()).navigateFragment(2); });
        return view;
    }


}
