package com.findeed;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.findeed.Bot.ChatActivity;
import com.github.pavlospt.roundedletterview.RoundedLetterView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.findeed.Bot.ChatActivity.lvlay;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {


    public ArrayList<com.findeed.Contacts> arraylist;
    public ArrayList<com.findeed.Contacts> contactlist=new ArrayList<>();



    public RecyclerAdapter(ArrayList<com.findeed.Contacts> contactList) {
        this.arraylist = contactList;
        this.contactlist.addAll(contactList);


    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.contactlist_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.title.setText(arraylist.get(position).getName());
        holder.phone.setText(arraylist.get(position).getPhone());
        holder.image.setTitleText(arraylist.get(position).getAvatar());

        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        holder.rlv_backgroundColorValue.setBackgroundColor(color);




      //  holder.actvSearch.setText("");
        holder.contactData.setVisibility(View.VISIBLE);
        holder.contactData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              Log.i("contctsss",holder.title.getText().toString());
                ContactActivity.cca.data = new Intent();
//
                ContactActivity.cca.data.putExtra("contact_name",holder.title.getText().toString());
                ContactActivity.cca.data.putExtra("contact_number",holder.phone.getText().toString());

                ContactActivity.cca.setResult(-1,ContactActivity.cca.data);
                ContactActivity.cca.finish();

                                         }
        });


    }

    @Override
    public int getItemCount() {
        return arraylist.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public TextView phone;
        public RoundedLetterView image;
        public RelativeLayout contactData;
        RoundedLetterView rlv_backgroundColorValue;
//        public RelativeLayout contactLayout;
//        public AutoCompleteTextView actvSearch;



        public ViewHolder(final View itemView) {
            super(itemView);
            this.setIsRecyclable(false);
            title =  itemView.findViewById(R.id.name);
            phone =  itemView.findViewById(R.id.no);
            image =  itemView.findViewById(R.id.rlv_name_view);
            contactData = itemView.findViewById(R.id.rl_contact_data);
            rlv_backgroundColorValue = itemView.findViewById(R.id.rlv_name_view);

//            contactLayout = itemView.findViewById(R.id.rl_searchLayout);
//            actvSearch = itemView.findViewById(R.id.actvSearch);
//            actvSearch.setText("");


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                }
            });
        }
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        Log.i("filterer",charText);
        Log.i("filterer",""+charText.length());
        arraylist.clear();
        if (charText.length() == 0) {
            Log.i("filterer0",""+charText);
            arraylist.addAll(contactlist);
        } else {
            for (com.findeed.Contacts cp : contactlist) {
                if (cp.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    arraylist.add(cp);
                    Log.i("filterer2",cp.getName());
                }
            }
        }
        notifyDataSetChanged();

    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}
