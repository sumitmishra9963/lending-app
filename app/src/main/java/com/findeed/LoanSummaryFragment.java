package com.findeed;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.findeed.Bot.ChatActivity;
import com.findeed.Bot.ChatPresenter;
import com.findeed.Bot.EditLoanInfo;

import java.util.Objects;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.findeed.ApplicationData.validateOTPResponse;
import static com.findeed.Bot.ChatActivity.CUSTOMER_DETAILS;
import static com.findeed.Bot.ChatActivity.CUSTOMER_ID;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoanSummaryFragment extends Fragment {


    public static boolean isLoanEdited;
    public  static String  lateFee;
    public  static String  fee;
    public LoanSummaryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view =inflater.inflate(R.layout.fragment_loan_summary, container, false);
        Button agreeLoanSummary=view.findViewById(R.id.loan_summary_agree);
        TextView editLoanSummary = view.findViewById(R.id.tv_edit_loansummary);
        TextView paymentDue=view.findViewById(R.id.tv_permonth);
        TextView nextDue=view.findViewById(R.id.tv_duedate_text);
        TextView loanAmount=view.findViewById(R.id.tv_amount);
        TextView paymentFrequency=view.findViewById(R.id.tv_monthly_weakly);
        TextView intrestRate=view.findViewById(R.id.tv_monthly_weakly);
        TextView loanTenure=view.findViewById(R.id.tv_loan);
        TextView editLoan=view.findViewById(R.id.tv_edit_loansummary);
        TextView lateFeeTextview=view.findViewById(R.id.tv_lateprice);
        TextView processingFeeTextView=view.findViewById(R.id.tv_processing_fee);

        lateFee= String.valueOf((Float.valueOf(validateOTPResponse.getData().getLoans().get(0).getRequestAmount()))*0.01);
        lateFeeTextview.setText("1% per day on amount due");


        if (Float.valueOf(validateOTPResponse.getData().getLoans().get(0).getRequestAmount()) < 30000) {
            fee = "600";
        } else {
            float lFee = (float) (Float.valueOf(validateOTPResponse.getData().getLoans().get(0).getRequestAmount()) * 0.02);
            if (lFee > 1000)
                fee = "1000";
            else
                fee = String.valueOf(Float.valueOf(lFee));
        }
        processingFeeTextView.setText("\u20B9 " + fee);
        editLoan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChatActivity.TOTAL_INTEREST=0.0;
                TabLayoutActivity.cIntenet.finish();
                Intent editLoanInfo = new Intent(TabLayoutActivity.ctx, EditLoanInfo.class);
                startActivity(editLoanInfo);

            }
        });


        try{
            paymentDue.setText("Rs "+ Math.round(Float.valueOf(validateOTPResponse.getData().getLoans().get(0).getEmiRate()))+" Per Month");
            loanAmount.setText(String.valueOf(Math.round(Float.valueOf(validateOTPResponse.getData().getLoans().get(0).getRequestAmount()))));
            paymentFrequency.setText("Monthly");
            loanTenure.setText(validateOTPResponse.getData().getLoans().get(0).getTenure()+"Months");
        }catch (Exception e){

            Log.i("FRAG",e.toString());
        }













        agreeLoanSummary.setOnClickListener(v -> {

            ((TabLayoutActivity)getActivity()).navigateFragment(1);


           /* ChatActivity.presenter.botMessage("Thank you!",null, ChatPresenter.chatObjects.size()+1);
            ChatActivity.presenter.botMessage("Loan Amount: Rs "+Math.round(Float.valueOf(validateOTPResponse.getData().getLoans().get(0).getRequestAmount()))+"\n"+
                    "Loan Tenure: "+validateOTPResponse.getData().getLoans().get(0).getTenure()+"Months"+"\n"+"Payment Frequency: Monthly"+"\n"
                    +"Interest Rate: 2% Per Month"+"\n"+"Payment Due: 10th of every month"+"\n"+"Late fee: Rs 70."+"\n"+
                    "Processing Fee: Rs 500",null,ChatPresenter.chatObjects.size()+1);
            ChatActivity.presenter.botMessage("Please proceed to the next step. Let’s complete a video call first.",ChatActivity.videoRecording,ChatPresenter.chatObjects.size()+1);
            Objects.requireNonNull(getActivity()).onBackPressed();*/

        });







        return view;
    }


}
