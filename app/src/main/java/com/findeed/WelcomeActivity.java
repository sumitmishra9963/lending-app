package com.findeed;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.findeed.Bot.ChatActivity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WelcomeActivity extends AppCompatActivity {
    Button welcomeButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        welcomeButton = findViewById(R.id.start_btn);
        welcomeButton.setOnClickListener(v -> {
            welcomeButton.setEnabled(false);
            Intent intent = new Intent(getApplicationContext(), ChatActivity.class);
            startActivity(intent);
        });

    }


    protected BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final Bundle bundle = intent.getExtras();
            try {
                if (bundle != null) {
                    final Object[] pdusObj = (Object[]) bundle.get("pdus");
                    for (Object o : pdusObj) {

                        SmsMessage currentMessage;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            String format = bundle.getString("format");
                            currentMessage = SmsMessage.createFromPdu((byte[]) o, format);
                            Log.e("Current Message", format + " : " + currentMessage.getDisplayOriginatingAddress());
                        } else {
                            currentMessage = SmsMessage.createFromPdu((byte[]) o);
                        }
                        Pattern regEx =
                                Pattern.compile("[a-zA-Z0-9]{2}-[a-zA-Z0-9]{6}");
                        Matcher m = regEx.matcher(currentMessage.getDisplayOriginatingAddress());
                        if (m.find()) {
                            try {
                                String phoneNumber = m.group(0);
                                Long date = currentMessage.getTimestampMillis();
                                String message = currentMessage.getDisplayMessageBody();
                                Log.e("SmsReceiver Mine", "senderNum: " + phoneNumber + "; message: " + message);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Log.e("Mismatch", "Mismatch value");
                        }
                    }
                }
            } catch (Exception e) {
                Log.e("SmsReceiver", "Exception smsReceiver" + e);
            }
        }
    };


}
