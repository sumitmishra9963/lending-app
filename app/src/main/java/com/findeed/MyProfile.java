package com.findeed;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyProfile extends AppCompatActivity {

    CircleImageView profileImage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_profile_layout);
        TextView profileName= findViewById(R.id.tv_user_name);
        TextView profileDob= findViewById(R.id.tv_dob);
        TextView profileContact= findViewById(R.id.tv_contact);
        TextView profileWhatsapp= findViewById(R.id.tv_whatsapp);
        TextView profileEmail= findViewById(R.id.tv_email);
        TextView profileAddress= findViewById(R.id.tv_address);
        TextView profilePan= findViewById(R.id.tv_pan);
        TextView profileAdhaar_Voter= findViewById(R.id.tv_aadhar);
        TextView profileAdhaarOrVoter= findViewById(R.id.tv_aadharheader);
        ImageView backArrowProfile = findViewById(R.id.backarrow_profileheader);
        ImageView iconImageUpload = findViewById(R.id.img_upload_icon);
        ImageView iconImageEdit = findViewById(R.id.img_edit_icon);
        TextView profileLanguage= findViewById(R.id.tv_language);
         profileImage = findViewById(R.id.img_profile);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);


        Bundle intent=getIntent().getExtras();
        profileName.setText(intent.getString("fname")+" "+intent.getString("lname"));


        backArrowProfile.setOnClickListener(v -> finish());

        iconImageUpload.setOnClickListener(v -> {

            Intent intentPicture = new Intent();
            intentPicture.setType("image/*");
            intentPicture.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(intentPicture, 0);
            iconImageUpload.setVisibility(View.GONE);
            iconImageEdit.setVisibility(View.VISIBLE);

        });

        iconImageEdit.setOnClickListener(v -> {

            Intent intentPicture = new Intent();
            intentPicture.setType("image/*");
            intentPicture.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(intentPicture, 0);
            iconImageUpload.setVisibility(View.GONE);
            iconImageEdit.setVisibility(View.VISIBLE);

        });






      /*  if(intent.getString("dob").equals("")){

        }
        else{
            profileDob.setText(intent.getString("dob"));
        }
        if(intent.getString("whatsapp").equals("")){

        }
        else{
            profileWhatsapp.setText(intent.getString("whatsapp"));
        }
        if(intent.getString("email").equals("")){

        }
        else{
            profileEmail.setText(intent.getString("email"));
        }
        if(intent.getString("address").equals("")){

        }
        else{
            profileAddress.setText(intent.getString("address"));
        }
        if(intent.getString("pan").equals("")){

        }
        else{
            profilePan.setText(intent.getString("pan"));
        }*/





        profileContact.setText(intent.getString("contact"));




       /* if(intent.getString("voter")!=null && !intent.getString("voter").equals("")){
            if(intent.getString("adhaar").equals("")){

            }
            else{
                profileAdhaarOrVoter.setText("AADHAAR NO.");
                profileAdhaar_Voter.setText(intent.getString("adhaar"));
            }

        }
        else{
            if(intent.getString("voter").equals("")){

            }
            else{
                profileAdhaarOrVoter.setText("VOTER ID NO.");
                profileAdhaar_Voter.setText(intent.getString("voter"));
            }

        }*/

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {


             Uri uri = data.getData();
             profileImage.setImageURI(uri);
             super.onActivityResult(requestCode, resultCode, data);


    }


}
