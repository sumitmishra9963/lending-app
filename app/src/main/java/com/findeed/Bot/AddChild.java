package com.findeed.Bot;

import android.view.View;

import com.findeed.Bot.ViewHolder.ChatObject;

public class AddChild extends ChatObject {

    boolean isItName;
    int dependentId;

    public int getDependentId() {
        return dependentId;
    }

    public void setDependentId(int dependentId) {
        this.dependentId = dependentId;
    }

    public boolean isItName() {
        return isItName;
    }

    public void setItName(boolean itName) {
        isItName = itName;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public View getViewType() {
        return viewType;
    }

    public void setViewType(View viewType) {
        this.viewType = viewType;
    }

    int pos;
    View viewType;

    @Override
    public int getType() {
        return ChatObject.ADD_CHILD;
    }
}
