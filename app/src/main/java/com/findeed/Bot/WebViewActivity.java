package com.findeed.Bot;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.findeed.R;

import java.security.MessageDigest;
import java.util.Random;
import java.util.StringTokenizer;
import com.findeed.ApplicationData;

public class WebViewActivity extends AppCompatActivity {

    public static boolean isVisited=false;
   public static String USER_ID,HASH,STAN;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.msite_web_view);
        Random rand = new Random();
        String requestId=getRequestId();
        STAN=String.valueOf(rand.nextInt(10000000));

        ProgressDialog pd = new ProgressDialog(WebViewActivity.this);
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        pd.show();

       HASH=sha256(ApplicationData.CLIENT_CODE+"|"+requestId+"|"+ApplicationData.API_KEY+"|"+ApplicationData.SALT);

        String unencodedHtml = "<html lang=\"en\">\n" +
                "<head>\n" +
                "  <meta charset=\"utf-8\">\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
                "\n" +
                "</head>\n"+
                "<body onload=\"document.msite.submit()\">\n"+
                "<form action=\"https://prod.veri5digital.com/video-id-kyc/_initWebVIKYC/\" method=\"post\" name=\"msite\">\n" +
                "<input type=\"hidden\" name=\"client_code\" value=\""+ApplicationData.CLIENT_CODE+"\" >\n" +
                "<input type=\"hidden\" name=\"api_key\" value=\""+ApplicationData.API_KEY+"\" >\n"+
                "<input type=\"hidden\" name=\"redirect_url\" value=\"findeed.in\" >\n"+
                "<input type=\"hidden\" name=\"request_id\" value=\""+requestId+"\" >\n"+
                "<input type=\"hidden\" name=\"stan\" value=\""+STAN+"\" >\n"+
                "<input type=\"hidden\" name=\"hash\" value=\""+HASH+"\" >\n"+
                "<input type=\"hidden\" name=\"mobile\" value=\"\">\n"+
                "<input type=\"hidden\" name=\"otp_required\" value=\"N\" >\n"+
                "</form>" +
                "</body>" +
                "</html>";

        String encodedHtml = Base64.encodeToString(unencodedHtml.getBytes(), Base64.NO_PADDING);
        Log.i("HTML",encodedHtml);

        WebView mWebview  = new WebView(this);
        mWebview.getSettings().setJavaScriptEnabled(true);
        mWebview.getSettings().setDomStorageEnabled(true);
        mWebview.getSettings().setLoadWithOverviewMode(true);
        mWebview.getSettings().setUseWideViewPort(true);
        mWebview.getSettings().setSupportMultipleWindows(true);
        mWebview.setWebViewClient(new WebViewClient() {
            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(getApplicationContext(), description, Toast.LENGTH_SHORT).show();
            }
            @TargetApi(android.os.Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                // Redirect to deprecated method, so you can use it in all SDK versions
                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                pd.dismiss();
                if(request.getUrl().toString().contains("findeed.in")){
                    String[] urlData = request.getUrl().toString().split("\\?");
                    String[] values = urlData[1].split("\\&");
                        StringTokenizer st = new StringTokenizer(values[1]);
                        st.nextToken("=");
                        USER_ID=st.nextToken();
                        finish();
                    return true;
                }
                return false;

            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                pd.dismiss();
                if(url.contains("findeed.in")){
                    String[] urlData = url.split("\\?");
                    String[] values = urlData[1].split("\\&");
                    StringTokenizer st = new StringTokenizer(values[1]);
                    st.nextToken("=");
                    USER_ID=st.nextToken();
                    finish();
                    return true;
                }
                return false;

            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

            }

        });

       // mWebview .loadUrl("http://www.devkraft.in");
        mWebview.loadData(encodedHtml, "text/html", "base64");
        setContentView(mWebview);

    }

    private String getRequestId() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 10) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        return salt.toString();
    }

        public static String sha256(String base) {
        try{
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuilder hexString = new StringBuilder();

            for (byte b : hash) {
                String hex = Integer.toHexString(0xff & b);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
