package com.findeed.Bot;

import com.findeed.API.Data;

import java.util.HashMap;

public class Persistence {
    HashMap<String, Data> persistenceData;

    public HashMap<String, Data> getPersistenceData() {
        return persistenceData;
    }

    public void setPersistenceData(HashMap<String, Data> persistenceData) {
        this.persistenceData = persistenceData;
    }
}
