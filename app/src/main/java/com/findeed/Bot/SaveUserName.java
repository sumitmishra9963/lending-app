package com.findeed.Bot;

import android.view.View;

import com.findeed.Bot.ViewHolder.ChatObject;

public class SaveUserName extends ChatObject {
    View viewType;
    int customerId;
    boolean isItFirstName;
    int pos;

    public View getViewType() {
        return viewType;
    }

    public void setViewType(View viewType) {
        this.viewType = viewType;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public boolean isItFirstName() {
        return isItFirstName;
    }

    public void setItFirstName(boolean itFirstName) {
        isItFirstName = itFirstName;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    @Override
    public int getType() {
        return ChatObject.USER_NAME;
    }
}
