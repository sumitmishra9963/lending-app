package com.findeed.Bot;
import android.graphics.Bitmap;

import com.findeed.API.Data;
import com.findeed.API.VData;
import com.findeed.Bot.ViewHolder.ChatObject;
import com.findeed.Bot.ViewHolder.ContactData;

import java.util.ArrayList;

public interface ChatContract {

    interface View {

        void notifyAdapterObjectAdded(int position);

        void scrollChatDown();

    }

    interface Presenter {


        void attachView(ChatContract.View view);

        ArrayList<ChatObject> getChatObjects();
        void onEditTextActionDone(String inputText, android.view.View viewType, int pos);
        void onEditTextActionDone(String inputText, android.view.View viewType, int pos,Data customerData);
        void onEditTextActionDone(String inputText, android.view.View viewType, int pos, VData voucherData);
        void botMessage(String inputText, android.view.View viewType,int pos);
        void setImage(Bitmap bitmap,android.view.View viewType,int pos,String name);
        void setBotMessageImage(Bitmap bitmap,android.view.View viewType,int pos);
        void sendDocsImage(Bitmap bitmap,android.view.View viewType,int pos);
        void sendContacts(ContactData voucher,android.view.View viewType,int pos);
        void sendUserLocation(Bitmap bitmap,android.view.View viewType,int pos);
        void addChildDetail(String inputtext,android.view.View viewType,int pos,boolean isItName,int dependentId);
        void sendVoterIdImage(Bitmap bitmap,android.view.View viewType,int pos);
        void addUserName(String inputtext,android.view.View viewType,int pos,boolean isItFirstName,int customerId);



    }
}
