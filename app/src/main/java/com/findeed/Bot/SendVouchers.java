package com.findeed.Bot;

import android.view.View;

import com.findeed.Bot.ViewHolder.ChatObject;

public class SendVouchers extends ChatObject {
    int pos;

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public View getViewType() {
        return viewType;
    }

    public void setViewType(View viewType) {
        this.viewType = viewType;
    }

    View viewType;



    @Override
    public int getType() {
        return ChatObject.SEND_CONTACT;
    }
}
