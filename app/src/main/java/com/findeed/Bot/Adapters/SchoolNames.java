package com.findeed.Bot.Adapters;

public class SchoolNames {

    private String schoolname;
    private String schooladdrs;

    public SchoolNames(String schoolname, String schooladdrs)
    {
        this.schoolname = schoolname;
        this.schooladdrs = schooladdrs;
    }

    public String getSchoolname() {
        return this.schoolname;
    }

    public String getSchooladdrs() {
        return schooladdrs;
    }
}
