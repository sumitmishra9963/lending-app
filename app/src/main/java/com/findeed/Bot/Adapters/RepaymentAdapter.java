package com.findeed.Bot.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.findeed.Bot.RepaymentSchedule;
import com.findeed.R;


import java.util.ArrayList;
import java.util.List;


public class RepaymentAdapter extends BaseAdapter {

    int count=0;
//    LayoutInflater layoutInflater;
//    Context mContext;
//    private List<CompaniesNames> cNameList;
//    private ArrayList<CompaniesNames> cNameArrayList;
//    private String companyname;


    LayoutInflater layoutInflater;
    Context mContext;
    private ArrayList<RepaymentSchedule> repaymentSchedules;


    public RepaymentAdapter(Context mContext, ArrayList<RepaymentSchedule> repaymentSchedules) {
        this.mContext = mContext;
        this.repaymentSchedules = repaymentSchedules;
        layoutInflater = LayoutInflater.from(mContext);


    }

    public static class ViewHolder {
        TextView payment;
        TextView principle;
        TextView Interest;
        TextView loanBalance;
    }


    @Override
    public int getCount() {
        return repaymentSchedules.size();
    }

    @Override
    public Object getItem(int position) {
        return repaymentSchedules.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.i("COUNTING_VIEW",String.valueOf(count));
        count++;
        final RepaymentAdapter.ViewHolder holder;
        if (convertView == null) {
            holder = new RepaymentAdapter.ViewHolder();
            layoutInflater = LayoutInflater.from(parent.getContext());
            convertView = layoutInflater.inflate(R.layout.repayment_schedule_layout, null);


            holder.payment = convertView.findViewById(R.id.fragment_payment);
            holder.loanBalance =  convertView.findViewById(R.id.fragment_loanbalance);
            holder.Interest=convertView.findViewById(R.id.fragment_interest);
            holder.principle=convertView.findViewById(R.id.fragment_principle);
            convertView.setTag(holder);

        } else {
            holder = (RepaymentAdapter.ViewHolder) convertView.getTag();
        }
        // Set the results into TextViews
        try {
            holder.payment.setText(repaymentSchedules.get(position).getPayment());
            holder.loanBalance.setText(repaymentSchedules.get(position).getLoanBalance());
            holder.Interest.setText(repaymentSchedules.get(position).getInterest());
            holder.principle.setText(repaymentSchedules.get(position).getPrinciple());
            return convertView;
        }catch (IndexOutOfBoundsException e){
            Log.d("@@@@@@@@@@",e.toString());
            return null;
        }
    }
}

