package com.findeed.Bot.Adapters;

public class CompaniesNames {

    private String companyname;
    private String companyaddress;

    public CompaniesNames(String companyname, String companyaddress)
    {

        this.companyname = companyname;
        this.companyaddress = companyaddress;
    }

    public String getCompanyname() {
        return companyname;
    }

    public String getCompanyaddress() {
        return companyaddress;
    }
}
