package com.findeed.Bot.Adapters;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;


import com.findeed.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

//implements Filterable
public class ListViewAdapter extends BaseAdapter  {

    Context mContext;
    LayoutInflater inflater;
    private List<SchoolNames> schoolList;
    private ArrayList<SchoolNames> arraylist;
    private String schoolname;
  //  private ItemFilter mFilter = new ItemFilter();


    public ListViewAdapter(Context context, ArrayList<SchoolNames> schoolList) {
        mContext = context;
        this.schoolList = schoolList;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = new ArrayList<>();
        this.arraylist.addAll(schoolList);
    }

//    @Override
//    public Filter getFilter() {
//       return mFilter;
//    }

    public static class ViewHolder {
        TextView address;
        TextView name;
    }

    public String getSchoolname(){
        return this.schoolname;
    }

    @Override
    public int getCount() {

        if(schoolList.size()<3){
            return schoolList.size();
        }else {
            return 3;
        }

    }

    @Override
    public SchoolNames getItem(int position) {
        return schoolList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.listview_item, null);


            holder.name = view.findViewById(R.id.child_school_name);
            holder.address =  view.findViewById(R.id.school_address);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        // Set the results into TextViews
            try {
                holder.name.setText(schoolList.get(position).getSchoolname());
                holder.address.setText(schoolList.get(position).getSchooladdrs());
                schoolname = schoolList.get(position).getSchoolname();
                return view;
            }catch (IndexOutOfBoundsException e){
                return view;
            }
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        schoolList.clear();
        if (charText.length() == 0) {
            schoolList.addAll(arraylist);
        } else {
            for (SchoolNames wp : arraylist) {
                if (wp.getSchoolname().toLowerCase(Locale.getDefault()).contains(charText)) {
                    schoolList.add(wp);
                }
            }
        }
        notifyDataSetChanged();

            }

//
//            private class ItemFilter extends Filter{
//
//                @Override
//                protected FilterResults performFiltering(CharSequence charSequence) {
//                    String filterString = charSequence.toString().toLowerCase();
//
//                    String filterableString ;
//
//                    for (int i = 0; i < schoolList.size(); i++) {
//                        filterableString = schoolList.get(i).getSchoolname();
//                        if (filterableString.toLowerCase().contains(filterString)) {
//                            nlist.add(filterableString);
//                        }
//                    }
//
//                    results.values = nlist;
//                    results.count = nlist.size();
//
//                    return results;
//                }
//
//                @Override
//                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
//
//                }
//            }
}
