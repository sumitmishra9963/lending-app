package com.findeed.Bot.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;

import com.findeed.Bot.ViewHolder.AddChildVH;
import com.findeed.Bot.ViewHolder.BaseViewHolder;
import com.findeed.Bot.ViewHolder.BotDocImageVH;
import com.findeed.Bot.ViewHolder.BotImageResponseVH;
import com.findeed.Bot.ViewHolder.ChatInputVH;
import com.findeed.Bot.ViewHolder.ChatObject;
import com.findeed.Bot.ViewHolder.ChatResponseVH;
import com.findeed.Bot.ViewHolder.ImageResponseVH;
import com.findeed.Bot.ViewHolder.SaveUserNameVH;
import com.findeed.Bot.ViewHolder.SendVoterImageVH;
import com.findeed.Bot.ViewHolder.SendVouchersVH;
import com.findeed.Bot.ViewHolder.UserLocationVH;
import com.findeed.R;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class ChatAdapter extends RecyclerView.Adapter<BaseViewHolder> {



    private ArrayList<ChatObject> chatObjects;

    public ChatAdapter(ArrayList<ChatObject> chatObjects) {
        this.chatObjects = chatObjects;
    }



    @NotNull
    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        // Create the ViewHolder based on the viewType
        final View itemView;

        if(viewType==ChatObject.INPUT_OBJECT){
            itemView = inflater.inflate(R.layout.chat_input_layout, parent, false);
            return new ChatInputVH(itemView);
        }else if(viewType==ChatObject.RESPONSE_OBJECT) {
            itemView = inflater.inflate(R.layout.chat_response_layout, parent, false);
            return new ChatResponseVH(itemView);
        }else if(viewType==ChatObject.IMAGE_OBJECT) {
            itemView=inflater.inflate(R.layout.image_chat_input,parent,false);
            return  new ImageResponseVH(itemView);
        }else if(viewType==ChatObject.DOC_IMAGE){
            itemView=inflater.inflate(R.layout.withoutimage_chatinput,parent,false);
            return new BotDocImageVH(itemView);

        }else if(viewType==ChatObject.SEND_CONTACT){

            itemView=inflater.inflate(R.layout.send_voucher,parent,false);
            return  new SendVouchersVH(itemView);
        }else if(viewType==ChatObject.SEND_LOCATION){
            itemView=inflater.inflate(R.layout.send_current_location_with_address,parent,false);
            return new UserLocationVH(itemView);
        }else if(viewType==ChatObject.ADD_CHILD){
            itemView=inflater.inflate(R.layout.add_child_deatil_layout,parent,false);
            return new AddChildVH(itemView);
        }
        else if(viewType==ChatObject.VOTER_IMAGE){
            itemView=inflater.inflate(R.layout.voterid_img_chatinput,parent,false);
            return new SendVoterImageVH(itemView);
        }
        else if(viewType==ChatObject.USER_NAME){
            itemView=inflater.inflate(R.layout.user_name_layout,parent,false);
            return new SaveUserNameVH(itemView);
        }
        else {
            itemView=inflater.inflate(R.layout.bot_image_response_layout,parent,false);
            return  new BotImageResponseVH(itemView);
        }
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {

        holder.onBindView(chatObjects.get(position));


    }

    @Override
    public int getItemViewType(int position) {

        return chatObjects.get(position).getType();
    }
    @Override
    public int getItemCount() {


        return chatObjects.size();

    }
}
