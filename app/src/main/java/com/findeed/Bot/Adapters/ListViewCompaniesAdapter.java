package com.findeed.Bot.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.findeed.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ListViewCompaniesAdapter extends BaseAdapter {


    LayoutInflater layoutInflater;
    Context mContext;
    private List<CompaniesNames> cNameList;
    private ArrayList<CompaniesNames> cNameArrayList;
    private String companyname;



    public ListViewCompaniesAdapter(Context mContext, ArrayList<CompaniesNames> cNameList) {
        this.mContext = mContext;
        this.cNameList = cNameList;
        layoutInflater = LayoutInflater.from(mContext);
        this.cNameArrayList = new ArrayList<>();
        this.cNameArrayList.addAll(cNameList);

    }


    public static class ViewHolder {
        TextView companyaddress;
        TextView companyname;
    }



    public String getCompanyname()
    {
        return this.companyname;
    }

    @Override
    public int getCount() {
        if (cNameList.size()<3)
        {
            return cNameList.size();
        }else{
            return 3;
        }

    }

    @Override
    public Object getItem(int position) {
        return cNameList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            layoutInflater = LayoutInflater.from(parent.getContext());
            convertView = layoutInflater.inflate(R.layout.companies_listview_items, null);


            holder.companyname = convertView.findViewById(R.id.company_name);
            holder.companyaddress =  convertView.findViewById(R.id.company_address);
            convertView.setTag(holder);
        } else {
            holder = (ListViewCompaniesAdapter.ViewHolder) convertView.getTag();
        }
        // Set the results into TextViews
        try {
            holder.companyname.setText(cNameList.get(position).getCompanyname());
            holder.companyaddress.setText(cNameList.get(position).getCompanyaddress());
            companyname = cNameList.get(position).getCompanyname();
            return convertView;
        }catch (IndexOutOfBoundsException e){
            Log.d("@@@@@@@@@@",e.toString());
            return null;
        }
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        cNameList.clear();
        if (charText.length() == 0) {
            //cNameArrayList.addAll(cNameArrayList);
        } else {
            for (CompaniesNames wp : cNameArrayList) {
                if (wp.getCompanyname().toLowerCase(Locale.getDefault()).contains(charText)) {
                    cNameList.add(wp);
                }
            }
        }
        notifyDataSetChanged();

    }



}
