package com.findeed.Bot.Adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.findeed.DetailedScheduleFragment;
import com.findeed.LoanAgreementFragment;
import com.findeed.LoanSummaryFragment;

public class TabAdapter extends FragmentPagerAdapter {

    int totalTabs;

    public TabAdapter(Context context, FragmentManager fm, int totalTabs) {
        super(fm);
        this.totalTabs = totalTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position){

            case 0:

                return new LoanSummaryFragment();
            case 1:
                return new DetailedScheduleFragment();
            case 2:
                return new LoanAgreementFragment();

            default:
                    return null;
        }
    }

    @Override
    public int getCount() {
        return totalTabs;
    }
}
