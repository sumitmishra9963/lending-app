package com.findeed.Bot;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.findeed.API.LoanRequest;
import com.findeed.API.ValidateOTPResponse;
import com.findeed.ApplicationData;
import com.findeed.R;
import com.google.gson.Gson;
import com.warkiz.widget.IndicatorSeekBar;
import com.warkiz.widget.OnSeekChangeListener;
import com.warkiz.widget.SeekParams;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.findeed.ApplicationData.validateOTPResponse;
import static java.lang.Float.parseFloat;
import static java.lang.Float.valueOf;

public class EditLoanInfo extends AppCompatActivity {

    Dialog dialogLoan;
    RelativeLayout rl3months;
    RelativeLayout rl6months;
    RelativeLayout rl9months;
    RelativeLayout rl12months;
    ImageView loanClear;
    TextView threemonth;
    TextView sixmonth;
    TextView ninthmonth;
    TextView twelvemonth;
    TextView eighteenmonth;
    ImageView tickthree;
    ImageView ticksix;
    ImageView tickninth;
    ImageView ticktwelve;
    ImageView tickeighteen;
    Button btnLoanTenure;
    String LoanTenure;
    int TENURE;
    IndicatorSeekBar eligibileLoanBar;
    String emiRate;
    TextView emiRateFindeed;
    TextView  showTenureFinal;
    Button saveMyData;
    RelativeLayout llLoanTenureFinalSelect;
    ProgressDialog ppd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_loan_info);


        showTenureFinal = findViewById(R.id.selected_tenureFinal);
        emiRateFindeed=findViewById(R.id.emi_rate_of_findeed);
        saveMyData=findViewById(R.id.btn_savemychoice);
        eligibileLoanBar = findViewById(R.id.seek_bar);
        llLoanTenureFinalSelect = findViewById(R.id.ll_LoanTenureFinalSelect);
        llLoanTenureFinalSelect.setOnClickListener(v -> {
            dialogLoan = new BottomSheetDialog(EditLoanInfo.this);
            //  dialogLoan.setContentView(R.layout.custom_bottomdialog_loantenure);

            View viewvvv = View.inflate(this, R.layout.custom_bottomdialog_loantenure, null);
            if (dialogLoan == null) {
                dialogLoan = new BottomSheetDialog(EditLoanInfo.this);
            }
            dialogLoan.setContentView(viewvvv);

            BottomSheetBehavior bottomSheetBehaviorloan = BottomSheetBehavior.from(((View) viewvvv.getParent()));
            bottomSheetBehaviorloan.setState(BottomSheetBehavior.STATE_EXPANDED);
            loanClear = dialogLoan.findViewById(R.id.btnLoanTenureClear);
            rl3months = dialogLoan.findViewById(R.id.rl_3months);
            rl6months = dialogLoan.findViewById(R.id.rl_6months);
            rl9months = dialogLoan.findViewById(R.id.rl_9months);
            rl12months = dialogLoan.findViewById(R.id.rl_12months);

            threemonth = dialogLoan.findViewById(R.id.txt_3months);
            sixmonth = dialogLoan.findViewById(R.id.txt_6months);
            ninthmonth = dialogLoan.findViewById(R.id.txt_9months);
            twelvemonth = dialogLoan.findViewById(R.id.txt_12months);
            eighteenmonth = dialogLoan.findViewById(R.id.txt_18months);

            btnLoanTenure = dialogLoan.findViewById(R.id.btn_Select_loantenure);
            tickthree = dialogLoan.findViewById(R.id.tick3);
            ticksix = dialogLoan.findViewById(R.id.tick6);
            tickninth = dialogLoan.findViewById(R.id.tick9);
            ticktwelve = dialogLoan.findViewById(R.id.tick12);
            tickeighteen = dialogLoan.findViewById(R.id.tick18);




            Window window = dialogLoan.getWindow();
            window.setGravity(Gravity.CENTER);

            dialogLoan.getWindow().setBackgroundDrawable(new
                    ColorDrawable(Color.TRANSPARENT));
            dialogLoan.show();

            loanClear.setOnClickListener(v12 -> {
                dialogLoan.dismiss();

            });
            rl3months.setOnClickListener(v1 -> {

                btnLoanTenure.setBackgroundResource(R.drawable.shape_btn_addloandark);
                tickthree.setVisibility(View.VISIBLE);
                ticksix.setVisibility(View.INVISIBLE);
                tickninth.setVisibility(View.INVISIBLE);
                ticktwelve.setVisibility(View.INVISIBLE);
                LoanTenure = "3 Months";
                TENURE=3;
            });
            rl6months.setOnClickListener(v13 -> {

                btnLoanTenure.setBackgroundResource(R.drawable.shape_btn_addloandark);
                tickthree.setVisibility(View.INVISIBLE);
                ticksix.setVisibility(View.VISIBLE);
                tickninth.setVisibility(View.INVISIBLE);
                ticktwelve.setVisibility(View.INVISIBLE);
                LoanTenure = "6 Months";
                TENURE=6;
            });

            rl9months.setOnClickListener(v14 -> {

                btnLoanTenure.setBackgroundResource(R.drawable.shape_btn_addloandark);
                tickthree.setVisibility(View.INVISIBLE);
                ticksix.setVisibility(View.INVISIBLE);
                tickninth.setVisibility(View.VISIBLE);
                ticktwelve.setVisibility(View.INVISIBLE);
                LoanTenure = "9 Months";
                TENURE=9;
            });

            rl12months.setOnClickListener(v15 -> {

                btnLoanTenure.setBackgroundResource(R.drawable.shape_btn_addloandark);
                tickthree.setVisibility(View.INVISIBLE);
                ticksix.setVisibility(View.INVISIBLE);
                tickninth.setVisibility(View.INVISIBLE);
                ticktwelve.setVisibility(View.VISIBLE);
                LoanTenure = "12 Months";
                TENURE = 12;
            });


            threemonth.setOnClickListener(v111 -> {

                btnLoanTenure.setBackgroundResource(R.drawable.shape_btn_addloandark);
                tickthree.setVisibility(View.VISIBLE);
                ticksix.setVisibility(View.INVISIBLE);
                tickninth.setVisibility(View.INVISIBLE);
                ticktwelve.setVisibility(View.INVISIBLE);
                LoanTenure = "3 Months";
                TENURE = 3;
            });

            sixmonth.setOnClickListener(v111 -> {

                btnLoanTenure.setBackgroundResource(R.drawable.shape_btn_addloandark);
                tickthree.setVisibility(View.INVISIBLE);
                ticksix.setVisibility(View.VISIBLE);
                tickninth.setVisibility(View.INVISIBLE);
                ticktwelve.setVisibility(View.INVISIBLE);
                LoanTenure = "6 Months";
                TENURE = 6;
            });

            ninthmonth.setOnClickListener(v111 -> {

                btnLoanTenure.setBackgroundResource(R.drawable.shape_btn_addloandark);
                tickthree.setVisibility(View.INVISIBLE);
                ticksix.setVisibility(View.INVISIBLE);
                tickninth.setVisibility(View.VISIBLE);
                ticktwelve.setVisibility(View.INVISIBLE);

                LoanTenure = "9 Months";
                TENURE = 9;
            });

            twelvemonth.setOnClickListener(v111 -> {

                btnLoanTenure.setBackgroundResource(R.drawable.shape_btn_addloandark);
                tickthree.setVisibility(View.INVISIBLE);
                ticksix.setVisibility(View.INVISIBLE);
                tickninth.setVisibility(View.INVISIBLE);
                ticktwelve.setVisibility(View.VISIBLE);
                //   tickeighteen.setVisibility(View.INVISIBLE);
                LoanTenure = "12 Months";
                TENURE = 12;
            });


            btnLoanTenure.setOnClickListener(view -> {
                showTenureFinal.setText(LoanTenure);


//                if(rbWeekly.isChecked()){
//                    // emiRate=  String.valueOf(Math.round(calculateEmi(eligibileLoanBar.getProgress(),12,tempTenure)/4));
//                    emiRate=  String.valueOf(Math.round(getEMI(eligibileLoanBar.getProgress(),tempTenure)));
//                    emiRateFindeed.setText("\u20B9 "+emiRate+"/Weekly");
//                }else {
//                    // emiRate=  String.valueOf(Math.round(calculateEmi(eligibileLoanBar.getProgress(),12,tempTenure)));
                    emiRate=  String.valueOf(Math.round(getEMI(eligibileLoanBar.getProgress(),TENURE)));
                    emiRateFindeed.setText("\u20B9 "+emiRate+"/Monthly");
              //  }


                dialogLoan.dismiss();
            });



        });

        float ELIGIBLE_AMOUNT = validateOTPResponse.getData().getLoans().get(0).getMaxLoanEligible();
        int selectedLoanAmount= (int) parseFloat(validateOTPResponse.getData().getLoans().get(0).getRequestAmount());
        if(selectedLoanAmount>50000)
            eligibileLoanBar.setMax(50000);
        else
            eligibileLoanBar.setMax(ELIGIBLE_AMOUNT);
        eligibileLoanBar.setProgress(valueOf(selectedLoanAmount));
        TENURE= Integer.parseInt(validateOTPResponse.getData().getLoans().get(0).getTenure());
        showTenureFinal.setText(TENURE+" Months");


        eligibileLoanBar.setOnSeekChangeListener(new OnSeekChangeListener() {
                @Override
                public void onSeeking(SeekParams seekParams) {
                    int cp = eligibileLoanBar.getProgress();
                    cp=cp/1000;
                    cp=cp*1000;
                    eligibileLoanBar.setProgress(valueOf(cp));
                    Log.i("Values",String.valueOf(cp));
                    Log.i("Values",String.valueOf(valueOf(cp)));
                    // emiRate=String.valueOf(Math.round(calculateEmi(eligibileLoanBar.getProgress(),12,tempTenure)));
                    emiRate=  String.valueOf(Math.round(getEMI(eligibileLoanBar.getProgress(),TENURE)));
                    emiRateFindeed.setText("\u20B9 "+emiRate+"/Month");

                }

                @Override
                public void onStartTrackingTouch(IndicatorSeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(IndicatorSeekBar seekBar) {

                }
            });




        saveMyData.setOnClickListener(v -> {

            ppd=new ProgressDialog(EditLoanInfo.this);
            ppd.setMessage("Please wait..");
            ppd.setCancelable(false);
            ppd.show();
            new SaveLoanDetails(eligibileLoanBar.getProgress()).execute();
        });

    }
    public static float getEMI(float principleAmount,float tenure) {
        float intrest=(principleAmount*2*tenure)/100;
        return Math.round((principleAmount+intrest)/tenure);
    }
    private class SaveLoanDetails extends AsyncTask<String,String ,String > {


        int loanValue;
        LoanRequest loanRequest;

        public SaveLoanDetails(int loanValue) {
            this.loanValue = loanValue;
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            ChatActivity.chatActivity.finish();
            Intent intent = new Intent(EditLoanInfo.this, ChatActivity.class);
            new StartOver(intent).execute();
        }

        @Override
        protected String doInBackground(String... strings) {
//            if(paymentFrequency.isChecked()){
//                frequency=1;
//            }else {
//                frequency=0;
//            }
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(ApplicationData.END_POINT+"customer/customerLoanRequest/"+ChatActivity.APPLICATION_ID+"/"+loanValue+"/"+TENURE+"/"+"1")
                    .build();

            try{
                Log.i("URL",request.url().toString());
                Response response = client.newCall(request).execute();
                String resData=response.body().string();
                loanRequest = new Gson().fromJson(resData,LoanRequest.class);
                Log.i("EMI",resData);
                return null;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }
    public class StartOver extends AsyncTask<String, String ,String>{

        Intent intent;

        public StartOver(Intent intent) {
            this.intent = intent;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            finish();
            ppd.dismiss();
            startActivity(intent);
        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(ApplicationData.END_POINT+"customer/getCustomerDetails/"+validateOTPResponse.getData().getPhoneNumber())
                    .build();
            try{
                Response response = client.newCall(request).execute();
                validateOTPResponse=new Gson().fromJson(response.body().string(),ValidateOTPResponse.class);
                return response.body().string();
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ChatActivity.chatAdapter.notifyDataSetChanged();
    }
}
