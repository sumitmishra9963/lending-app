package com.findeed.Bot.Services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.findeed.Bot.ChatActivity;

import java.util.Random;

import in.finbox.mobileriskmanager.FinBox;

public class FinboxService extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {


        return null;
    }


    @Override
    public void onCreate() {
        super.onCreate();

        String aKey="1JfXsGdNlS3CLHwLLIzX2a5eCSNHOyk3V0KfLyIb";
        String borrowerId= ChatActivity.APPLICATION_ID;
        Log.i("AID",ChatActivity.APPLICATION_ID);


        try{
            FinBox.createUser(aKey, borrowerId, new FinBox.FinBoxAuthCallback() {
                @Override
                public void onSuccess(String accessToken) {
                    Log.w("Sucess", accessToken);
                    FinBox finbox = new FinBox();
                    finbox.startPeriodicSync();

                }
                @Override
                public void onError(int error) {
                    Log.w("Error", String.valueOf(error));

                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    private String getRequestId() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 10) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }

        return salt.toString();
    }


}
