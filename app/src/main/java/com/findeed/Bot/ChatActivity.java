package com.findeed.Bot;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.findeed.API.AadhaarAndPAN.RequestData.SaveDocument;
import com.findeed.API.AadhaarAndPAN.ResponseData.SaveDocumentResponse;
import com.findeed.API.AddNewEmployerRequest;
import com.findeed.API.Children;
import com.findeed.API.ClassData2;
import com.findeed.API.ClassInSchool;
import com.findeed.API.ClassInformation;
import com.findeed.API.CompanyNameData;
import com.findeed.API.CreateLoanData;
import com.findeed.API.CustomerVoucher;
import com.findeed.API.Data;
import com.findeed.API.DocInfoExtract.VoterIdPayload;
import com.findeed.API.DocumentData;
import com.findeed.API.ErrorList;
import com.findeed.API.GetLivelinessResult;
import com.findeed.API.LivelinessReq;
import com.findeed.API.Loan;
import com.findeed.API.LoanAmount;
import com.findeed.API.LoanFlags;
import com.findeed.API.LoanRequest;
import com.findeed.API.LoanType;
import com.findeed.API.LocationService;
import com.findeed.API.MsiteFetchDataRequest;
import com.findeed.API.PanDetails;
import com.findeed.API.PanRequestPayload;
import com.findeed.API.PincodeList;
import com.findeed.API.RequestVoterIdDetail;
import com.findeed.API.SaveChildResponse;
import com.findeed.API.SaveChildSchoolResponse;
import com.findeed.API.SaveCustomerAddressRequest;
import com.findeed.API.SaveDocumentDetailsRequest;
import com.findeed.API.SaveEmailRequest;
import com.findeed.API.SchoolData;
import com.findeed.API.SendWhatsappNumber;
import com.findeed.API.UserAddress;
import com.findeed.API.ValidateOTPResponse;
import com.findeed.API.VerifyUserIdDocRequestPayload;
import com.findeed.API.VerifyVoterIdRequestPayload;
import com.findeed.API.Voucher;
import com.findeed.API.VoucherResponseData;
import com.findeed.API.customerAllLoanDetailsData;
import com.findeed.ApplicationData;
import com.findeed.Bot.Adapters.ChatAdapter;
import com.findeed.Bot.Adapters.CompaniesNames;
import com.findeed.Bot.Adapters.ListViewAdapter;
import com.findeed.Bot.Adapters.ListViewCompaniesAdapter;
import com.findeed.Bot.Adapters.SchoolNames;
import com.findeed.Bot.Services.FinboxService;
import com.findeed.Bot.ViewHolder.ChatObject;
import com.findeed.Bot.ViewHolder.ChatResponseVH;
import com.findeed.Bot.ViewHolder.ContactData;
import com.findeed.CameraActivity;
import com.findeed.ContactActivity;
import com.findeed.KYC.VideoIdKYCREq.MsiteFetchRequest;
import com.findeed.KYC.VideoIdKYCREq.headers;
import com.findeed.KYC.VideoIdKycRes.KYCInfo;
import com.findeed.KYC.VideoIdKycRes.OriginalKYCInfo;
import com.findeed.KYC.VideoIdKycRes.ResponseData;
import com.findeed.MyProfile;
import com.findeed.NotificationActivity;
import com.findeed.PreKYC.AddNewSchool;
import com.findeed.PrivacyActivity;
import com.findeed.R;
import com.findeed.TabLayoutActivity;
import com.findeed.TermsActivity;
import com.findeed.Voucher.VoucherChatActivity;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.khosla.base.FaceAuthResults;
import com.khosla.facesdk.FaceAuthRequestImage;
import com.khosla.facesdk.FaceAuthResponse;
import com.khosla.facesdk.init.FaceAuthInitActivity;
import com.khosla.facesdk.init.FaceAuthInitRequest;
import com.warkiz.widget.IndicatorSeekBar;
import com.warkiz.widget.OnSeekChangeListener;
import com.warkiz.widget.SeekParams;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.findeed.ApplicationData.CLIENT_CODE;
import static com.findeed.ApplicationData.KHOSALA_URL;
import static com.findeed.ApplicationData.isStartOverClicked;
import static com.findeed.ApplicationData.validateOTPResponse;
import static java.lang.Float.*;


@SuppressWarnings("ALL")

public class ChatActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, ChatContract.View ,AdapterView.OnItemSelectedListener,View.OnFocusChangeListener, View.OnClickListener, Cloneable {



    public  static Data CUSTOMER_DETAILS;
    public  static Data CUSTOMER_DETAILS_PERSISTANCE;
    public  static Data CUSTOMER_DETAILS_PERSISTANCE2;
    public static Loan loanClass=new Loan();
    public static Loan loanClass2=new Loan();
    public static LoanType loanTypeClass=new LoanType();
    public  static ArrayList < Loan > loansArray = new ArrayList<> ();
    public  static ArrayList <Children> childArray = new ArrayList<> ();
    public  static ArrayList <DocumentData> documentArray = new ArrayList<> ();
    public  static LoanFlags loanFlags = new LoanFlags();
    public  static ArrayList <Voucher> voucherArray = new ArrayList<> ();
    public static Children children=new Children();
    public ErrorList errorList=new ErrorList();
    public  ArrayList <ErrorList> errorArray = new ArrayList<> ();
    public static SchoolData schoolData=new SchoolData();
    public static ClassInformation classInformation=new ClassInformation();

    public static ArrayList < Loan > loansArray2 = new ArrayList<> ();

    public static  Loan loanClass3=new Loan();
    public static ArrayList<DocumentData> docArray=new ArrayList<>();
    public static ArrayList<SchoolData> sd;
    public static ClassInSchool classInSchool=new ClassInSchool();
    ArrayList<ClassData2> classInSchoolsList=new ArrayList<ClassData2>();
    public static String ClassValue=new String();
    public static ClassData2 cd2=new ClassData2();
    public static ClassData2 cd3=new ClassData2();



    public static ListView listView;
    public static ListView listView2;
    public static TextView textView;
    public static String[] listclasses;

    HashMap<String,Integer> docStatus=new HashMap<>();

    public static  ArrayList<RepaymentSchedule> repaymentList;
    String latefee,fee;
    public static String Voucher1=new String();
    ProgressDialog savePan ;







    String END_POINT =ApplicationData.END_POINT;
    Gson gson = new Gson();
    public static  String  MARITAL_STATUS;
    public static int mine=0;
    static String phone;
    ProgressDialog cChat;

    int PAN_UPLOAD_NUMBER;
    int CUSTOMER_AGE_RESTRICTION;
    int CCS_PERMISSION_DENIED;
    int BUREAU_PERMISSION_DENIED;
    int PIN_CODE_VERIFICATION;
    int GOVT_VERIFICATION_PAN;
    int STABILITY_OFFICE_PREMISES;
    int SCHOOL_CHECK;
    int EMPLOYER_CHECK;
    int PAN_AADHAR_MATCH;
    int MINIMUM_SALARY;
    int CREDIT_RULES_STATUS;
    int PAN_OCR_FAILED;
    int IS_PAN_EDITED;
    int VOTER_OCR_FAILED;
    int IS_VOTER_EDITED;
    int GOVT_VERIFICATION_VOTER;




    public BroadcastReceiver locationUpdateReceiver;

    public BroadcastReceiver predictedLocationReceiver;
    ProgressDialog pd;


    //  public static HashMap<String, Data> chatData=new HashMap<>();


    public  static  ChatActivity chatActivity;

    //    HashMap<String ,Integer> flags=new HashMap<>();
    final ArrayList<SchoolNames> arraylist = new ArrayList<>();

    Map<String,Integer>SchoolId=new HashMap<>();
    Map<String,Integer>companyId=new HashMap<>();
    int schoolPos;
    int PAN_COUNT=0;
    int PAN_CHECK=0;
    float ELIGIBLE_AMOUNT;

    /*Persistance variable*/

    boolean isLoanTypeClicked = false;
    boolean isChildSelected = false;
    boolean isRequestedAmountSelected = false;
    boolean isTenureSelected = false;
    boolean isEmploymentTypeSelected = false;
    boolean isIncomeRangeClicked = false;
    boolean isPanSubmitted = false;
    boolean isAddressProofSubmitted = false;
    boolean isCurrentAddressSame=false;
    boolean isGasOrElectricityBill=false;
    boolean isAddressSame=false;
    boolean isLoanDetailsSubmitted;
    boolean isCcsPermissionGranted;
    boolean isLoanSummarySaved;
    boolean isEmiSummarySave;
    boolean isVocherSaved;
    boolean isLoanAgreementAccepted;
    boolean isFaceAuthSave;
    boolean isWhatsAppSavd;
    boolean isNameGiven;
    boolean isEmployeeIdSaved;
    boolean isEmailSaved;
    boolean isWorkingYearSaved;
    boolean isSalaryRangeSelected;
    boolean isChildSelectedPersonal;
    boolean isMaritalStatusSaved;
    boolean isLoanRequirementSaved;
    boolean isSeekChanged;
    boolean isLayoutChangeCalled;
    boolean isCreditStatusTrue;
    boolean isVoucher1Verified;
    boolean isVoucher2Verified;
    boolean isEmployerVerified;
    boolean isSchoolVerified;


//    public LocationService locationService;

    /*Flags*/

    boolean isBureauAgreeClicked=false;


    /*Activity Result code*/

    static final int PAN_PICK_PHOTO = 111;
    static final int CAMERA_PERMISSION = 9;
    static final int CLICK_VOTER_PHOTO_PERMISSION = 298;
    static final int TAKE_AADHAR_PHOTO = 222;
    static final int REQUEST_CODE_ASK_PERMISSIONS = 121;
    static final int FACE_AUTH_CODE = 786;
    static final int CONTACT_PICKER_REQUEST = 587;
    static final int PAN_BACK_CLICK_PERMISSION=14;
    static final int CLICK_VOTER_PHOTO = 166;
    static final int REQUEST_TAKE_PHOTO_PAN = 25;
    static final int REQUEST_TAKE_PHOTO_PAN_BACK=15;
    static final int REQUEST_PICK_PHOTO_PAN_BACK=16;
    static final int REQUEST_TAKE_PHOTO_VOTER_FRONT = 2;
    static final int REQUEST_TAKE_PHOTO_VOTER_BACK = 3;
    static final int REQUEST_PICK_PHOTO_VOTER_FRONT = 4;
    static final int REQUEST_PICK_PHOTO_VOTER_BACK = 5;
    static final int TERMS_AND_CONDITIONS = 8;
    static final int LOCATION_PERMISSION = 10;
    static final int READ_CONTACT=13;
    static final int CLICK_GAS=14;
    static final int PICK_GAS=17;
    static final int CLICK_GAS_PERMISSION=18;
    static final int CLICK_ELECTRICITY=19;
    static final int CLICK_ELECTRICITY_PERMISSION=20;
    static final int PICK_ELECTRICITY=21;
    static final int REQUEST_LOCATION=22;
    static final int CONTACT_PICK=734;

    final int PIC_CROP = 2;
    Double INTREST_RATE;
    int PRINCIPAL_AMOUNT;




    /*User data*/
    RadioButton paymentFrequency;
    Double latitude=null;
    Double longitude=null;
    String cityName, addressLine,pin,loc;
    public  int TENURE;
    // public static int DEPENDENT_ID;
    public static String APPLICATION_ID;
    public String USER_PIN_CODE;
    public String customerApplicationId;
    public static int CUSTOMER_ID;
    public  String VOTER_ADDRESS;
    public  String VOTER_DOCUMENT_ID;
    public  String VOTER_FIRST_NAME;
    public  String VOTER_LAST_NAME;
    public  String VOTER_DOB;
    public  String VOTER_GENDER;
    public  String AADHAR_ADDRESS;
    public  String AADHAR_DOC_ID;
    public  String AADHAR_FIRST_NAME;
    public  String AADHAR_LAST_NAME;
    public  String AADHAR_DOB;
    public  String AADHAR_GENDER;
    public  String USER_FACE;
    public  Float matchedValue;

    public static Bitmap PanFrontBitmap;
    public static Bitmap PanBackBitmap;
    public static Bitmap VoterFrontBitmap;
    public static Bitmap VoterBackBitmap;
    public static Bitmap AadhaarFrontBitmap;
    public static Bitmap GasBillBitmap;
    public static Bitmap ElectricityBillBitmap;



    public  Bitmap AADHAR_FRONT, AADHAR_BACK;
    public  Bitmap VOTER_FRONT, VOTER_BACK;
    public Bitmap GAS_BILL,ELECTRICITY_BILL;
    private  int LOAN_TYPE;
    public  int NUMBER_OF_CHILDREN;
    public  String EMPLOYMENT_TYPE = null;
    public int EMI_RATE;
    public static    int SCHOOL_ID;
    public static String userLocation;
    int step = 1000;
    File currentPhotoPath;
    public  static Uri panURI, voterBackURI, voterFrontURI,panBackUri,gasUri,electricityUri;
    public static int  tempTenure;
    public String  emiRate;


    public boolean next = true;
    public static boolean isEditPanClicked = false;
    public static boolean isEditVoterClicked = false;
    public static boolean isAadhaarEditClicked = false;
    public boolean isGovtCheckFailPan=false;
    public static String DOC_TYPE;
    public static String className;
    public static String LoanTenure;
    public static boolean flag = false;
    public static String schoolName = null;
    public static boolean isNumberPickerValueChanged = false;
    public static boolean isIsNumberPickerValueChangedRelation = false;
    public static boolean isLocationCheck = false;
    public static boolean isContactCheck = false;
    public static boolean isSMSCheck = false;
    public static boolean isDeviceCheck = false;
    public static boolean chooseedAadhar = false;
    public static boolean doubleBackToExitPressedOnce = false;
    public String sex;
    String vDob;
    String PAN_DOB;





    public  OriginalKYCInfo UserAddressData;
    public  String selectedValue;
    public  int selectedIndex;
    public  int selectedLoanAmount;
    public static LinearLayout llayoutNoPanCard;
    public  String panNumber, panName, panDob;
    ScrollView AllAboutLoan;
    ProgressBar progressBar, progressBarVote;
    public static RelativeLayout llAddSchoolRequest;
    public  String  base64String;


    // View
    private RecyclerView rvChatList;
    public static ChatAdapter chatAdapter;
    private ArrayList<ChatObject> chatObjects;
    public static ArrayList<CompaniesNames> companyList;
    public static double  TOTAL_INTEREST=0.0;



    /*child layout*/
    TextView txtone;
    TextView txttwo;
    TextView txtmoretwo;
    TextView txtthree;
    TextView txtfour;
    TextView txtfifth;
    TextView childFirstNameError;
    TextView childSurnameError;
    TextView childClassError;
    LinearLayout llayoutchildmore;
    public static LinearLayout llayoutchildcount, childLayout;
    public static Button addChild;
    public static TextView childHeader;
    public  EditText firstname2, surname2;

    public  String ADDRESS_LINE_1,ADDRESS_LINE_2,CITY,PINCODE,LOCALITY;



    /*company layout*/
    CompanyNameData companyNameList;
    public static ListViewCompaniesAdapter listViewCompaniesAdapter;


    // Presenter
    public static ChatPresenter presenter;
    public static AutoCompleteTextView actv;


    /*uri's*/
    private Uri outputFileUri;


    /* List views layouts*/
    private ListView listv;
    public static LinearLayout lvlay;


    /*seekBar layouts*/
    public  LinearLayout llayoutseekbar;
    public LinearLayout llayoutseekbarSomeOne;
    public LinearLayout llayoutseekbarSelfEmployee;
    TextView textaddseekbar;
    TextView txtaddseekbarWorkForSomeOne;
    TextView txtaddseekbarSelfEmployee;


    public static LinearLayout loanType;
    boolean isOpen = false;
    LinearLayout llayoutFillAddressManually;
    public static Button videoRecording;




    public  LinearLayout llayoutwhatyoudo, panCardLayout,gasOption;

    Button btn_workforsomeone, btn_selfemployee, btn_nowork;
    public  FrameLayout rlWhereDoYouWork;

    public  RelativeLayout rlWhatIsYourBussiness;
    public  EditText whatIsYourBussiness;
    public  TextView bussinessTypeError;
    public  String bussinessType;
    public  RelativeLayout rlWhatIsYourBussinessName;
    public  EditText whatIsYourBussinessName;
    public  String bussinessName;

    public static Context ctx;
    int USER_AGE;
    float maxEligibleAmount;



    NumberPicker numberPicker;
    private DrawerLayout dl;
    private ActionBar actionBar;
    private NavigationView nv;


    LinearLayout llayoutcallus;
    LinearLayout llayoutnoidoption;
    LinearLayout llayoutprocessingvoterid;
    LinearLayout llayoutAddressManually;
    LinearLayout llayoutVoucherVerification;
    LinearLayout layout_Btn_Personal;
    LinearLayout layout_Btn_Educational;
    public  LinearLayout llayoutyesorno;
    LinearLayout llayoutAdditionalInfo;
    public LinearLayout llayoutaadharpicture;
    public LinearLayout llayoutneedhelp;
    public LinearLayout llayoutaadhardetailedit;


    TextView profileName;
    public LinearLayout llIncomeRange;
    public LinearLayout llayoutoptionselectforvoterid;
    public LinearLayout llayoutoption;
    public LinearLayout llayoutvoterdetailedit;
    public LinearLayout llayoutagreeorcancel;
    Button btneditinfo;
    Button btnconfirm;
    Button btntakeaadharphoto;
    Button cancelAadhar;
    Button btneditinfoaadhar;
    Button btnidconfirmaadhar;
    TextView txtneedhelp, txtnooption;
    Button clickVoterFront;
    Button pickVoterFront;
    Button btncontinuevoterid;
    Button imgvoterid;
    Button btneditvoterid;
    Button btnconfirmvoterid;
    Button btnagree;
    Button btnCancelBureau;
    Button btnManually;
    Button btnCurrentLocation;
    Button addAddressManually;
    Button btnESign;
    ImageView imagechoose;
    TextView txtNoGreen;
    TextView txtRed;
    Button  btnFileUpload;
    Button btnMonthlyEmi;
    Button btnWeeklyEmi;
    Button btnSelectVoucher;
    Button btnVoucherContinue;
    Button viewLoanDetails;
    Button btnVideoContinue;
    Button btnVoucherVerification;
    Button btnNotifySecond;
    Button btnAgreeandContinue;
    Button btnAgreeandContinueTwo;
    Button WillUploadPan;
    public static Button saveMyChoice;
    ImageView imgone, imgtwo;
    public  IndicatorSeekBar Indicator_seekBar;
    public IndicatorSeekBar seekBarSelfEmployed;
    public IndicatorSeekBar seekBarWorkForSomeOne;
    public static FrameLayout actvFrameLayout;
    public static ImageView btnActvClear;
    public static MediaPlayer mp;
    LinearLayout iHavePan, iDontHavePan;
    Button btnaddRange;
    public String HASH;
    EditText etname, etarea, etcity;
    LinearLayout lLayoutProcessingPan;
    RelativeLayout lLayoutUpdateAadharInfo;
    ScrollView lLayoutUpdatePanInfo,employmentLayout;

    LinearLayout doYouHaveKid;
    LinearLayout yesIdo;
    LinearLayout noIdoNot;




    Button clickPanFront;
    Button btnPanPickPhoto;
    Button btnSavePanDetail;
    Button btnSaveAadharDetail;
    Bitmap bitmap;
    public  Button btnPanContinue;
    ImageView imgKaran, imgKunal;
    View lineView;
    ImageView iconRestart;
    //ScrollView loanSummary;
    Button agreeAndContinue, verifiedScreen;
    LinearLayout voucherVerified;
    public  LinearLayout llAadharDetails;

    Button buttonLoanSummary, loanTenureButton;
    public  LinearLayout loanTenure;
    public LinearLayout lLayoutPanOption;
    public LinearLayout lLayoutEditPanInfo;
    public LinearLayout clickPickForVoterBack;
    public LinearLayout clickPickPanBack;

    TextView selectChildClass;
    BottomSheetDialog testdialog;
    BottomSheetBehavior mBehavior;
    TextView firstStand;
    TextView secondStand;
    TextView thirdStand;
    TextView fourthStand;
    TextView fifthStand;
    TextView sixStand;
    TextView sevenStand;
    TextView eightStand;
    TextView nineStand;
    TextView tenStand;
    Button selectClass;
    ImageView childClear;
    ImageView okFirst;
    ImageView okSecond;
    ImageView okThird;
    ImageView okFour;
    ImageView okFifth;
    ImageView okSix;
    ImageView okSeven;
    ImageView okEight;
    ImageView okNine;
    ImageView okTen;
    ImageView useCurrentLocationCross;
    BottomSheetDialog dialogLoan;
    ImageView imagepan, imagenopan;
    EditText panDobEditText, panIdEditText, panNameEditText;
    RelativeLayout llLoanTenureSelect;
    RelativeLayout llLoanTenureFinalSelect;
    TextView threemonth;
    TextView sixmonth;
    TextView ninthmonth;
    TextView twelvemonth;
    TextView eighteenmonth;
    ImageView tickthree;
    ImageView ticksix;
    ImageView tickninth;
    ImageView ticktwelve;
    ImageView tickeighteen;
    ImageView loanClear;
    Button btnLoanTenure;
    TextView showTenure, showTenureFinal;

    LinearLayout btnMoreInfoYes, btnMoreInfoNo,sendingNotification;
    ImageView imgDeclineNoify, imgAgreeNotify;
    LinearLayout whatsAppInput;
    ImageView imageNo, imageYes;
    LinearLayout btnaadhar, btnvoterid,employmentLengthLayout;
    ImageView imageVoter, imageAadhar;

    public  LinearLayout llayoutpantext;
    public  RelativeLayout llUpdateVoterInfo;
    EditText voterIdEditText;
    EditText voterNameEditText;
    EditText voterDobEditText;
    EditText voterAddressEditText;
    EditText edtVoterState;
    EditText edtVoterPin;
    Button saveVoterDetail;
    ImageView btnClearAddSchool;
    ImageView numberPickerDownImagePink;
    ImageView numberPickerDownImageGray;
    ImageView numberpickerUpimageGray;
    ImageView numberPickerUpimagePink;
    ImageView imgCurrLocUp;
    ImageView imgCurrLocDown;
    ImageView imgContactUp;
    ImageView imgContactDown;
    ImageView imgSmsUp;
    ImageView imgSmsDown;
    ImageView imgDeviceInfoup;
    ImageView imgDeviceInfoDown;
    TextView txtCurrLoc;
    TextView txtContact;
    TextView txtSms;
    TextView txtDeviceInfo;
    TextView txtCurrentLocation;
    LinearLayout llCurrentLocation;
    LinearLayout llContact;
    LinearLayout llSMS;
    LinearLayout llDevice;
    ImageView tickLocation;
    ImageView tickContact;
    ImageView tickSMS;
    ImageView tickDevice;
    ImageView panCross;
    ImageView clearAadharEdit;
    ImageView clearVoterEdit;
    EditText editAadharName;
    EditText editAadharGender;
    EditText editAadhaarDob;
    EditText editAadharAddress;
    IndicatorSeekBar eligibileLoanBar;
    TextView seekBarUpperLimit;
    LinearLayout btnyes, btnno;
    ImageView imgyess, imgnoo;
    Button btnEditLoanPayment;
    Button btnAgreeLoanPayment;
    LinearLayout llEMIAmountEMITenure;
    LinearLayout llShowApprovedLoanAmount;
    LinearLayout llShowLoanTenureFinalSelect,maritalStatus;
    LinearLayout electricityLayout,useMyCurrentLocationLayout,electricityOption;
    Button btnSaveEMIAmount;
    ScrollView SVFinalDisclosure;
    ImageView clearCurrAdd;
    Button agreeDisclosure;
    Button btnIwantToChange;
    Button btnFaceAuth;
    Button pickVoterBack;
    Button clickVoterBack;
    Button select2ndVoucher;
    TextView findeedProcessingFee;
    TextView schoolNameTv;
    TextView areaTv;
    TextView cityTv;
    TextView firstNameTV;
    TextView lastNameTv;
    TextView classTv;
    TextView voterIdTV;
    TextView voterNameTV;
    TextView voterGenderTV;
    TextView voterdobTV;
    TextView voterAddTV;
    EditText addressLineOne;
    EditText locality;
    EditText city;
    EditText pinCode;
    TextView currentAddressError;
    TextView currentLocalityError;
    TextView currentCityError;
    TextView currentPinError;
    TextView corrospondingAddressTV;
    TextView corrospondingAreaTV;
    TextView corrospondingCityTV;
    TextView corrospondingPincodeTv;
    LinearLayout userEmailLayout,doNotHaveEmail;
    RelativeLayout rlFirstStand;
    RelativeLayout rlSecondStand;
    RelativeLayout rlThirdStand;
    RelativeLayout rlFourStand;
    RelativeLayout rlFifthStand;
    RelativeLayout rlSixStand;
    RelativeLayout rlseventhstand;
    RelativeLayout rleightstand;
    RelativeLayout rlninthstand;
    RelativeLayout rltenstand;
    RelativeLayout rl3months;
    RelativeLayout rl6months;
    RelativeLayout rl9months;
    RelativeLayout rl12months;
    Button loanDisclosure,clickPanBack,pickPanBack;
    EditText businessEdittext;

    RadioGroup radioGroup,radioGroupSex;
    RadioButton rbWeekly, rbMonthly;
    TabLayout tabLayout;
    ViewPager viewPager;
    RelativeLayout eduEmailLayout;
    EditText currentAddressLineOne;
    EditText currentLocality;
    EditText currentCity;
    EditText currentPincode;
    Button saveCurrentLocation;
    Button getBackLater,requestEmployerAddProceed;
    Button gasContinue;
    Button electricityContinue;
    public static LinearLayout rlCoachMarks;
    public static FrameLayout frameLayout;
    Button iHavePanNow;
    RadioButton rbMale, rbFemale, rbOther;
    DatePickerDialog picker;
    TextView tvProgressLaon;

    String companyName,EMPLOYMENT_AGE;
    TextView tvErrorSchoolName;
    TextView tvErrorSchoolArea;
    TextView tvErrorSchoolCity;
    EditText employerName,employerArea,employerCity ;

    EditText firstName;
    EditText lastName;
    public static String sName;

    PincodeList pincodeList ;
    ActionBarDrawerToggle actionBarDrawerToggle;

    String USER_NAME;




    public static LinearLayout companyLayout;
    public  ListView companyListView;
    public  FrameLayout searchEmployer;
    public  AutoCompleteTextView employerSearchBar;
    public ImageView employerClear;
    public ScrollView userDetailLayout;
    ImageView imgNoKid, imgYesKid;
    public  static TextView emiRateFindeed;
    RadioButton frequencyWeekly;
    boolean isItforSchool=false;

    /*Loan eligibility summary*/
    TextView estimatedEMI;
    TextView estimatedLoanTenure;
    TextView estimatedPaymentFreq;
    TextView requiredLoanAmount;
    ImageView numPicDownPink;
    ImageView numPicDownGray;
    ImageView numPicUpPink;
    ImageView numPicUpGray;
    Toolbar myToolbar;
    public static LinearLayout whatsApp;

    ProgressDialog billDialog;
    boolean isPinMatch;


    TextView voterIdError;
    TextView voterNameError;
    TextView voterAddressError;
    TextView voterGenderError;
//    ImageView numPicDownPink, numPicDownGray, numPicUpPink, numPicUpGray;
//    Toolbar myToolbar;






    //validations
    TextView panIdValidations;
    TextView panNameValidations;
    TextView panDOBValidations;
    TextView employerNameError;
    TextView employerCityError;
    TextView employerAreaError;
    TextView emiLateFee;
    TextView emiProcessingFee;

    TextView applicantFirstNameErr;
    TextView applicantSurNameErr;
    ImageView imgAccYes, imgAccNo;
    LinearLayout btnAccYes, btnAccNo;

    // layout Bank Detail
    EditText etAccNumber, etReEnterNumber, etAccHolder, etIfscCode;
    TextView errAccNumber, errReEnterNumber, errAccHolder, errIfscCode, tvBankHeader;
    Button btnAddBankAcc;

    // layout Token Amount

    EditText etTokenAmount;
    Button btnVerifyToken;

    LinearLayout botMainActivity,permissionInPersistence;

    // layout amount transfer

    TextView tvAmountTransferred, tvSchoolBank, tvBankName, tvTransactionDate;

    // Salary Account layout (Personal Flow)

    TextView tvBankAccountP, tvBankNameP, tvIfscP;

    // Layout Show Transferred Amount (Personal)

    TextView tvShowAmountP, tvShowAccountP, tvShowBankP, tvShowIfscP;

    ImageView imgNoEmail, imgNoApplicable;
    LinearLayout tvNotApplicable;




    // Start

    private void hideKeyboard() {
//        View view = this.getCurrentFocus();
//        if (view != null) {
//            view.clearFocus();
//            InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
//            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
//        }


        InputMethodManager imm =(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(findViewById(android.R.id.content).getRootView()
                .getWindowToken(), 0);
    }

    public static String sha256(String base) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    private String getRequestId() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 10) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }


    public Object clone() throws CloneNotSupportedException{
        return super.clone();
    }

    @SuppressLint("ClickableViewAccessibility")
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bot_main_activity);
        // mp = MediaPlayer.create(this, R.raw.recieved_msg);
        chatActivity=this;
        this.ctx=getApplicationContext();
        new GetSchoolList().execute();
        new GetCompanyList().execute();
        new GetPinCodes().execute();

        ApplicationData.isItVoucher=false;

//        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
//            @Override
//            public void uncaughtException(Thread thread, Throwable ex) {
//                //inform yourself that the activity unexpectedly stopped
//                //or
//                Log.d("@@@@uncaughtException@@@",ex.toString());
//            }
//        });




        final String[] values= { getString(R.string.Rs) + " 10,000  " + getString(R.string.dash) + "    " + getString(R.string.Rs) + " 11,000", getString(R.string.Rs) + " 11,000  " + getString(R.string.dash) + "    " + getString(R.string.Rs) + " 12,000", getString(R.string.Rs) + " 12,000  " + getString(R.string.dash) + "    " + getString(R.string.Rs) + " 13,000", getString(R.string.Rs) + " 13,000  " + getString(R.string.dash) + "    " + getString(R.string.Rs) + " 14,000", getString(R.string.Rs) + " 14,000  " + getString(R.string.dash) + "    " + getString(R.string.Rs) + " 15,000", getString(R.string.Rs) + " 15,000  " + getString(R.string.dash) + "    " + getString(R.string.Rs) + " 16,000", getString(R.string.Rs) + " 16,000  " + getString(R.string.dash) + "    " + getString(R.string.Rs) + " 17,000", getString(R.string.Rs) + " 17,000  " + getString(R.string.dash) + "    " + getString(R.string.Rs) + " 18,000", getString(R.string.Rs) + " 18,000  " + getString(R.string.dash) + "    " + getString(R.string.Rs) + " 19,000", getString(R.string.Rs) + " 19,000  " + getString(R.string.dash) + "    " + getString(R.string.Rs) + " 20,000", getString(R.string.Rs) + " 20,000  " + getString(R.string.dash) + "    " + getString(R.string.Rs) + " 21,000", getString(R.string.Rs) + " 21,000  " + getString(R.string.dash) + "    " + getString(R.string.Rs) + " 22,000",getString(R.string.Rs) + " 22,000  " + getString(R.string.dash) + "    " + getString(R.string.Rs) + " 23,000",getString(R.string.Rs) + " 23,000  " + getString(R.string.dash) + "    " + getString(R.string.Rs) + " 24,000",getString(R.string.Rs) + " 24,000  " + getString(R.string.dash) + "    " + getString(R.string.Rs) + " 25,000",getString(R.string.Rs) + " 25,000  " + getString(R.string.dash) + "    " + getString(R.string.Rs) + " 26,000",getString(R.string.Rs) + " 26,000  " + getString(R.string.dash) + "    " + getString(R.string.Rs) + " 27,000",getString(R.string.Rs) + " 27,000  " + getString(R.string.dash) + "    " + getString(R.string.Rs) + " 28,000", getString(R.string.Rs) + " 28,000  " + getString(R.string.dash) + "    " + getString(R.string.Rs) + " 29,000",getString(R.string.Rs) + " 29,000  " + getString(R.string.dash) + "    " + getString(R.string.Rs) + " 30,000",getString(R.string.Rs) + " 30,000  " + getString(R.string.dash) + "    " + getString(R.string.Rs) + " 31,000",getString(R.string.Rs) + " 31,000  " + getString(R.string.dash) + "    " + getString(R.string.Rs) + " 32,000",getString(R.string.Rs) + " 32,000  " + getString(R.string.dash) + "    " + getString(R.string.Rs) + " 33,000",getString(R.string.Rs) + " 33,000  " + getString(R.string.dash) + "    " + getString(R.string.Rs) + " 34,000",getString(R.string.Rs) + " 34,000  " + getString(R.string.dash) + "    " + getString(R.string.Rs) + " 35,000",getString(R.string.Rs) + " 35,000  " + getString(R.string.dash) + "    " + getString(R.string.Rs) + " 36,000",getString(R.string.Rs) + " 36,000  " + getString(R.string.dash) + "    " + getString(R.string.Rs) + " 37,000",getString(R.string.Rs) + " 37,000  " + getString(R.string.dash) + "    " + getString(R.string.Rs) + " 38,000", getString(R.string.Rs) + " 38,000  " + getString(R.string.dash) + "    " + getString(R.string.Rs) + " 39,000", getString(R.string.Rs) + " 39,000  " + getString(R.string.dash) + "    " + getString(R.string.Rs) + " 40,000", getString(R.string.Rs) + ">40000"};

        loanType = findViewById(R.id.loanSelection);
        llayoutchildcount = findViewById(R.id.llchildcount);
        llayoutchildmore = findViewById(R.id.llchildtwoplus);
        txtone = findViewById(R.id.circle_one);
        txttwo = findViewById(R.id.circle_two);
        txtmoretwo = findViewById(R.id.circle_twoplus);
        txtthree = findViewById(R.id.circle_three);
        txtfour = findViewById(R.id.circle_four);
        txtfifth = findViewById(R.id.circle_fifth);
        llayoutseekbar = findViewById(R.id.llseekbar);
        //  simpleseekbar = findViewById(R.id.simpleSeekBar);
        textaddseekbar = findViewById(R.id.txt_add_seekbar);
        llayoutwhatyoudo = findViewById(R.id.llwhatdoyou);

        btn_workforsomeone = findViewById(R.id.btn_workforsomeone);
        btn_selfemployee = findViewById(R.id.btn_selfemployed);
        btn_nowork = findViewById(R.id.btn_notworking);

        rlWhereDoYouWork=findViewById(R.id.fl_search_employer);

        rlWhatIsYourBussiness=findViewById(R.id.bussinessTypeLayout);
        whatIsYourBussiness=findViewById(R.id.bussinessType_edittext);
        bussinessTypeError=findViewById(R.id.bussinesss_type_error);


        rlWhatIsYourBussinessName=findViewById(R.id.bussinessNameLayout);
        whatIsYourBussinessName=findViewById(R.id.businessname_edittext);



        //wheredoyouwork=findViewById(R.id.companyname_edittext);

        rvChatList = findViewById(R.id.rv_chat);
        actv = findViewById(R.id.actv);
        lvlay = findViewById(R.id.lvlay);
        TextView lvbutn = findViewById(R.id.lvbutn);
        iHavePan = findViewById(R.id.btn_pancard);
        iDontHavePan = findViewById(R.id.btn_nopancard);
        btnaddRange = findViewById(R.id.btn_addRange);
        lLayoutProcessingPan = findViewById(R.id.llprocessingpan);
        lLayoutUpdateAadharInfo = findViewById(R.id.ll_UpdateAadharInfo);
        childLayout = findViewById(R.id.lvchild2);
        panCardLayout = findViewById(R.id.panCradLinerLayout);
        numberPickerDownImagePink = findViewById(R.id.numberpickerdownimagepink);
        numberpickerUpimageGray= findViewById(R.id.numberpickerUpimagegray);
        numberPickerDownImageGray = findViewById(R.id.numberpickerdownimagegray);
        numberPickerUpimagePink = findViewById(R.id.numberpickerUpimagepink);
        btneditinfo = findViewById(R.id.btn_editinfo);
        btnconfirm = findViewById(R.id.btn_idconfirm);
        llayoutneedhelp = findViewById(R.id.llneedhelp);
        txtneedhelp = findViewById(R.id.needhelp);
        llayoutcallus = findViewById(R.id.llcallus);
        llayoutnoidoption = findViewById(R.id.llnoidoptiontext);
        llayoutoption = findViewById(R.id.llnooptionselect);
        btnaadhar = findViewById(R.id.btn_aadhar);
        btnvoterid = findViewById(R.id.btn_voterid);
        txtnooption = findViewById(R.id.txtnooption);
        llayoutaadharpicture = findViewById(R.id.lloptionselect);
        btntakeaadharphoto = findViewById(R.id.btn_takeaadharphoto);
        cancelAadhar = findViewById(R.id.btn_cancel_aadhar);
        llayoutaadhardetailedit = findViewById(R.id.llaadhardetailedit);
        btnidconfirmaadhar = findViewById(R.id.btn_idconfirmaadhar);
        btneditinfoaadhar = findViewById(R.id.btn_editinfoaadhar);
        llayoutoptionselectforvoterid = findViewById(R.id.lloptionselectforvoterid);
        clickVoterFront = findViewById(R.id.btn_takeavoterphoto);
        pickVoterFront = findViewById(R.id.btn_pickvoterphoto);
        btncontinuevoterid = findViewById(R.id.btn_continue_voterid);
        llayoutprocessingvoterid = findViewById(R.id.llprocessingvoterid);
        llayoutvoterdetailedit = findViewById(R.id.llvoterdetailedit);
        btneditvoterid = findViewById(R.id.btn_editinfovoterid);
        btnconfirmvoterid = findViewById(R.id.btn_idconfirmvoterid);
        llayoutagreeorcancel = findViewById(R.id.llagreeorcancel);
        btnagree = findViewById(R.id.btn_agree);
        btnCancelBureau = findViewById(R.id.btn_cancel_bureau);
        llayoutyesorno = findViewById(R.id.llyesorno);
        btnyes = findViewById(R.id.btn_yes);
        btnno = findViewById(R.id.btn_no);
        llayoutAdditionalInfo = findViewById(R.id.ll_Additional_Info);
        btnMoreInfoYes = findViewById(R.id.btn_moreinfo_yes);
        btnMoreInfoNo = findViewById(R.id.btn_moreinfo_no);
        llayoutAddressManually = findViewById(R.id.ll_Address_Manually);
        btnCurrentLocation = findViewById(R.id.btn_currentlocation);
        btnManually = findViewById(R.id.btn_manually);
        llayoutFillAddressManually = findViewById(R.id.llfilladdressmanually);
        addAddressManually = findViewById(R.id.btn_add_address_manually);
        saveMyChoice = findViewById(R.id.btn_savemychoice);
        btnFileUpload = findViewById(R.id.btn_fileUpload);
        btnSelectVoucher = findViewById(R.id.btn_select_Voucher);
        btnVoucherContinue = findViewById(R.id.btn_VoucherContinue);
        viewLoanDetails = findViewById(R.id.btn_view_loan_details);
        btnVideoContinue = findViewById(R.id.btn_videoContinue);
        btnVoucherVerification = findViewById(R.id.btn_voucherVerification);
        llayoutVoucherVerification = findViewById(R.id.llvoucherVerification);
        btnNotifySecond = findViewById(R.id.btn_NofifySecond);
        btnESign = findViewById(R.id.btn_ESign);
        /*scrollViewApprovedLoan = findViewById(R.id.SViewApprovedLoan);
        btnAgreeandContinue = findViewById(R.id.btn_AgreeandContinue);*/
        btnAgreeandContinueTwo = findViewById(R.id.btn_AgreeandContinuetwo);
        llayoutNoPanCard = findViewById(R.id.llNoPanCard);
        WillUploadPan = findViewById(R.id.I_Will_Back);
        // BackLater = findViewById(R.id.btn__Will_Upload);
        imgone = findViewById(R.id.imageone);
        imgtwo = findViewById(R.id.imagtwo);
        layout_Btn_Educational = findViewById(R.id.layout2);
        layout_Btn_Personal = findViewById(R.id.layout1);
        Indicator_seekBar = findViewById(R.id.indicator_Seekbar);
        actvFrameLayout = findViewById(R.id.actv_Frame_Layout);
        btnActvClear = findViewById(R.id.clear);
        numberPicker = findViewById(R.id.numberPicker);
        llIncomeRange = findViewById(R.id.llIncomeRange);
        numberPicker.setMinValue(0);
        numberPicker.setMaxValue(values.length - 1);
        numberPicker.setDisplayedValues(values);
        numberPicker.setWrapSelectorWheel(false);

        lLayoutPanOption = findViewById(R.id.llpanoptionselect);
        clickPanFront = findViewById(R.id.btn_takepanphoto);
        btnPanPickPhoto = findViewById(R.id.btn_pickpanphoto);
        btnPanContinue = findViewById(R.id.btn_PanContinue);
        lLayoutEditPanInfo = findViewById(R.id.llpandetailedit);
        lLayoutUpdatePanInfo = findViewById(R.id.ll_UpdatePanInfo);
        btnSavePanDetail = findViewById(R.id.btn_savePandetail);
        btnSaveAadharDetail = findViewById(R.id.btn_saveAadhardetail);




        iconRestart = findViewById(R.id.icon_restart);
        imageNo = findViewById(R.id.imageno);
        imageYes = findViewById(R.id.imageyes);


        imageVoter = findViewById(R.id.imagevoter);
        imageAadhar = findViewById(R.id.imageaadhar);


        // loanSummary = findViewById(R.id.loanSummary);
        // agreeAndContinue = findViewById(R.id.agreeAndContinue);
        voucherVerified = findViewById(R.id.llvoucherVerifications);
        verifiedScreen = findViewById(R.id.verifiedScreen);
        buttonLoanSummary = findViewById(R.id.buttonLoanSummary);
        loanTenure = findViewById(R.id.ll_LoanTenure);
        loanTenureButton = findViewById(R.id.btn_LoanTenureSelect);
        //  loanTenureSpinner = findViewById(R.id.spinner);
        selectChildClass = findViewById(R.id.child_class);
        imagepan = findViewById(R.id.imagehavepan);
        imagenopan = findViewById(R.id.imagenopan);
        final DatePickerDialog[] mDatePickerDialog = new DatePickerDialog[1];
        final DatePickerDialog[] mDatePickerDialogvoter = new DatePickerDialog[1];

        etname = findViewById(R.id.et_name);
        etarea = findViewById(R.id.et_area);
        etcity = findViewById(R.id.et_city);
        final ImageView imgcross = findViewById(R.id.img_cancel_school);
        llAddSchoolRequest = findViewById(R.id.ll_requestAddChild);
        llLoanTenureSelect = findViewById(R.id.ll_LoanTenureSelect);
        showTenure = findViewById(R.id.selected_tenure);
        panIdEditText = findViewById(R.id.pan_number);
        panNameEditText = findViewById(R.id.pan_name);
        panDobEditText = findViewById(R.id.pan_dob);
        //  llAadharDetails = findViewById(R.id.llaadhardetails);
        showTenureFinal = findViewById(R.id.selected_tenureFinal);
        llLoanTenureFinalSelect = findViewById(R.id.ll_LoanTenureFinalSelect);
        llUpdateVoterInfo = findViewById(R.id.ll_UpdateVoterInfo);
        voterIdEditText = findViewById(R.id.id_number);
        voterNameEditText = findViewById(R.id.voter_name);
        voterAddressEditText = findViewById(R.id.voteraddress);
//        edtVoterState = findViewById(R.id.voterstate);
        // edtVoterPin = findViewById(R.id.voterpin);
        saveVoterDetail = findViewById(R.id.btn_VoterInfoUpdate);
        // genderSpinner = findViewById(R.id.gender_spinner);
        btnClearAddSchool = findViewById(R.id.img_cancel_school);
        etname = findViewById(R.id.et_name);
        etarea = findViewById(R.id.et_area);
        etcity = findViewById(R.id.et_city);
        Button btreq = findViewById(R.id.btnreq);
        //llayoutpantext = findViewById(R.id.llayoutpantext);

        imgCurrLocUp = findViewById(R.id.img_currLoc_up);
        imgCurrLocDown = findViewById(R.id.img_currLoc_down);
        imgContactUp = findViewById(R.id.img_contact_up);
        imgContactDown = findViewById(R.id.img_contact_down);
        imgSmsUp = findViewById(R.id.img_smsup);
        imgSmsDown = findViewById(R.id.img_smsdown);
        imgDeviceInfoup = findViewById(R.id.img_deviceinfoup);
        imgDeviceInfoDown = findViewById(R.id.img_deviceinfodown);
        txtCurrLoc = findViewById(R.id.txt_currLoc);
        txtContact = findViewById(R.id.txt_contact);
        txtSms = findViewById(R.id.txt_sms);
        txtDeviceInfo = findViewById(R.id.txt_deviceinfo);
        txtCurrentLocation = findViewById(R.id.txtCurrentLocation);
        llCurrentLocation = findViewById(R.id.ll_CurrentLocation);
        llContact = findViewById(R.id.ll_Contact);
        llSMS = findViewById(R.id.ll_SMS);
        llDevice = findViewById(R.id.ll_Device);
        tickLocation = findViewById(R.id.tickLocation);
        tickContact = findViewById(R.id.tickContact);
        tickSMS = findViewById(R.id.tickSMS);
        tickDevice = findViewById(R.id.tickDevice);
        panCross = findViewById(R.id.img_cancel_panedit);
        clearAadharEdit = findViewById(R.id.img_cancel_aadharedit);
        clearVoterEdit = findViewById(R.id.img_cancel_voteredit);
        editAadharName = findViewById(R.id.edit_name_aadhar);
        editAadharGender = findViewById(R.id.edit_gender_aadhar);
        editAadhaarDob = findViewById(R.id.edit_gender_aadhar);
        editAadharAddress = findViewById(R.id.edit_address_aadhaar);
        eligibileLoanBar = findViewById(R.id.indicator_Seekbar_EligibleLoan);
        seekBarUpperLimit = findViewById(R.id.seek_bar_upper_limit);


        clearCurrAdd = findViewById(R.id.img_cancel_currentedit);
        imgyess = findViewById(R.id.imageyess);
        imgnoo = findViewById(R.id.imagenoo);
        btnEditLoanPayment = findViewById(R.id.btn_editloanpayment);
        btnAgreeLoanPayment = findViewById(R.id.btn_agreeLoanPayment);
        AllAboutLoan = findViewById(R.id.AllAboutLoan);
        llEMIAmountEMITenure = findViewById(R.id.ll_EMIAmountEMITenure);
        btnSaveEMIAmount = findViewById(R.id.btn_saveEMIAmount);
        llShowApprovedLoanAmount = findViewById(R.id.ll_showApprovedLoanAmount);
        llShowLoanTenureFinalSelect = findViewById(R.id.ll_showLoanTenureFinalSelect);
        SVFinalDisclosure = findViewById(R.id.SV_FinalDisclosure);
        agreeDisclosure = findViewById(R.id.btn_agreeDisclosure);
        btnIwantToChange = findViewById(R.id.btn_iwantChange);
        progressBar = findViewById(R.id.progressBar);
        progressBarVote = findViewById(R.id.progressBarvote);
        btnFaceAuth = findViewById(R.id.start_face_auth);
        findeedProcessingFee = findViewById(R.id.findeed_processing_fee);
        classTv = findViewById(R.id.classTV);
        clickPickForVoterBack=findViewById(R.id.pick_click_voter_back);
        pickVoterBack=findViewById(R.id.pick_voter_back);
        clickVoterBack=findViewById(R.id.click_voter_back);
        select2ndVoucher=findViewById(R.id.btn_select_Voucher2);
        loanDisclosure=findViewById(R.id.final_loan_disclosure);

        addressLineOne = findViewById(R.id.address_line_one);
        currentAddressError=findViewById(R.id.current_address_error);
        locality = findViewById(R.id.locality);
        currentLocalityError=findViewById(R.id.current_area_error);
        pinCode = findViewById(R.id.pin_code);
        currentPinError=findViewById(R.id.current_pin_error);
        city = findViewById(R.id.city);
        currentCityError=findViewById(R.id.current_city_error);
        businessEdittext = findViewById(R.id.businessname_edittext);

        radioGroup = findViewById(R.id.radio_group);
        rbMonthly = findViewById(R.id.rb_monthly);
        rbWeekly = findViewById(R.id.payment_frequency);
        voterDobEditText = findViewById(R.id.voterdob);

        useMyCurrentLocationLayout=findViewById(R.id.user_my_current_location);
        currentAddressLineOne=findViewById(R.id.use_my_location_address_line_one);
        currentLocality=findViewById(R.id.use_my_location_locality_data);
        currentCity=findViewById(R.id.use_my_location_city_data);
        currentPincode=findViewById(R.id.use_my_location_pin_code_data);

        saveCurrentLocation =findViewById(R.id.use_my_location_save);

        useCurrentLocationCross=findViewById(R.id.use_current_location_cross);
        rlCoachMarks = findViewById(R.id.coching_marks);
        clickPickForVoterBack = findViewById(R.id.pick_click_voter_back);
        pickVoterBack = findViewById(R.id.pick_voter_back);
        clickVoterBack = findViewById(R.id.click_voter_back);
        select2ndVoucher = findViewById(R.id.btn_select_Voucher2);
        loanDisclosure = findViewById(R.id.final_loan_disclosure);
        addressLineOne = findViewById(R.id.address_line_one);
        locality = findViewById(R.id.locality);
        city = findViewById(R.id.city);
        pinCode = findViewById(R.id.pin_code);
        paymentFrequency = findViewById(R.id.payment_frequency);
        frameLayout = findViewById(R.id.main_layout);
        getBackLater=findViewById(R.id.get_back_later);
        iHavePanNow = findViewById(R.id.btn_now_have_pan);
        videoRecording=findViewById(R.id.video_recording);
        rbMale = findViewById(R.id.rb_male);
        rbFemale =findViewById(R.id.rb_female);
        rbOther = findViewById(R.id.rb_other);
        clickPickPanBack=findViewById(R.id.pick_click_pan_back);
        pickPanBack=findViewById(R.id.pick_pan_back);
        clickPanBack=findViewById(R.id.click_pan_back);
        radioGroupSex=findViewById(R.id.voter_sex);
        tvProgressLaon = findViewById(R.id.isb_progress);
        emiRateFindeed=findViewById(R.id.emi_rate_of_findeed);
        estimatedEMI=findViewById(R.id.estimated_emi);
        estimatedLoanTenure=findViewById(R.id.estimated_loan_tenure);
        estimatedPaymentFreq=findViewById(R.id.estimated_payment_frequency);
        requiredLoanAmount=findViewById(R.id.required_loan_amount);

        /*inLine error validation*/
        TextView error=findViewById(R.id.error_msg);
        TextView panNameError=findViewById(R.id.error_pan_name);
        voterNameError=findViewById(R.id.error_voter_name);
        voterIdError=findViewById(R.id.error_voter_id);
        voterAddressError=findViewById(R.id.error_voter_address);

        TextView voterAddressError=findViewById(R.id.error_voter_address);
        panIdValidations=findViewById(R.id.error_msg);
        panNameValidations=findViewById(R.id.error_pan_name);
        panDOBValidations=findViewById(R.id.error_pan_dob);

        /*add company related views*/
        searchEmployer=findViewById(R.id.fl_search_employer);
        employerSearchBar=findViewById(R.id.actv_search_employer);
        employerClear=findViewById(R.id.img_clear_actv);
        companyLayout=findViewById(R.id.lv_companies_layout);
        companyListView=findViewById(R.id.lv_companies);
        RelativeLayout addEmployer=findViewById(R.id.request_add_employer);
        TextView addRequestCompany=findViewById(R.id.btn_add_new_company);
        ImageView requestEmployerCross=findViewById(R.id.img_clear_employer);
        requestEmployerAddProceed=findViewById(R.id.btn_add_employer);
        employerName=findViewById(R.id.et_name_emp);
        employerArea=findViewById(R.id.et_area_emp);
        employerCity=findViewById(R.id.et_city_emp);
        employerNameError=findViewById(R.id.et_name_error);
        employerCityError=findViewById(R.id.et_city_error);
        employerAreaError=findViewById(R.id.et_area_error);
        voterGenderError=findViewById(R.id.voter_gender_error);
        botMainActivity=findViewById(R.id.bot_main_actv);
        emiLateFee=findViewById(R.id.emi_summary_late_fee);
        emiProcessingFee=findViewById(R.id.emi_processing_fee);
        permissionInPersistence=findViewById(R.id.permission_in_persistence);




        /* Personal loan full name layout*/
        userDetailLayout=findViewById(R.id.layout_name_info);
        firstName=findViewById(R.id.et_first_name);
        lastName=findViewById(R.id.et_surname);

        applicantFirstNameErr=findViewById(R.id.applicant_firstname_err);
        applicantSurNameErr=findViewById(R.id.applicant_surname_err);
        Button saveUser=findViewById(R.id.btn_savenamedetail);
        ImageView layoutCross=findViewById(R.id.img_clear_emp_name);

        /*employee id*/
        employmentLayout=findViewById(R.id.ll_employee_info);
        ImageView clearButton = findViewById(R.id.employee_id_cross);
        EditText employeeId=findViewById(R.id.employee_id);
        Button saveEmpId=findViewById(R.id.save_emp_id);

        /*user emil id layout*/
        EditText userEmail=findViewById(R.id.user_email);
        userEmailLayout=findViewById(R.id.user_email_layout);
        doNotHaveEmail=findViewById(R.id.email_personal_loan);



        /*employment length range select*/

        employmentLengthLayout=findViewById(R.id.relationship_age);
        LinearLayout employmentLengthInnerLayout=findViewById(R.id.ll_knownuserpicker);
        NumberPicker employmentLengthPicker=findViewById(R.id.relationship_age_picker);
        Button saveEmploymentAge=findViewById(R.id.save_relationship_age);
        ImageView numPicDownGray = findViewById(R.id.num_pic_Downimagegray);
        ImageView numPicDownPink = findViewById(R.id.num_pic_Downimagepink);
        ImageView numPicUpGray = findViewById(R.id.num_pic_Upimagegray);
        ImageView numPicUpPink = findViewById(R.id.num_pic_Upimagepink);

        /*marital status*/
        maritalStatus=findViewById(R.id.marital_status);
        RadioGroup maritalResponse=findViewById(R.id.marital_status_radio);
        RadioButton single=findViewById(R.id.single);
        RadioButton married=findViewById(R.id.married);

        /*do you have any kid layout*/
        doYouHaveKid=findViewById(R.id.any_kid);
        yesIdo=findViewById(R.id.yes_i_have);
        noIdoNot=findViewById(R.id.no_i_do_not_have);
        imgNoKid = findViewById(R.id.no_i_do_not_have_image);
        imgYesKid = findViewById(R.id.yes_i_have_image);

        /*school validation*/
        tvErrorSchoolName = findViewById(R.id.tv_error_schoolname);
        tvErrorSchoolArea = findViewById(R.id.tv_error_schoolarea);
        tvErrorSchoolCity = findViewById(R.id.tv_error_schoolcity);

        ImageView navicon = findViewById(R.id.nav_icon);
        // myToolbar=  findViewById(R.id.my_awesome_toolbar);


        /*  gas or electricity bill layout*/

        electricityLayout=findViewById(R.id.layout_gas_or_electricity);
        LinearLayout gasBill=findViewById(R.id.layout_gas);
        ImageView gasTick=findViewById(R.id.img_gas);
        ImageView electricityTick=findViewById(R.id.img_electricity);
        LinearLayout electricityBill=findViewById(R.id.layout_electricity);
        gasOption=findViewById(R.id.gas_option);
        electricityOption=findViewById(R.id.electricity_option);
        Button clickGas=findViewById(R.id.gas_bill_click);
        Button pickGas=findViewById(R.id.gas_bill_pick);
        gasContinue=findViewById(R.id.gas_continue);
        Button clickElectricity=findViewById(R.id.electricity_bill_click);
        Button pickElectricity=findViewById(R.id.electricity_bill_pick);
        electricityContinue=findViewById(R.id.electricity_continue);

        /*whatsapp layout*/
        whatsApp=findViewById(R.id.layout_whatsapp_selection);
        LinearLayout sameNumber=findViewById(R.id.layout_sameno);
        LinearLayout differentNumber=findViewById(R.id.layout_diffno);
        LinearLayout noWhatsapp=findViewById(R.id.layout_no_whatsapp);
        ImageView imgTickWhatsappOne = findViewById(R.id.img_tickwhatsapp_one);
        ImageView imgTickWhatsappTwo = findViewById(R.id.img_tickwhatsapp_two);
        ImageView imgTickWhatsappThree = findViewById(R.id.img_tickwhatsapp_three);

        LinearLayout doNotUse=findViewById(R.id.layout_no_whatsapp);
        sendingNotification=findViewById(R.id.layout_sendingnotification);
        whatsAppInput=findViewById(R.id.whatsapp_number_input);
        EditText enteredNumber=findViewById(R.id.number_edit_text);
        LinearLayout agreeWhatsapp=findViewById(R.id.layout_agree);
        LinearLayout declineWhatsapp = findViewById(R.id.layout_decline);
        imgDeclineNoify = findViewById(R.id.img_decline_notify);
        imgAgreeNotify = findViewById(R.id.img_agree_notify);



        /*educational email*/
        eduEmailLayout=findViewById(R.id.add_email);
        EditText eduEmail = findViewById(R.id.email_edit_text);
        LinearLayout eduDoNotHaveEmail=findViewById(R.id.do_not_have_email);
        ImageView imgTickNoEmail = findViewById(R.id.img_ticknoemail);


        imgAccYes = findViewById(R.id.img_ac_yes);
        imgAccNo = findViewById(R.id.img_ac_no);
        btnAccNo = findViewById(R.id.btn_ac_no);
        btnAccYes = findViewById(R.id.btn_ac_yes);

        // Bank Account Layout (Personal Flow)

        etAccNumber = findViewById(R.id.et_ac_number);
        etReEnterNumber = findViewById(R.id.et_renter_account);
        etAccHolder = findViewById(R.id.et_acc_holder);
        etIfscCode = findViewById(R.id.et_ifsc);

        errAccNumber = findViewById(R.id.error_ac_number);
        errReEnterNumber = findViewById(R.id.error_renter_account);
        errAccHolder = findViewById(R.id.error_acc_holder);
        errIfscCode = findViewById(R.id.error_et_ifsc);
        tvBankHeader = findViewById(R.id.tv_bankheader);
        btnAddBankAcc = findViewById(R.id.btn_add_bank_account);

        etTokenAmount = findViewById(R.id.et_token_amount);
        btnVerifyToken = findViewById(R.id.btn_verify_token);

        tvAmountTransferred = findViewById(R.id.tv_amount_transferred);
        tvSchoolBank = findViewById(R.id.tv_school_bank);
        tvBankName = findViewById(R.id.tv_bankname);
        tvTransactionDate = findViewById(R.id.tv_transaction_date);

        tvBankAccountP = findViewById(R.id.tv_account_p);
        tvBankNameP = findViewById(R.id.tv_bank_p);
        tvIfscP = findViewById(R.id.tv_ifsc_p);

        // Layout Show Trasferred Amount (Personal)

        tvShowAmountP = findViewById(R.id.tv_show_amount_p);
        tvShowAccountP = findViewById(R.id.tv_show_account_p);
        tvShowBankP = findViewById(R.id.tv_showbank_p);
        tvShowIfscP = findViewById(R.id.tv_show_ifsc_p);
        profileName=findViewById(R.id.user_name);
        imgNoEmail = findViewById(R.id.img_noemail);
        tvNotApplicable = findViewById(R.id.tv_notapplicable);
        imgNoApplicable = findViewById(R.id.img_noapplicable);



        tvNotApplicable.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                imgNoApplicable.setVisibility(View.VISIBLE);
                return false;
            }
        });

        tvNotApplicable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });




        //profileName.setText(validateOTPResponse.getData().getFirstName());

        eduDoNotHaveEmail.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                imgTickNoEmail.setVisibility(View.VISIBLE);
                return false;
            }
        });


        eduDoNotHaveEmail.setOnClickListener(this);

        layout_Btn_Personal.setOnClickListener(this);
        pickPanBack.setOnClickListener(this);
        clickPanBack.setOnClickListener(this);

        //    actionBarDrawerToggle = new ActionBarDrawerToggle(this, dl, myToolbar, R.string.Open, R.string.Close);
//        dl.setDrawerListener(actionBarDrawerToggle);





        noWhatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                whatsApp.setVisibility(View.GONE);
                presenter.onEditTextActionDone("I don't use whatsapp",null,ChatPresenter.chatObjects.size()+1);
                presenter.botMessage("Please share your Email ID",eduEmailLayout,ChatPresenter.chatObjects.size()+1);

            }
        });



        eduEmail.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (eduEmail.getRight() - eduEmail.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width()))
                    {


                        String usermail = eduEmail.getText().toString();
                        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";


                        if(isNull(usermail)){
                            Toast.makeText(ChatActivity.this, "Please enter email id", Toast.LENGTH_SHORT).show();
                        }else{

                            if(usermail.matches(EMAIL_PATTERN)){
                                eduEmailLayout.setVisibility(View.GONE);
                                presenter.onEditTextActionDone(usermail,null,ChatPresenter.chatObjects.size()+1);
                                presenter.botMessage("Congratulations !",sendingNotification,ChatPresenter.chatObjects.size()+1);
                                presenter.botMessage("Our team will reach out to collect the KYC documents from you during business hours. Post collection, your money will be disbursed",null,ChatPresenter.chatObjects.size()+1);

                                hideKeyboard();

                                new SaveCustomerEmailAddress(usermail).execute();


                            }else {
                                Toast.makeText(ChatActivity.this, "Invalid email id", Toast.LENGTH_SHORT).show();
                            }
                        }

                        return true;
                    }
                }
                return false;
            }
        });

       /* btnAccNo.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                imgAccNo.setVisibility(View.VISIBLE);
                imgAccYes.setVisibility(View.GONE);

                return false;
            }
        });

        btnAccYes.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                imgAccYes.setVisibility(View.VISIBLE);
                imgAccNo.setVisibility(View.GONE);
                return false;
            }
        });*/



        enteredNumber.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (enteredNumber.getRight() - enteredNumber.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width()))
                    {
                        String wNumber=enteredNumber.getText().toString();


                        if(isNull(wNumber)){
                            Toast.makeText(ChatActivity.this, "Please enter your whatsapp number", Toast.LENGTH_SHORT).show();
                        }else{
                            if(enteredNumber.getText().toString().matches("^[6-9]\\d{9}$")){
                                whatsAppInput.setVisibility(View.GONE);
                                presenter.onEditTextActionDone(wNumber,null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                                presenter.botMessage("Please share your Email ID",eduEmailLayout,ChatPresenter.chatObjects.size()+1);
                                hideKeyboard();
                                new SaveWhatsappNumber(wNumber).execute();
                            }else {
                                Toast.makeText(ChatActivity.this, "Please enter valid number", Toast.LENGTH_SHORT).show();

                            }

                        }

                        return true;
                    }
                }
                return false;
            }
        });


        agreeWhatsapp.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                imgAgreeNotify.setVisibility(View.VISIBLE);
                imgDeclineNoify.setVisibility(View.INVISIBLE);

                return false;
            }
        });



        agreeWhatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendingNotification.setVisibility(View.GONE);
                presenter.botMessage("Please share your Email ID",eduEmailLayout,ChatPresenter.chatObjects.size()+1);
            }
        });

        sameNumber.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                imgTickWhatsappOne.setVisibility(View.VISIBLE);
                imgTickWhatsappTwo.setVisibility(View.INVISIBLE);
                imgTickWhatsappThree.setVisibility(View.INVISIBLE);
                return false;
            }
        });

        declineWhatsapp.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                imgDeclineNoify.setVisibility(View.VISIBLE);
                imgAgreeNotify.setVisibility(View.GONE);
                return false;
            }
        });

        declineWhatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        sameNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                whatsApp.setVisibility(View.GONE);
                presenter.onEditTextActionDone("Yes, i'm using the same number",null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                presenter.botMessage("We will be sending notifications to this number.",sendingNotification,ChatPresenter.chatObjects.size()+1);
                new SaveWhatsappNumber(validateOTPResponse.getData().getPhoneNumber()).execute();
            }
        });

        differentNumber.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                imgTickWhatsappOne.setVisibility(View.INVISIBLE);
                imgTickWhatsappTwo.setVisibility(View.VISIBLE);
                imgTickWhatsappThree.setVisibility(View.INVISIBLE);
                return false;
            }
        });


        differentNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                whatsApp.setVisibility(View.GONE);
                presenter.onEditTextActionDone("No, i'm using a different number",null,ChatPresenter.chatObjects.size()+1);
                presenter.botMessage("Please enter your whatsapp number",whatsAppInput,ChatPresenter.chatObjects.size()+1);

            }
        });

        noWhatsapp.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                imgTickWhatsappOne.setVisibility(View.INVISIBLE);
                imgTickWhatsappTwo.setVisibility(View.INVISIBLE);
                imgTickWhatsappThree.setVisibility(View.VISIBLE);
                return false;
            }
        });



        try{
            CUSTOMER_ID = ApplicationData.validateOTPResponse.getData().getCustomerId();
            Log.i("www",String.valueOf(CUSTOMER_ID));
        }catch (Exception e){
            e.printStackTrace();
            CUSTOMER_ID=1;
        }

        addRequestCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchEmployer.setVisibility(View.GONE);
                companyLayout.setVisibility(View.GONE);
                employerName.requestFocus();
                presenter.botMessage("Please fill the employer details",addEmployer,ChatPresenter.chatObjects.size()+1);
            }
        });
        requestEmployerCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addEmployer.setVisibility(View.GONE);
                ChatPresenter.chatObjects.subList(ChatPresenter.chatObjects.size()-1,ChatPresenter.chatObjects.size()).clear();
                chatAdapter.notifyDataSetChanged();
            }
        });



        employerName.setOnFocusChangeListener(this);
        employerArea.setOnFocusChangeListener(this);
        employerCity.setOnFocusChangeListener(this);



        requestEmployerAddProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                employerName.clearFocus();
                employerArea.clearFocus();
                employerCity.clearFocus();

                if(employerNameError.getVisibility()==View.VISIBLE || employerAreaError.getVisibility()==View.VISIBLE || employerCityError.getVisibility()==View.VISIBLE){

                }
                if(isNullOrBlank(employerName.getText().toString())||isNullOrBlank(employerArea.getText().toString())||isNullOrBlank(employerCity.getText().toString())) {
                    if (isNullOrBlank(employerName.getText().toString())) {
                        employerNameError.setVisibility(View.VISIBLE);
                    }
                    if (isNullOrBlank(employerArea.getText().toString())) {
                        employerAreaError.setVisibility(View.VISIBLE);
                    }
                    if (isNullOrBlank(employerCity.getText().toString())) {
                        employerCityError.setVisibility(View.VISIBLE);
                    }
                }else {

                    hideKeyboard();
                    addEmployer.setVisibility(View.GONE);
                    ChatPresenter.chatObjects.subList(ChatPresenter.chatObjects.size() - 1, ChatPresenter.chatObjects.size()).clear();
                    chatAdapter.notifyDataSetChanged();
                    presenter.botMessage("We have received your request to add employer. We will get in touch when the employer is added to the list.", null, ChatPresenter.chatObjects.size() + 1);
                    new AddEmployer(employerName.getText().toString(),employerCity.getText().toString(),employerArea.getText().toString()).execute();


                }
            }

        });


        employerCity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                requestEmployerAddProceed.setBackgroundResource(R.drawable.shape_btn_addchilddark);

            }
        });

        clickElectricity.setOnClickListener(v -> {
            electricityOption.setVisibility(View.GONE);
            if (ContextCompat.checkSelfPermission(this,Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                Intent takePictureIntent = new Intent(this, CameraActivity.class);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                        final File file=new File(storageDir, "pic.jpg");
                        photoFile=file;
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    if (photoFile != null) {
                        electricityUri = FileProvider.getUriForFile(ChatActivity.this,
                                "com.findeed.fileprovider",
                                photoFile);
//                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, electricityUri);
//                            takePictureIntent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                        takePictureIntent.putExtra("image_side","FRONT SIDE");
                        takePictureIntent.putExtra("doc_type","VOTER");
                        startActivityForResult(takePictureIntent, CLICK_ELECTRICITY);
                    }
                }

            } else {
                ActivityCompat.requestPermissions(this,new String[]{
                                Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE
                        }

                        , CLICK_ELECTRICITY_PERMISSION);
            }

        });


        pickElectricity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    electricityOption.setVisibility(View.GONE);
                    Intent galleryIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                    galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
                    galleryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, false);
                    galleryIntent.setType("image/*");
                    startActivityForResult(galleryIntent, PICK_ELECTRICITY);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        });


        electricityBill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                electricityLayout.setVisibility(View.GONE);
                presenter.onEditTextActionDone("Electricity Bill",null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                presenter.botMessage("Can you provide your Electricity Bill for verification?",electricityOption,ChatPresenter.chatObjects.size()+1);


            }
        });

        electricityBill.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                gasTick.setVisibility(View.GONE);
                electricityTick.setVisibility(View.VISIBLE);
                return false;

            }
        });



        pickGas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    gasOption.setVisibility(View.GONE);
                    Intent galleryIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                    galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
                    galleryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, false);
                    galleryIntent.setType("image/*");
                    startActivityForResult(galleryIntent, PICK_GAS);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        gasContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                billDialog=new ProgressDialog(ChatActivity.this);
                billDialog.setCancelable(false);
                billDialog.setMessage("Please wait....");
                billDialog.show();
                gasContinue.setVisibility(View.GONE);
                DOC_TYPE="GAS_BILL";
                new  SaveDocumentDetails().execute();
            }
        });

        electricityContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                billDialog=new ProgressDialog(ChatActivity.this);
                billDialog.setMessage("Please wait....");
                billDialog.setCancelable(false);
                billDialog.show();
                electricityContinue.setVisibility(View.GONE);
                DOC_TYPE="ELECTRICITY_BILL";
                new SaveDocumentDetails().execute();

            }
        });


//        clickGas.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                gasOption.setVisibility(View.GONE);
//
//                if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
//
//                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
//                        File photoFile = null;
//                        try {
//                            photoFile = createImageFile();
//                        } catch (IOException ex) {
//                            ex.printStackTrace();
//                        }
//                        if (photoFile != null) {
//                            gasUri = FileProvider.getUriForFile(ChatActivity.this,
//                                    "com.findeed.fileprovider",
//                                    photoFile);
//                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, gasUri);
//                            takePictureIntent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//                            startActivityForResult(takePictureIntent, CLICK_GAS);
//                        }
//                    }
//
//                } else {
//                    requestPermissions(new String[]{
//                                    Manifest.permission.CAMERA,
//                                    Manifest.permission.READ_EXTERNAL_STORAGE
//                            }
//
//                            , CLICK_GAS_PERMISSION);
//                }
//            }
//        });

        clickGas.setOnClickListener(v -> {
            gasOption.setVisibility(View.GONE);

            if (ContextCompat.checkSelfPermission(this,Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                Intent takePictureIntent = new Intent(this,CameraActivity.class);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                        final File file=new File(storageDir, "pic.jpg");
                        photoFile=file;
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    if (photoFile != null) {
                        gasUri = FileProvider.getUriForFile(ChatActivity.this,
                                "com.findeed.fileprovider",
                                photoFile);
//          takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, gasUri);
//          takePictureIntent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                        takePictureIntent.putExtra("image_side","FRONT SIDE");
                        takePictureIntent.putExtra("doc_type","VOTER");
                        startActivityForResult(takePictureIntent, CLICK_GAS);
                    }
                }

            } else {
                requestPermissions(new String[]{
                                Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE
                        }

                        , CLICK_GAS_PERMISSION);
            }

        });



        gasBill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                electricityLayout.setVisibility(View.GONE);
                presenter.onEditTextActionDone("Gas Bill",null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                presenter.botMessage("Can you provide your Gas Bill for verification?",gasOption,ChatPresenter.chatObjects.size()+1);

            }
        });


        gasBill.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                gasTick.setVisibility(View.VISIBLE);
                electricityTick.setVisibility(View.GONE);
                return false;
            }
        });






        locationUpdateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Location newLocation = intent.getParcelableExtra("location");
                ApplicationData.mapLocation = newLocation;


                try {
                    latitude = ApplicationData.mapLocation.getLatitude();
                    longitude = ApplicationData.mapLocation.getLongitude();
                    Geocoder gcd = new Geocoder(getBaseContext(), Locale.getDefault());
                    List<Address> addresses;

                    addresses = gcd.getFromLocation(latitude,
                            longitude, 1);
                    if (addresses.size() > 0) {
                        cityName = addresses.get(0).getLocality();
                        addressLine = addresses.get(0).getAddressLine(0);
                        pin = addresses.get(0).getPostalCode();
                        loc = addresses.get(0).getSubLocality();
                        ADDRESS_LINE_1=addressLine;
                        CITY=cityName;
                        PINCODE=pin;
                    }

                    if(pd.isShowing()){
                        pd.dismiss();
                        currentAddressLineOne.setText(addressLine);
                        currentCity.setText(cityName);
                        currentPincode.setText(pin);
                        currentLocality.setText(loc);
                        presenter.botMessage("Please confirm your address",useMyCurrentLocationLayout,ChatPresenter.chatObjects.size()+1);

                    }


                } catch (Exception e) {
                    e.printStackTrace();

                }
            }

        };



        predictedLocationReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Location newLocation = intent.getParcelableExtra("location");
                ApplicationData.mapLocation = newLocation;
            }
        };

        LocalBroadcastManager.getInstance(this).registerReceiver(
                locationUpdateReceiver,
                new IntentFilter("LocationUpdated"));



        LocalBroadcastManager.getInstance(this).registerReceiver(
                predictedLocationReceiver,
                new IntentFilter("PredictLocation"));





        // setSupportActionBar(myToolbar);





        yesIdo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doYouHaveKid.setVisibility(View.GONE);
                presenter.onEditTextActionDone("Yes, i do",null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                presenter.botMessage("How many children do you have ?",llayoutchildcount,ChatPresenter.chatObjects.size()+1);
                new SaveHaveKid("1").execute();
            }
        });



        noIdoNot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doYouHaveKid.setVisibility(View.GONE);
                presenter.onEditTextActionDone("No, i don't",null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                presenter.botMessage("How much loan do you require and for how long ?",llayoutseekbar,ChatPresenter.chatObjects.size()+1);
                new SaveHaveKid("2").execute();
            }
        });

        noIdoNot.setOnTouchListener((v, event) -> {

            imgNoKid.setVisibility(View.VISIBLE);
            imgYesKid.setVisibility(View.GONE);
            return false;
        });

        yesIdo.setOnTouchListener((v, event) -> {
            imgYesKid.setVisibility(View.VISIBLE);
            imgNoKid.setVisibility(View.GONE);
            return false;
        });








        single.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MARITAL_STATUS="Single";
                maritalStatus.setVisibility(View.GONE);
                presenter.onEditTextActionDone("Single",null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                presenter.botMessage("Do you have any kids ?",doYouHaveKid,ChatPresenter.chatObjects.size()+1);
                new SaveMaritalStatus("Single").execute();
            }
        });

        married.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MARITAL_STATUS="Married";
                maritalStatus.setVisibility(View.GONE);
                presenter.onEditTextActionDone("Married",null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                presenter.botMessage("Do you have any kids ?",doYouHaveKid,ChatPresenter.chatObjects.size()+1);
                new SaveMaritalStatus("Married").execute();
            }
        });



        rbMonthly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // emiRate= String.valueOf(Math.round(calculateEmi(eligibileLoanBar.getProgress(),12,tempTenure)));
                emiRate=  String.valueOf(Math.round(getEMI(eligibileLoanBar.getProgress(),tempTenure)));
                emiRateFindeed.setText("\u20B9 "+emiRate+"/Month");

            }
        });

        rbWeekly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // emiRate= String.valueOf(Math.round(calculateEmi(eligibileLoanBar.getProgress(),12,tempTenure)/4));
                emiRate=  String.valueOf(Math.round(getEMI(eligibileLoanBar.getProgress(),tempTenure)));
                emiRateFindeed.setText("\u20B9 "+emiRate+"/Weekly");
            }
        });


        employerSearchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try{

                    companyLayout.setVisibility(View.VISIBLE);
                    employerClear.setVisibility(View.VISIBLE);
                    listViewCompaniesAdapter.filter(s.toString());


                }catch (Exception e){
                    e.printStackTrace();
                    Log.d("Listview exception",e.toString());
                }


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });



        companyListView.setOnItemClickListener((parent, view, position, id) -> {
            try{

                if(LOAN_TYPE==1){
                    hideKeyboard();
                    companyLayout.setVisibility(View.GONE);
                    searchEmployer.setVisibility(View.GONE);

                    employerClear.setVisibility(View.GONE);
                    companyName=companyList.get(position).getCompanyname();
                    Log.i("SELECTED_COMPANY",companyName);
                    Bitmap icon = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                            R.drawable.location_placeholder);
                    schoolName=companyName;
                    presenter.setImage(icon,searchEmployer,ChatPresenter.chatObjects.size()+1,companyName);
                    presenter.botMessage("How much do you earn in a month?",llIncomeRange,ChatPresenter.chatObjects.size()+1);
                    new SaveCustomerEmployer(companyId.get(companyList.get(position).getCompanyname()).intValue()).execute();

                }else {
                    hideKeyboard();
                    companyLayout.setVisibility(View.GONE);
                    searchEmployer.setVisibility(View.GONE);
                    employerClear.setVisibility(View.GONE);
                    companyName=companyList.get(position).getCompanyname();
                    Bitmap icon = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                            R.drawable.location_placeholder);
                    presenter.setImage(icon,searchEmployer,ChatPresenter.chatObjects.size()+1,companyName);
                    presenter.botMessage("What is your full name ?",userDetailLayout,ChatPresenter.chatObjects.size()+1);
                    new SaveCustomerEmployer(companyId.get(companyList.get(position).getCompanyname()).intValue()).execute();

                }
            }catch(Exception e){
                e.printStackTrace();
                Log.d("Item click listener catch",e.toString());
            }

        });

        employerClear.setOnClickListener(view -> employerSearchBar.setText(""));

        String[] empAgeValues={"<6 Months","6 Months - 1 Year", "1 Year - 2 Years", ">2 Years"};
        employmentLengthPicker.setMinValue(0);
        employmentLengthPicker.setMaxValue(empAgeValues.length - 1);
        employmentLengthPicker.setDisplayedValues(empAgeValues);
        employmentLengthPicker.setWrapSelectorWheel(false);


        employmentLengthPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                EMPLOYMENT_AGE=empAgeValues[newVal];
                if(EMPLOYMENT_AGE.equals(empAgeValues[empAgeValues.length-1])){
                    numPicUpPink.setVisibility(View.VISIBLE);
                    numPicDownGray.setVisibility(View.VISIBLE);
                    numPicDownPink.setVisibility(View.GONE);
                    numPicUpGray.setVisibility(View.GONE);
                }
                else if(newVal==0){
                    numPicUpPink.setVisibility(View.GONE);
                    numPicDownGray.setVisibility(View.GONE);
                    numPicDownPink.setVisibility(View.VISIBLE);
                    numPicUpGray.setVisibility(View.VISIBLE);
                }

                else{
                    numPicUpPink.setVisibility(View.VISIBLE);
                    numPicDownGray.setVisibility(View.GONE);
                    numPicDownPink.setVisibility(View.VISIBLE);
                    numPicUpGray.setVisibility(View.GONE);
                }

            }
        });

        saveEmploymentAge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNullOrBlank(EMPLOYMENT_AGE)){
                    EMPLOYMENT_AGE=empAgeValues[0];
                }

                employmentLengthLayout.setVisibility(View.GONE);
                presenter.onEditTextActionDone(EMPLOYMENT_AGE,null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                presenter.botMessage("How much do you earn in a month ?",llIncomeRange,ChatPresenter.chatObjects.size()+1);
                new SaveWorkingYear().execute();
            }
        });

        doNotHaveEmail.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                imgNoEmail.setVisibility(View.VISIBLE);
                return false;
            }
        });


        doNotHaveEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userEmailLayout.setVisibility(View.GONE);
                hideKeyboard();
                presenter.onEditTextActionDone("I don't have Email",null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                presenter.botMessage("How long have you been with this company? ",employmentLengthLayout,ChatPresenter.chatObjects.size()+1);
                new SaveCustomerEmailAddress("2").execute();
            }
        });


        userEmail.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (userEmail.getRight() - userEmail.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width()))
                    {


                        String usermail = userEmail.getText().toString();
                        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";


//                        if (usermail.matches("")) {
//                            Toast.makeText(ChatActivity.this, "You did not enter a email id", Toast.LENGTH_SHORT).show();
//
//                        }

                        if(isNull(userEmail.getText().toString())){
                            Toast.makeText(ChatActivity.this, "Please enter email id", Toast.LENGTH_SHORT).show();
                        }else{

                            if(usermail.matches(EMAIL_PATTERN)){
                                hideKeyboard();
                                userEmailLayout.setVisibility(View.GONE);
                                presenter.onEditTextActionDone("Email: "+userEmail.getText().toString(),null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                                presenter.botMessage("How long have you been with this company? ",employmentLengthLayout,ChatPresenter.chatObjects.size()+1);
                                employmentLengthLayout.setVisibility(View.VISIBLE);
                                employmentLengthInnerLayout.setVisibility(View.VISIBLE);
                                new SaveCustomerEmailAddress(userEmail.getText().toString()).execute();

                            }else {
                                Toast.makeText(ChatActivity.this, "Invalid email id", Toast.LENGTH_SHORT).show();
                            }

                        }



                        return true;
                    }
                }
                return false;
            }
        });

//        HashMap<String,String> cList=new HashMap<>();
//        cList.put("Amazon","HSR Layout, Banglore");
//        cList.put("Accenture","HSR Layout, Banglore");
//
//        final ArrayList<CompaniesNames> companyList= new ArrayList<>();
//        for (Map.Entry<String, String> entry : cList.entrySet()) {
//            companyList.add(new CompaniesNames(entry.getKey(), entry.getValue()));
//
//        }
        saveEmpId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNullOrBlank(employeeId.getText().toString())){
                    Toast.makeText(ChatActivity.this, "Please enter employee id", Toast.LENGTH_SHORT).show();
                }else {
                    employmentLayout.setVisibility(View.GONE);
                    presenter.onEditTextActionDone("Employee Id: "+employeeId.getText().toString(),null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                    presenter.botMessage("Please share your email id",userEmailLayout,ChatPresenter.chatObjects.size()+1);
                    hideKeyboard();
                    new SaveEmployeeId(employeeId.getText().toString(),CUSTOMER_ID).execute();

                }
            }
        });


        employeeId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence str, int start, int count, int after)    {

            }

            @Override
            public void onTextChanged(CharSequence str, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable str) {
                if(str.toString().trim().length()>0){
                    saveEmpId.setBackgroundResource(R.drawable.shape_btn_addchilddark);
                }else{
                    saveEmpId.setBackgroundResource(R.drawable.shape_btn_addchild);
                }
            }
        });


        firstName.setOnFocusChangeListener(this);
        lastName.setOnFocusChangeListener(this);



//            panNameEditText.clearFocus();
//            PanId.clearFocus();
//            PanDob.clearFocus();
//
//            if(panNameValidations.getVisibility()==View.VISIBLE || panIdValidations.getVisibility()==View.VISIBLE){
//
//            }else {
//                hideKeyboard();

        saveUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firstName.clearFocus();
                lastName.clearFocus();


                if (applicantFirstNameErr.getVisibility() == View.VISIBLE || applicantSurNameErr.getVisibility() == View.VISIBLE ) {
                }
                else if(isNullOrBlank(firstName.getText().toString())){
                    if(isNullOrBlank(firstName.getText().toString())){applicantFirstNameErr.setVisibility(View.VISIBLE);}


                }
                else{
                    hideKeyboard();
                    userDetailLayout.setVisibility(View.GONE);
                    presenter.addUserName("First Name: "+ firstName.getText().toString(),null,ChatPresenter.chatObjects.size()+1,true, CUSTOMER_ID);
                    presenter.addUserName("Surname: " + lastName.getText().toString(), null, ChatPresenter.chatObjects.size() + 1,false,CUSTOMER_ID);
                    presenter.botMessage("What is your employee id ?", employmentLayout, ChatPresenter.chatObjects.size() + 1);
                    new SaveCustomerName(firstName.getText().toString(), lastName.getText().toString(), CUSTOMER_ID).execute();

                }

            }
        });



        clickPanBack.setOnClickListener(v -> {
            clickPickPanBack.setVisibility(View.GONE);
            if (ContextCompat. checkSelfPermission(this,Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Intent takePictureIntent = new Intent(this, CameraActivity.class);
                //Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                        final File file=new File(storageDir, "pic.jpg");
                        photoFile=file;
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    if (photoFile != null) {
                        panBackUri = FileProvider.getUriForFile(this,
                                "com.findeed.fileprovider",
                                photoFile);
                        // takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, panURI);
                        // takePictureIntent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                        // startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO_PAN);
                        takePictureIntent.putExtra("image_side","BACK SIDE");
                        takePictureIntent.putExtra("doc_type","PAN");
                        startActivityForResult(takePictureIntent,REQUEST_TAKE_PHOTO_PAN_BACK);
                    }
                }

            } else {
                requestPermissions(new String[]{
                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                }, PAN_BACK_CLICK_PERMISSION);
            }

        });


        videoRecording.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoRecording.setVisibility(View.GONE);
                String clientCode = "FIND9043";
                String apiKey = "kjhef3498";
                String requestId = getRequestId();
                String salt = "hjgsd6q3";
                String hash = sha256(clientCode + "|" + requestId + "|" + apiKey + "|" + salt);

                Intent myIntent;
                FaceAuthInitRequest.Builder faceAuthBuilder = new FaceAuthInitRequest.Builder(
                        clientCode,
                        apiKey,
                        "A12W3",
                        requestId,
                        hash,
                        "LOGIN",
                        salt
                );

                FaceAuthRequestImage.getInstance().setFaceImage(USER_FACE);
                myIntent = new Intent(ChatActivity.this, FaceAuthInitActivity.class);
                myIntent.putExtra("init_request", faceAuthBuilder.build());
                startActivityForResult(myIntent, FACE_AUTH_CODE);
            }
        });


        getBackLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llayoutNoPanCard.setVisibility(View.GONE);
                presenter.botMessage("Please get back to us when you have the PAN card details and we will certainly assist you with your loan.", iHavePanNow, ChatPresenter.chatObjects.size()+1);


            }
        });


        iHavePanNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iHavePanNow.setVisibility(View.GONE);
                presenter.botMessage("Great, Please choose from the options below to upload your PAN card", lLayoutPanOption, ChatPresenter.chatObjects.size() + 1);

            }
        });




        rlCoachMarks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rlCoachMarks.setVisibility(View.GONE);


            }

        });

        useCurrentLocationCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                useMyCurrentLocationLayout.setVisibility(View.GONE);
                ChatPresenter.chatObjects.subList(ChatPresenter.chatObjects.size()-1,ChatPresenter.chatObjects.size()).clear();
                ChatActivity.chatAdapter.notifyDataSetChanged();
            }
        });

        loanDisclosure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                loanDisclosure.setVisibility(View.GONE);
//                Intent i = new Intent(ChatActivity.this, TabLayoutActivity.class);
//                //startActivityForResult(i, TERMS_AND_CONDITIONS);
//                startActivity(i);
            }
        });





        saveCurrentLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isNullOrBlank(currentAddressLineOne.getText().toString())|| isNullOrBlank(currentLocality.getText().toString())|| isNullOrBlank(currentCity.getText().toString()) || isNullOrBlank(currentPincode.getText().toString())){
                    if(isNullOrBlank(currentAddressLineOne.getText().toString())){
                        Toast.makeText(ChatActivity.this, "Please enter Address line 1", Toast.LENGTH_SHORT).show();
                    }else if(isNullOrBlank(currentLocality.getText().toString())){
                        Toast.makeText(ChatActivity.this, "Please enter Locality", Toast.LENGTH_SHORT).show();

                    }else if(isNullOrBlank(currentCity.getText().toString())){
                        Toast.makeText(ChatActivity.this, "Please enter city", Toast.LENGTH_SHORT).show();


                    }else {
                        Toast.makeText(ChatActivity.this, "Please enter pin code", Toast.LENGTH_SHORT).show();

                    }
                }else {
                    if(!isValidPin(currentPincode.getText().toString())){
                        Toast.makeText(ChatActivity.this, "Please enter correct pin code", Toast.LENGTH_SHORT).show();


                    }else {

                        hideKeyboard();
                        userLocation=currentCity.getText().toString()+","+currentPincode.getText().toString();
                        ADDRESS_LINE_1=currentAddressLineOne.getText().toString();
                        CITY=currentCity.getText().toString();
                        LOCALITY=currentLocality.getText().toString();
                        PINCODE=currentPincode.getText().toString();
                        new SendCurrentLocation().execute();


                    }


                }






            }
        });


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if(rbMonthly.isChecked())
                {
                    rbMonthly.setTextColor(ContextCompat.getColor(ChatActivity.ctx,R.color.very_dark_gray));
                    rbWeekly.setTextColor(getColor(R.color.color_interest));
                }
                if (rbWeekly.isChecked())
                {
                    rbWeekly.setTextColor(ContextCompat.getColor(ChatActivity.ctx,R.color.very_dark_gray));
                    rbMonthly.setTextColor(ContextCompat.getColor(ChatActivity.ctx,R.color.color_interest));
                }


            }
        });

        btnCurrentLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                llayoutAddressManually.setVisibility(View.GONE);

                boolean isGps=hasGPSDevice(ChatActivity.this);
                if(isGps){

                    final LocationManager manager = (LocationManager) ChatActivity.this.getSystemService(Context.LOCATION_SERVICE);
                    if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER) && hasGPSDevice(getApplicationContext())) {

                        enableLoc();

                    }else {


                        if (ContextCompat.checkSelfPermission(ChatActivity.ctx,Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(ChatActivity.ctx,Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                            final Intent locationService = new Intent(ChatActivity.this, LocationService.class);
                            ChatActivity.this.startService(locationService);
                            ChatActivity.this.bindService(locationService, serviceConnection, Context.BIND_AUTO_CREATE);

                            callForLocation();

                        }else{
                            requestPermissions(new String[]{
                                    Manifest.permission.ACCESS_FINE_LOCATION,
                                    Manifest.permission.ACCESS_COARSE_LOCATION
                            }, LOCATION_PERMISSION);
                        }


                    }




                }else {
                    Toast.makeText(ChatActivity.this, "Phone does not have GPS", Toast.LENGTH_SHORT).show();
                }


            }
        });


        businessEdittext.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (businessEdittext.getRight() - businessEdittext.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here

                        return true;
                    }
                }
                return false;
            }
        });


        TextWatcher tw = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (firstName.getText().length() > 0 && lastName.getText().length() > 0) {
                    saveUser.setBackgroundResource(R.drawable.shape_btn_addchilddark);
                } else {
                    saveUser.setBackgroundResource(R.drawable.shape_btn_addchild);
                }
            }

            @Override
            public void afterTextChanged(Editable s) { }
        };

        // text watcher registration to the 2 EditText
        firstName.addTextChangedListener(tw);
        lastName.addTextChangedListener(tw);



        btnSaveEMIAmount.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                llEMIAmountEMITenure.setVisibility(View.GONE);
                SVFinalDisclosure.setVisibility(View.VISIBLE);

            }
        });


        btnIwantToChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                SVFinalDisclosure.setVisibility(View.GONE);
                llEMIAmountEMITenure.setVisibility(View.VISIBLE);

            }
        });

        llShowLoanTenureFinalSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialogLoan = new BottomSheetDialog(ChatActivity.this);
                dialogLoan.setContentView(R.layout.custom_bottomdialog_loantenure);

                rl3months = dialogLoan.findViewById(R.id.rl_3months);
                rl6months = dialogLoan.findViewById(R.id.rl_6months);
                rl9months = dialogLoan.findViewById(R.id.rl_9months);
                rl12months = dialogLoan.findViewById(R.id.rl_12months);

                loanClear = dialogLoan.findViewById(R.id.btnLoanTenureClear);
                threemonth = dialogLoan.findViewById(R.id.txt_3months);
                sixmonth = dialogLoan.findViewById(R.id.txt_6months);
                ninthmonth = dialogLoan.findViewById(R.id.txt_9months);
                twelvemonth = dialogLoan.findViewById(R.id.txt_12months);
                // eighteenmonth = dialogLoan.findViewById(R.id.txt_18months);

                btnLoanTenure = dialogLoan.findViewById(R.id.btn_Select_loantenure);
                tickthree = dialogLoan.findViewById(R.id.tick3);
                ticksix = dialogLoan.findViewById(R.id.tick6);
                tickninth = dialogLoan.findViewById(R.id.tick9);
                ticktwelve = dialogLoan.findViewById(R.id.tick12);
                // tickeighteen = dialogLoan.findViewById(R.id.tick18);

                Window window = dialogLoan.getWindow();
                window.setGravity(Gravity.CENTER);

                dialogLoan.getWindow().setBackgroundDrawable(new
                        ColorDrawable(Color.TRANSPARENT));
                if(!dialogLoan.isShowing()) {
                    dialogLoan.show();
                }

                dialogLoan.show();

                loanClear.setOnClickListener(v12 -> {
                    dialogLoan.dismiss();

                });


                rl3months.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        btnLoanTenure.setBackgroundResource(R.drawable.shape_btn_addloandark);
                        tickthree.setVisibility(View.VISIBLE);
                        ticksix.setVisibility(View.INVISIBLE);
                        tickninth.setVisibility(View.INVISIBLE);
                        ticktwelve.setVisibility(View.INVISIBLE);
                        LoanTenure = "3 Months";
                        TENURE = 3;


                    }
                });

                rl6months.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        btnLoanTenure.setBackgroundResource(R.drawable.shape_btn_addloandark);
                        tickthree.setVisibility(View.INVISIBLE);
                        ticksix.setVisibility(View.VISIBLE);
                        tickninth.setVisibility(View.INVISIBLE);
                        ticktwelve.setVisibility(View.INVISIBLE);
                        LoanTenure = "6 Months";
                        TENURE = 6;
                    }
                });

                rl9months.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        btnLoanTenure.setBackgroundResource(R.drawable.shape_btn_addloandark);
                        tickthree.setVisibility(View.INVISIBLE);
                        ticksix.setVisibility(View.INVISIBLE);
                        tickninth.setVisibility(View.VISIBLE);
                        ticktwelve.setVisibility(View.INVISIBLE);
                        LoanTenure = "9 Months";
                        TENURE = 9;
                    }
                });

                rl12months.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        btnLoanTenure.setBackgroundResource(R.drawable.shape_btn_addloandark);
                        tickthree.setVisibility(View.INVISIBLE);
                        ticksix.setVisibility(View.INVISIBLE);
                        tickninth.setVisibility(View.INVISIBLE);
                        ticktwelve.setVisibility(View.VISIBLE);
                        LoanTenure = "12 Months";
                        TENURE = 12;
                    }
                });

                threemonth.setOnClickListener(v111 -> {
                    btnLoanTenure.setBackgroundResource(R.drawable.shape_btn_addloandark);
                    tickthree.setVisibility(View.VISIBLE);
                    ticksix.setVisibility(View.INVISIBLE);
                    tickninth.setVisibility(View.INVISIBLE);
                    ticktwelve.setVisibility(View.INVISIBLE);
                    // tickeighteen.setVisibility(View.INVISIBLE);
                    LoanTenure = "3 Months";
                    TENURE = 3;
                });

                sixmonth.setOnClickListener(v111 -> {
                    btnLoanTenure.setBackgroundResource(R.drawable.shape_btn_addloandark);
                    tickthree.setVisibility(View.INVISIBLE);
                    ticksix.setVisibility(View.VISIBLE);
                    tickninth.setVisibility(View.INVISIBLE);
                    ticktwelve.setVisibility(View.INVISIBLE);
                    tickeighteen.setVisibility(View.INVISIBLE);
                    LoanTenure = "6 Months";
                    TENURE = 6;
                });

                ninthmonth.setOnClickListener(v111 -> {

                    btnLoanTenure.setBackgroundResource(R.drawable.shape_btn_addloandark);
                    tickthree.setVisibility(View.INVISIBLE);
                    ticksix.setVisibility(View.INVISIBLE);
                    tickninth.setVisibility(View.VISIBLE);
                    ticktwelve.setVisibility(View.INVISIBLE);
                    tickeighteen.setVisibility(View.INVISIBLE);

                    LoanTenure = "9 Months";
                    TENURE = 9;
                });

                twelvemonth.setOnClickListener(v111 -> {

                    btnLoanTenure.setBackgroundResource(R.drawable.shape_btn_addloandark);
                    tickthree.setVisibility(View.INVISIBLE);
                    ticksix.setVisibility(View.INVISIBLE);
                    tickninth.setVisibility(View.INVISIBLE);
                    ticktwelve.setVisibility(View.VISIBLE);
                    tickeighteen.setVisibility(View.INVISIBLE);
                    LoanTenure = "12 Months";
                    TENURE = 12;
                });

                /*eighteenmonth.setOnClickListener(v111 -> {
                    btnLoanTenure.setBackgroundColor(R.color.dark_pink);
                    btnLoanTenure.setBackgroundResource(R.drawable.shape_btn_addchilddark);
                    tickthree.setVisibility(View.INVISIBLE);
                    ticksix.setVisibility(View.INVISIBLE);
                    tickninth.setVisibility(View.INVISIBLE);
                    ticktwelve.setVisibility(View.INVISIBLE);
                    tickeighteen.setVisibility(View.VISIBLE);
                    LoanTenure = "18 Months";
                });*/


                btnLoanTenure.setOnClickListener(view -> {
                    showTenure.setText(LoanTenure);
                    tempTenure=TENURE;
                    dialogLoan.dismiss();
                });

            }
        });


        agreeDisclosure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SVFinalDisclosure.setVisibility(View.GONE);
                presenter.botMessage("Now you need to accept our terms and conditions", loanDisclosure, ChatPresenter.chatObjects.size() + 1);


                //  presenter.botMessage("Thank You, All your details have been submitted sucessfully",loanDisclosure,ChatPresenter.chatObjects.size()+1);

//                presenter.botMessage("Next we will take you through the e-sign flow where in you have to e-sign your application",null,ChatPresenter.chatObjects.size()+1);
//                btnESign.setVisibility(View.VISIBLE);
            }
        });




        clearCurrAdd.setOnClickListener(v -> {
            useMyCurrentLocationLayout.setVisibility(View.GONE);
            llayoutFillAddressManually.setVisibility(View.GONE);
            ChatPresenter.chatObjects.subList(ChatPresenter.chatObjects.size()-1,ChatPresenter.chatObjects.size()).clear();
            ChatActivity.chatAdapter.notifyDataSetChanged();

        });

        clearVoterEdit.setOnClickListener(v -> {
            llUpdateVoterInfo.setVisibility(View.GONE);
            ChatPresenter.chatObjects.subList(ChatPresenter.chatObjects.size() - 1, ChatPresenter.chatObjects.size()).clear();
            chatAdapter.notifyDataSetChanged();

        });


        panCross.setOnClickListener(v -> {
            lLayoutUpdatePanInfo.setVisibility(View.GONE);
            if(isGovtCheckFailPan){
                ChatPresenter.chatObjects.subList(ChatPresenter.chatObjects.size() - 2, ChatPresenter.chatObjects.size()).clear();
                chatAdapter.notifyDataSetChanged();
                isGovtCheckFailPan=false;
            }else {
                ChatPresenter.chatObjects.subList(ChatPresenter.chatObjects.size() - 1, ChatPresenter.chatObjects.size()).clear();
                chatAdapter.notifyDataSetChanged();
            }



        });

        clearAadharEdit.setOnClickListener(v -> {
            lLayoutUpdateAadharInfo.setVisibility(View.GONE);
            ChatPresenter.chatObjects.subList(ChatPresenter.chatObjects.size() - 1, ChatPresenter.chatObjects.size()).clear();
            chatAdapter.notifyDataSetChanged();

        });


        imgCurrLocDown.setOnClickListener(v -> {

            imgCurrLocDown.setVisibility(View.GONE);
            imgCurrLocUp.setVisibility(View.VISIBLE);
            txtCurrLoc.setVisibility(View.VISIBLE);
        });



        llCurrentLocation.setOnClickListener(v -> {
            if(isLocationCheck){
                imgCurrLocDown.setVisibility(View.VISIBLE);
                imgCurrLocUp.setVisibility(View.GONE);
                txtCurrLoc.setVisibility(View.INVISIBLE);
                isLocationCheck=false;

            }else {

                imgCurrLocDown.setVisibility(View.GONE);
                imgCurrLocUp.setVisibility(View.VISIBLE);
                txtCurrLoc.setVisibility(View.VISIBLE);
                isLocationCheck=true;
            }

        });



        llContact.setOnClickListener(v -> {

            if(isContactCheck){
                imgContactDown.setVisibility(View.VISIBLE);
                imgContactUp.setVisibility(View.GONE);
                txtContact.setVisibility(View.INVISIBLE);
                isContactCheck=false;

            }else {
                imgContactDown.setVisibility(View.VISIBLE);
                imgContactUp.setVisibility(View.GONE);
                txtContact.setVisibility(View.VISIBLE);
                isContactCheck = true;
            }



        });


        llSMS.setOnClickListener(v -> {
            if(isSMSCheck){
                imgSmsDown.setVisibility(View.VISIBLE);
                imgSmsUp.setVisibility(View.GONE);
                txtSms.setVisibility(View.INVISIBLE);
                isSMSCheck = false;

            }else {
                imgSmsDown.setVisibility(View.GONE);
                imgSmsUp.setVisibility(View.VISIBLE);
                txtSms.setVisibility(View.VISIBLE);
                isSMSCheck = true;
            }
        });



        llDevice.setOnClickListener(v -> {

            if(isDeviceCheck){
                imgDeviceInfoDown.setVisibility(View.VISIBLE);
                imgDeviceInfoup.setVisibility(View.GONE);
                txtDeviceInfo.setVisibility(View.INVISIBLE);
                isDeviceCheck = false;

            }else {
                imgDeviceInfoDown.setVisibility(View.GONE);
                imgDeviceInfoup.setVisibility(View.VISIBLE);
                txtDeviceInfo.setVisibility(View.VISIBLE);
                isDeviceCheck = true;
            }
        });



        btnClearAddSchool.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llAddSchoolRequest.setVisibility(View.GONE);
            }
        });

        WillUploadPan.setOnClickListener(view -> {
            llayoutNoPanCard.setVisibility(View.GONE);
            presenter.onEditTextActionDone("I will upload the PAN card", null, ChatPresenter.chatObjects.size() + 1);
            presenter.botMessage("Great, Please choose from the options below to upload your PAN card", lLayoutPanOption, ChatPresenter.chatObjects.size() + 1);
            //  currentPos=ChatPresenter.chatObjects.size()+1;

        });



        llLoanTenureFinalSelect.setOnClickListener(v -> {
            dialogLoan = new BottomSheetDialog(ChatActivity.this);
            //  dialogLoan.setContentView(R.layout.custom_bottomdialog_loantenure);

            View viewvvv = View.inflate(this, R.layout.custom_bottomdialog_loantenure, null);
            if (dialogLoan == null) {
                dialogLoan = new BottomSheetDialog(ChatActivity.this);
            }
            dialogLoan.setContentView(viewvvv);

            BottomSheetBehavior bottomSheetBehaviorloan = BottomSheetBehavior.from(((View) viewvvv.getParent()));
            bottomSheetBehaviorloan.setState(BottomSheetBehavior.STATE_EXPANDED);
            loanClear = dialogLoan.findViewById(R.id.btnLoanTenureClear);
            rl3months = dialogLoan.findViewById(R.id.rl_3months);
            rl6months = dialogLoan.findViewById(R.id.rl_6months);
            rl9months = dialogLoan.findViewById(R.id.rl_9months);
            rl12months = dialogLoan.findViewById(R.id.rl_12months);

            threemonth = dialogLoan.findViewById(R.id.txt_3months);
            sixmonth = dialogLoan.findViewById(R.id.txt_6months);
            ninthmonth = dialogLoan.findViewById(R.id.txt_9months);
            twelvemonth = dialogLoan.findViewById(R.id.txt_12months);
            eighteenmonth = dialogLoan.findViewById(R.id.txt_18months);

            btnLoanTenure = dialogLoan.findViewById(R.id.btn_Select_loantenure);
            tickthree = dialogLoan.findViewById(R.id.tick3);
            ticksix = dialogLoan.findViewById(R.id.tick6);
            tickninth = dialogLoan.findViewById(R.id.tick9);
            ticktwelve = dialogLoan.findViewById(R.id.tick12);
            tickeighteen = dialogLoan.findViewById(R.id.tick18);

            Window window = dialogLoan.getWindow();
            window.setGravity(Gravity.CENTER);

            dialogLoan.getWindow().setBackgroundDrawable(new
                    ColorDrawable(Color.TRANSPARENT));
            dialogLoan.show();

            loanClear.setOnClickListener(v12 -> {
                dialogLoan.dismiss();

            });
            rl3months.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    btnLoanTenure.setBackgroundResource(R.drawable.shape_btn_addloandark);
                    tickthree.setVisibility(View.VISIBLE);
                    ticksix.setVisibility(View.INVISIBLE);
                    tickninth.setVisibility(View.INVISIBLE);
                    ticktwelve.setVisibility(View.INVISIBLE);
                    LoanTenure = "3 Months";
                }
            });
            rl6months.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    btnLoanTenure.setBackgroundResource(R.drawable.shape_btn_addloandark);
                    tickthree.setVisibility(View.INVISIBLE);
                    ticksix.setVisibility(View.VISIBLE);
                    tickninth.setVisibility(View.INVISIBLE);
                    ticktwelve.setVisibility(View.INVISIBLE);
                    LoanTenure = "6 Months";
                }
            });

            rl9months.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    btnLoanTenure.setBackgroundResource(R.drawable.shape_btn_addloandark);
                    tickthree.setVisibility(View.INVISIBLE);
                    ticksix.setVisibility(View.INVISIBLE);
                    tickninth.setVisibility(View.VISIBLE);
                    ticktwelve.setVisibility(View.INVISIBLE);
                    LoanTenure = "9 Months";
                }
            });

            rl12months.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    btnLoanTenure.setBackgroundResource(R.drawable.shape_btn_addloandark);
                    tickthree.setVisibility(View.INVISIBLE);
                    ticksix.setVisibility(View.INVISIBLE);
                    tickninth.setVisibility(View.INVISIBLE);
                    ticktwelve.setVisibility(View.VISIBLE);
                    LoanTenure = "12 Months";
                    TENURE = 12;
                }
            });


            threemonth.setOnClickListener(v111 -> {

                btnLoanTenure.setBackgroundResource(R.drawable.shape_btn_addloandark);
                tickthree.setVisibility(View.VISIBLE);
                ticksix.setVisibility(View.INVISIBLE);
                tickninth.setVisibility(View.INVISIBLE);
                ticktwelve.setVisibility(View.INVISIBLE);
                //   tickeighteen.setVisibility(View.INVISIBLE);
                LoanTenure = "3 Months";
                TENURE = 3;
            });

            sixmonth.setOnClickListener(v111 -> {

                btnLoanTenure.setBackgroundResource(R.drawable.shape_btn_addloandark);
                tickthree.setVisibility(View.INVISIBLE);
                ticksix.setVisibility(View.VISIBLE);
                tickninth.setVisibility(View.INVISIBLE);
                ticktwelve.setVisibility(View.INVISIBLE);
                //     tickeighteen.setVisibility(View.INVISIBLE);
                LoanTenure = "6 Months";
                TENURE = 6;
            });

            ninthmonth.setOnClickListener(v111 -> {

                btnLoanTenure.setBackgroundResource(R.drawable.shape_btn_addloandark);
                tickthree.setVisibility(View.INVISIBLE);
                ticksix.setVisibility(View.INVISIBLE);
                tickninth.setVisibility(View.VISIBLE);
                ticktwelve.setVisibility(View.INVISIBLE);
//                tickeighteen.setVisibility(View.INVISIBLE);

                LoanTenure = "9 Months";
                TENURE = 9;
            });

            twelvemonth.setOnClickListener(v111 -> {

                btnLoanTenure.setBackgroundResource(R.drawable.shape_btn_addloandark);
                tickthree.setVisibility(View.INVISIBLE);
                ticksix.setVisibility(View.INVISIBLE);
                tickninth.setVisibility(View.INVISIBLE);
                ticktwelve.setVisibility(View.VISIBLE);
                //   tickeighteen.setVisibility(View.INVISIBLE);
                LoanTenure = "12 Months";
                TENURE = 12;
            });

   /*         eighteenmonth.setOnClickListener(v111 -> {
                btnLoanTenure.setBackgroundColor(R.color.dark_pink);
                btnLoanTenure.setBackgroundResource(R.drawable.shape_btn_addchilddark);
                tickthree.setVisibility(View.INVISIBLE);
                ticksix.setVisibility(View.INVISIBLE);
                tickninth.setVisibility(View.INVISIBLE);
                ticktwelve.setVisibility(View.INVISIBLE);
                tickeighteen.setVisibility(View.VISIBLE);
                LoanTenure = "18 Months";
            });*/


            btnLoanTenure.setOnClickListener(view -> {
                showTenure.setText(LoanTenure);
                showTenureFinal.setText(LoanTenure);
                tempTenure=TENURE;

                if(rbWeekly.isChecked()){
                    // emiRate=  String.valueOf(Math.round(calculateEmi(eligibileLoanBar.getProgress(),12,tempTenure)/4));
                    emiRate=  String.valueOf(Math.round(getEMI(eligibileLoanBar.getProgress(),tempTenure)));
                    emiRateFindeed.setText("\u20B9 "+emiRate+"/Weekly");
                }else {
                    // emiRate=  String.valueOf(Math.round(calculateEmi(eligibileLoanBar.getProgress(),12,tempTenure)));
                    emiRate=  String.valueOf(Math.round(getEMI(eligibileLoanBar.getProgress(),tempTenure)));
                    emiRateFindeed.setText("\u20B9 "+emiRate+"/Monthly");
                }

                dialogLoan.dismiss();
            });



        });

        llLoanTenureSelect.setOnClickListener(v -> {

            dialogLoan = new BottomSheetDialog(ChatActivity.this);
            //dialogLoan.setContentView(R.layout.custom_bottomdialog_loantenure);

            View viewvvv = View.inflate(this, R.layout.custom_bottomdialog_loantenure, null);
            if (dialogLoan == null) {
                dialogLoan = new BottomSheetDialog(ChatActivity.this);
            }
            dialogLoan.setContentView(viewvvv);

            BottomSheetBehavior bottomSheetBehaviorloan = BottomSheetBehavior.from(((View) viewvvv.getParent()));
            // bottomSheetBehavior.setPeekHeight(1800);

            bottomSheetBehaviorloan.setState(BottomSheetBehavior.STATE_EXPANDED);

            rl3months = dialogLoan.findViewById(R.id.rl_3months);
            rl6months = dialogLoan.findViewById(R.id.rl_6months);
            rl9months = dialogLoan.findViewById(R.id.rl_9months);
            rl12months = dialogLoan.findViewById(R.id.rl_12months);

            loanClear = dialogLoan.findViewById(R.id.btnLoanTenureClear);
            threemonth = dialogLoan.findViewById(R.id.txt_3months);
            sixmonth = dialogLoan.findViewById(R.id.txt_6months);
            ninthmonth = dialogLoan.findViewById(R.id.txt_9months);
            twelvemonth = dialogLoan.findViewById(R.id.txt_12months);
            // eighteenmonth = dialogLoan.findViewById(R.id.txt_18months);

            btnLoanTenure = dialogLoan.findViewById(R.id.btn_Select_loantenure);
            tickthree = dialogLoan.findViewById(R.id.tick3);
            ticksix = dialogLoan.findViewById(R.id.tick6);
            tickninth = dialogLoan.findViewById(R.id.tick9);
            ticktwelve = dialogLoan.findViewById(R.id.tick12);
            //tickeighteen = dialogLoan.findViewById(R.id.tick18);

            Window window = dialogLoan.getWindow();
            window.setGravity(Gravity.CENTER);

            dialogLoan.getWindow().setBackgroundDrawable(new
                    ColorDrawable(Color.TRANSPARENT));
            dialogLoan.show();

            loanClear.setOnClickListener(v12 -> {
                dialogLoan.dismiss();

            });

            rl3months.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    btnLoanTenure.setBackgroundResource(R.drawable.shape_btn_addloandark);
                    tickthree.setVisibility(View.VISIBLE);
                    ticksix.setVisibility(View.INVISIBLE);
                    tickninth.setVisibility(View.INVISIBLE);
                    ticktwelve.setVisibility(View.INVISIBLE);
                    LoanTenure = "3 Months";
                }
            });
            rl6months.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    btnLoanTenure.setBackgroundResource(R.drawable.shape_btn_addloandark);
                    tickthree.setVisibility(View.INVISIBLE);
                    ticksix.setVisibility(View.VISIBLE);
                    tickninth.setVisibility(View.INVISIBLE);
                    ticktwelve.setVisibility(View.INVISIBLE);
                    LoanTenure = "6 Months";
                    TENURE = 6;

                }
            });

            rl9months.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    btnLoanTenure.setBackgroundResource(R.drawable.shape_btn_addloandark);
                    tickthree.setVisibility(View.INVISIBLE);
                    ticksix.setVisibility(View.INVISIBLE);
                    tickninth.setVisibility(View.VISIBLE);
                    ticktwelve.setVisibility(View.INVISIBLE);
                    LoanTenure = "9 Months";
                    TENURE = 9;
                }
            });

            rl12months.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    btnLoanTenure.setBackgroundResource(R.drawable.shape_btn_addloandark);
                    tickthree.setVisibility(View.INVISIBLE);
                    ticksix.setVisibility(View.INVISIBLE);
                    tickninth.setVisibility(View.INVISIBLE);
                    ticktwelve.setVisibility(View.VISIBLE);
                    LoanTenure = "12 Months";
                    TENURE = 12;
                }
            });


            threemonth.setOnClickListener(v111 -> {


                btnLoanTenure.setBackgroundResource(R.drawable.shape_btn_addloandark);
                tickthree.setVisibility(View.VISIBLE);
                ticksix.setVisibility(View.INVISIBLE);
                tickninth.setVisibility(View.INVISIBLE);
                ticktwelve.setVisibility(View.INVISIBLE);
                //     tickeighteen.setVisibility(View.INVISIBLE);
                LoanTenure = "3 Months";
                TENURE = 3;
            });

            sixmonth.setOnClickListener(v111 -> {

                btnLoanTenure.setBackgroundResource(R.drawable.shape_btn_addloandark);
                tickthree.setVisibility(View.INVISIBLE);
                ticksix.setVisibility(View.VISIBLE);
                tickninth.setVisibility(View.INVISIBLE);
                ticktwelve.setVisibility(View.INVISIBLE);
                //   tickeighteen.setVisibility(View.INVISIBLE);
                LoanTenure = "6 Months";
                TENURE = 6;
            });

            ninthmonth.setOnClickListener(v111 -> {

                btnLoanTenure.setBackgroundResource(R.drawable.shape_btn_addloandark);
                tickthree.setVisibility(View.INVISIBLE);
                ticksix.setVisibility(View.INVISIBLE);
                tickninth.setVisibility(View.VISIBLE);
                ticktwelve.setVisibility(View.INVISIBLE);
                //  tickeighteen.setVisibility(View.INVISIBLE);
                LoanTenure = "9 Months";
                TENURE = 9;
            });

            twelvemonth.setOnClickListener(v111 -> {

                btnLoanTenure.setBackgroundResource(R.drawable.shape_btn_addloandark);
                tickthree.setVisibility(View.INVISIBLE);
                ticksix.setVisibility(View.INVISIBLE);
                tickninth.setVisibility(View.INVISIBLE);
                ticktwelve.setVisibility(View.VISIBLE);
                //   tickeighteen.setVisibility(View.INVISIBLE);
                LoanTenure = "12 Months";
                TENURE = 12;

            });

          /*  eighteenmonth.setOnClickListener(v111 -> {
                btnLoanTenure.setBackgroundColor(R.color.dark_pink);
                btnLoanTenure.setBackgroundResource(R.drawable.shape_btn_addchilddark);
                tickthree.setVisibility(View.INVISIBLE);
                ticksix.setVisibility(View.INVISIBLE);
                tickninth.setVisibility(View.INVISIBLE);
                ticktwelve.setVisibility(View.INVISIBLE);
                tickeighteen.setVisibility(View.VISIBLE);
                LoanTenure = "18 Months";
            });*/


            btnLoanTenure.setOnClickListener(view -> {
                showTenure.setText(LoanTenure);
                tempTenure=TENURE;
                showTenureFinal.setText(LoanTenure);
                dialogLoan.dismiss();
            });
        });

        findeedProcessingFee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                LayoutInflater inflater = getLayoutInflater();
                View layout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));

                TextView tv = (TextView) layout.findViewById(R.id.txtvw);
                Toast toast = new Toast(getApplicationContext());
                toast.setDuration(Toast.LENGTH_LONG);
                toast.setView(layout);
                toast.show();
                toast.setDuration(20000);


            }
        });





        panDobEditText.setOnTouchListener((v, event) -> {

            final int DRAWABLE_LEFT = 0;
            final int DRAWABLE_TOP = 1;
            final int DRAWABLE_RIGHT = 2;
            final int DRAWABLE_BOTTOM = 3;

            if (event.getAction() == MotionEvent.ACTION_UP) {

                final Calendar cldr = Calendar.getInstance();
                cldr.add(Calendar.YEAR, -23);


                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog


                picker = new DatePickerDialog(ChatActivity.this, R.style.MyDatePickerDialogStyle, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        String selectedDate;
                        boolean dTrue=checkDay(dayOfMonth);
                        boolean mTrue=checkMonth(month+1);
                        if(mTrue && dTrue)
                            selectedDate="0"+dayOfMonth + "/" +"0"+ (month + 1) + "/" + year;
                        else if(dTrue)
                            selectedDate="0"+dayOfMonth + "/" + (month + 1) + "/" + year;
                        else if(mTrue)
                            selectedDate=dayOfMonth + "/" +"0"+ (month + 1) + "/" + year;
                        else
                            selectedDate=dayOfMonth + "/" + (month + 1) + "/" + year;
                        panDobEditText.setText(selectedDate);


                    }
                },year, month, day);

                picker.show();
                picker.getButton(DatePickerDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(this,R.color.dark_gray));
                picker.getButton(DatePickerDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(this,R.color.dark_pink));
                picker.getDatePicker().setMaxDate(cldr.getTimeInMillis());


                return true;

            }

            return false;
        });


        panDobEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        voterDobEditText.setOnTouchListener((v, event) -> {

            final int DRAWABLE_LEFT = 0;
            final int DRAWABLE_TOP = 1;
            final int DRAWABLE_RIGHT = 2;
            final int DRAWABLE_BOTTOM = 3;
            panIdEditText.clearFocus();
            panNameEditText.clearFocus();

            if (event.getAction() == MotionEvent.ACTION_UP) {

                final Calendar cldr = Calendar.getInstance();
                cldr.add(Calendar.YEAR, -23);
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker



                picker = new DatePickerDialog(ChatActivity.this, R.style.MyDatePickerDialogStyle, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        String  selectedDate;
                        boolean dTrue=checkDay(dayOfMonth);
                        boolean mTrue=checkMonth(month+1);
                        if(mTrue && dTrue)
                            selectedDate="0"+dayOfMonth + "/" +"0"+ (month + 1) + "/" + year;
                        else if(dTrue)
                            selectedDate="0"+dayOfMonth + "/" + (month + 1) + "/" + year;
                        else if(mTrue)
                            selectedDate=dayOfMonth + "/" +"0"+ (month + 1) + "/" + year;
                        else
                            selectedDate=dayOfMonth + "/" + (month + 1) + "/" + year;
                        voterDobEditText.setText(selectedDate);
                    }
                },year, month, day);

                picker.show();
                picker.getButton(DatePickerDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(this,R.color.dark_gray));
                picker.getButton(DatePickerDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(this,R.color.dark_pink));
                picker.getDatePicker().setMaxDate(cldr.getTimeInMillis());

                return true;

            }
            return false;
        });

        loanTenureButton.setOnClickListener(view -> {
            try{
                if(LOAN_TYPE==1){
                    if (LoanTenure == null || LoanTenure.equals("")) {
                        Toast.makeText(getApplicationContext(), "For How Long ?", Toast.LENGTH_SHORT).show();
                    } else {
                        loanTenure.setVisibility(View.GONE);

                        Log.i("school_check",new Gson().toJson(CUSTOMER_DETAILS));

                        presenter.onEditTextActionDone(LoanTenure, null, ChatPresenter.chatObjects.size() + 1,CUSTOMER_DETAILS);
                        presenter.botMessage("So what you do?", llayoutwhatyoudo, ChatPresenter.chatObjects.size() + 1);
                        new SaveLoanTenure().execute();
                    }
                }

                else {
                    if (LoanTenure == null || LoanTenure.equals("")) {
                        Toast.makeText(getApplicationContext(), "For How Long ?", Toast.LENGTH_SHORT).show();
                    } else {
                        ChatResponseVH.currentActiveView.setVisibility(View.GONE);
                        presenter.onEditTextActionDone(LoanTenure, null, ChatPresenter.chatObjects.size() + 1,CUSTOMER_DETAILS);
                        presenter.botMessage("Can you provide us your PAN card for ID verification",panCardLayout,ChatPresenter.chatObjects.size()+1);
                        new SaveLoanTenure().execute();
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
                Log.d("Error in sending loan tenure",e.toString());
            }


        });






        numberPicker.setOnValueChangedListener((picker, oldVal, newVal) -> {
            selectedValue = values[newVal];
            selectedIndex=newVal;
            isNumberPickerValueChanged = true;
            if(selectedValue.equals(values[values.length-1])==true) {


                numberPickerDownImageGray.setVisibility(View.VISIBLE);
                numberPickerUpimagePink.setVisibility(View.VISIBLE);
                numberPickerDownImagePink.setVisibility(View.GONE);
                numberpickerUpimageGray.setVisibility(View.GONE);

            } else if(selectedValue.equals(values[0])==true) {

                numberPickerDownImageGray.setVisibility(View.GONE);
                numberPickerUpimagePink.setVisibility(View.GONE);
                numberPickerDownImagePink.setVisibility(View.VISIBLE);
                numberpickerUpimageGray.setVisibility(View.VISIBLE);
            }
            else


            {

                numberPickerDownImageGray.setVisibility(View.GONE);
                numberPickerUpimagePink.setVisibility(View.VISIBLE);
                numberPickerDownImagePink.setVisibility(View.VISIBLE);
                numberpickerUpimageGray.setVisibility(View.GONE);
            }

        });




      /*  numberPickerDownImagePink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                numberPicker.setValue(selectedIndex+1);
                selectedIndex=selectedIndex+1;
                selectedValue=values[selectedIndex];

                isNumberPickerValueChanged = true;
                if(selectedValue.equals(values[values.length-1])==true) {
                    numberPickerDownImageGray.setVisibility(View.VISIBLE);
                    numberPickerUpimagePink.setVisibility(View.VISIBLE);
                    numberPickerDownImagePink.setVisibility(View.GONE);
                    numberpickerUpimageGray.setVisibility(View.GONE);

                } else if(selectedValue.equals(values[0])==true) {
                    numberPickerDownImageGray.setVisibility(View.GONE);
                    numberPickerUpimagePink.setVisibility(View.GONE);
                    numberPickerDownImagePink.setVisibility(View.VISIBLE);
                    numberpickerUpimageGray.setVisibility(View.VISIBLE);
                }
                else
                {
                    numberPickerDownImageGray.setVisibility(View.GONE);
                    numberPickerUpimagePink.setVisibility(View.VISIBLE);
                    numberPickerDownImagePink.setVisibility(View.VISIBLE);
                    numberpickerUpimageGray.setVisibility(View.GONE);
                }
            }
        });*/

     /*   numberPickerUpimagePink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                numberPicker.setValue(selectedIndex-1);
                selectedIndex=selectedIndex-1;
                selectedValue=values[selectedIndex];
                isNumberPickerValueChanged = true;
                if(selectedValue.equals(values[values.length-1])==true) {
                    numberPickerDownImageGray.setVisibility(View.VISIBLE);
                    numberPickerUpimagePink.setVisibility(View.VISIBLE);
                    numberPickerDownImagePink.setVisibility(View.GONE);
                    numberpickerUpimageGray.setVisibility(View.GONE);

                } else if(selectedValue.equals(values[0])==true) {
                    numberPickerDownImageGray.setVisibility(View.GONE);
                    numberPickerUpimagePink.setVisibility(View.GONE);
                    numberPickerDownImagePink.setVisibility(View.VISIBLE);
                    numberpickerUpimageGray.setVisibility(View.VISIBLE);
                }
                else
                {
                    numberPickerDownImageGray.setVisibility(View.GONE);
                    numberPickerUpimagePink.setVisibility(View.VISIBLE);
                    numberPickerDownImagePink.setVisibility(View.VISIBLE);
                    numberpickerUpimageGray.setVisibility(View.GONE);
                }
            }
        });*/









   /*     agreeAndContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                agreeAndContinue.setVisibility(View.GONE);
                loanSummary.setVisibility(View.GONE);
                voucherVerified.setVisibility(View.GONE);
                presenter.botMessage("Can you provide us 2 pepole who can stand for you?", btnSelectVoucher, ChatPresenter.chatObjects.size() + 1);

            }
        });*/
       /* buttonLoanSummary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonLoanSummary.setVisibility(View.GONE);
                loanSummary.setVisibility(View.VISIBLE);
            }
        });*/

        verifiedScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                llayoutVoucherVerification.setVisibility(View.GONE);
                //  presenter.botMessage("Both the vouchers have completed the vouching process. We are happy to take your loan application forward!");
                voucherVerified.setVisibility(View.VISIBLE);
            }
        });

        btnPanContinue.setOnClickListener(v -> {

            btnPanContinue.setVisibility(View.GONE);
//            ChatPresenter.chatObjects.subList(ChatPresenter.chatObjects.size() - 1, ChatPresenter.chatObjects.size()).clear();
//            ChatActivity.chatAdapter.notifyDataSetChanged();
            lLayoutProcessingPan.setVisibility(View.VISIBLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            DOC_TYPE = "PAN";
            PAN_COUNT=0;
            if(PAN_COUNT>1){
                new SendErrorFlags("PAN_UPLOAD_NUMBER","2").execute();
                lLayoutProcessingPan.setVisibility(View.GONE);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                presenter.botMessage("Looks like you are having an issue uploading the Pan. We will reach out to you to resolve this. Thanks for your patience",null,ChatPresenter.chatObjects.size()+1);
            }else {
                new GetDocumentData().execute();

            }
            PAN_COUNT++;


        });


        iconRestart.setOnClickListener(v -> {

            isStartOverClicked = true;

            final Dialog dialog = new Dialog(ChatActivity.this);
            dialog.setContentView(R.layout.custom_dialog_startover);

            TextView txtYesStartOver = dialog.findViewById(R.id.txt_yesStartOver);
            TextView txtNoStartOver = dialog.findViewById(R.id.txt_cancelStartOver);

            dialog.getWindow().setBackgroundDrawable(new
                    ColorDrawable(Color.TRANSPARENT));



            txtYesStartOver.setOnClickListener(v1 -> {
                Intent intent = getIntent();
                new StartOver(intent).execute();
                dialog.dismiss();




//                ChatPresenter.chatObjects.subList(0, ChatPresenter.chatObjects.size()).clear();
//                chatAdapter.notifyDataSetChanged();

//                presenter.botMessage("Good Morning! "+getResources().getString(R.string.smiley),null,ChatPresenter.chatObjects.size()+1);
//                presenter.botMessage("I hope you are doing well.",null,ChatPresenter.chatObjects.size()+1);
//                presenter.botMessage("What type of loan are you looking out for?",loanType,ChatPresenter.chatObjects.size()+1);
//                dialog.dismiss();
            });


            txtNoStartOver.setOnClickListener(v1 -> {
                dialog.dismiss();
            });
            dialog.show();
        });


        firstname2 = findViewById(R.id.firstname2);
        surname2 = findViewById(R.id.surname2);
        // surname2 = findViewById(R.id.surname2);
        addChild = findViewById(R.id.childBt2);
        childHeader = findViewById(R.id.addChildHeader);

        childFirstNameError=findViewById(R.id.child_firstname_error);
        childSurnameError=findViewById(R.id.child_surname_error);
        childClassError=findViewById(R.id.child_class_error);


        firstname2.setOnFocusChangeListener(this);
        surname2.setOnFocusChangeListener(this);
        // childClass2.setOnFocusChangeListener(this);



       /* selectChildClass.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {


                return false;
            }
        });*/


        ctx = getApplicationContext();


        clickPanFront.setOnClickListener(v -> {

            if ( ContextCompat.checkSelfPermission(ChatActivity.ctx,Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Intent takePictureIntent = new Intent(this, CameraActivity.class);
                //Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                        final File file=new File(storageDir, "pic.jpg");
                        photoFile=file;
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    if (photoFile != null) {
                        panURI = FileProvider.getUriForFile(this,
                                "com.findeed.fileprovider",
                                photoFile);
                        // takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, panURI);
                        // takePictureIntent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                        // startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO_PAN);
                        takePictureIntent.putExtra("image_side","FRONT SIDE");
                        takePictureIntent.putExtra("doc_type","PAN");

                        startActivityForResult(takePictureIntent,REQUEST_TAKE_PHOTO_PAN);
                    }
                }

            } else {
                requestPermissions(new String[]{
                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                }, CAMERA_PERMISSION);
            }

        });


        btnPanPickPhoto.setOnClickListener(v -> {
            try {
                Intent galleryIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
                galleryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent, PAN_PICK_PHOTO);

            } catch (Exception e) {
                e.printStackTrace();
            }
        });








        /*getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeAsUpIndicator(R.drawable.arrow_drop_down);


*/

        dl = findViewById(R.id.activity_main);
        nv = findViewById(R.id.nv);
        nv.setNavigationItemSelectedListener(this);
        nv.setItemIconTintList(null);
        // dl.setStatusBarBackgroundColor(getColor(R.color.dark_pink));
        // actionBar.setDisplayHomeAsUpEnabled(true);


        View headerView = nv.getHeaderView(0);
        TextView navUsername =  headerView.findViewById(R.id.user_name);
        ImageView navOpenProfile = headerView.findViewById(R.id.open_profile);
        navUsername.setText(validateOTPResponse.getData().getFirstName()+" "+validateOTPResponse.getData().getLastName());







        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        navicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!dl.isDrawerOpen(GravityCompat.START)) {
                    dl.openDrawer(GravityCompat.START);
                } else {
                    dl.closeDrawer(GravityCompat.START);
                }
            }
        });


        navOpenProfile.setOnClickListener(new View.OnClickListener() {



            @Override
            public void onClick(View v) {
                DocumentData docPan=new DocumentData();
                DocumentData docVoter=new DocumentData();
                DocumentData docAdhaar=new DocumentData();
                Log.i("docsss", new Gson().toJson(CUSTOMER_DETAILS));
                for(int i=0;i<(CUSTOMER_DETAILS.getDocuments().size());i++){
                    if(CUSTOMER_DETAILS.getDocuments().get(i).getDocumentType().equals("PAN")){
                        docPan=CUSTOMER_DETAILS.getDocuments().get(i);
                    }
                    if(CUSTOMER_DETAILS.getDocuments().get(i).getDocumentType().equals("VOTER_ID")){
                        docVoter=CUSTOMER_DETAILS.getDocuments().get(i);
                    }
                    if(CUSTOMER_DETAILS.getDocuments().get(i).getDocumentType().equals("AADHAAR")){
                        docAdhaar=CUSTOMER_DETAILS.getDocuments().get(i);
                    }
                }

                Intent intent = new Intent(ChatActivity.this, MyProfile.class);
                intent.putExtra("fname",validateOTPResponse.getData().getFirstName());
                intent.putExtra("lname",validateOTPResponse.getData().getLastName());
//               intent.putExtra("dob",docPan!=null?(!docPan.getDateOfBirth().equals("")?docPan.getDateOfBirth():""):"");
//               intent.putExtra("contact",CUSTOMER_DETAILS.getPhoneNumber());
//               intent.putExtra("whatsapp",CUSTOMER_DETAILS.getWhatsappNumber()!=null?CUSTOMER_DETAILS.getWhatsappNumber():"");
//               intent.putExtra("email",CUSTOMER_DETAILS.getEmailAddress()!=null?CUSTOMER_DETAILS.getEmailAddress():"");
//               intent.putExtra("address",CUSTOMER_DETAILS.getAddress()!=null?(!CUSTOMER_DETAILS.getAddress().getAddressLine1().equals("")?CUSTOMER_DETAILS.getAddress().getAddressLine1():""):"");
//               intent.putExtra("pan",docPan!=null?(!docPan.getDocumentId().equals("")?docPan.getDocumentId():""):"");
//
//              if(docAdhaar!=null){
//                  intent.putExtra("adhaar",docAdhaar.getDocumentId()!=null?(!docAdhaar.getDocumentId().equals("")?docAdhaar.getDocumentId():""):"");
//
//              }
//               if(docVoter!=null){
//                  intent.putExtra("voter",docVoter.getDocumentId()!=null?(!docVoter.getDocumentId().equals("")?docVoter.getDocumentId():""):"");
//
//              }
//               intent.putExtra("language",CUSTOMER_DETAILS.getLanguage()!=null? CUSTOMER_DETAILS.getLanguage():"English");
//
//
                //              startActivity(intent);
               startActivity(intent);
//
            }
        });






        layout_Btn_Personal.setOnTouchListener((v, event) -> {

            imgone.setVisibility(View.VISIBLE);
            imgtwo.setVisibility(View.INVISIBLE);
            return false;
        });


        layout_Btn_Educational.setOnTouchListener((v, event) -> {
            imgtwo.setVisibility(View.VISIBLE);
            imgone.setVisibility(View.INVISIBLE);

            return false;
        });

        btnvoterid.setOnTouchListener((v, event) -> {
            imageVoter.setVisibility(View.INVISIBLE);
            imageAadhar.setVisibility(View.INVISIBLE);
            return false;
        });

        btnaadhar.setOnTouchListener((v, event) -> {
            imageAadhar.setVisibility(View.VISIBLE);
            imageVoter.setVisibility(View.INVISIBLE);
            return false;
        });



        iHavePan.setOnTouchListener((v, event) -> {
            // flags.put("isPanAvailable",1);
            imagepan.setVisibility(View.VISIBLE);
            imagenopan.setVisibility(View.INVISIBLE);
            // new SaveCustomerFlag().execute();
            return false;
        });

        iDontHavePan.setOnTouchListener((v, event) -> {
            // flags.put("isPanAvailable",0);
            imagenopan.setVisibility(View.VISIBLE);
            imagepan.setVisibility(View.INVISIBLE);
            // new SaveCustomerFlag().execute();
            return false;
        });





        layout_Btn_Educational.setOnClickListener(view -> {

            try{

                loanType.setVisibility(View.GONE);
                presenter.onEditTextActionDone("Educational Loan", lvlay, ChatPresenter.chatObjects.size() + 1,validateOTPResponse.getData());
                presenter.botMessage("Which school does your child go to ?", actvFrameLayout, ChatPresenter.chatObjects.size() + 1);
                LOAN_TYPE = 1;
                new createCustomerLoan().execute();




            }catch (Exception e){
                e.printStackTrace();
            }


        });


        btneditinfo.setOnClickListener(v -> {
            new SendErrorFlags("IS_PAN_EDITED","1").execute();
            isEditPanClicked = true;
            panCardLayout.setVisibility(View.GONE);
            lLayoutEditPanInfo.setVisibility(View.GONE);
            panIdEditText.setText(panNumber);
            panNameEditText.setText(panName);
            panDobEditText.setText(panDob);
            presenter.botMessage("Please edit PAN details in the following window", lLayoutUpdatePanInfo, ChatPresenter.chatObjects.size() + 1);
            // new SaveCustomerFlag().execute();

        });




        panIdEditText.setOnFocusChangeListener(this);
        panNameEditText.setOnFocusChangeListener(this);

        btnSavePanDetail.setOnClickListener(v -> {
            panNameEditText.clearFocus();
            panIdEditText.clearFocus();
            panDobEditText.clearFocus();

            if(panNameValidations.getVisibility()==View.VISIBLE || panIdValidations.getVisibility()==View.VISIBLE){

            }
            else if(isNullOrBlank(panNameEditText.getText().toString()) || isNullOrBlank(panIdEditText.getText().toString()) || isNullOrBlank(panDobEditText.getText().toString())){
                if(isNullOrBlank(panNameEditText.getText().toString())){
                    panNameValidations.setVisibility(View.VISIBLE);
                }
                if(isNullOrBlank(panIdEditText.getText().toString())){
                    panIdValidations.setVisibility(View.VISIBLE);
                }
                if(isNullOrBlank(panDobEditText.getText().toString())){
                    panDOBValidations.setVisibility(View.VISIBLE);
                }
            }

            else {

                new VerifyUserDocumentPAN(panIdEditText.getText().toString(), panNameEditText.getText().toString(), panDobEditText.getText().toString()).execute();
                PAN_DOB= panDobEditText.getText().toString();
                hideKeyboard();
                DOC_TYPE = "PAN";
                savePan=new ProgressDialog(ChatActivity.this);
                savePan.setMessage("Please Wait...");
                savePan.setCancelable(false);
                savePan.show();
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                new SaveDocumentDetails().execute();
                USER_NAME= panNameEditText.getText().toString();
                new SaveCustomerName(USER_NAME, "", CUSTOMER_ID).execute();

            }

        });


        panDobEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                panDOBValidations.setVisibility(View.GONE);

            }
        });


        btnconfirm.setOnClickListener(v -> {

            boolean age=validateAge();
            if(age){
                if(panDob!=null){
                    new SendErrorFlags("CUSTOMER_AGE_RESTRICTION","1").execute();
                    panCardLayout.setVisibility(View.GONE);
                    lLayoutEditPanInfo.setVisibility(View.GONE);
                    presenter.botMessage("Awesome now we have your ID proof.", null, ChatPresenter.chatObjects.size() + 1);
                    presenter.botMessage("Can you provide your address proof? You can give us Your Aadhar or Voter's ID card. Which one would you like to use.", llayoutoption, ChatPresenter.chatObjects.size() + 1);
                }else
                    presenter.botMessage("We were not able to read data from the image you provided, Please fill your Voter's Id details manually",llUpdateVoterInfo,ChatPresenter.chatObjects.size()+1);


            }else {
                panCardLayout.setVisibility(View.GONE);
                lLayoutEditPanInfo.setVisibility(View.GONE);
                new SendErrorFlags("CUSTOMER_AGE_RESTRICTION","2").execute();
                presenter.botMessage("Sorry we are unable to process your application. The applicant must be atleast  23 years old and cannot be older than 60 years. Please email us on contact@findeed.in if need further help.",null,ChatPresenter.chatObjects.size()+1);
            }

            new SendErrorFlags("IS_PAN_EDITED","2").execute();

        });

        txtneedhelp.setOnClickListener(v -> {


            llayoutneedhelp.setVisibility(View.GONE);

//            final Dialog dialog = new Dialog(ChatActivity.this);
//            dialog.setContentView(R.layout.custom_dialog_call_us);
//
//            final LinearLayout layoutcallus = dialog.findViewById(R.id.llcallus);
//            layoutcallus.setOnClickListener(v13 -> {
//                layoutcallus.setVisibility(View.GONE);
//                llayoutneedhelp.setVisibility(View.GONE);
//                dialog.dismiss();
//                /*presenter.botMessage("Awesome now we have your ID proof.");
//                presenter.botMessage("Can you provide your address proof? You can give us your Aadhar or Voter's ID card. Which one would you like  to use?");*/
//
//
//            });
//
//            dialog.show();

        });

        txtnooption.setOnClickListener(v -> {

            llayoutnoidoption.setVisibility(View.GONE);
            presenter.onEditTextActionDone("We are waiting for your details. Please select one option. If you currently don't have a proof, we will wait for you to come back with the details.", null, ChatPresenter.chatObjects.size() + 1);
        });

        btnaadhar.setOnClickListener(v -> {
            //  llayoutnoidoption.setVisibility(View.GONE);
            presenter.onEditTextActionDone("Aadhaar Card", null, ChatPresenter.chatObjects.size() + 1,CUSTOMER_DETAILS);
            llayoutoption.setVisibility(View.GONE);
            presenter.botMessage("Steps to follow:" + getResources().getString(R.string.aadhaar_steps), llayoutaadharpicture, ChatPresenter.chatObjects.size() + 1);
            chooseedAadhar = true;
            DOC_TYPE = "AADHAAR";
        });


        btnvoterid.setOnClickListener(v -> {
            final Dialog voterDialog = new Dialog(ChatActivity.this);
            voterDialog.setContentView(R.layout.dialog_vouchervoteridinfo);

            TextView useAadhaarCard = voterDialog.findViewById(R.id.use_aadhaar_card);
            TextView continueWithVoter = voterDialog.findViewById(R.id.continue_with_voter);

            voterDialog.getWindow().setBackgroundDrawable(new
                    ColorDrawable(Color.TRANSPARENT));
            voterDialog.show();


            continueWithVoter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    llayoutoption.setVisibility(View.GONE);
                    voterDialog.dismiss();
                    presenter.onEditTextActionDone("Voter Id", null, ChatPresenter.chatObjects.size() + 1,CUSTOMER_DETAILS);
                    presenter.botMessage("Can you provide your Voter's ID for verification?", llayoutoptionselectforvoterid, ChatPresenter.chatObjects.size() + 1);
                }
            });

            useAadhaarCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    voterDialog.dismiss();
                    llayoutoption.setVisibility(View.GONE);
                    presenter.onEditTextActionDone("Aadhaar Card", null, ChatPresenter.chatObjects.size() + 1);
                    presenter.botMessage("Steps to follow:" + getResources().getString(R.string.aadhaar_steps), llayoutaadharpicture, ChatPresenter.chatObjects.size() + 1);
                    chooseedAadhar = true;
                    DOC_TYPE = "AADHAAR";
                }
            });

        });


        btntakeaadharphoto.setOnClickListener(v -> {

            llayoutaadharpicture.setVisibility(View.GONE);

            Intent i = new Intent(ChatActivity.this, WebViewActivity.class);
            startActivityForResult(i, TAKE_AADHAR_PHOTO);

//            presenter.botMessage("Here are your details in the aadhaar ",null,ChatPresenter.chatObjects.size()+1);
//            Bitmap icon = BitmapFactory.decodeResource(getApplicationContext().getResources(),
//                    R.drawable.maya_cropped);
//            presenter.setBotMessageImage(icon);
//            presenter.botMessage("Name: Maya Caster \n Aadhar Number: XXXXXXXX1457 \n Date of Birth: 12 Mar 1997 \n Address: New delhi", llayoutaadhardetailedit, ChatPresenter.chatObjects.size() + 1);

        });



        cancelAadhar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseedAadhar=false;
                llayoutaadharpicture.setVisibility(View.GONE);
                ChatPresenter.chatObjects.subList(ChatPresenter.chatObjects.size()-2,ChatPresenter.chatObjects.size()).clear();
                ChatActivity.chatAdapter.notifyDataSetChanged();
            }
        });

//        clickVoterFront.setOnClickListener(v -> {
//            llayoutoptionselectforvoterid.setVisibility(View.GONE);
//            if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
//                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
//                    File photoFile = null;
//                    try {
//                        photoFile = createImageFile();
//                    } catch (IOException ex) {
//                        ex.printStackTrace();
//                    }
//                    if (photoFile != null) {
//                        voterFrontURI = FileProvider.getUriForFile(this,
//                                "com.findeed.fileprovider",
//                                photoFile);
//                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, voterFrontURI);
//                        takePictureIntent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION,ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//                        startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO_VOTER_FRONT);
//                    }
//                }
//
//            } else {
//                requestPermissions(new String[]{
//                                Manifest.permission.CAMERA,
//                                Manifest.permission.READ_EXTERNAL_STORAGE
//                        }
//
//                        , CLICK_VOTER_PHOTO_PERMISSION);
//
//            }
//
//
//        });
        clickVoterFront.setOnClickListener(v -> {
            llayoutoptionselectforvoterid.setVisibility(View.GONE);
            if ( ContextCompat.checkSelfPermission(this,Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Intent takePictureIntent = new Intent(this, CameraActivity.class);
                //Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                        final File file=new File(storageDir, "pic.jpg");
                        photoFile=file;
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    if (photoFile != null) {
                        voterFrontURI = FileProvider.getUriForFile(this,
                                "com.findeed.fileprovider",
                                photoFile);
                        //    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, voterFrontURI);
//                        takePictureIntent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION,ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                        Log.i("VOTER_FRONT",String.valueOf(voterFrontURI));
                        takePictureIntent.putExtra("image_side","FRONT SIDE");
                        takePictureIntent.putExtra("doc_type","VOTER");
                        startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO_VOTER_FRONT);
                    }
                }

            } else {
                requestPermissions(new String[]{
                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                }, CLICK_VOTER_PHOTO_PERMISSION);
            }

        });


//     clickVoterBack.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                clickPickForVoterBack.setVisibility(View.GONE);
//                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
//                    File photoFile = null;
//                    try {
//                        photoFile = createImageFile();
//                    } catch (IOException ex) {
//                        ex.printStackTrace();
//                    }
//                    if (photoFile != null) {
//                        voterBackURI = FileProvider.getUriForFile(getApplicationContext(),
//                                "com.findeed.fileprovider",
//                                photoFile);
//                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, voterBackURI);
//                        takePictureIntent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION,ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//                        startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO_VOTER_BACK);
//                    }
//                }
//
//            }
//        });

        clickVoterBack.setOnClickListener(v -> {
            clickPickForVoterBack.setVisibility(View.GONE);
            if ( ContextCompat.checkSelfPermission(this,Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Intent takePictureIntent = new Intent(this, CameraActivity.class);
                //Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                        final File file=new File(storageDir, "pic.jpg");
                        photoFile=file;
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    if (photoFile != null) {
                        voterBackURI = FileProvider.getUriForFile(this,
                                "com.findeed.fileprovider",
                                photoFile);
                        takePictureIntent.putExtra("image_side","BACK SIDE");
                        takePictureIntent.putExtra("doc_type","VOTER");
                        startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO_VOTER_BACK);
                    }
                }

            } else {
                requestPermissions(new String[]{
                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                }, CAMERA_PERMISSION);
            }

        });

        pickVoterFront.setOnClickListener(v -> {
            Intent galleryIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
            galleryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            galleryIntent.setType("image/*");
            startActivityForResult(galleryIntent, REQUEST_PICK_PHOTO_VOTER_FRONT);
        });

        pickVoterBack.setOnClickListener(v -> {
            Intent galleryIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
            galleryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            galleryIntent.setType("image/*");
            startActivityForResult(galleryIntent, REQUEST_PICK_PHOTO_VOTER_BACK);
        });


        btncontinuevoterid.setOnClickListener(v -> {
            btncontinuevoterid.setVisibility(View.GONE);
            ChatPresenter.chatObjects.subList(ChatPresenter.chatObjects.size() - 1, ChatPresenter.chatObjects.size()).clear();
            ChatActivity.chatAdapter.notifyDataSetChanged();
            llayoutprocessingvoterid.setVisibility(View.VISIBLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            DOC_TYPE = "VOTER_ID";
            new GetDocumentData().execute();


        });





//        btneditinfoaadhar.setOnClickListener(v -> {
//            // Toast.makeText(getApplicationContext(),"Sorry! i'm in development phase",Toast.LENGTH_SHORT).show();
//            isAadhaarEditClicked = true;
//            editAadharName.setText(UserAddressData.getName());
//            editAadharGender.setText(UserAddressData.getGender());
//            editAadhaarDob.setText(UserAddressData.getDob());
//            editAadharAddress.setText(UserAddressData.getAddress());
//            presenter.botMessage("Please edit Aadhaar details in the following window", lLayoutUpdateAadharInfo, ChatPresenter.chatObjects.size() + 1);
//            llayoutaadhardetailedit.setVisibility(View.GONE);
//
//        });

        btnSaveAadharDetail.setOnClickListener(v -> {

            hideKeyboard();

//            presenter.botMessage("Sorry, looks like the information you provided for pan card are not correct.");
//            llayoutneedhelp.setVisibility(View.VISIBLE);
//            scrollChatDown();


            //  presenter.botMessage("Thank you for providing the ID and Address proofs.");
            //presenter.botMessage("we will forward these details to the Bureau for verification.");
            presenter.botMessage("Please click \"I Agree\" to allow Findeed to share the details with the lending partners and check your credit information with the credit bureau.", llayoutagreeorcancel, ChatPresenter.chatObjects.size() + 1);
            //Click here for the detailed Terms & Conditions.
            lLayoutUpdateAadharInfo.setVisibility(View.GONE);


        });


        btneditvoterid.setOnClickListener(v -> {

            llayoutvoterdetailedit.setVisibility(View.GONE);
            isEditVoterClicked = true;
            voterIdEditText.setText(VOTER_DOCUMENT_ID);
            voterNameEditText.setText(VOTER_FIRST_NAME);
            voterDobEditText.setText(VOTER_DOB);
            voterAddressEditText.setText(VOTER_ADDRESS);
            presenter.botMessage("Please edit VOTER details in the following window", llUpdateVoterInfo, ChatPresenter.chatObjects.size() + 1);
            new SendErrorFlags("IS_VOTER_EDITED","1").execute();
        });




        voterIdEditText.setOnFocusChangeListener(this);
        voterNameEditText.setOnFocusChangeListener(this);
        voterDobEditText.setOnFocusChangeListener(this);
        voterAddressEditText.setOnFocusChangeListener(this);

        saveVoterDetail.setOnClickListener(v -> {

            voterNameEditText.clearFocus();
            voterIdEditText.clearFocus();
            voterAddressEditText.clearFocus();
            if(voterNameError.getVisibility()==View.VISIBLE || voterIdError.getVisibility()==View.VISIBLE || voterAddressError.getVisibility()==View.VISIBLE){

            }else if(isNullOrBlank(voterAddressEditText.getText().toString())||isNullOrBlank(voterIdEditText.getText().toString())||isNullOrBlank(voterNameEditText.getText().toString())){
                if(isNullOrBlank(voterAddressEditText.getText().toString())){
                    voterAddressError.setVisibility(View.VISIBLE);
                }
                if(isNullOrBlank(voterIdEditText.getText().toString())){
                    voterIdError.setVisibility(View.VISIBLE);
                }
                if(isNullOrBlank(voterNameEditText.getText().toString())){
                    voterNameError.setVisibility(View.VISIBLE);
                }
            }
            else{
                try {

                    new VerifyUserDocumentVoter(voterIdEditText.getText().toString()).execute();
                    VOTER_ADDRESS= voterAddressEditText.getText().toString();
                    isPinMatch=findAndMatchPin(VOTER_ADDRESS);
                    Log.i("pinMatchVoter",String.valueOf(isPinMatch));
                    int id = radioGroupSex.getCheckedRadioButtonId();
                    if (id == findViewById(R.id.rb_male).getId()) {
                        sex = "Male";
                    } else if (id == findViewById(R.id.rb_female).getId()) {
                        sex = "Female";
                    } else
                        sex = "Other";
                    if(id==-1){
                        voterGenderError.setVisibility(View.VISIBLE);
                    }else {
                        boolean t;

                        VOTER_DOB= voterDobEditText.getText().toString();
                        t=matchAge(VOTER_DOB,PAN_DOB);

                        if(t){
                            new SendErrorFlags("PAN_AADHAR_MATCH","1").execute();
                            llUpdateVoterInfo.setVisibility(View.GONE);
                            voterNameError.setVisibility(View.INVISIBLE);
                            voterIdError.setVisibility(View.INVISIBLE);
                            voterAddressError.setVisibility(View.INVISIBLE);
                            presenter.onEditTextActionDone("Voter Id:" + voterIdEditText.getText().toString() + "\n" + "Name: " + voterNameEditText.getText().toString() + "\n" + "Date of birth:" + voterDobEditText.getText().toString() + "\n" + "Address: " + voterAddressEditText.getText().toString() +"\n" + "Gender: " + sex, null, ChatPresenter.chatObjects.size() + 1);
                            presenter.botMessage("Thank you  for providing the ID and Address proofs.", null, ChatPresenter.chatObjects.size() + 1);
                            presenter.botMessage("Please click \"I Agree\" to allow Findeed to share the details with the lending partners and check your credit information with the credit bureau.", llayoutagreeorcancel, ChatPresenter.chatObjects.size() + 1);
                            // Click here for the detailed Terms & Conditions.
                            DOC_TYPE = "EDITED_VOTER_ID";
                            hideKeyboard();
                            new SaveDocumentDetails().execute();
                        }else {
                            new SendErrorFlags("PAN_AADHAR_MATCH","2").execute();
                            hideKeyboard();
                            llUpdateVoterInfo.setVisibility(View.GONE);
                            presenter.onEditTextActionDone("Voter Id:" + voterIdEditText.getText().toString() + "\n" + "Name: " + voterNameEditText.getText().toString() + "\n" + "Date of birth:" + voterDobEditText.getText().toString() + "\n" + "Address: " + voterAddressEditText.getText().toString() + "\n"+ "Gender: " + sex, null, ChatPresenter.chatObjects.size() + 1);
                            presenter.botMessage("The date of birth you provided on the PAN did not match the address proof. Please check your details again and resubmit",null,ChatPresenter.chatObjects.size()+1);

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();

                }
            }




        });


        radioGroupSex.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                voterGenderError.setVisibility(View.INVISIBLE);
            }
        });



        btnconfirmvoterid.setOnClickListener(v -> {
            try{
                llayoutvoterdetailedit.setVisibility(View.GONE);
                boolean isAgeSame;
                isAgeSame= matchAge(VOTER_DOB,PAN_DOB);
                if(isAgeSame){
                    new SendErrorFlags("PAN_AADHAR_MATCH","1").execute();
                    presenter.botMessage("Thank you  for providing the ID and Address proofs.", null, ChatPresenter.chatObjects.size() + 1);
                    presenter.botMessage("Please click \"I Agree\" to allow Findeed to share the details with the lending partners and check your credit information with the credit bureau.", llayoutagreeorcancel, ChatPresenter.chatObjects.size() + 1);
                    DOC_TYPE = "VOTER_ID";
                    new SaveDocumentDetails().execute();
                }else{
                    new SendErrorFlags("PAN_AADHAR_MATCH","2").execute();
                    presenter.onEditTextActionDone("ID:"+VOTER_DOCUMENT_ID+"\n"+"Name: "+VOTER_FIRST_NAME+"\n"+"DOB: "+VOTER_DOB+"\n"+"Address:"+VOTER_ADDRESS,null,ChatPresenter.chatObjects.size(),CUSTOMER_DETAILS);
                    presenter.botMessage("The date of birth you provided on the PAN did not match the address proof. Please check your details again and resubmit",null,ChatPresenter.chatObjects.size()+1);
                }

            }catch (Exception e){
                e.printStackTrace();
            }

            new SendErrorFlags("IS_VOTER_EDITED","2").execute();

        });




        btnagree.setOnClickListener(v -> {
            llayoutagreeorcancel.setVisibility(View.GONE);
            new SendErrorFlags("BUREAU_PERMISSION_DENIED","1").execute();
            if(DOC_TYPE.equals("VOTER_ID")|| DOC_TYPE.equals("EDITED_VOTER_ID")){
                presenter.botMessage("Is the address in your VoterId card your current address?", llayoutyesorno,ChatPresenter.chatObjects.size() + 1);
            }else {
                presenter.botMessage("Is the address in your Aadhaar card your current address?", llayoutyesorno,ChatPresenter.chatObjects.size() + 1);
            }

        });

        btnCancelBureau.setOnClickListener(v -> {
            //  flags.put("shareCreditBureau",1);
            new SendErrorFlags("BUREAU_PERMISSION_DENIED","2").execute();
            final Dialog dialog = new Dialog(ChatActivity.this);
            dialog.setContentView(R.layout.custom_dialog_nolocation);
            TextView textView = dialog.findViewById(R.id.txt_goback);
            TextView text = dialog.findViewById(R.id.bureau_check_no_text);
            text.setText("Please click 'I Agree', without your permission we can not proceed further in this loan process");
            Window window = dialog.getWindow();
            window.setGravity(Gravity.CENTER);
            dialog.getWindow().setBackgroundDrawable(new
                    ColorDrawable(Color.TRANSPARENT));
            textView.setOnClickListener(v17 -> {
                dialog.dismiss();
                llayoutagreeorcancel.setVisibility(View.VISIBLE);

            });
            dialog.show();
        });

        btnyes.setOnClickListener(v -> {

            // flags.put("aadhaarCorrectAddress",1);
            isCurrentAddressSame=true;
            llayoutyesorno.setVisibility(View.GONE);
            presenter.onEditTextActionDone("Yes", null, ChatPresenter.chatObjects.size() + 1,CUSTOMER_DETAILS);
            Log.i("isPinMatch",String.valueOf(isPinMatch));
            isPinMatch=true;
            if(isPinMatch){
                new SendErrorFlags("PINCODE_VERIFICATION","1").execute();
                if (ContextCompat.checkSelfPermission(ChatActivity.this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED)
                {

                    /*Strat finbox in case user have stratover the application*/
                    final Intent finboxService = new Intent(ChatActivity.this, FinboxService.class);
                    ChatActivity.this.startService(finboxService);

                    if(DOC_TYPE.equals("AADHAAR")){
                        presenter.botMessage("Can you provide us 2 people who can stand in for you ?", btnSelectVoucher, ChatPresenter.chatObjects.size() + 1);


                    }else {
                        presenter.botMessage("Can you provide your supporting document for address proof? You can give us your Gas Bill or Electricity Bill. Which one would you like to use?",electricityLayout,ChatPresenter.chatObjects.size()+1);

                    }

                } else {
                    presenter.botMessage("We would require some additional information from you for smooth processing of your loan application", llayoutAdditionalInfo, ChatPresenter.chatObjects.size() + 1);
                }
            }else {
                new SendErrorFlags("PINCODE_VERIFICATION","2").execute();
                presenter.botMessage("Sorry! We do not service the Pin code currently",null,ChatPresenter.chatObjects.size()+1);

            }

            if(DOC_TYPE.equals("AADHAAR")){
                 DOC_TYPE="AADHAAR";
                new SaveUserAddress("AADHAAR").execute();
            }else {
                  DOC_TYPE="VOTER_ID";
                new SaveUserAddress("VOTER_ID").execute();
            }

            new IsCurrentAddressSame("1").execute();
            // new SaveCustomerFlag().execute();




        });

        btnno.setOnTouchListener((v, event) -> {

            imgnoo.setVisibility(View.VISIBLE);
            imgyess.setVisibility(View.GONE);
            return false;
        });

        btnyes.setOnTouchListener((v, event) -> {
            imgyess.setVisibility(View.VISIBLE);
            imgnoo.setVisibility(View.GONE);
            return false;
        });


        btnno.setOnClickListener(v -> {
            isCurrentAddressSame=false;
            llayoutyesorno.setVisibility(View.GONE);
            presenter.onEditTextActionDone("No", null, ChatPresenter.chatObjects.size() + 1,CUSTOMER_DETAILS);
            presenter.botMessage("Do you want to use your current location or fill it by yourself", llayoutAddressManually, ChatPresenter.chatObjects.size() + 1);
            new IsCurrentAddressSame("2").execute();

        });

      /*  btnCurrentLocation.setOnClickListener(v -> {
i
            Dialog dialogLocation = new Dialog(ChatActivity.this);
            dialogLocation.setContentView(R.layout.custom_dialog_uselocation);

            TextView AllowLocation = dialogLocation.findViewById(R.id.txt_locationAllow);
            TextView DenyLocation = dialogLocation.findViewById(R.id.txt_locationDeny);

            Window window = dialogLocation.getWindow();
            window.setGravity(Gravity.CENTER);

            dialogLocation.getWindow().setBackgroundDrawable(new
                    ColorDrawable(Color.TRANSPARENT));
            dialogLocation.show();

            AllowLocation.setOnClickListener(v112 -> {
                dialogLocation.dismiss();
            });
            DenyLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

            llayoutAddressManually.setVisibility(View.GONE);
            requestPermissions(new String[]{
                            Manifest.permission.READ_CONTACTS,
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.READ_CALL_LOG,
                            Manifest.permission.READ_SMS},

                    REQUEST_CODE_ASK_PERMISSIONS);
            Bitmap icon = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                    R.drawable.current_location);
            presenter.setImage(icon);
            presenter.onEditTextActionDone("Okhla, New Delhi",null);
            scrollChatDown();
        });*/

        btnManually.setOnClickListener(v -> {
            llayoutAddressManually.setVisibility(View.GONE);
            presenter.botMessage("Please enter your current address", llayoutFillAddressManually, ChatPresenter.chatObjects.size() + 1);

        });

        addressLineOne.setOnFocusChangeListener(this);
        locality.setOnFocusChangeListener(this);
        city.setOnFocusChangeListener(this);
        pinCode.setOnFocusChangeListener(this);

        addAddressManually.setOnClickListener(v -> {
            addressLineOne.clearFocus();
            locality.clearFocus();
            city.clearFocus();
            pinCode.clearFocus();
            if (currentLocalityError.getVisibility() == View.VISIBLE || currentLocalityError.getVisibility() == View.VISIBLE || currentCityError.getVisibility() == View.VISIBLE || currentPinError.getVisibility() == View.VISIBLE) {

            }
            if(isNullOrBlank(addressLineOne.getText().toString())||isNullOrBlank(locality.getText().toString())||isNullOrBlank(city.getText().toString())||isNullOrBlank(pinCode.getText().toString())){
                if(isNullOrBlank(addressLineOne.getText().toString())){currentAddressError.setVisibility(View.VISIBLE);}
                if(isNullOrBlank(locality.getText().toString())){currentLocalityError.setVisibility(View.VISIBLE);}
                if(isNullOrBlank(city.getText().toString())){currentCityError.setVisibility(View.VISIBLE);}
                if(isNullOrBlank(pinCode.getText().toString())){currentPinError.setVisibility(View.VISIBLE);}
            }
            else {
                boolean isPinCodeMatch=matchPinCode(pinCode.getText().toString());
                ADDRESS_LINE_1=addressLineOne.getText().toString();
                CITY=city.getText().toString();
                PINCODE=pinCode.getText().toString();
                LOCALITY=locality.getText().toString();
                if(isPinCodeMatch){
                    hideKeyboard();
                    llayoutFillAddressManually.setVisibility(View.GONE);
                    userLocation =  ADDRESS_LINE_1+ "," + PINCODE;
                    Bitmap staticmage = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                            R.drawable.location_placeholder);
                    presenter.sendUserLocation(staticmage, useMyCurrentLocationLayout, ChatPresenter.chatObjects.size() + 1);
                    new SendErrorFlags("PINCODE_VERIFICATION","1").execute();
                    presenter.botMessage("Can you provide your supporting document for address proof? You can give us your Gas Bill or Electricity Bill. Which one would you like to use?",electricityLayout,ChatPresenter.chatObjects.size()+1);
                    //  presenter.botMessage("We are processing your application and this may take a few minutes. We will notify you when the eligibility check is complete",null,ChatPresenter.chatObjects.size()+1);
                    new SaveUserAddress("").execute();
                }else {
                    new SendErrorFlags("PINCODE_VERIFICATION","2").execute();
                    hideKeyboard();
                    llayoutFillAddressManually.setVisibility(View.GONE);
                    userLocation =  ADDRESS_LINE_1+ "," + PINCODE;
                    Bitmap staticmage = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                            R.drawable.location_placeholder);
                    presenter.sendUserLocation(staticmage, useMyCurrentLocationLayout, ChatPresenter.chatObjects.size() + 1);
                    presenter.botMessage("Sorry! We do not service the Pin code currently",null,ChatPresenter.chatObjects.size()+1);
                }

            }

        });


        btnMoreInfoYes.setOnClickListener(v -> {
            llayoutAdditionalInfo.setVisibility(View.GONE);
            requestPermissions(new String[]{
                            Manifest.permission.READ_CONTACTS,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.READ_PHONE_STATE,
                            Manifest.permission.READ_SMS},
                    REQUEST_CODE_ASK_PERMISSIONS);


        });

        btnMoreInfoNo.setOnClickListener(v -> {

            final Dialog dialog = new Dialog(ChatActivity.this);
            dialog.setContentView(R.layout.custom_dialog_nolocation);
            TextView textView = dialog.findViewById(R.id.txt_goback);
            Window window = dialog.getWindow();
            window.setGravity(Gravity.CENTER);
            dialog.getWindow().setBackgroundDrawable(new
                    ColorDrawable(Color.TRANSPARENT));
            textView.setOnClickListener(v17 -> {
                dialog.dismiss();
                llayoutAdditionalInfo.setVisibility(View.VISIBLE);
            });
            dialog.show();
            if(isCcsPermissionGranted){
                new SendErrorFlags("CCS_PERMISSION_DENIED","1").execute();

            }

            else
                new SendErrorFlags("CCS_PERMISSION_DENIED","2").execute();

        });
        btnMoreInfoNo.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                imageNo.setVisibility(View.INVISIBLE);
                imageYes.setVisibility(View.INVISIBLE);
                return false;
            }
        });
        btnMoreInfoYes.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                imageYes.setVisibility(View.VISIBLE);
                imageNo.setVisibility(View.GONE);
                return false;
            }
        });




        saveMyChoice.setOnClickListener(v -> {
            try{
                frameLayout.setVisibility(View.GONE);
                if(isSeekChanged){
                       if (Float.valueOf(eligibileLoanBar.getProgress()) < 30000) {
                           fee = "600";
                       } else {
                           float lFee = (float) (Float.valueOf(eligibileLoanBar.getProgress()) * 0.02);
                           if (lFee > 1000)
                               fee = "1000";
                           else
                               fee = String.valueOf(Float.valueOf(lFee));
                       }
                       emiProcessingFee.setText("\u20B9 " + fee);
                       latefee = String.valueOf("\u20B9 " +(Float.valueOf(eligibileLoanBar.getProgress())) * 0.01);
                       emiLateFee.setText("1% per day on amount due");
                       requiredLoanAmount.setText("\u20B9 "+Math.round(eligibileLoanBar.getProgress()));
                       estimatedEMI.setText("\u20B9 "+emiRate+"/Month");
                       estimatedLoanTenure.setText(String.valueOf(tempTenure+" Months"));
                       boolean isItmonthly=rbMonthly.isChecked();
                       if(isItmonthly)
                           estimatedPaymentFreq.setText("Monthly");
                       else
                           estimatedPaymentFreq.setText("Weekly");
                       presenter.botMessage("Your Loan Eligibility Summary based on your application", AllAboutLoan, ChatPresenter.chatObjects.size() + 1);
                       new SaveLoanDetails(eligibileLoanBar.getProgress()).execute();
                   }else{
                       if (Float.valueOf(validateOTPResponse.getData().getLoans().get(0).getRequestAmount()) < 30000) {
                           fee = "600";
                       } else {
                           float lFee = (float) (Float.valueOf(validateOTPResponse.getData().getLoans().get(0).getRequestAmount()) * 0.02);
                           if (lFee > 1000)
                               fee = "1000";
                           else
                               fee = String.valueOf(Float.valueOf(lFee));
                       }

                       emiProcessingFee.setText("\u20B9 " + fee);
                       latefee = String.valueOf("\u20B9 " +(Float.valueOf(validateOTPResponse.getData().getLoans().get(0).getRequestAmount())) * 0.01);
                       emiLateFee.setText("1% per day on amount due");                       emiRate=String.valueOf(Math.round(valueOf(validateOTPResponse.getData().getLoans().get(0).getEmiRate())));
                       tempTenure=Integer.parseInt(validateOTPResponse.getData().getLoans().get(0).getTenure());
                       requiredLoanAmount.setText("\u20B9 "+Math.round(eligibileLoanBar.getProgress()));
                       estimatedEMI.setText("\u20B9 "+emiRate+"/Month");
                       estimatedLoanTenure.setText(String.valueOf(tempTenure+" Months"));
                       boolean isItmonthly=rbMonthly.isChecked();
                       if(isItmonthly)
                           estimatedPaymentFreq.setText("Monthly");
                       else
                           estimatedPaymentFreq.setText("Weekly");


                    presenter.botMessage("Your Loan Eligibility Summary based on your application", AllAboutLoan, ChatPresenter.chatObjects.size() + 1);
                    new SaveLoanDetails(Integer.parseInt(validateOTPResponse.getData().getLoans().get(0).getTenure())).execute();
                }
                //}
            }catch(Exception e){
                e.printStackTrace();
            }

        });

        btnFileUpload.setOnClickListener(v -> {


//            Intent FileIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
//            FileIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
//            FileIntent.setType("application/pdf");
//            startActivityForResult(FileIntent, 123);
//
//
//            //presenter.botMessage("Thank you for providing bank details");
//
//
//            Handler handler = new Handler();
//            handler.postDelayed(() -> {
//                //   presenter.botMessage("Can you provide us 2 people who can stand in for you?");
//                btnFileUpload.setVisibility(View.GONE);
//                btnSelectVoucher.setVisibility(View.VISIBLE);
//            }, 2000);

        });


        btnSelectVoucher.setOnClickListener(v -> {

            if( ContextCompat. checkSelfPermission(this,Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED )  {
                Intent contactIntent=new Intent(ChatActivity .this, ContactActivity.class);
                contactIntent.putExtra("voucher1","azk");
                startActivityForResult(contactIntent,CONTACT_PICKER_REQUEST );
                btnSelectVoucher.setVisibility(View.GONE);

            } else {
                requestPermissions(new String[]{
                                Manifest.permission.READ_CONTACTS,

                        }

                        , READ_CONTACT);

            }




        });

        select2ndVoucher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                select2ndVoucher.setVisibility(View.GONE);
                Intent contactIntent=new Intent(ChatActivity.this, ContactActivity.class);
                contactIntent.putExtra("voucher1",Voucher1);
                startActivityForResult(contactIntent,CONTACT_PICK );
                new TriggerCreditRule().execute();
            }
        });


        viewLoanDetails.setOnClickListener(v -> {
            viewLoanDetails.setVisibility(View.GONE);
            try {
                PRINCIPAL_AMOUNT=Math.round(Float.valueOf(validateOTPResponse.getData().getLoans().get(0).getRequestAmount()));
                double t=Double.valueOf(validateOTPResponse.getData().getLoans().get(0).getTenure());
                double e=Math.round(Double.valueOf(validateOTPResponse.getData().getLoans().get(0).getEmiRate()));

                INTREST_RATE=  getRepayment(t,e,PRINCIPAL_AMOUNT);
//                 INTREST_RATE=  getRepayment(abc,xyz,PRINCIPAL_AMOUNT);
                repaymentList=new ArrayList<>();
                for(int j=1;j<=tempTenure;j++){
                    double interest=  Math.round(((INTREST_RATE*PRINCIPAL_AMOUNT)));
                    double principleDue=Math.round(e-interest);
                    double balanceLoan=Math.round(PRINCIPAL_AMOUNT-principleDue);
                    RepaymentSchedule repaymentSchedule = new RepaymentSchedule();
                    repaymentSchedule.setInterest("\u20B9 "+String.valueOf(interest));
                    repaymentSchedule.setPrinciple("\u20B9 "+String.valueOf(principleDue));
                    if(j==tempTenure)
                        repaymentSchedule.setLoanBalance("\u20B9 0");
                    else
                        repaymentSchedule.setLoanBalance("\u20B9 "+String.valueOf(balanceLoan));
                    repaymentSchedule.setPayment(String.valueOf(j));
                    repaymentList.add(repaymentSchedule);
                    PRINCIPAL_AMOUNT= (int) balanceLoan;
                    TOTAL_INTEREST=TOTAL_INTEREST+interest;
                }

                Intent i = new Intent(ChatActivity.this, TabLayoutActivity.class);
                startActivity(i);
                Log.i("ass","aa");

            }catch (Exception e){

                e.printStackTrace();
                Toast.makeText(chatActivity, "Something Went wrong!", Toast.LENGTH_SHORT).show();

            }



        });


        btnVoucherVerification.setOnClickListener(v -> {
            btnVoucherVerification.setVisibility(View.GONE);

            llayoutVoucherVerification.setVisibility(View.VISIBLE);


        });

        btnNotifySecond.setOnClickListener(v -> {


//            llayoutVoucherVerification.setVisibility(View.GONE);
//
//            final Dialog dialog = new Dialog(ChatActivity.this);
//            dialog.setContentView(R.layout.custom_dialog_verification);
//
//            Button btnTakePicVerify = dialog.findViewById(R.id.btn_takephotoverify);
//            Button btnPickPicVerify = dialog.findViewById(R.id.btn_pick_photoverify);
//
//
//            btnTakePicVerify.setOnClickListener(v16 -> {
//                btnNotifySecond.setVisibility(View.GONE);
//                presenter.botMessage("Sorry, your loan application cannot be processed at this point of time.");
//                presenter.botMessage("Please get back to us with the correct proofs to proceed further.");
//                dialog.dismiss();
//
//                Handler handler = new Handler();
//                handler.postDelayed(() -> btnNotifySecond.setVisibility(View.VISIBLE), 2000);
//
//            });
//
//
//            btnPickPicVerify.setOnClickListener(v14 -> {
//
//                btnNotifySecond.setVisibility(View.GONE);
//                presenter.botMessage("Thank You!");
//                presenter.botMessage("Lorem ipsum dolor sit amet");
//                dialog.dismiss();
//                btnESign.setVisibility(View.VISIBLE);
//            });
//
//            dialog.show();


//            btnNotifySecond.setVisibility(View.GONE);
//            voucherVerified.setVisibility(View.GONE);
//            Intent myIntent = new Intent(ChatActivity.this, VideoIdKycInitActivity.class);
//            myIntent.putExtra("init_request", videoIdKycInitRequest);
//            startActivityForResult(myIntent, 10);

        });

//        btnESign.setOnClickListener(v -> {
//
//            btnESign.setVisibility(View.GONE);
//             presenter.botMessage("Your e-Sign have been completed, we will notify you when the loan is ready to be disbursed",null,ChatPresenter.chatObjects.size()+1);
//
//        });

       /* btnAgreeandContinue.setOnClickListener(v -> {
            scrollViewApprovedLoan.setVisibility(View.GONE);
            //presenter.botMessage("Findeed will be charging Rs. X fee for processing your loan application.");
            btnAgreeandContinueTwo.setVisibility(View.VISIBLE);

        });*/
//        btnAgreeandContinueTwo.setOnClickListener(v -> {
//
//            Intent intent = new Intent(ChatActivity.this, ChargeVerifyOTP.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(intent);
//
//        });


        txtone.setOnClickListener(v -> {
            llayoutchildcount.setVisibility(View.GONE);
            llayoutchildmore.setVisibility(View.GONE);
            NUMBER_OF_CHILDREN = 1;
            if(LOAN_TYPE==1){
                firstname2.setText("");
                surname2.setText("");
                selectChildClass.setText("");
                // chatData.put("Educational",CUSTOMER_DETAILS);
                presenter.onEditTextActionDone("1 Child", null, ChatPresenter.chatObjects.size() + 1,CUSTOMER_DETAILS);
                new NumberOfChildren().execute();
                childHeader.setText("First Child");
                presenter.botMessage("Can you provide me the details of the first child ?", childLayout, ChatPresenter.chatObjects.size() + 1);
                addChild.setOnClickListener(view -> {


                    childLayout.setVisibility(View.GONE);
                    childClick(childLayout, NUMBER_OF_CHILDREN, firstname2, surname2, className);

                });
            }else {
                presenter.onEditTextActionDone("1 Child", null, ChatPresenter.chatObjects.size() + 1);
                presenter.botMessage("How much loan do you require?",llayoutseekbar,ChatPresenter.chatObjects.size()+1);
                new NumberOfChildren().execute();


            }
        });


        txttwo.setOnClickListener(v -> {

            llayoutchildcount.setVisibility(View.GONE);
            llayoutchildmore.setVisibility(View.GONE);
            NUMBER_OF_CHILDREN = 2;

            if(LOAN_TYPE==1){
                firstname2.setText("");
                surname2.setText("");
                selectChildClass.setText("");
                presenter.onEditTextActionDone("2 Child", null, ChatPresenter.chatObjects.size() + 1);
                new NumberOfChildren().execute();
                childHeader.setText("First Child");
                presenter.botMessage("Can you provide me the details of the first child ?", childLayout, ChatPresenter.chatObjects.size() + 1);
                addChild.setOnClickListener(view -> {


                    childLayout.setVisibility(View.GONE);
                    childClick(childLayout, NUMBER_OF_CHILDREN, firstname2, surname2, className);

                });
            }else {
                presenter.onEditTextActionDone("2 Child", null, ChatPresenter.chatObjects.size() + 1,CUSTOMER_DETAILS);
                presenter.botMessage("How much loan do you require?",llayoutseekbar,ChatPresenter.chatObjects.size()+1);
                new NumberOfChildren().execute();


            }


            // Toast.makeText(this.getApplicationContext(), "Disabled due to backend dependency", Toast.LENGTH_SHORT).show();
//            presenter.onEditTextActionDone("2 Children",null,ChatPresenter.chatObjects.size()+1);
//            NUMBER_OF_CHILDREN = 2;
//            new NumberOfChildren().execute();
//            llayoutchildcount.setVisibility(View.GONE);
//            llayoutchildmore.setVisibility(View.GONE);
//            childHeader.setText("First Child");
//            presenter.botMessage("Can you provide me the details of the first child ?", childLayout, ChatPresenter.chatObjects.size() + 1);
//            scrollChatDown();
//            addChild.setOnClickListener(view -> childClick(childLayout, NUMBER_OF_CHILDREN, firstname2, surname2, className));

        });


        txtmoretwo.setOnClickListener(view -> {

            //  Toast.makeText(this.getApplicationContext(), "Disabled due to backend dependecy", Toast.LENGTH_SHORT).show();
            if (!isOpen) {
                llayoutchildmore.setVisibility(View.VISIBLE);
                txtmoretwo.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                txtmoretwo.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.arrow_drop_up);
                txtmoretwo.setBackgroundResource(R.drawable.childtwoplus_selector);
                isOpen = true;
            } else {
                llayoutchildmore.setVisibility(View.GONE);
                txtmoretwo.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.arrow_drop_down);
                isOpen = false;
            }


        });


        txtthree.setOnClickListener(v -> {
            llayoutchildcount.setVisibility(View.GONE);
            llayoutchildmore.setVisibility(View.GONE);
            NUMBER_OF_CHILDREN = 3;

            if(LOAN_TYPE==1){
                firstname2.setText("");
                surname2.setText("");
                selectChildClass.setText("");
                presenter.onEditTextActionDone("3 Child", null, ChatPresenter.chatObjects.size() + 1);
                new NumberOfChildren().execute();
                childHeader.setText("First Child");
                presenter.botMessage("Can you provide me the details of the first child ?", childLayout, ChatPresenter.chatObjects.size() + 1);
                addChild.setOnClickListener(view -> {


                    childLayout.setVisibility(View.GONE);
                    childClick(childLayout, NUMBER_OF_CHILDREN, firstname2, surname2, className);

                });
            }else {
                presenter.onEditTextActionDone("3 Child", null, ChatPresenter.chatObjects.size() + 1,CUSTOMER_DETAILS);
                presenter.botMessage("How much loan do you require and?",llayoutseekbar,ChatPresenter.chatObjects.size()+1);
                new NumberOfChildren().execute();


            }
//            presenter.onEditTextActionDone("3 Children", null, ChatPresenter.chatObjects.size() + 1);
//            NUMBER_OF_CHILDREN = 3;
//            new NumberOfChildren().execute();
//            llayoutchildcount.setVisibility(View.GONE);
//            llayoutchildmore.setVisibility(View.GONE);
//            childHeader.setText("First Child");
//            presenter.botMessage("Can you provide me the details of the first child ?", childLayout, ChatPresenter.chatObjects.size() + 1);
//            scrollChatDown();
//            addChild.setOnClickListener(view -> childClick(childLayout, NUMBER_OF_CHILDREN, firstname2, surname2, className));
//
        });

        txtfour.setOnClickListener(v -> {
            llayoutchildcount.setVisibility(View.GONE);
            llayoutchildmore.setVisibility(View.GONE);
            NUMBER_OF_CHILDREN = 4;
            if(LOAN_TYPE==1){
                firstname2.setText("");
                surname2.setText("");
                selectChildClass.setText("");
                presenter.onEditTextActionDone("4 Child", null, ChatPresenter.chatObjects.size() + 1);
                new NumberOfChildren().execute();
                childHeader.setText("First Child");
                presenter.botMessage("Can you provide me the details of the first child ?", childLayout, ChatPresenter.chatObjects.size() + 1);
                addChild.setOnClickListener(view -> {
                    childLayout.setVisibility(View.GONE);
                    childClick(childLayout, NUMBER_OF_CHILDREN, firstname2, surname2, className);
                });
            }else {
                presenter.onEditTextActionDone("4 Child", null, ChatPresenter.chatObjects.size() + 1,CUSTOMER_DETAILS);
                presenter.botMessage("How much loan do you require ?",llayoutseekbar,ChatPresenter.chatObjects.size()+1);
                new NumberOfChildren().execute();


            }
//            presenter.onEditTextActionDone("4 Children", null, ChatPresenter.chatObjects.size() + 1);
//            NUMBER_OF_CHILDREN = 4;
//            new NumberOfChildren().execute();
//            llayoutchildcount.setVisibility(View.GONE);
//            llayoutchildmore.setVisibility(View.GONE);
//            childHeader.setText("First Child");
//            presenter.botMessage("Can you provide me the details of the first child ?", childLayout, ChatPresenter.chatObjects.size() + 1);
//            addChild.setOnClickListener(view -> childClick(childLayout, NUMBER_OF_CHILDREN, firstname2, surname2, className));
        });
        txtfifth.setOnClickListener(v -> {
            llayoutchildcount.setVisibility(View.GONE);
            llayoutchildmore.setVisibility(View.GONE);
            NUMBER_OF_CHILDREN = 5;
            if(LOAN_TYPE==1){
                firstname2.setText("");
                surname2.setText("");
                selectChildClass.setText("");
                presenter.onEditTextActionDone("5 Child", null, ChatPresenter.chatObjects.size() + 1);
                new NumberOfChildren().execute();
                childHeader.setText("First Child");
                presenter.botMessage("Can you provide me the details of the first child ?", childLayout, ChatPresenter.chatObjects.size() + 1);
                addChild.setOnClickListener(view -> {


                    childLayout.setVisibility(View.GONE);
                    childClick(childLayout, NUMBER_OF_CHILDREN, firstname2, surname2, className);

                });
            }else {
                presenter.onEditTextActionDone("5 Child", null, ChatPresenter.chatObjects.size() + 1,CUSTOMER_DETAILS);
                presenter.botMessage("How much loan do you require ?",llayoutseekbar,ChatPresenter.chatObjects.size()+1);
                new NumberOfChildren().execute();


            }
//            presenter.onEditTextActionDone("5 Children", null, ChatPresenter.chatObjects.size() + 1);
//            NUMBER_OF_CHILDREN = 5;
//            new NumberOfChildren().execute();
//            llayoutchildcount.setVisibility(View.GONE);
//            llayoutchildmore.setVisibility(View.GONE);
//            childHeader.setText("First Child");
//            presenter.botMessage("Can you provide me the details of the first child ?", childLayout, ChatPresenter.chatObjects.size() + 1);
//            addChild.setOnClickListener(view -> childClick(childLayout, NUMBER_OF_CHILDREN, firstname2, surname2, className));

        });


        Indicator_seekBar.setOnSeekChangeListener(new OnSeekChangeListener() {

            @Override
            public void onSeeking(SeekParams seekParams) {
                int p = Indicator_seekBar.getProgress();
                p = p / 1000;
                p = p * 1000;
             /*   tvProgressLaon.setTextColor(Color.BLACK);
                tvProgressLaon.setText(Float.valueOf(p).toString());*/

                Indicator_seekBar.setProgress(valueOf(p));
            }


            @Override
            public void onStartTrackingTouch(IndicatorSeekBar seekBar) {


            }

            @Override
            public void onStopTrackingTouch(IndicatorSeekBar seekBar) {

            }


        });

        textaddseekbar.setOnClickListener(v -> {
            hideKeyboard();
            llayoutseekbar.setVisibility(View.GONE);
            selectedLoanAmount = Indicator_seekBar.getProgress();
            Log.i("seek_bar",new Gson().toJson(CUSTOMER_DETAILS));
            presenter.onEditTextActionDone("Rs " + String.valueOf(selectedLoanAmount), null, ChatPresenter.chatObjects.size() + 1,CUSTOMER_DETAILS);
            presenter.botMessage("For How Long ?", loanTenure, ChatPresenter.chatObjects.size() + 1);
            new LoanRequirement(String.valueOf(selectedLoanAmount)).execute();
        });







        btnaddRange.setOnClickListener(v -> {
            llIncomeRange.setVisibility(View.GONE);
            if(LOAN_TYPE==1){
                if (!isNumberPickerValueChanged) {

                    selectedValue = values[0];

                    presenter.onEditTextActionDone(selectedValue, null, ChatPresenter.chatObjects.size() + 1,CUSTOMER_DETAILS);
                } else {
                    presenter.onEditTextActionDone(selectedValue, null, ChatPresenter.chatObjects.size() + 1,CUSTOMER_DETAILS);
                }
//                if(selectedValue.equals(values[0]) || selectedValue.equals(values[1]) || selectedValue.equals(values[2])|| selectedValue.equals(values[3])|| selectedValue.equals(values[4])){
//                    presenter.botMessage("The minimum applicant income needs to be Rs 15,000",null,ChatPresenter.chatObjects.size()+1);
//                    new SendErrorFlags("MINIMUM_SALARY","2").execute();
//                }else {
                presenter.botMessage("Great!", null, ChatPresenter.chatObjects.size() + 1);
                presenter.botMessage("Can you provide us your PAN card for ID verification?", panCardLayout, ChatPresenter.chatObjects.size() + 1);
                new SendErrorFlags("MINIMUM_SALARY","1").execute();
                // }


//            new Handler().postDelayed(() -> presenter.botMessage("Can you provide us your PAN card for ID verification?", panCardLayout, ChatPresenter.chatObjects.size() + 1), 1000);
                new SaveCustomerSalaryRange().execute();
            }else {
                if(isNullOrBlank(selectedValue)){
                    selectedValue=values[0];
                }

//                if(selectedValue.equals(values[0]) || selectedValue.equals(values[1]) || selectedValue.equals(values[2])|| selectedValue.equals(values[3])|| selectedValue.equals(values[4])){
//                    presenter.onEditTextActionDone(selectedValue,null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
//                    presenter.botMessage("The minimum applicant income needs to be Rs 15,000",null,ChatPresenter.chatObjects.size()+1);
//                    new SendErrorFlags("MINIMUM_SALARY","2").execute();
//
//                }else {
                presenter.onEditTextActionDone(selectedValue,null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                presenter.botMessage("What is your Marital Status ?",maritalStatus,ChatPresenter.chatObjects.size()+1);
                new SendErrorFlags("MINIMUM_SALARY","1").execute();

                // }
                new SaveCustomerSalaryRange().execute();

            }

        });


//        btn_workforsomeone.setOnClickListener(v -> {
//            llayoutwhatyoudo.setVisibility(View.GONE);
//            presenter.onEditTextActionDone("I work for someone", null, ChatPresenter.chatObjects.size() + 1);
//            presenter.botMessage("How much do you earn in a month?", llIncomeRange, ChatPresenter.chatObjects.size() + 1);
//            EMPLOYMENT_TYPE = "Work for someone";
//            new EmploymentType().execute();
//        });

        //inserting where do you work flow
        btn_workforsomeone.setOnClickListener(v -> {
            if(LOAN_TYPE==1){
                llayoutwhatyoudo.setVisibility(View.GONE);
                presenter.onEditTextActionDone("I work for someone",null, ChatPresenter.chatObjects.size() + 1,CUSTOMER_DETAILS);
                presenter.botMessage("Where do you work?", rlWhereDoYouWork, ChatPresenter.chatObjects.size() + 1);
                EMPLOYMENT_TYPE = "Work for someone";
                new EmploymentType().execute();
            }
        });

        btn_workforsomeone.setOnTouchListener((v, event) -> {

            btn_workforsomeone.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick_button, 0, 0, 0);
            btn_selfemployee.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick_unselected, 0, 0, 0);
            btn_nowork.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick_unselected, 0, 0, 0);

            return false;
        });

        btn_selfemployee.setOnTouchListener((v, event) -> {

            btn_selfemployee.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick_button, 0, 0, 0);
            btn_workforsomeone.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick_unselected, 0, 0, 0);
            btn_nowork.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick_unselected, 0, 0, 0);
            return false;
        });

        btn_nowork.setOnTouchListener((v, event) -> {

            btn_nowork.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick_button, 0, 0, 0);
            btn_workforsomeone.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick_unselected, 0, 0, 0);
            btn_selfemployee.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick_unselected, 0, 0, 0);

            return false;

        });


//        btn_selfemployee.setOnClickListener(v -> {
//            llayoutwhatyoudo.setVisibility(View.GONE);
//            presenter.onEditTextActionDone("self employed/own a business", null, ChatPresenter.chatObjects.size() + 1);
//            presenter.botMessage("How much do you earn in a month?", llIncomeRange, ChatPresenter.chatObjects.size() + 1);
//            EMPLOYMENT_TYPE = "Self Employeed";
//            new EmploymentType().execute();
//
//        });

        btn_selfemployee.setOnClickListener(v -> {
            llayoutwhatyoudo.setVisibility(View.GONE);
            presenter.onEditTextActionDone("self employed/own a business", null, ChatPresenter.chatObjects.size() + 1);
            presenter.botMessage("What is your bussiness?", rlWhatIsYourBussiness, ChatPresenter.chatObjects.size() + 1);
            EMPLOYMENT_TYPE = "Self Employeed";
            new EmploymentType().execute();

        });

        btn_nowork.setOnClickListener(view -> {
            llayoutwhatyoudo.setVisibility(View.GONE);
            presenter.onEditTextActionDone("I am not working ", null, ChatPresenter.chatObjects.size() + 1);
            presenter.botMessage("Your loan application can not be processed at this point of time", null, ChatPresenter.chatObjects.size() + 1);
            EMPLOYMENT_TYPE = "No work";
            new EmploymentType().execute();
        });

        //   whatIsYourBussiness.setOnFocusChangeListener(this);
        whatIsYourBussiness.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {



                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;
                //  whatIsYourBussiness.clearFocus();


                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (whatIsYourBussiness.getRight() - whatIsYourBussiness.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {


//                        if (bussinessTypeError.getVisibility() == View.VISIBLE) {
//
//                        } else {
                        rlWhatIsYourBussiness.setVisibility(View.GONE);
                        bussinessType = String.valueOf(whatIsYourBussiness.getText());
                        presenter.onEditTextActionDone(bussinessType, null, ChatPresenter.chatObjects.size() + 1,CUSTOMER_DETAILS);
                        presenter.botMessage("What is your bussiness name?", rlWhatIsYourBussinessName, ChatPresenter.chatObjects.size() + 1);
                        new SaveBussinessName(bussinessType).execute();
                        return true;
                        // }
                    }
                }
                return false;

            }
        });
        whatIsYourBussinessName.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (whatIsYourBussinessName.getRight() - whatIsYourBussinessName.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {

                        rlWhatIsYourBussinessName.setVisibility(View.GONE);
                        bussinessName=String.valueOf(whatIsYourBussinessName.getText());
                        presenter.onEditTextActionDone(bussinessName, null, ChatPresenter.chatObjects.size() + 1,CUSTOMER_DETAILS);
                        presenter.botMessage("How much do you earn in a month?", llIncomeRange, ChatPresenter.chatObjects.size() + 1);
                        new SaveBussinessType(bussinessName).execute();
                        hideKeyboard();
                        return true;

                    }
                }
                return false;
            }
        });



        iHavePan.setOnClickListener(view -> {

            panCardLayout.setVisibility(View.GONE);
            presenter.onEditTextActionDone("Yes, I have a pan card", null, ChatPresenter.chatObjects.size() + 1,CUSTOMER_DETAILS);
            presenter.botMessage("Please upload your pan card for verification", lLayoutPanOption, ChatPresenter.chatObjects.size() + 1);

        });


        iDontHavePan.setOnClickListener(view -> {
            presenter.onEditTextActionDone("I don't have PAN card", null, ChatPresenter.chatObjects.size() + 1,CUSTOMER_DETAILS);
            presenter.botMessage("We are sorry, PAN card is mandatory to process the application. You can upload the PAN card if you do have one or get back to us when you get the card details. Your information will be saved with us", llayoutNoPanCard, ChatPresenter.chatObjects.size() + 1);
            panCardLayout.setVisibility(View.GONE);
        });

        selectChildClass.setOnClickListener(v -> {
            hideKeyboard();


            testdialog = new BottomSheetDialog(ChatActivity.this);
            // testdialog.setContentView(R.layout.custom_bottomdialog_childselect);


            if(testdialog==null){
                testdialog = new BottomSheetDialog(ChatActivity.this);
            }
            View viewvv = View.inflate(this, R.layout.custom_bottomdialog_childselect, null);
            testdialog.setContentView(viewvv);
            BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(((View) viewvv.getParent()));
            // bottomSheetBehavior.setPeekHeight(1800);

            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);


            rlFirstStand = testdialog.findViewById(R.id.rl_firststandard);
            rlSecondStand = testdialog.findViewById(R.id.rl_secondstandard);
            rlThirdStand = testdialog.findViewById(R.id.rl_thirdstandard);
            rlFourStand = testdialog.findViewById(R.id.rl_forthstandard);
            rlFifthStand = testdialog.findViewById(R.id.rl_fifthstandard);
            rlSixStand= testdialog.findViewById(R.id.rl_sixstandard);
            rlseventhstand = testdialog.findViewById(R.id.rl_sevenstandard);
            rleightstand = testdialog.findViewById(R.id.rl_eightstandard);
            rlninthstand = testdialog.findViewById(R.id.rl_ninthstandard);
            rltenstand = testdialog.findViewById(R.id.rl_tenstandard);
            firstStand = testdialog.findViewById(R.id.txt_FirstStandard);
            secondStand = testdialog.findViewById(R.id.txt_SecondtStandard);
            thirdStand = testdialog.findViewById(R.id.txt_ThirdStandard);
            fourthStand = testdialog.findViewById(R.id.txt_FourthStandard);
            fifthStand = testdialog.findViewById(R.id.txt_FifthStandard);
            sixStand = testdialog.findViewById(R.id.txt_SixStandard);
            sevenStand = testdialog.findViewById(R.id.txt_SevenStandard);
            eightStand = testdialog.findViewById(R.id.txt_EightStandard);
            nineStand = testdialog.findViewById(R.id.txt_NineStandard);
            tenStand= testdialog.findViewById(R.id.txt_tenStandard);
            selectClass = testdialog.findViewById(R.id.select_class);


            rltenstand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    className="10th Standard";
                    okTen.setVisibility(View.VISIBLE);
                    okFirst.setVisibility(View.INVISIBLE);
                    okSecond.setVisibility(View.INVISIBLE);
                    okThird.setVisibility(View.INVISIBLE);
                    okFour.setVisibility(View.INVISIBLE);
                    okFifth.setVisibility(View.INVISIBLE);
                    okSix.setVisibility(View.INVISIBLE);
                    okSeven.setVisibility(View.INVISIBLE);
                    okEight.setVisibility(View.INVISIBLE);
                    okNine.setVisibility(View.INVISIBLE);


                    selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);

                }
            });
            rlninthstand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    className="9th Standard";
                    okTen.setVisibility(View.INVISIBLE);
                    okFirst.setVisibility(View.INVISIBLE);
                    okSecond.setVisibility(View.INVISIBLE);
                    okThird.setVisibility(View.INVISIBLE);
                    okFour.setVisibility(View.INVISIBLE);
                    okFifth.setVisibility(View.INVISIBLE);
                    okSix.setVisibility(View.INVISIBLE);
                    okSeven.setVisibility(View.INVISIBLE);
                    okEight.setVisibility(View.INVISIBLE);
                    okNine.setVisibility(View.VISIBLE);


                    selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);

                }
            });

            rleightstand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    className="8th Standard";
                    okTen.setVisibility(View.INVISIBLE);
                    okFirst.setVisibility(View.INVISIBLE);
                    okSecond.setVisibility(View.INVISIBLE);
                    okThird.setVisibility(View.INVISIBLE);
                    okFour.setVisibility(View.INVISIBLE);
                    okFifth.setVisibility(View.INVISIBLE);
                    okSix.setVisibility(View.INVISIBLE);
                    okSeven.setVisibility(View.INVISIBLE);
                    okEight.setVisibility(View.VISIBLE);
                    okNine.setVisibility(View.INVISIBLE);


                    selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);

                }
            });

            rlseventhstand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    className="7th Standard";
                    okTen.setVisibility(View.INVISIBLE);
                    okFirst.setVisibility(View.INVISIBLE);
                    okSecond.setVisibility(View.INVISIBLE);
                    okThird.setVisibility(View.INVISIBLE);
                    okFour.setVisibility(View.INVISIBLE);
                    okFifth.setVisibility(View.INVISIBLE);
                    okSix.setVisibility(View.INVISIBLE);
                    okSeven.setVisibility(View.VISIBLE);
                    okEight.setVisibility(View.INVISIBLE);
                    okNine.setVisibility(View.INVISIBLE);


                    selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);

                }
            });

            rlSixStand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    className="6th Standard";
                    okTen.setVisibility(View.INVISIBLE);
                    okFirst.setVisibility(View.INVISIBLE);
                    okSecond.setVisibility(View.INVISIBLE);
                    okThird.setVisibility(View.INVISIBLE);
                    okFour.setVisibility(View.INVISIBLE);
                    okFifth.setVisibility(View.INVISIBLE);
                    okSix.setVisibility(View.VISIBLE);
                    okSeven.setVisibility(View.INVISIBLE);
                    okEight.setVisibility(View.INVISIBLE);
                    okNine.setVisibility(View.INVISIBLE);


                    selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);

                }
            });

            rlFifthStand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    className="5th Standard";
                    okTen.setVisibility(View.INVISIBLE);
                    okFirst.setVisibility(View.INVISIBLE);
                    okSecond.setVisibility(View.INVISIBLE);
                    okThird.setVisibility(View.INVISIBLE);
                    okFour.setVisibility(View.INVISIBLE);
                    okFifth.setVisibility(View.VISIBLE);
                    okSix.setVisibility(View.INVISIBLE);
                    okSeven.setVisibility(View.INVISIBLE);
                    okEight.setVisibility(View.INVISIBLE);
                    okNine.setVisibility(View.INVISIBLE);


                    selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);

                }
            });

            rlFourStand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    className="4th Standard";
                    okTen.setVisibility(View.INVISIBLE);
                    okFirst.setVisibility(View.INVISIBLE);
                    okSecond.setVisibility(View.INVISIBLE);
                    okThird.setVisibility(View.INVISIBLE);
                    okFour.setVisibility(View.VISIBLE);
                    okFifth.setVisibility(View.INVISIBLE);
                    okSix.setVisibility(View.INVISIBLE);
                    okSeven.setVisibility(View.INVISIBLE);
                    okEight.setVisibility(View.INVISIBLE);
                    okNine.setVisibility(View.INVISIBLE);


                    selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);

                }
            });

            rlThirdStand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    className = "3rd Standard";
                    okThird.setVisibility(View.VISIBLE);
                    // okTen.setVisibility(View.INVISIBLE);
                    okFirst.setVisibility(View.INVISIBLE);
                    okSecond.setVisibility(View.INVISIBLE);
//                    okFour.setVisibility(View.INVISIBLE);
//                    okFifth.setVisibility(View.INVISIBLE);
//                    okSix.setVisibility(View.INVISIBLE);
//                    okSeven.setVisibility(View.INVISIBLE);
//                    okEight.setVisibility(View.INVISIBLE);
//                    okNine.setVisibility(View.INVISIBLE);


                    selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);
                }
            });

            rlSecondStand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    className = "2nd Standard";
                    okTen.setVisibility(View.INVISIBLE);
                    okFirst.setVisibility(View.INVISIBLE);
                    okSecond.setVisibility(View.VISIBLE);
                    okThird.setVisibility(View.INVISIBLE);
                    okFour.setVisibility(View.INVISIBLE);
                    okFifth.setVisibility(View.INVISIBLE);
                    okSix.setVisibility(View.INVISIBLE);
                    okSeven.setVisibility(View.INVISIBLE);
                    okEight.setVisibility(View.INVISIBLE);
                    okNine.setVisibility(View.INVISIBLE);


                    selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);

                }
            });

            rlFirstStand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    className = "1st Standard";
                    okFirst.setVisibility(View.VISIBLE);
                    okSecond.setVisibility(View.INVISIBLE);
                    okThird.setVisibility(View.INVISIBLE);
                    okFour.setVisibility(View.INVISIBLE);
                    okFifth.setVisibility(View.INVISIBLE);
                    okSix.setVisibility(View.INVISIBLE);
                    okSeven.setVisibility(View.INVISIBLE);
                    okEight.setVisibility(View.INVISIBLE);
                    okNine.setVisibility(View.INVISIBLE);
                    okTen.setVisibility(View.INVISIBLE);


                    selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);

                }
            });


           /* firstStand.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {

                    selectClass.setBackgroundColor(R.color.dark_pink);
                    selectClass.setBackgroundResource(R.drawable.shape_btn_addchilddark);
                    return false;
                }
            });

            secondStand.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    selectClass.setBackgroundColor(R.color.dark_pink);
                    selectClass.setBackgroundResource(R.drawable.shape_btn_addchilddark);
                    return false;
                }
            });

            thirdStand.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    selectClass.setBackgroundColor(R.color.dark_pink);
                    selectClass.setBackgroundResource(R.drawable.shape_btn_addchilddark);
                    return false;
                }
            });

            fourthStand.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    selectClass.setBackgroundColor(R.color.dark_pink);
                    selectClass.setBackgroundResource(R.drawable.shape_btn_addchilddark);
                    return false;
                }
            });

            fifthStand.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    selectClass.setBackgroundColor(R.color.dark_pink);
                    selectClass.setBackgroundResource(R.drawable.shape_btn_addchilddark);
                    return false;
                }
            });
*/

            childClear = testdialog.findViewById(R.id.btnChildClear);
            okFirst = testdialog.findViewById(R.id.okfirst);
            okSecond = testdialog.findViewById(R.id.oksecond);
            okThird = testdialog.findViewById(R.id.okthird);
            okFour = testdialog.findViewById(R.id.okfourth);
            okFifth = testdialog.findViewById(R.id.okfifth);
            okSix = testdialog.findViewById(R.id.oksix);
            okSeven = testdialog.findViewById(R.id.okseven);
            okEight = testdialog.findViewById(R.id.okeight);
            okNine = testdialog.findViewById(R.id.oknine);
            okTen = testdialog.findViewById(R.id.okten);

            Window window = testdialog.getWindow();
            window.setGravity(Gravity.CENTER);

            testdialog.getWindow().setBackgroundDrawable(new
                    ColorDrawable(Color.TRANSPARENT));
            testdialog.show();

            childClear.setOnClickListener(v12 -> {
                testdialog.dismiss();

            });

            firstStand.setOnClickListener(v14 -> {
                className = firstStand.getText().toString();
                okFirst.setVisibility(View.VISIBLE);
                okSecond.setVisibility(View.INVISIBLE);
                okThird.setVisibility(View.INVISIBLE);
                okFour.setVisibility(View.INVISIBLE);
                okFifth.setVisibility(View.INVISIBLE);
                okSix.setVisibility(View.INVISIBLE);
                okSeven.setVisibility(View.INVISIBLE);
                okEight.setVisibility(View.INVISIBLE);
                okNine.setVisibility(View.INVISIBLE);
                okTen.setVisibility(View.INVISIBLE);


                selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);

            });

            secondStand.setOnClickListener(v16 -> {
                className = secondStand.getText().toString();
                okFirst.setVisibility(View.INVISIBLE);
                okSecond.setVisibility(View.VISIBLE);
                okThird.setVisibility(View.INVISIBLE);
                okFour.setVisibility(View.INVISIBLE);
                okFifth.setVisibility(View.INVISIBLE);
                okSix.setVisibility(View.INVISIBLE);
                okSeven.setVisibility(View.INVISIBLE);
                okEight.setVisibility(View.INVISIBLE);
                okNine.setVisibility(View.INVISIBLE);
                okTen.setVisibility(View.INVISIBLE);


                selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);
            });
            thirdStand.setOnClickListener(v18 -> {
                className = thirdStand.getText().toString();

                okFirst.setVisibility(View.INVISIBLE);
                okSecond.setVisibility(View.INVISIBLE);
                okThird.setVisibility(View.VISIBLE);
                okFour.setVisibility(View.INVISIBLE);
                okFifth.setVisibility(View.INVISIBLE);
                okSix.setVisibility(View.INVISIBLE);
                okSeven.setVisibility(View.INVISIBLE);
                okEight.setVisibility(View.INVISIBLE);
                okNine.setVisibility(View.INVISIBLE);
                okTen.setVisibility(View.INVISIBLE);

                selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);
            });

            fourthStand.setOnClickListener(v110 -> {
                className = fourthStand.getText().toString();

                okFirst.setVisibility(View.INVISIBLE);
                okSecond.setVisibility(View.INVISIBLE);
                okThird.setVisibility(View.INVISIBLE);
                okFour.setVisibility(View.VISIBLE);
                okFifth.setVisibility(View.INVISIBLE);
                okSix.setVisibility(View.INVISIBLE);
                okSeven.setVisibility(View.INVISIBLE);
                okEight.setVisibility(View.INVISIBLE);
                okNine.setVisibility(View.INVISIBLE);
                okTen.setVisibility(View.INVISIBLE);


                selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);

            });

            fifthStand.setOnClickListener(v19 -> {
                className = fifthStand.getText().toString();

                okFirst.setVisibility(View.INVISIBLE);
                okSecond.setVisibility(View.INVISIBLE);
                okThird.setVisibility(View.INVISIBLE);
                okFour.setVisibility(View.INVISIBLE);
                okFifth.setVisibility(View.VISIBLE);
                okSix.setVisibility(View.INVISIBLE);
                okSeven.setVisibility(View.INVISIBLE);
                okEight.setVisibility(View.INVISIBLE);
                okNine.setVisibility(View.INVISIBLE);
                okTen.setVisibility(View.INVISIBLE);


                selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);
            });

            sixStand.setOnClickListener(v19 -> {
                className = sixStand.getText().toString();
                okFirst.setVisibility(View.INVISIBLE);
                okSecond.setVisibility(View.INVISIBLE);
                okThird.setVisibility(View.INVISIBLE);
                okFour.setVisibility(View.INVISIBLE);
                okFifth.setVisibility(View.INVISIBLE);
                okSix.setVisibility(View.VISIBLE);
                okSeven.setVisibility(View.INVISIBLE);
                okEight.setVisibility(View.INVISIBLE);
                okNine.setVisibility(View.INVISIBLE);
                okTen.setVisibility(View.INVISIBLE);


                selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);
            });
            sevenStand.setOnClickListener(v19 -> {
                className = sevenStand.getText().toString();
                okFirst.setVisibility(View.INVISIBLE);
                okSecond.setVisibility(View.INVISIBLE);
                okThird.setVisibility(View.INVISIBLE);
                okFour.setVisibility(View.INVISIBLE);
                okFifth.setVisibility(View.INVISIBLE);
                okSix.setVisibility(View.INVISIBLE);
                okSeven.setVisibility(View.VISIBLE);
                okEight.setVisibility(View.INVISIBLE);
                okNine.setVisibility(View.INVISIBLE);
                okTen.setVisibility(View.INVISIBLE);


                selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);
            });
            eightStand.setOnClickListener(v19 -> {
                className = eightStand.getText().toString();
                okFirst.setVisibility(View.INVISIBLE);
                okSecond.setVisibility(View.INVISIBLE);
                okThird.setVisibility(View.INVISIBLE);
                okFour.setVisibility(View.INVISIBLE);
                okFifth.setVisibility(View.INVISIBLE);
                okSix.setVisibility(View.INVISIBLE);
                okSeven.setVisibility(View.INVISIBLE);
                okEight.setVisibility(View.VISIBLE);
                okNine.setVisibility(View.INVISIBLE);
                okTen.setVisibility(View.INVISIBLE);


                selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);
            });
            nineStand.setOnClickListener(v19 -> {
                className = nineStand.getText().toString();
                okFirst.setVisibility(View.INVISIBLE);
                okSecond.setVisibility(View.INVISIBLE);
                okThird.setVisibility(View.INVISIBLE);
                okFour.setVisibility(View.INVISIBLE);
                okFifth.setVisibility(View.INVISIBLE);
                okSix.setVisibility(View.INVISIBLE);
                okSeven.setVisibility(View.INVISIBLE);
                okEight.setVisibility(View.INVISIBLE);
                okNine.setVisibility(View.VISIBLE);
                okTen.setVisibility(View.INVISIBLE);

                selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);
            });
            tenStand.setOnClickListener(v19 -> {
                className = tenStand.getText().toString();
                okFirst.setVisibility(View.INVISIBLE);
                okSecond.setVisibility(View.INVISIBLE);
                okThird.setVisibility(View.INVISIBLE);
                okFour.setVisibility(View.INVISIBLE);
                okFifth.setVisibility(View.INVISIBLE);
                okSix.setVisibility(View.INVISIBLE);
                okSeven.setVisibility(View.INVISIBLE);
                okEight.setVisibility(View.INVISIBLE);
                okNine.setVisibility(View.INVISIBLE);
                okTen.setVisibility(View.VISIBLE);

                selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);
            });

            selectClass.setOnClickListener(view -> {
                selectChildClass.setText(className);

                addChild.setBackgroundResource(R.drawable.shape_btn_addchilddark);
                testdialog.dismiss();
            });
        });


        eligibileLoanBar.setOnSeekChangeListener(new OnSeekChangeListener() {
            @Override
            public void onSeeking(SeekParams seekParams) {

                isSeekChanged=true;
                int cp = eligibileLoanBar.getProgress();
                cp=cp/1000;
                cp=cp*1000;
                eligibileLoanBar.setProgress(valueOf(cp));
                Log.i("Values",String.valueOf(cp));
                Log.i("Values",String.valueOf(valueOf(cp)));
                // emiRate=String.valueOf(Math.round(calculateEmi(eligibileLoanBar.getProgress(),12,tempTenure)));
                emiRate=  String.valueOf(Math.round(getEMI(eligibileLoanBar.getProgress(),tempTenure)));
                emiRateFindeed.setText("\u20B9 "+emiRate+"/Month");

            }

            @Override
            public void onStartTrackingTouch(IndicatorSeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(IndicatorSeekBar seekBar) {

            }
        });





        lvbutn.setOnClickListener(view -> {
            hideKeyboard();
            actvFrameLayout.setVisibility(View.GONE);
            lvlay.setVisibility(View.GONE);
            presenter.botMessage("Please fill the school details",llAddSchoolRequest,ChatPresenter.chatObjects.size()+1);
        });

        imgcross.setOnClickListener(v -> {
            ChatPresenter.chatObjects.subList(ChatPresenter.chatObjects.size()-1,ChatPresenter.chatObjects.size()).clear();
            ChatActivity.chatAdapter.notifyDataSetChanged();
            llAddSchoolRequest.setVisibility(View.GONE);
            actvFrameLayout.setVisibility(View.VISIBLE);
        });


        etname.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return false;
            }
        });
        etarea.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return false;
            }
        });
        etcity.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                btreq.setBackgroundColor(R.color.dark_pink);
                btreq.setBackgroundResource(R.drawable.shape_btn_addchilddark);
                //  etcity.requestFocus();
                // llAddSchoolRequest.findViewById(R.id.).fullScroll(ScrollView.FOCUS_UP);

                return false;
            }
        });




        etname.setOnFocusChangeListener(this);
        etarea.setOnFocusChangeListener(this);
        etcity.setOnFocusChangeListener(this);




        btreq.setOnClickListener(view12 -> {

            etname.clearFocus();
            etarea.clearFocus();
            etcity.clearFocus();

            String name = etname.getText().toString();
            String area = etarea.getText().toString();
            String city = etcity.getText().toString();
            if(tvErrorSchoolName.getVisibility()==View.VISIBLE || tvErrorSchoolArea.getVisibility()==View.VISIBLE || tvErrorSchoolCity.getVisibility()==View.VISIBLE){

            }
            else if(isNullOrBlank(etname.getText().toString())||isNullOrBlank(etarea.getText().toString()) || isNullOrBlank(etcity.getText().toString()) ){
                if(isNullOrBlank(name)){
                    tvErrorSchoolName.setVisibility(View.VISIBLE);
                }if(isNullOrBlank(area)){
                    tvErrorSchoolArea.setVisibility(View.VISIBLE);

                }if(isNullOrBlank(city)){
                    tvErrorSchoolCity.setVisibility(View.VISIBLE);

                }
            }

            else {
                hideKeyboard();
                actvFrameLayout.setVisibility(View.GONE);
                llAddSchoolRequest.setVisibility(View.GONE);
                tvErrorSchoolName.setVisibility(View.INVISIBLE);
                tvErrorSchoolArea.setVisibility(View.INVISIBLE);
                tvErrorSchoolCity.setVisibility(View.INVISIBLE);
                etname.setBackgroundResource(R.drawable.shape_schooledittext);
                etarea.setBackgroundResource(R.drawable.shape_schooledittext);
                etcity.setBackgroundResource(R.drawable.shape_schooledittext);
                hideKeyboard();
                new CreateSchool().execute();

            }

        });

        btnEditLoanPayment.setOnClickListener(v -> {

            AllAboutLoan.setVisibility(View.GONE);

            frameLayout.setVisibility(View.VISIBLE);


        });

        btnAgreeLoanPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AllAboutLoan.setVisibility(View.GONE);
                presenter.botMessage("Estimated EMI: "+estimatedEMI.getText().toString()+"\n"+" Loan Amt."+requiredLoanAmount.getText().toString()+"\n"+" Frequency: Monthly"+"\n"+" Interest Rate: 2% Per Month",null,ChatPresenter.chatObjects.size()+1);
                if(CREDIT_RULES_STATUS==1){
                    if(isVoucher1Verified && isVoucher2Verified){
                        if(LOAN_TYPE==1){
                            if(SCHOOL_CHECK==1){
                                presenter.botMessage("Congratulations, your loan request has been approved.",viewLoanDetails,ChatPresenter.chatObjects.size()+1);
                            }else {
                                presenter.botMessage("Please wait while we confirm verification of the selected school",null,ChatPresenter.chatObjects.size()+1);
                                next=false;
                            }
                        }else {
                            if(EMPLOYER_CHECK==1){
                                presenter.botMessage("Congratulations, your loan request has been approved.",viewLoanDetails,ChatPresenter.chatObjects.size()+1);
                            }else {
                                presenter.botMessage("Please wait while we confirm verification of the selected employer",null,ChatPresenter.chatObjects.size()+1);
                                next=false;
                            }
                        }

                    }
                    else {
                        presenter.botMessage("Please wait while we confirm the vouching process of the above selected voucher",null,ChatPresenter.chatObjects.size()+1);
                    }


                    next =false;
                }else if(CREDIT_RULES_STATUS==3){
                    presenter.botMessage("Thank you for completing the verification process. We will update you on your loan status shortly.",null,ChatPresenter.chatObjects.size()+1);
                }
                new SaveIsEmiSummary("1").execute();

            }
        });






        this.chatObjects = new ArrayList<>();
        this.presenter = new ChatPresenter();
        presenter.attachView(this);

        // Instantiate the adapter and give it the list of chat objects
        this.chatAdapter = new ChatAdapter(presenter.getChatObjects());

        // Set up the RecyclerView with adapter and layout manager

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        linearLayoutManager.setItemPrefetchEnabled(true);
        rvChatList.setAdapter(chatAdapter);
        rvChatList.setLayoutManager(linearLayoutManager);
        rvChatList.setItemAnimator(new DefaultItemAnimator());
        presenter.botMessage("Hello! Welcome to Findeed" + getResources().getString(R.string.smiley), null, ChatPresenter.chatObjects.size() + 1);
        presenter.botMessage("I hope you are doing well.", null, ChatPresenter.chatObjects.size() + 1);
        presenter.botMessage("What type of loan are you looking out for?", loanType, ChatPresenter.chatObjects.size() + 1);


//        if (isStartOverClicked) {
//           // presenter.botMessage("What type of loan are you looking out for?", loanType, ChatPresenter.chatObjects.size() + 1);
//
//        } else {
//                    if(validateOTPResponse.getData().getUnempanelledSchools()!=null){
//                        if(validateOTPResponse.getData().getUnempanelledSchools().size()!=0){
//                            presenter.botMessage("What type of loan are you looking out for?", loanType, ChatPresenter.chatObjects.size() + 1);
//                            presenter.onEditTextActionDone(validateOTPResponse.getData().getLoans().get(validateOTPResponse.getData().getLoans().size()-1).getLoanType().getLoanTypeName(),null,ChatPresenter.chatObjects.size()+1);
//                            presenter.botMessage("Which school does your child go to? ", actvFrameLayout, ChatPresenter.chatObjects.size() + 1);
//                            Bitmap icon = BitmapFactory.decodeResource(getApplicationContext().getResources(),
//                                    R.drawable.school_static_image);
//                            schoolName = validateOTPResponse.getData().getUnempanelledSchools().get(validateOTPResponse.getData().getUnempanelledSchools().size()-1).getSchoolName();
//                            presenter.setImage(icon,null,ChatPresenter.chatObjects.size()+1);
//                            presenter.botMessage("We have received your request. We will get back to you when the school is listed with us.",null,-123);
//                        }else {
//                            createChat();
//                        }
//                    }else {
//                        createChat();
//                    }


//        try {
//            new CreateChat().execute().get();
//        } catch (ExecutionException e) {
//            e.printStackTrace();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        botMainActivity.setVisibility(View.INVISIBLE);
        cChat=new ProgressDialog(ChatActivity.this);
        cChat.setMessage("Loading data....");
        cChat.setCancelable(false);
        cChat.show();
        createChat();



        //  buildChat();


        //}




        rvChatList.addOnLayoutChangeListener((v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom) -> {
            scrollChatDown();

            isLayoutChangeCalled=true;
            if (bottom < oldBottom) {
                // rvChatList.postDelayed(() -> rvChatList.smoothScrollToPosition(rvChatList.getAdapter().getItemCount() - 1), 100);
                rvChatList.smoothScrollToPosition(rvChatList.getAdapter().getItemCount() - 1);
                rvChatList.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        cChat.dismiss();
                        botMainActivity.setVisibility(View.VISIBLE);
                    }
                },500);

                //rvChatList.smoothScrollToPosition(bottom);
                /// rvChatList.smoothScrollToPosition(ChatPresenter.chatObjects.size());
            }


        });

        if(!isLayoutChangeCalled){
            scrollChatDown();
            rvChatList.postDelayed(new Runnable() {
                @Override
                public void run() {
                    cChat.dismiss();
                    botMainActivity.setVisibility(View.VISIBLE);
                }
            },1000);
        }
    }




    public static double getRepayment(double nper, double pmt, double pv) {
        double error = 0.0000001;
        double high = 1.00;
        double low = 0.00;

        double rate = (2.0 * (nper * pmt - pv)) / (pv * nper);

        while (true) {
            // check for error margin
            double calc = Math.pow(1 + rate, nper);
            calc = (rate * calc) / (calc - 1.0);
            calc -= pmt / pv;

            if (calc > error) {
                // guess too high, lower the guess
                high = rate;
                rate = (high + low) / 2;
            } else if (calc < -error) {
                // guess too low, higher the guess
                low = rate;
                rate = (high + low) / 2;
            } else {
                // acceptable guess
                break;
            }
        }
        return rate;
    }




    private boolean checkMonth(int dayOfMonth) {
        if(dayOfMonth<=9){
            return true;
        }else
            return false;
    }

    private boolean checkDay(int dayOfMonth) {
        if(dayOfMonth<=9){
            return true;
        }else
            return false;
    }


    private boolean matchPinCode(String pincode) {
        for(int i=0;i<pincodeList.getData().size();i++){
            if(pincodeList.getData().get(i).getPincodeName().equals(pincode)){
                return true;
            }
        }

        return false;
    }


    private boolean validateAge() {

        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Calcutta"));
        int currentYear = cal.get(Calendar.YEAR);
        return (currentYear-USER_AGE>=23 && currentYear-USER_AGE<=60);
    }

    private void callForLocation() {
        pd = new ProgressDialog(ChatActivity.this);
        pd.setMessage("Getting Your Location .....");
        pd.setCancelable(false);
        pd.show();
    }


    private GoogleApiClient googleApiClient;

    private void enableLoc() {
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(Bundle bundle) {
                            Log.d("Connected ","APi");

                        }

                        @Override
                        public void onConnectionSuspended(int i) {
                            googleApiClient.connect();
                        }







                    })
                    .addOnConnectionFailedListener(connectionResult -> Log.d("Location error","Location error " + connectionResult.getErrorCode())).build();
            googleApiClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(10);
            locationRequest.setFastestInterval(50);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            builder.setAlwaysShow(true);

            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and checkVideoEnd the result in onActivityResult().
                                status.startResolutionForResult(ChatActivity.this, REQUEST_LOCATION);
//                                    finish();
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                    }
                }
            });
        }else {
            googleApiClient=null;
            enableLoc();
        }

    }

    private boolean hasGPSDevice(Context context) {
        final LocationManager mgr = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);
        if (mgr == null)
            return false;
        final List<String> providers = mgr.getAllProviders();
        if (providers == null)
            return false;
        return providers.contains(LocationManager.GPS_PROVIDER);

    }

    private boolean matchAge(String voter_dob, String panDob) { return voter_dob.equals(panDob); }
    boolean isPermission;

    private void createChat() {

        try{
            setFlags(validateOTPResponse.getData().getLoans().get(0).getErrorList());
            if(ContextCompat.checkSelfPermission(this,Manifest.permission.READ_CONTACTS)==PackageManager.PERMISSION_GRANTED && CCS_PERMISSION_DENIED==0){
                new SendErrorFlags("CCS_PERMISSION_DENIED","1").execute();
                CCS_PERMISSION_DENIED=1;
            }

            CUSTOMER_DETAILS=new Data();
            CUSTOMER_DETAILS.setCustomerId(validateOTPResponse.getData().getCustomerId());
            CUSTOMER_DETAILS.setPhoneNumber(validateOTPResponse.getData().getPhoneNumber());
            CUSTOMER_DETAILS.setChildren(null);
            CUSTOMER_DETAILS.setDocuments(null);
            CUSTOMER_DETAILS.setUnempanelledSchools(null);
            CUSTOMER_DETAILS.setLoans(null);


            if(validateOTPResponse.getData().getLoans()!=null) {
                if(validateOTPResponse.getData().getLoans().size()!=0){
                    presenter.onEditTextActionDone(validateOTPResponse.getData().getLoans().get(0).getLoanType().getLoanTypeName(),null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                    CUSTOMER_DETAILS= (Data) getJson(validateOTPResponse.getData(),"education").clone();
                    if(validateOTPResponse.getData().getLoans().get(0).getLoanType().getLoanTypeName().equals("Education Loan"))
                        LOAN_TYPE=1;
                    else
                        LOAN_TYPE=2;
                    APPLICATION_ID=validateOTPResponse.getData().getLoans().get(0).getApplicationNumber();
                    isLoanTypeClicked=true;
                }else {
                    presenter.botMessage("What type of loan are you looking out for?", loanType, ChatPresenter.chatObjects.size() + 1);
                    next=false;
                }
            }
            else {
                presenter.botMessage("What type of loan are you looking out for?", loanType, ChatPresenter.chatObjects.size() + 1);
                next=false;
            }


            /* for personal loan flow*/
            if(LOAN_TYPE==2){
                if(validateOTPResponse.getData().getFirstName()!=null && next){
                    presenter.botMessage("Can you provide your employer information to avail the loan",searchEmployer,ChatPresenter.chatObjects.size()+1);
                    Bitmap icon = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                            R.drawable.location_placeholder);
                    presenter.setImage(icon,searchEmployer,ChatPresenter.chatObjects.size()+1,validateOTPResponse.getData().getEmployerName());
                    presenter.botMessage("What is your full name ?",userDetailLayout,ChatPresenter.chatObjects.size()+1);
                    presenter.addUserName("First Name: "+validateOTPResponse.getData().getFirstName(),null,ChatPresenter.chatObjects.size()+1,true,CUSTOMER_ID);
                    presenter.addUserName("Surname: "+validateOTPResponse.getData().getLastName(),null,ChatPresenter.chatObjects.size()+1,false,CUSTOMER_ID);
                    USER_NAME=validateOTPResponse.getData().getFirstName();
                    CUSTOMER_DETAILS= (Data) getJson(validateOTPResponse.getData(),"personal_name").clone();
                    isNameGiven=true;
                }else if(isLoanTypeClicked){
                    presenter.botMessage("Can you provide your employer information to avail the loan",searchEmployer,ChatPresenter.chatObjects.size()+1);
                    next=false;
                }

                if(validateOTPResponse.getData().getEmployeeId()!=null && next){
                    presenter.botMessage("What is your employee id ?", employmentLayout, ChatPresenter.chatObjects.size() + 1);
                    presenter.onEditTextActionDone("Employee Id: "+validateOTPResponse.getData().getEmployeeId(),null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                    CUSTOMER_DETAILS= (Data) getJson(validateOTPResponse.getData(),"employee_id").clone();
                    isEmployeeIdSaved=true;
                }else if(isNameGiven){
                    presenter.botMessage("What is your employee id ?", employmentLayout, ChatPresenter.chatObjects.size() + 1);
                    next = false;
                }

//

                if(validateOTPResponse.getData().getEmailAddress()!=null && next){
                    if(validateOTPResponse.getData().getEmailAddress().equals("2")){
                        presenter.botMessage("Please share your email id",userEmailLayout,ChatPresenter.chatObjects.size()+1);
                        presenter.onEditTextActionDone("I do not have Email",null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                        CUSTOMER_DETAILS= (Data) getJson(validateOTPResponse.getData(),"email_id").clone();
                        isEmailSaved=true;
                    }else {
                        presenter.botMessage("Please share your email id",userEmailLayout,ChatPresenter.chatObjects.size()+1);
                        presenter.onEditTextActionDone("Email: "+validateOTPResponse.getData().getEmailAddress(),null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                        CUSTOMER_DETAILS= (Data) getJson(validateOTPResponse.getData(),"email_id").clone();
                        isEmailSaved=true;
                    }
                }else if(isEmployeeIdSaved){
                    presenter.botMessage("Please share your email id",userEmailLayout,ChatPresenter.chatObjects.size()+1);
                    next=false;
                }



                if(validateOTPResponse.getData().getWorkingYears()!=null && next){
                    presenter.botMessage("How long have you been with this company? ",employmentLengthLayout,ChatPresenter.chatObjects.size()+1);
                    presenter.onEditTextActionDone(validateOTPResponse.getData().getWorkingYears(),null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                    CUSTOMER_DETAILS= (Data) getJson(validateOTPResponse.getData(),"working_years").clone();
                    isWorkingYearSaved=true;

                }else if(isEmailSaved){
                    presenter.botMessage("How long have you been with this company? ",employmentLengthLayout,ChatPresenter.chatObjects.size()+1);
                    next =false;
                }



                if(validateOTPResponse.getData().getSalaryRange()!=null && next){
                    presenter.botMessage("How much do you earn in a month",llIncomeRange,ChatPresenter.chatObjects.size()+1);
                    if(MINIMUM_SALARY==2){
                        presenter.onEditTextActionDone(validateOTPResponse.getData().getSalaryRange(),null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                        CUSTOMER_DETAILS= (Data) getJson(validateOTPResponse.getData(),"salary").clone();
                        presenter.botMessage("The minimum applicant income needs to be Rs 15,000",null,ChatPresenter.chatObjects.size()+1);
                        next=false;
                    }else {
                        presenter.onEditTextActionDone(validateOTPResponse.getData().getSalaryRange(),null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                        CUSTOMER_DETAILS= (Data) getJson(validateOTPResponse.getData(),"salary").clone();
                        isSalaryRangeSelected=true;
                    }
                }else if(isWorkingYearSaved){
                    presenter.botMessage("How much do you earn in a month",llIncomeRange,ChatPresenter.chatObjects.size()+1);
                    next=false;
                }



                if(validateOTPResponse.getData().getMaritalStatus()!=null){
                    presenter.botMessage("What is your Marital Status ?",maritalStatus,ChatPresenter.chatObjects.size()+1);
                    presenter.onEditTextActionDone(validateOTPResponse.getData().getMaritalStatus(),null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                    CUSTOMER_DETAILS= (Data) getJson(validateOTPResponse.getData(),"marital_status").clone();
                    isMaritalStatusSaved=true;
                }else if(isSalaryRangeSelected){
                    presenter.botMessage("What is your Marital Status ?",maritalStatus,ChatPresenter.chatObjects.size()+1);
                    next=false;
                }



                if((validateOTPResponse.getData().getIsSaveHaveKids()==0 || validateOTPResponse.getData().getIsSaveHaveKids()==1 || validateOTPResponse.getData().getIsSaveHaveKids()==2) && next){
                    if(validateOTPResponse.getData().getIsSaveHaveKids()==0){
                        presenter.botMessage("Do you have any kids ?",doYouHaveKid,ChatPresenter.chatObjects.size()+1);
                        next=false;
                    }
                    else if(validateOTPResponse.getData().getIsSaveHaveKids()==1){
                        presenter.botMessage("Do you have any kids ? ?",doYouHaveKid,ChatPresenter.chatObjects.size()+1);
                        presenter.onEditTextActionDone("Yes,I do",null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                        CUSTOMER_DETAILS= (Data) getJson(validateOTPResponse.getData(),"do_have_kids").clone();
                        if(validateOTPResponse.getData().getNumberOfChildren()!=0){
                            presenter.botMessage("How many children do you have ?",llayoutchildcount,ChatPresenter.chatObjects.size()+1);
                            presenter.onEditTextActionDone(String.valueOf(validateOTPResponse.getData().getNumberOfChildren())+" Children",null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                            CUSTOMER_DETAILS= (Data) getJson(validateOTPResponse.getData(),"num_kids").clone();
                            isChildSelectedPersonal=true;
                        }
                        else{
                            presenter.botMessage("How many children do you have ?",llayoutchildcount,ChatPresenter.chatObjects.size()+1);
                            next=false;
                        }
                    }
                    else if(validateOTPResponse.getData().getIsSaveHaveKids()==2){
                        presenter.botMessage("Do you have any kids ? ?",doYouHaveKid,ChatPresenter.chatObjects.size()+1);
                        presenter.onEditTextActionDone("No, I don't",null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                        CUSTOMER_DETAILS= (Data) getJson(validateOTPResponse.getData(),"do_have_kids").clone();
                        isChildSelectedPersonal=true;
                    }
                }else if(isMaritalStatusSaved){
                    presenter.botMessage("Do you have any kids ?",doYouHaveKid,ChatPresenter.chatObjects.size()+1);
                    next=false;
                }




                if(validateOTPResponse.getData().getLoans()!=null && next){
                    if(validateOTPResponse.getData().getLoans().size()!=0 ){
                        if(validateOTPResponse.getData().getLoans().get(0).getRequestAmount()!=null){
                            if(valueOf(validateOTPResponse.getData().getLoans().get(0).getRequestAmount())>=10000){
                                presenter.botMessage("How much loan do you require",llayoutseekbar,ChatPresenter.chatObjects.size()+1);
                                presenter.onEditTextActionDone("Rs "+validateOTPResponse.getData().getLoans().get(0).getRequestAmount(),null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                                CUSTOMER_DETAILS= (Data) getJson(validateOTPResponse.getData(),"loan_amount").clone();
                                isLoanRequirementSaved=true;
                            }else if(isChildSelectedPersonal) {
                                presenter.botMessage("How much loan do you require ?",llayoutseekbar,ChatPresenter.chatObjects.size()+1);
                                next=false;
                            }
                        }else if(isChildSelectedPersonal) {
                            presenter.botMessage("How much loan do you require ?",llayoutseekbar,ChatPresenter.chatObjects.size()+1);
                            next=false;
                        }
                    }else if(isChildSelectedPersonal){
                        presenter.botMessage("How much loan do you require ?",llayoutseekbar,ChatPresenter.chatObjects.size()+1);
                        next=false;
                    }

                }else  if(isChildSelectedPersonal){
                    presenter.botMessage("How much loan do you require ?",llayoutseekbar,ChatPresenter.chatObjects.size()+1);
                    next=false;
                }




                if(validateOTPResponse.getData().getLoans()!=null && next){
                    if(validateOTPResponse.getData().getLoans().size()!=0 && valueOf(validateOTPResponse.getData().getLoans().get(0).getTenure())>0){
                        presenter.botMessage("For How Long ?",loanTenure,ChatPresenter.chatObjects.size()+1);
                        presenter.onEditTextActionDone(validateOTPResponse.getData().getLoans().get(0).getTenure()+" Months",null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                        CUSTOMER_DETAILS= (Data) getJson(validateOTPResponse.getData(),"tenure").clone();
                        TENURE=Integer.parseInt(validateOTPResponse.getData().getLoans().get(0).getTenure());
                        tempTenure=TENURE;
                        showTenureFinal.setText(String.valueOf(TENURE)+" Months");
                        isIncomeRangeClicked=true;

                    }else if(isLoanRequirementSaved){
                        presenter.botMessage("For How Long ?",loanTenure,ChatPresenter.chatObjects.size()+1);
                        next=false;
                    }

                }else  if(isLoanRequirementSaved){
                    presenter.botMessage("For How Long ?",loanTenure,ChatPresenter.chatObjects.size()+1);
                    next=false;
                }

            }

            /*
            educational loan flow

            */
            else {
                if(validateOTPResponse.getData().getChildren()!=null && next && validateOTPResponse.getData().getNumberOfChildren()!=0){
                    if(validateOTPResponse.getData().getChildren().size()!=0){
                        presenter.botMessage("Which school does your child go to? ", actvFrameLayout, ChatPresenter.chatObjects.size() + 1);
                        Bitmap icon = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                                R.drawable.location_placeholder);
                        sName = validateOTPResponse.getData().getChildren().get(validateOTPResponse.getData().getChildren().size()-1).getSchool().getSchoolName();
                        SCHOOL_ID=validateOTPResponse.getData().getChildren().get(validateOTPResponse.getData().getChildren().size()-1).getSchool().getSchoolId();
                        presenter.setImage(icon,actvFrameLayout,ChatPresenter.chatObjects.size()+1,sName);
                        CUSTOMER_DETAILS= (Data) getJson(validateOTPResponse.getData(),"which_school").clone();
                        presenter.botMessage("How many of your children study in " + sName + " ?", llayoutchildcount, ChatPresenter.chatObjects.size() + 1);
                        presenter.onEditTextActionDone(String.valueOf(validateOTPResponse.getData().getNumberOfChildren())+" Children",null,ChatPresenter.chatObjects.size()+1, CUSTOMER_DETAILS);
                        CUSTOMER_DETAILS= (Data) getJson(validateOTPResponse.getData(),"num_child").clone();
                        isChildSelected=true;
                        for(int i = 0; i< validateOTPResponse.getData().getChildren().size(); i++){
                            if(validateOTPResponse.getData().getChildren().get(i).getFirstName()!=null && validateOTPResponse.getData().getChildren().get(i).getLastName()!=null){
                                switch (i){
                                    case 0:
                                        presenter.botMessage("Please enter the details of first child ",null,ChatPresenter.chatObjects.size()+1);
                                        presenter.addChildDetail(validateOTPResponse.getData().getChildren().get(i).getFirstName()+" "+ validateOTPResponse.getData().getChildren().get(i).getLastName(),null,ChatPresenter.chatObjects.size()+1,true, Integer.parseInt(validateOTPResponse.getData().getChildren().get(i).getCustomerDependentId()));
                                        presenter.addChildDetail("2nd Standard",null,ChatPresenter.chatObjects.size()+1,false, Integer.parseInt(validateOTPResponse.getData().getChildren().get(i).getCustomerDependentId()));
                                        break;
                                    case 1:
                                        presenter.botMessage("Please enter the details of Second child ",null,ChatPresenter.chatObjects.size()+1);
                                        presenter.addChildDetail(validateOTPResponse.getData().getChildren().get(i).getFirstName()+" "+ validateOTPResponse.getData().getChildren().get(i).getLastName(),null,ChatPresenter.chatObjects.size()+1,true, Integer.parseInt(validateOTPResponse.getData().getChildren().get(i).getCustomerDependentId()));
                                        presenter.addChildDetail("3rd Standard",null,ChatPresenter.chatObjects.size()+1,false, Integer.parseInt(validateOTPResponse.getData().getChildren().get(i).getCustomerDependentId()));

                                        break;
                                    case 2:
                                        presenter.botMessage("Please enter the details of Third child ",null,ChatPresenter.chatObjects.size()+1);

                                        presenter.addChildDetail(validateOTPResponse.getData().getChildren().get(i).getFirstName()+" "+ validateOTPResponse.getData().getChildren().get(i).getLastName(),null,ChatPresenter.chatObjects.size()+1,true, Integer.parseInt(validateOTPResponse.getData().getChildren().get(i).getCustomerDependentId()));
                                        presenter.addChildDetail("2nd Standard",null,ChatPresenter.chatObjects.size()+1,false, Integer.parseInt(validateOTPResponse.getData().getChildren().get(i).getCustomerDependentId()));
                                        break;
                                    case 3:
                                        presenter.botMessage("Please enter the details of Fourth child ",null,ChatPresenter.chatObjects.size()+1);
                                        presenter.addChildDetail(validateOTPResponse.getData().getChildren().get(i).getFirstName()+" "+ validateOTPResponse.getData().getChildren().get(i).getLastName(),null,ChatPresenter.chatObjects.size()+1,true, Integer.parseInt(validateOTPResponse.getData().getChildren().get(i).getCustomerDependentId()));
                                        presenter.addChildDetail("2nd Standard",null,ChatPresenter.chatObjects.size()+1,false, Integer.parseInt(validateOTPResponse.getData().getChildren().get(i).getCustomerDependentId()));

                                        break;
                                    case 4:
                                        presenter.botMessage("Please enter the details of Fifth child ",null,ChatPresenter.chatObjects.size()+1);
                                        presenter.addChildDetail(validateOTPResponse.getData().getChildren().get(i).getFirstName()+" "+ validateOTPResponse.getData().getChildren().get(i).getLastName(),null,ChatPresenter.chatObjects.size()+1,true, Integer.parseInt(validateOTPResponse.getData().getChildren().get(i).getCustomerDependentId()));
                                        presenter.addChildDetail("2nd Standard",null,ChatPresenter.chatObjects.size()+1,false, Integer.parseInt(validateOTPResponse.getData().getChildren().get(i).getCustomerDependentId()));
                                        break;
                                    default:
                                        break;

                                }

                            }
                        }

                    }
                    else if(isLoanTypeClicked) {
                        presenter.botMessage("Which school does your child go to? ", actvFrameLayout, ChatPresenter.chatObjects.size() + 1);
                        next=false;
                    }

                }else if(isLoanTypeClicked) {
                    presenter.botMessage("Which school does your child go to? ", actvFrameLayout, ChatPresenter.chatObjects.size() + 1);
                    next=false;
                }


                if(validateOTPResponse.getData().getLoans()!=null && next){
                    if(validateOTPResponse.getData().getLoans().size()!=0){

                        if(validateOTPResponse.getData().getLoans().get(0).getRequestAmount()!=null && !validateOTPResponse.getData().getLoans().get(0).getRequestAmount().equals("0") && valueOf(validateOTPResponse.getData().getLoans().get(0).getRequestAmount())>=10000){
                            presenter.botMessage("How much loan do you require ?",llayoutseekbar,ChatPresenter.chatObjects.size()+1);
                            loansArray.set(0,loanClass);
                            CUSTOMER_DETAILS_PERSISTANCE.setLoans(loansArray);
                            CUSTOMER_DETAILS= (Data) CUSTOMER_DETAILS_PERSISTANCE.clone();
                            presenter.onEditTextActionDone("Rs "+validateOTPResponse.getData().getLoans().get(0).getRequestAmount(),null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                            CUSTOMER_DETAILS= (Data) getJson(validateOTPResponse.getData(),"loan_amount").clone();
                            isRequestedAmountSelected=true;
                        }else if(isChildSelected) {

                            presenter.botMessage("How much loan do you require ?",llayoutseekbar,ChatPresenter.chatObjects.size()+1);
                            next=false;
                        }


                    }else if(isChildSelected){
                        presenter.botMessage("How much loan do you require ?",llayoutseekbar,ChatPresenter.chatObjects.size()+1);
                        next=false;
                    }


                }else  if(isChildSelected){
                    presenter.botMessage("How much loan do you require ?",llayoutseekbar,ChatPresenter.chatObjects.size()+1);
                    next=false;
                }


                if(validateOTPResponse.getData().getLoans()!=null && next){
                    if(validateOTPResponse.getData().getLoans().size()!=0  && !validateOTPResponse.getData().getLoans().get(0).getTenure().equals("0")){
                        presenter.botMessage("For how long ?",loanTenure,ChatPresenter.chatObjects.size()+1);


                        presenter.onEditTextActionDone(validateOTPResponse.getData().getLoans().get(validateOTPResponse.getData().getLoans().size()-1).getTenure()+" Months",null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                        // getJson(validateOTPResponse.getData(),"tenure");
                        CUSTOMER_DETAILS= (Data) getJson(validateOTPResponse.getData(),"tenure").clone();


                        TENURE=Integer.parseInt(validateOTPResponse.getData().getLoans().get(0).getTenure());
                        tempTenure=TENURE;
                        showTenureFinal.setText(String.valueOf(TENURE)+" Months");
                        isTenureSelected=true;
                        LoanTenure=validateOTPResponse.getData().getLoans().get(validateOTPResponse.getData().getLoans().size()-1).getTenure();

                    }else  if(isRequestedAmountSelected){
                        presenter.botMessage("For How Long ?",loanTenure,ChatPresenter.chatObjects.size()+1);
                        next=false;
                    }

                }else  if(isRequestedAmountSelected){
                    presenter.botMessage("For How Long ?",loanTenure,ChatPresenter.chatObjects.size()+1);
                    next=false;
                }




                if(validateOTPResponse.getData().getEmploymentType()!=null && next){

                    presenter.botMessage("So what you do?",llayoutwhatyoudo,ChatPresenter.chatObjects.size()+1);
                    if(validateOTPResponse.getData().employmentType.equals("Work for someone")){

                        presenter.onEditTextActionDone(validateOTPResponse.getData().getEmploymentType(),null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                        CUSTOMER_DETAILS= (Data) getJson(validateOTPResponse.getData(),"employment_type").clone();

                        presenter.botMessage("Where do you work?", rlWhereDoYouWork, ChatPresenter.chatObjects.size() + 1);
                        if(validateOTPResponse.getData().getEmployerName()!=null) {

                            Bitmap icon = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                                    R.drawable.location_placeholder);
                            presenter.setImage(icon, searchEmployer, ChatPresenter.chatObjects.size() + 1, validateOTPResponse.getData().getEmployerName());
                            isEmploymentTypeSelected = true;
                        }
                        else{
                            next=false;
                        }
                    }else if(validateOTPResponse.getData().employmentType.equals("Self Employeed")){
                        presenter.onEditTextActionDone(validateOTPResponse.getData().getEmploymentType(),null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                        CUSTOMER_DETAILS= (Data) getJson(validateOTPResponse.getData(),"employement_type").clone();

                        presenter.botMessage("What is your bussiness?", rlWhatIsYourBussiness, ChatPresenter.chatObjects.size() + 1);
                        if(validateOTPResponse.getData().getBusinessName()==null){
                            next=false;
                        }else {

                            presenter.onEditTextActionDone(validateOTPResponse.getData().getBusinessName(),null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                            // getJson(validateOTPResponse.getData(),"bussiness_name");
                            CUSTOMER_DETAILS= (Data) getJson(validateOTPResponse.getData(),"bussiness_type").clone();

                            if(validateOTPResponse.getData().getBusinessType()==null){
                                presenter.botMessage("What is your bussiness name?", rlWhatIsYourBussinessName, ChatPresenter.chatObjects.size() + 1);
                                next=false;
                            }else {

                                presenter.botMessage("What is your bussiness name?", rlWhatIsYourBussinessName, ChatPresenter.chatObjects.size() + 1);
                                presenter.onEditTextActionDone(validateOTPResponse.getData().getBusinessType(),null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                                // getJson(validateOTPResponse.getData(),"bussiness_type");
                                CUSTOMER_DETAILS= (Data) getJson(validateOTPResponse.getData(),"bussiness_name").clone();

                                isEmploymentTypeSelected=true;
                            }

                        }

                    }else {
                        presenter.onEditTextActionDone(validateOTPResponse.getData().employmentType,null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                        CUSTOMER_DETAILS= (Data) getJson(validateOTPResponse.getData(),"employement_type").clone();

                        presenter.botMessage("Sorry, your loan application cannot be processed at this point of time.",null,ChatPresenter.chatObjects.size());
                        next=false;
                    }

                }else if(isTenureSelected) {
                    presenter.botMessage("So what you do?",llayoutwhatyoudo,ChatPresenter.chatObjects.size()+1);
                    next=false;
                }




                if(validateOTPResponse.getData().getSalaryRange()!=null && next){
                    presenter.botMessage("How much do you earn in a month",llIncomeRange,ChatPresenter.chatObjects.size()+1);
                    presenter.onEditTextActionDone(validateOTPResponse.getData().salaryRange,null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                    //  getJson(validateOTPResponse.getData(),"salary");
                    CUSTOMER_DETAILS= (Data) getJson(validateOTPResponse.getData(),"salary").clone();


                    if(MINIMUM_SALARY==2){


                        presenter.botMessage("The minimum applicant income needs to be Rs 15,000",null,ChatPresenter.chatObjects.size()+1);
                        next=false;
                    }else {
                        presenter.botMessage("Great!",null, ChatPresenter.chatObjects.size()+1);
                        isIncomeRangeClicked=true;
                    }



                }else if(isEmploymentTypeSelected){
                    presenter.botMessage("How much do you earn in a month",llIncomeRange,ChatPresenter.chatObjects.size()+1);
                    next=false;
                }

            }



         /*




            common flow
            */

            if(validateOTPResponse.getData().getDocuments()!=null && next){
                if(validateOTPResponse.getData().getDocuments().size()!=0){
                    for(int i=0;i<validateOTPResponse.getData().getDocuments().size();i++){
                        String docType=validateOTPResponse.getData().getDocuments().get(i).getDocumentType();
                        if(docType.equals("PAN")){
                            docStatus.put("PAN",i);
                            DOC_TYPE="PAN";
                            presenter.botMessage("Can you provide us your PAN card for ID verification?",panCardLayout,ChatPresenter.chatObjects.size()+1);
                            presenter.onEditTextActionDone("Yes, I have PAN card",null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                            CUSTOMER_DETAILS= (Data) getJson(validateOTPResponse.getData(),"pan_card").clone();
                            PAN_COUNT=validateOTPResponse.getData().getDocuments().get(i).getUploadCount();
                            presenter.botMessage("Please Upload your PAN card for verification",lLayoutPanOption,ChatPresenter.chatObjects.size()+1);

                        }else
                            continue;
                    }
                    if(docStatus.get("PAN")!=null){
                        new GetImageFromUrl(validateOTPResponse.getData().getDocuments().get(docStatus.get("PAN")).getFrontImageURL(), "PAN", "FRONT").execute().get();

                        presenter.sendDocsImage(PanFrontBitmap, null, ChatPresenter.chatObjects.size() + 1);
                        presenter.botMessage("Please continue for us to process your PAN", btnPanContinue, ChatPresenter.chatObjects.size() + 1);

                        String docName=validateOTPResponse.getData().getDocuments().get(docStatus.get("PAN")).getFirstName();
                        String docDob=validateOTPResponse.getData().getDocuments().get(docStatus.get("PAN")).getDateOfBirth();

                        if(docName!=null && docDob!=null) {
                            presenter.botMessage("Here are your details in the PAN card", null, ChatPresenter.chatObjects.size() + 1);
                            PAN_DOB = validateOTPResponse.getData().getDocuments().get(docStatus.get("PAN")).getDateOfBirth();
                            Log.i("PAN_DOB", PAN_DOB);
                            presenter.botMessage("PAN ID: " + validateOTPResponse.getData().getDocuments().get(docStatus.get("PAN")).getDocumentId() + "\n" + "Name: " + validateOTPResponse.getData().getDocuments().get(docStatus.get("PAN")).getFirstName() + "\n" + "Date of Birth: " + validateOTPResponse.getData().getDocuments().get(docStatus.get("PAN")).getDateOfBirth(), lLayoutEditPanInfo, ChatPresenter.chatObjects.size() + 1);
//                          if(PAN_OCR_FAILED==1){
                            PAN_UPLOAD_NUMBER = 1;
                            if (PAN_UPLOAD_NUMBER == 2) {
                                presenter.botMessage("Looks like you are having an issue uploading the Pan. We will reach out to you to resolve this. Thanks for your patience", null, ChatPresenter.chatObjects.size() + 1);
                                next = false;
                            } else {
                                if (CUSTOMER_AGE_RESTRICTION == 1) {
                                    if(GOVT_VERIFICATION_PAN==1){
                                        presenter.botMessage("Awesome now we have your ID proof", null, ChatPresenter.chatObjects.size() + 1);
                                        isPanSubmitted = true;
                                    }else {
                                        presenter.botMessage("Looks like the PAN details you uploaded were not valid",null,ChatPresenter.chatObjects.size()+1);
                                        next=false;
                                    }
                                } else if (CUSTOMER_AGE_RESTRICTION == 2) {
//                                      presenter.botMessage("Awesome now we have your ID proof",null,ChatPresenter.chatObjects.size()+1);
//                                      isPanSubmitted=true;
                                    presenter.botMessage("Sorry we are unable to process your application. The applicant must be atleast  23 years old and cannot be older than 60 years. Please email us on contact@findeed.in if need further help.", null, ChatPresenter.chatObjects.size() + 1);
                                    next = false;
                                } else {
                                    if(GOVT_VERIFICATION_PAN==1){
                                        presenter.botMessage("Awesome now we have your ID proof", null, ChatPresenter.chatObjects.size() + 1);
                                        isPanSubmitted = true;
                                    }else {
                                        presenter.botMessage("Looks like the PAN details you uploaded were not valid",null,ChatPresenter.chatObjects.size()+1);
                                        next=false;
                                    }

//                                      presenter.botMessage("Can you provide us your PAN card for ID verification?",panCardLayout,ChatPresenter.chatObjects.size()+1);
//                                      next=false;
                                }
                            }
                        }
                        else{
                            next=false;
                        }
                    }else {
                        presenter.botMessage("Can you provide us your PAN card for ID verification?",panCardLayout,ChatPresenter.chatObjects.size()+1);
                        next=false;
                    }
                }else if(isIncomeRangeClicked){
                    presenter.botMessage("Can you provide us your PAN card for ID verification?",panCardLayout,ChatPresenter.chatObjects.size()+1);
                    next=false;
                }
            }else if(isIncomeRangeClicked){
                presenter.botMessage("Can you provide us your PAN card for ID verification?",panCardLayout,ChatPresenter.chatObjects.size()+1);
                next=false;
            }


            if(validateOTPResponse.getData().getDocuments()!=null && next){
                if(validateOTPResponse.getData().getDocuments().size()!=0){

                    presenter.botMessage("Can you provide us your address proof? You can give us your Aadhaar or Voter's ID card. Which one would you like to use",llayoutoption,ChatPresenter.chatObjects.size()+1);
                    for(int i=0;i<validateOTPResponse.getData().getDocuments().size();i++){
                        String docType=validateOTPResponse.getData().getDocuments().get(i).getDocumentType();
                        Log.i("insideLoop",String.valueOf(i)+","+docType);
                        if(docType.equals("AADHAAR") || docType.equals("VOTER_ID")){
                            if(docType.equals("AADHAAR")){
                                docStatus.put("AADHAAR",i);
                                chooseedAadhar=true;
                                isAddressProofSubmitted=true;
                                DOC_TYPE="AADHAAR";
                                AADHAR_ADDRESS=validateOTPResponse.getData().getDocuments().get(i).getAddress();
                                AADHAR_DOC_ID=validateOTPResponse.getData().getDocuments().get(i).getDocumentId();
                                AADHAR_FIRST_NAME=validateOTPResponse.getData().getDocuments().get(i).getFirstName();
                                AADHAR_DOB=validateOTPResponse.getData().getDocuments().get(i).getDateOfBirth();
                                AADHAR_GENDER=validateOTPResponse.getData().getDocuments().get(i).getGender();
                                isPinMatch=findAndMatchPin(AADHAR_ADDRESS);
                                presenter.onEditTextActionDone("Aadhaar Card",null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                                CUSTOMER_DETAILS= (Data) getJson(validateOTPResponse.getData(),"aadhaar_card").clone();
                                presenter.botMessage("Steps to follow:" + getResources().getString(R.string.aadhaar_steps), llayoutaadharpicture, ChatPresenter.chatObjects.size() + 1);
                               break;

                            }else if(docType.equals("VOTER_ID"))  {
                                docStatus.put("VOTER_ID",i);
                                isAddressProofSubmitted=true;
                                DOC_TYPE="VOTER_ID";
                                VOTER_ADDRESS=validateOTPResponse.getData().getDocuments().get(i).getAddress();
                                VOTER_DOCUMENT_ID=validateOTPResponse.getData().getDocuments().get(i).getDocumentId();
                                VOTER_FIRST_NAME=validateOTPResponse.getData().getDocuments().get(i).getFirstName();
                                VOTER_DOB=validateOTPResponse.getData().getDocuments().get(i).getDateOfBirth();
                                VOTER_GENDER=validateOTPResponse.getData().getDocuments().get(i).getGender();
                                presenter.onEditTextActionDone("Voter Id", null, ChatPresenter.chatObjects.size() + 1, CUSTOMER_DETAILS);
                                CUSTOMER_DETAILS = (Data) getJson(validateOTPResponse.getData(), "voter_id").clone();
                                presenter.botMessage("Can you provide your Voter's ID for verification?", llayoutoptionselectforvoterid, ChatPresenter.chatObjects.size() + 1);

                                break;
                            }
                        }else if(i==validateOTPResponse.getData().getDocuments().size()-1 && isPanSubmitted && !isAddressProofSubmitted){
                            next=false;
                        }
                    }
                    if(docStatus.get("AADHAAR")!=null){
                        new GetImageFromUrl(validateOTPResponse.getData().getDocuments().get(docStatus.get("AADHAAR")).getFrontImageURL(), "AADHAAR", "FRONT").execute().get();
                        presenter.setBotMessageImage(AadhaarFrontBitmap, null, ChatPresenter.chatObjects.size() + 1);

                        presenter.botMessage("Aadhaar Number: "+validateOTPResponse.getData().getDocuments().get(docStatus.get("AADHAAR")).getDocumentId()+"\n" +
                                "Name: "+validateOTPResponse.getData().getDocuments().get(docStatus.get("AADHAAR")).getFirstName()+"\n" +
                                "Date of Birth: "+validateOTPResponse.getData().getDocuments().get(docStatus.get("AADHAAR")).getDateOfBirth()+"\n" +
                                "Address: "+validateOTPResponse.getData().getDocuments().get(docStatus.get("AADHAAR")).getAddress(),llayoutaadhardetailedit,ChatPresenter.chatObjects.size()+1);

                       if(PAN_AADHAR_MATCH==1){
                           presenter.botMessage("Thank you for providing the ID and address proofs.",null,ChatPresenter.chatObjects.size()+1);
                       }else {
                           presenter.botMessage("The date of birth you provided on the PAN did not match the address proof. Please check your details again and resubmit",null,ChatPresenter.chatObjects.size()+1);
                            next=false;
                       }

                    }
                    else if(docStatus.get("VOTER_ID")!=null){
                        if(VOTER_FIRST_NAME!=null && VOTER_DOB!=null) {
                            new GetImageFromUrl(validateOTPResponse.getData().getDocuments().get(docStatus.get("VOTER_ID")).getFrontImageURL(), "VOTER", "FRONT").execute().get();
                            VOTER_FRONT=VoterFrontBitmap;
                            presenter.sendVoterIdImage(VoterFrontBitmap, null, ChatPresenter.chatObjects.size() + 1);
                            presenter.botMessage("Please upload backside image of the Voter's ID", clickPickForVoterBack, ChatPresenter.chatObjects.size() + 1);
                            new GetImageFromUrl(validateOTPResponse.getData().getDocuments().get(docStatus.get("VOTER_ID")).getBackImageURL(), "VOTER", "BACK").execute().get();
                            VOTER_BACK=VoterBackBitmap;
                            presenter.sendVoterIdImage(VoterBackBitmap, null, ChatPresenter.chatObjects.size() + 1);

                            presenter.botMessage("Here are your details in the Voter's ID card.",null,ChatPresenter.chatObjects.size()+1);
//                                Bitmap icon4 = BitmapFactory.decodeResource(getApplicationContext().getResources(),
//                                        R.drawable.dummy_voter_front);
                            presenter.botMessage("Full Name: "+validateOTPResponse.getData().getDocuments().get(docStatus.get("VOTER_ID")).getFirstName()+"\n"+"Gender-"+validateOTPResponse.getData().getDocuments().get(docStatus.get("VOTER_ID")).getGender()+"\n"+"Date of Birth - "+validateOTPResponse.getData().getDocuments().get(docStatus.get("VOTER_ID")).getDateOfBirth()+"\n"+"Address -"+validateOTPResponse.getData().getDocuments().get(docStatus.get("VOTER_ID")).getAddress(),llayoutvoterdetailedit,ChatPresenter.chatObjects.size()+1);
                            if(PAN_AADHAR_MATCH==1){
                                presenter.botMessage("Thank you for providing the ID and address proofs.",null,ChatPresenter.chatObjects.size()+1);
                            }else {
                                presenter.botMessage("The date of birth you provided on the PAN did not match the address proof. Please check your details again and resubmit",null,ChatPresenter.chatObjects.size()+1);
                                next=false;
                            }

                        }
                        else{
                            next=false;
                        }

                    }
                    else{
                        next=false;
                    }
                }else if(isPanSubmitted){
                    presenter.botMessage("Can you provide us your address proof? You can give us your Aadhaar or Voter's ID card. Which one would you like to use",llayoutoption,ChatPresenter.chatObjects.size()+1);
                    next=false;
                }
            }else if(isPanSubmitted){
                presenter.botMessage("Can you provide us your address proof? You can give us your Aadhaar or Voter's ID card. Which one would you like to use",llayoutoption,ChatPresenter.chatObjects.size()+1);
                next=false;
            }




            if((BUREAU_PERMISSION_DENIED==1 ||BUREAU_PERMISSION_DENIED==2 || BUREAU_PERMISSION_DENIED==0) && next){

                if(BUREAU_PERMISSION_DENIED==1){
                    isBureauAgreeClicked=true;
                    presenter.botMessage("Please click \"I Agree\" to allow Findeed to share the details with the lending partners and check your credit information with the credit bureau. Click here for the detailed Terms & Conditions.", llayoutagreeorcancel, ChatPresenter.chatObjects.size() + 1);
                    presenter.onEditTextActionDone("I Agree",null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                    //  CUSTOMER_DETAILS= (Data) getJson(validateOTPResponse.getData(),"bureau_permission").clone();
                }else if(BUREAU_PERMISSION_DENIED==2){
                    next=false;
                    presenter.botMessage("Please click \"I Agree\" to allow Findeed to share the details with the lending partners and check your credit information with the credit bureau. Click here for the detailed Terms & Conditions.", llayoutagreeorcancel, ChatPresenter.chatObjects.size() + 1);
                }else {
                    next=false;
                    presenter.botMessage("Please click \"I Agree\" to allow Findeed to share the details with the lending partners and check your credit information with the credit bureau. Click here for the detailed Terms & Conditions.", llayoutagreeorcancel, ChatPresenter.chatObjects.size() + 1);
                }
            }else if(isAddressProofSubmitted){
                next=false;
            }



            if((validateOTPResponse.getData().getIsCurrentPermanentAddressSame()==0 ||validateOTPResponse.getData().getIsCurrentPermanentAddressSame()==1 ||validateOTPResponse.getData().getIsCurrentPermanentAddressSame()==2)&&next) {
                        CUSTOMER_DETAILS_PERSISTANCE.setIsCurrentPermanentAddressSame(0);
                        CUSTOMER_DETAILS= (Data) CUSTOMER_DETAILS_PERSISTANCE.clone();

                        if(DOC_TYPE.equals("AADHAAR"))
                            presenter.botMessage("Is the address in your Aadhar card your current address?", llayoutyesorno, ChatPresenter.chatObjects.size() + 1);
                        else
                            presenter.botMessage("Is the address in your Voter's Id card your current address?", llayoutyesorno, ChatPresenter.chatObjects.size() + 1);

                if(validateOTPResponse.getData().getIsCurrentPermanentAddressSame()==1){

                    presenter.onEditTextActionDone("Yes", null, ChatPresenter.chatObjects.size() + 1,CUSTOMER_DETAILS);
                    CUSTOMER_DETAILS= (Data) getJson(validateOTPResponse.getData(),"same_address").clone();

                    if(DOC_TYPE.equals("AADHAAR")){
                    }else {
                        presenter.botMessage("Can you provide your supporting document for address proof? You can give us your Gas Bill or Electricity Bill. Which one would you like to use?",electricityLayout,ChatPresenter.chatObjects.size()+1);

                    }
                    isAddressSame=true;

                }else if(validateOTPResponse.getData().getIsCurrentPermanentAddressSame()==2){
                    presenter.onEditTextActionDone("No", null, ChatPresenter.chatObjects.size() + 1,CUSTOMER_DETAILS);
                    CUSTOMER_DETAILS= (Data) getJson(validateOTPResponse.getData(),"same_address").clone();
                    if(validateOTPResponse.getData().getAddress()!=null ){
                        presenter.botMessage("Do you want to use your current location or fill it by yourself", llayoutAddressManually, ChatPresenter.chatObjects.size() + 1);

                        if(PIN_CODE_VERIFICATION==1){
                            if(validateOTPResponse.getData().getAddress().getAddressLine1()!=null &&validateOTPResponse.getData().getAddress().getZipcode()!=null) {
                                presenter.botMessage("Please enter your current address", llayoutFillAddressManually, ChatPresenter.chatObjects.size() + 1);
                                Bitmap icon4 = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                                        R.drawable.location_placeholder);
                                userLocation = validateOTPResponse.getData().getAddress().getCity() + ", " + validateOTPResponse.getData().getAddress().getZipcode();
                                presenter.sendUserLocation(icon4, useMyCurrentLocationLayout, ChatPresenter.chatObjects.size() + 1);
                                presenter.botMessage("Can you provide your supporting document for address proof? You can give us your Gas Bill or Electricity Bill. Which one would you like to use?", electricityLayout, ChatPresenter.chatObjects.size() + 1);
                                isAddressSame = true;
                            }
                            else{
                                presenter.botMessage("Please enter your current address", llayoutFillAddressManually, ChatPresenter.chatObjects.size() + 1);

                                next=false;
                            }
                        }else if(PIN_CODE_VERIFICATION==2){
                            presenter.botMessage("Please enter your current address", llayoutFillAddressManually, ChatPresenter.chatObjects.size() + 1);
                            Bitmap icon4 = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                                    R.drawable.location_placeholder);
                            userLocation=validateOTPResponse.getData().getAddress().getCity();
                            presenter.sendUserLocation(icon4,useMyCurrentLocationLayout,ChatPresenter.chatObjects.size()+1);
                            presenter.botMessage("Sorry! We do not service the Pin code currently",null,ChatPresenter.chatObjects.size()+1);
                            next=false;
                        }else {
                            next=false;
                        }

                    }else {
                        presenter.botMessage("Do you want to use your current location or fill it by yourself", llayoutAddressManually, ChatPresenter.chatObjects.size() + 1);
                        next=false;
                    }

                }

                else {
                    next = false;
                }
            }else if(isBureauAgreeClicked){
                if(chooseedAadhar)
                    presenter.botMessage("Is the address in your Aadhar card your current address?", llayoutyesorno, ChatPresenter.chatObjects.size() + 1);
                else
                    presenter.botMessage("Is the address in your Voter's Id card your current address?", llayoutyesorno, ChatPresenter.chatObjects.size() + 1);
                next = false;
            }



            if((CCS_PERMISSION_DENIED==0 ||CCS_PERMISSION_DENIED==1 ||CCS_PERMISSION_DENIED==2)&&next){
                if(CCS_PERMISSION_DENIED==1 ){
                    if(ContextCompat.checkSelfPermission(this,Manifest.permission.READ_CONTACTS)==PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,Manifest.permission.READ_SMS)==PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,Manifest.permission.READ_PHONE_STATE)==PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_COARSE_LOCATION)==PackageManager.PERMISSION_GRANTED)
                        isCcsPermissionGranted=true;
                    else {
                        isCcsPermissionGranted=true;
                        llayoutAdditionalInfo.setVisibility(View.VISIBLE);
                    }

                }else if(CCS_PERMISSION_DENIED==2){
                    presenter.botMessage("We would require some additional information from you for smooth processing of your loan application", llayoutAdditionalInfo, ChatPresenter.chatObjects.size() + 1);
                    next=false;
                }else {
                    presenter.botMessage("We would require some additional information from you for smooth processing of your loan application", llayoutAdditionalInfo, ChatPresenter.chatObjects.size() + 1);
                    next=false;
                }

            }else if(isAddressSame){
                presenter.botMessage("We would require some additional information from you for smooth processing of your loan application", llayoutAdditionalInfo, ChatPresenter.chatObjects.size() + 1);
                next=false;
            }


            /*Here checking whether user had choosed aadhar and is his currentAddress and permanent address are same if> > yes> do'nt ask for gas or electricty bill else ask for it*/

            if(DOC_TYPE.equals("AADHAAR") && next && validateOTPResponse.getData().isCurrentPermanentAddressSame==1){
                    isGasOrElectricityBill=true;

            }else if(isCcsPermissionGranted && next) {
                        if(validateOTPResponse.getData().getDocuments()!=null){
                            if(validateOTPResponse.getData().getDocuments().size()!=0){
                                for(int i=0;i<validateOTPResponse.getData().getDocuments().size();i++){
                                String docType=validateOTPResponse.getData().getDocuments().get(i).getDocumentType();
                                if(docType.equals("GAS_BILL") || docType.equals("ELECTRICITY_BILL")){
                                        if(docType.equals("GAS_BILL")){
                                            isGasOrElectricityBill=true;
                                            docStatus.put("GAS_BILL", i);
                                            DOC_TYPE="GAS_BILL";
                                            presenter.onEditTextActionDone("GAS BILL",null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                                            CUSTOMER_DETAILS= (Data) getJson(validateOTPResponse.getData(),"gas_bill").clone();
                                            presenter.botMessage("Can you provide your gas bill for verification?",gasOption,ChatPresenter.chatObjects.size()+1);
                                           break;
                                        } else {
                                            isGasOrElectricityBill = true;
                                            docStatus.put("ELECTRICITY_BILL", i);
                                            DOC_TYPE = "ELECTRICITY_BILL";
                                            presenter.onEditTextActionDone("ELECTRICITY BILL", null, ChatPresenter.chatObjects.size() + 1, CUSTOMER_DETAILS);
                                            CUSTOMER_DETAILS = (Data) getJson(validateOTPResponse.getData(), "electricity_bill").clone();
                                            presenter.botMessage("Can you provide your Electricity bill for verification?", electricityOption, ChatPresenter.chatObjects.size() + 1);

                                        }
                                    } else {
                                        if (i == validateOTPResponse.getData().getDocuments().size() - 1 && isCcsPermissionGranted && !isGasOrElectricityBill)
                                            next = false;
                                    }
                                }

                                    if(docStatus.get("GAS_BILL")!=null){

                                        new GetImageFromUrl(validateOTPResponse.getData().getDocuments().get(docStatus.get("GAS_BILL")).getFrontImageURL(), "GAS_BILL", "FRONT").execute().get();
                                        presenter.sendVoterIdImage(GasBillBitmap, null, ChatPresenter.chatObjects.size() + 1);



                                    }
                                    else if(docStatus.get("ELECTRICITY_BILL")!=null){
                                        new GetImageFromUrl(validateOTPResponse.getData().getDocuments().get(docStatus.get("ELECTRICITY_BILL")).getFrontImageURL(), "ELECTRICITY_BILL", "FRONT").execute().get();
                                        presenter.sendVoterIdImage(ElectricityBillBitmap, null, ChatPresenter.chatObjects.size() + 1);

                                    }
                                    else{
                                        next=false;
                                    }





                }else if(isCcsPermissionGranted){
                    presenter.botMessage("Can you provide your supporting document for address proof? You can give us your Gas Bill or Electricity Bill. Which one would you like to use?",electricityLayout,ChatPresenter.chatObjects.size()+1);
                    next=false;
                }
            }else if(isCcsPermissionGranted){
                presenter.botMessage("Can you provide your supporting document for address proof? You can give us your Gas Bill or Electricity Bill. Which one would you like to use?",electricityLayout,ChatPresenter.chatObjects.size()+1);
                next=false;
            }
        }



            if(validateOTPResponse.getData().getVouchers()!=null && next ){
                    if(validateOTPResponse.getData().getVouchers().size()!=0 ){
                        ArrayList<Voucher> vca=new ArrayList<>();
                        Voucher vc1=new Voucher();
                        Voucher vc2=new Voucher();
                        if(validateOTPResponse.getData().getVouchers().size()==1) {
                            vc1 = validateOTPResponse.getData().getVouchers().get(0);
                            vca.add(vc1);
                        }
                        else{
                            vc1 = validateOTPResponse.getData().getVouchers().get(0);
                            vc2 = validateOTPResponse.getData().getVouchers().get(1);
                            vca.add(vc1);
                            vca.add(vc2);
                        }

                    CUSTOMER_DETAILS_PERSISTANCE.setVouchers(vca);
                    CUSTOMER_DETAILS= (Data) CUSTOMER_DETAILS_PERSISTANCE.clone();
                    presenter.botMessage("Can you provide us 2 pepole who can stand for you?", btnSelectVoucher, ChatPresenter.chatObjects.size() + 1);
                    if(validateOTPResponse.getData().getVouchers().size()==1){
                        ContactData cd = new ContactData();
                        cd.setVoucherImage(validateOTPResponse.getData().getVouchers().get(0).getVoucherName().substring(0,1));
                        cd.setVoucherName(validateOTPResponse.getData().getVouchers().get(0).getVoucherName());
                        cd.setVoucherNumber("Voucher 1");
                        Voucher1=validateOTPResponse.getData().getVouchers().get(0).getPhoneNumber();//.split(" ")[1]+validateOTPResponse.getData().getVouchers().get(0).getPhoneNumber().split(" ")[2];
                        isVoucher1Verified=isVoucherVerified(validateOTPResponse.getData().getVouchers().get(0));
                        if(isVoucher1Verified){
                            Bitmap icon2 = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                                    R.drawable.tick_green);
                            cd.setVoucherStatusImage(icon2);
                            cd.setVoucherStatus("Verified");
                        }else {

                            cd.setVoucherStatus("Pending");
                        }
                        presenter.sendContacts(cd,null,ChatPresenter.chatObjects.size()+1);
                        presenter.botMessage(" Kindly select your 2nd Voucher",select2ndVoucher,ChatPresenter.chatObjects.size()+1);
                        next=false;

                    }
                    else{
                        ContactData voucher1 = new ContactData();
                        voucher1.setVoucherImage(validateOTPResponse.getData().getVouchers().get(0).getVoucherName().substring(0,1));
                        voucher1.setVoucherName(validateOTPResponse.getData().getVouchers().get(0).getVoucherName());
                        voucher1.setVoucherNumber("Voucher 1");
                        isVoucher1Verified=isVoucherVerified(validateOTPResponse.getData().getVouchers().get(0));
                        if(isVoucher1Verified){
                            Bitmap icon2 = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                                    R.drawable.tick_green);
                            voucher1.setVoucherStatusImage(icon2);
                            voucher1.setVoucherStatus("Verified");
                        }else {
                            voucher1.setVoucherStatus("Pending");
                        }

                        presenter.sendContacts(voucher1,null,ChatPresenter.chatObjects.size()+1);
                        presenter.botMessage(" Kindly select your 2nd Voucher",select2ndVoucher,ChatPresenter.chatObjects.size()+1);
                        isVoucher2Verified=isVoucherVerified(validateOTPResponse.getData().getVouchers().get(1));
                        ContactData voucher2 = new ContactData();
                        voucher2.setVoucherImage(validateOTPResponse.getData().getVouchers().get(1).getVoucherName().substring(0,1));
                        voucher2.setVoucherName(validateOTPResponse.getData().getVouchers().get(1).getVoucherName());
                        voucher2.setVoucherNumber("Voucher 2");
                        if(isVoucher2Verified){
                            Bitmap icon3 = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                                    R.drawable.tick_green);
                            voucher2.setVoucherStatusImage(icon3);
                            voucher2.setVoucherStatus("Verified");
                        }else {
                            voucher2.setVoucherStatus("Pending");
                        }
                        presenter.sendContacts(voucher2,null,ChatPresenter.chatObjects.size()+1);
                        ContactData school = new ContactData();
                        if(LOAN_TYPE==1){
                            if(sName==null)
                                sName="Kumarans Public School";
                            school.setVoucherImage(sName.substring(0,1));
                            school.setVoucherName(sName);
                            school.setVoucherNumber("School");
                            if(SCHOOL_CHECK==1){
                                school.setVoucherStatus("Verified");
                                Bitmap icon3 = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                                        R.drawable.tick_green);
                                school.setVoucherStatusImage(icon3);
                            }else {
                                school.setVoucherStatus("Pending");
                            }
                        }else {
                            sName=validateOTPResponse.getData().getEmployerName();
                            if(sName==null)
                                sName="Wipro Tech.";
                            school.setVoucherImage(sName.substring(0,1));
                            school.setVoucherName(sName);
                            school.setVoucherNumber("Employer");
                            if(EMPLOYER_CHECK==1){
                                school.setVoucherStatus("Verified");
                                Bitmap icon3 = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                                        R.drawable.tick_green);
                                school.setVoucherStatusImage(icon3);
                            }else {
                                school.setVoucherStatus("Pending");
                            }

                        }
                        presenter.sendContacts(school,null,ChatPresenter.chatObjects.size()+1);
                        isVocherSaved=true;

                    }

                }else if(isGasOrElectricityBill){
                    presenter.botMessage("Can you provide us 2 pepole who can stand for you?", btnSelectVoucher, ChatPresenter.chatObjects.size() + 1);
                    next=false;
                }

            }else if(isGasOrElectricityBill) {
                presenter.botMessage("Can you provide us 2 pepole who can stand for you?", btnSelectVoucher, ChatPresenter.chatObjects.size() + 1);
                next=false;
            }




        if((CREDIT_RULES_STATUS==1 || CREDIT_RULES_STATUS==2 || CREDIT_RULES_STATUS==0 || CREDIT_RULES_STATUS==3 || CREDIT_RULES_STATUS==4 )&&next){
            if(CREDIT_RULES_STATUS==1 || CREDIT_RULES_STATUS==3){
                isCreditStatusTrue=true;
                presenter.botMessage("Congratulations! You are eligible for a loan amount of upto Rs. 50000",null,ChatPresenter.chatObjects.size()+1);
            }else if(CREDIT_RULES_STATUS==2){
                presenter.botMessage("Sorry, We are unable to approve your application for X reason.",null,ChatPresenter.chatObjects.size()+1);
                next=false;
            }
            else if(CREDIT_RULES_STATUS==0 ||CREDIT_RULES_STATUS==4){
                presenter.botMessage("Thanks for providing details. Once your references complete the steps on their phone, will complete the reference checks and employee verification and get back to you. Pls note we are closed on Sat & Sun",null,ChatPresenter.chatObjects.size()+1);
                next=false;
            }
        }else if(isVocherSaved){
            presenter.botMessage("Thanks for providing details. Once your references complete the steps on their phone, will complete the reference checks and employee verification and get back to you. Pls note we are closed on Sat & Sun",null,ChatPresenter.chatObjects.size()+1);
            next=false;

        }






        if(( validateOTPResponse.getData().getIsSaveLoanSummary()==1||validateOTPResponse.getData().getIsSaveLoanSummary()==2 ||validateOTPResponse.getData().getIsSaveLoanSummary()==0)&& next){
                if(validateOTPResponse.getData().getIsSaveLoanSummary()==1 && isCreditStatusTrue ){
                    isLoanSummarySaved=true;
                    ELIGIBLE_AMOUNT=validateOTPResponse.getData().getLoans().get(0).getMaxLoanEligible();
                    seekBarUpperLimit.setText("\u20B9 "+String.valueOf(Math.round(ELIGIBLE_AMOUNT)));
                    Log.i("MAX_ELIGIBLE",seekBarUpperLimit.getText().toString());
                    selectedLoanAmount= (int) parseFloat(validateOTPResponse.getData().getLoans().get(0).getRequestAmount());
                    if(selectedLoanAmount>50000)
                        eligibileLoanBar.setMax(50000);
                    else
                        eligibileLoanBar.setMax(ELIGIBLE_AMOUNT);
                    eligibileLoanBar.setProgress(valueOf(selectedLoanAmount));
                    presenter.botMessage("Here is a summary based on your earlier selection. You can change the required loan amount, tenure and time of payment to see your EMI.",frameLayout,ChatPresenter.chatObjects.size()+1);

                }else if(validateOTPResponse.getData().getIsSaveLoanSummary()==2 && isCreditStatusTrue){


                    ELIGIBLE_AMOUNT=validateOTPResponse.getData().getLoans().get(0).getMaxLoanEligible();
                    seekBarUpperLimit.setText("\u20B9 "+String.valueOf(Math.round(ELIGIBLE_AMOUNT)));
                    Log.i("MAX_ELIGIBLE",seekBarUpperLimit.getText().toString());
                    selectedLoanAmount= (int) parseFloat(validateOTPResponse.getData().getLoans().get(0).getRequestAmount());
                    if(selectedLoanAmount>50000)
                        eligibileLoanBar.setMax(50000);
                    else
                        eligibileLoanBar.setMax(ELIGIBLE_AMOUNT);
                    eligibileLoanBar.setProgress(valueOf(selectedLoanAmount));

                    if(validateOTPResponse.getData().getLoans().get(0).getEmiRate()==null){
                        float rate=  getEMI(Math.round(valueOf(validateOTPResponse.getData().getLoans().get(0).getRequestAmount())), valueOf(validateOTPResponse.getData().getLoans().get(0).getTenure()))  ;
                        emiRateFindeed.setText(String.valueOf(rate));
                        // eligibileLoanBar.setProgress(Math.round(Float.valueOf(validateOTPResponse.getData().getLoans().get(0).getRequestAmount())));
                        presenter.botMessage("Here is a summary based on your earlier selection. You can change the required loan amount, tenure and time of payment to see your EMI.",frameLayout,ChatPresenter.chatObjects.size()+1);
                        next=false;
                    }else {
                        emiRateFindeed.setText("\u20B9 "+String.valueOf(Math.round(valueOf(validateOTPResponse.getData().getLoans().get(0).getEmiRate())))+"/Month");
                        // eligibileLoanBar.setProgress(Math.round(Float.valueOf(validateOTPResponse.getData().getLoans().get(0).getRequestAmount())));
                        presenter.botMessage("Here is a summary based on your earlier selection. You can change the required loan amount, tenure and time of payment to see your EMI.",frameLayout,ChatPresenter.chatObjects.size()+1);
                        next=false;
                    }

                }else if(validateOTPResponse.getData().getIsSaveLoanSummary()==0&&isCreditStatusTrue) {
                    ELIGIBLE_AMOUNT=validateOTPResponse.getData().getLoans().get(0).getMaxLoanEligible();
                    seekBarUpperLimit.setText("\u20B9 "+String.valueOf(Math.round(ELIGIBLE_AMOUNT)));
                    Log.i("MAX_ELIGIBLE",seekBarUpperLimit.getText().toString());
                    selectedLoanAmount= (int) parseFloat(validateOTPResponse.getData().getLoans().get(0).getRequestAmount());
                    if(selectedLoanAmount>50000)
                        eligibileLoanBar.setMax(50000);
                    else
                        eligibileLoanBar.setMax(ELIGIBLE_AMOUNT);
                    eligibileLoanBar.setProgress(valueOf(selectedLoanAmount));

                    if(validateOTPResponse.getData().getLoans().get(0).getEmiRate()==null){
                        float rate=  getEMI(Math.round(valueOf(validateOTPResponse.getData().getLoans().get(0).getRequestAmount())), valueOf(validateOTPResponse.getData().getLoans().get(0).getTenure()))  ;
                        emiRateFindeed.setText("\u20B9 "+String.valueOf(rate)+"/Month");                       // eligibileLoanBar.setProgress(Math.round(Float.valueOf(validateOTPResponse.getData().getLoans().get(0).getRequestAmount())));
                        presenter.botMessage("Here is a summary based on your earlier selection. You can change the required loan amount, tenure and time of payment to see your EMI.",frameLayout,ChatPresenter.chatObjects.size()+1);
                        next=false;
                    }else {
                        emiRateFindeed.setText("\u20B9 "+String.valueOf(Math.round(valueOf(validateOTPResponse.getData().getLoans().get(0).getEmiRate())))+"/Month");
                        // eligibileLoanBar.setProgress(Math.round(Float.valueOf(validateOTPResponse.getData().getLoans().get(0).getRequestAmount())));
                        presenter.botMessage("Here is a summary based on your earlier selection. You can change the required loan amount, tenure and time of payment to see your EMI.",frameLayout,ChatPresenter.chatObjects.size()+1);
                        next=false;
                    }
                }

            }else if(isCreditStatusTrue) {
                ELIGIBLE_AMOUNT=validateOTPResponse.getData().getLoans().get(0).getMaxLoanEligible();
                seekBarUpperLimit.setText("\u20B9 "+String.valueOf(Math.round(ELIGIBLE_AMOUNT)));
                Log.i("MAX_ELIGIBLE",seekBarUpperLimit.getText().toString());
                selectedLoanAmount= (int) parseFloat(validateOTPResponse.getData().getLoans().get(0).getRequestAmount());
                if(selectedLoanAmount>50000)
                    eligibileLoanBar.setMax(50000);
                else
                    eligibileLoanBar.setMax(ELIGIBLE_AMOUNT);
                eligibileLoanBar.setProgress(valueOf(selectedLoanAmount));
                if(validateOTPResponse.getData().getLoans().get(0).getEmiRate()==null){
                    float rate=  getEMI(Math.round(valueOf(validateOTPResponse.getData().getLoans().get(0).getRequestAmount())), valueOf(validateOTPResponse.getData().getLoans().get(0).getTenure()))  ;
                    emiRateFindeed.setText("\u20B9 "+String.valueOf(rate)+"/Month");
                    //eligibileLoanBar.setProgress(Math.round(Float.valueOf(validateOTPResponse.getData().getLoans().get(0).getRequestAmount())));
                    presenter.botMessage("Here is a summary based on your earlier selection. You can change the required loan amount, tenure and time of payment to see your EMI.",frameLayout,ChatPresenter.chatObjects.size()+1);
                    next=false;
                }else {
                    emiRateFindeed.setText("\u20B9 "+String.valueOf(Math.round(valueOf(validateOTPResponse.getData().getLoans().get(0).getEmiRate())))+"/Month");
                    // eligibileLoanBar.setProgress(Math.round(Float.valueOf(validateOTPResponse.getData().getLoans().get(0).getRequestAmount())));
                    presenter.botMessage("Here is a summary based on your earlier selection. You can change the required loan amount, tenure and time of payment to see your EMI.",frameLayout,ChatPresenter.chatObjects.size()+1);
                    next=false;
                }
            }





            if((validateOTPResponse.getData().getIsSaveEmiSummary()==1 ||validateOTPResponse.getData().getIsSaveEmiSummary()==2 ||validateOTPResponse.getData().getIsSaveEmiSummary()==0 )&& next) {
                if (validateOTPResponse.getData().getIsSaveEmiSummary() == 1) {
                    isEmiSummarySave = true;
                    if (Float.valueOf(validateOTPResponse.getData().getLoans().get(0).getRequestAmount()) < 30000) {
                        fee = "600";
                    } else {

                        float lFee = (float) (Float.valueOf(validateOTPResponse.getData().getLoans().get(0).getRequestAmount()) * 0.02);
                        if (lFee > 1000)
                            fee = "1000";
                        else
                            fee = String.valueOf(Float.valueOf(lFee));
                    }
                    emiProcessingFee.setText(fee);

                    presenter.botMessage("Your Loan Eligibility Summary based on your application", AllAboutLoan, ChatPresenter.chatObjects.size() + 1);
                    presenter.botMessage("Estimated EMI: " + String.valueOf(Math.round(valueOf(validateOTPResponse.getData().getLoans().get(0).getEmiRate()))) + "/Month" +
                            "\n" + "Loan Amount: " + String.valueOf(Math.round(valueOf(validateOTPResponse.getData().getLoans().get(0).getRequestAmount()))) + "\n" +
                            "Frequency: Monthly" + "\n" + "Interest Rate: 2% Per Month", null, ChatPresenter.chatObjects.size() + 1);

                } else if (validateOTPResponse.getData().getIsSaveEmiSummary() == 2) {
                    if (Float.valueOf(validateOTPResponse.getData().getLoans().get(0).getRequestAmount()) < 30000) {
                        fee = "600";
                    } else {
                        float lFee = (float) (Float.valueOf(validateOTPResponse.getData().getLoans().get(0).getRequestAmount()) * 0.02);
                        if (lFee > 1000)
                            fee = "1000";
                        else
                            fee = String.valueOf(Float.valueOf(lFee));
                    }


                    emiProcessingFee.setText("\u20B9 " + fee);
                    emiLateFee.setText("1% per day on amount due");
                    estimatedEMI.setText("\u20B9 " + String.valueOf(Math.round(valueOf(validateOTPResponse.getData().getLoans().get(0).getEmiRate()))) + " Per month");
                    requiredLoanAmount.setText("\u20B9 " + String.valueOf(Math.round(valueOf(validateOTPResponse.getData().getLoans().get(0).getRequestAmount()))));
                    estimatedLoanTenure.setText(validateOTPResponse.getData().getLoans().get(0).getTenure() + "Months");
                    presenter.botMessage("Your Loan Eligibility Summary based on your application", AllAboutLoan, ChatPresenter.chatObjects.size() + 1);
                    next = false;
                } else {

                    if (Float.valueOf(validateOTPResponse.getData().getLoans().get(0).getRequestAmount()) < 30000) {
                        fee = "600";
                    } else {
                        float lFee = (float) (Float.valueOf(validateOTPResponse.getData().getLoans().get(0).getRequestAmount()) * 0.02);
                        if (lFee > 1000)
                            fee = "1000";
                        else
                            fee = String.valueOf(Float.valueOf(lFee));
                    }
                        emiProcessingFee.setText("\u20B9 " + fee);
                        emiLateFee.setText("1% per day on amount due");
                        estimatedEMI.setText("\u20B9 " + String.valueOf(Math.round(valueOf(validateOTPResponse.getData().getLoans().get(0).getEmiRate()))) + " Per month");
                        requiredLoanAmount.setText("\u20B9 " + String.valueOf(Math.round(valueOf(validateOTPResponse.getData().getLoans().get(0).getRequestAmount()))));
                        estimatedLoanTenure.setText(validateOTPResponse.getData().getLoans().get(0).getTenure() + "Months");
                        presenter.botMessage("Your Loan Eligibility Summary based on your application", AllAboutLoan, ChatPresenter.chatObjects.size() + 1);
                        next = false;
                    }


            }else if(isLoanSummarySaved) {

                if (Float.valueOf(validateOTPResponse.getData().getLoans().get(0).getRequestAmount()) < 30000) {
                    fee = "600";
                } else {
                    float lFee = (float) (Float.valueOf(validateOTPResponse.getData().getLoans().get(0).getRequestAmount()) * 0.02);
                    if (lFee > 1000)
                        fee = "1000";
                    else
                        fee = String.valueOf(Float.valueOf(lFee));
                }
                emiProcessingFee.setText("\u20B9 " + fee);
                emiLateFee.setText("1% per day on amount due");
                emiRate=String.valueOf(Math.round(valueOf(validateOTPResponse.getData().getLoans().get(0).getEmiRate())));
                estimatedEMI.setText("\u20B9 "+String.valueOf(Math.round(valueOf(validateOTPResponse.getData().getLoans().get(0).getEmiRate())))+" Per month");
                requiredLoanAmount.setText("\u20B9 "+String.valueOf(Math.round(valueOf(validateOTPResponse.getData().getLoans().get(0).getRequestAmount()))));
                estimatedLoanTenure.setText(validateOTPResponse.getData().getLoans().get(0).getTenure()+"Months");
                presenter.botMessage("Your Loan Eligibility Summary based on your application", AllAboutLoan, ChatPresenter.chatObjects.size() + 1);
                next= false;
            }





            if((validateOTPResponse.getData().getIsSaveLoanAggrement()==0 ||validateOTPResponse.getData().getIsSaveLoanAggrement()==1 ||validateOTPResponse.getData().getIsSaveLoanAggrement()==2)&&next ){

                if(validateOTPResponse.getData().getIsSaveLoanAggrement()==1){
                    isLoanAgreementAccepted=true;
                    presenter.botMessage("Thank you!",null,ChatPresenter.chatObjects.size()+1);
                    presenter.botMessage("Loan Amount: \u20B9 "+Math.round(valueOf(validateOTPResponse.getData().getLoans().get(0).getRequestAmount()))+"\n"+
                            "Loan Tenure: "+validateOTPResponse.getData().getLoans().get(0).getTenure()+"Months"+"\n"+"Payment Frequency: Monthly"+"\n"
                            +"Interest Rate: 2% Per Month"+"\n"+"Payment Due: 10th of every month"+"\n"+"Late fee: 1% per day on amount due "+"\n"+
                            "Processing Fee: Rs "+fee,null,ChatPresenter.chatObjects.size()+1);

                }else if(validateOTPResponse.getData().getIsSaveLoanAggrement()==2) {
                    if(CREDIT_RULES_STATUS==1){
                        if(isVoucher1Verified && isVoucher2Verified){
                            if(LOAN_TYPE==1){
                                if(SCHOOL_CHECK==1){
                                    presenter.botMessage("Congratulations, your loan request has been approved.",viewLoanDetails,ChatPresenter.chatObjects.size()+1);
                                    next =false;
                                }else {
                                    presenter.botMessage("Please wait while we confirm verification of the selected school",null,ChatPresenter.chatObjects.size()+1);
                                    next=false;
                                }
                            }else {
                                if(EMPLOYER_CHECK==1){
                                    presenter.botMessage("Congratulations, your loan request has been approved.",viewLoanDetails,ChatPresenter.chatObjects.size()+1);
                                    next =false;
                                }else {
                                    presenter.botMessage("Please wait while we confirm verification of the selected employer",null,ChatPresenter.chatObjects.size()+1);
                                    next=false;
                                }
                            }

                        }
                        else {
                            presenter.botMessage("Please wait while we confirm the vouching process of the above selected voucher",null,ChatPresenter.chatObjects.size()+1);
                            next=false;
                        }
                    }else if(CREDIT_RULES_STATUS==3){
                        presenter.botMessage("Thank you for completing the verification process. We will update you on your loan status shortly.",null,ChatPresenter.chatObjects.size()+1);
                        next=false;
                    }


                }else {
                    if(CREDIT_RULES_STATUS==1){
                        if(isVoucher1Verified && isVoucher2Verified){
                            if(LOAN_TYPE==1){
                                if(SCHOOL_CHECK==1){
                                    presenter.botMessage("Congratulations, your loan request has been approved.",viewLoanDetails,ChatPresenter.chatObjects.size()+1);
                                    next =false;
                                }else {
                                    presenter.botMessage("Please wait while we confirm verification of the selected school",null,ChatPresenter.chatObjects.size()+1);
                                    next=false;
                                }
                            }else {
                                if(EMPLOYER_CHECK==1){
                                    presenter.botMessage("Congratulations, your loan request has been approved.",viewLoanDetails,ChatPresenter.chatObjects.size()+1);
                                    next =false;
                                }else {
                                    presenter.botMessage("Please wait while we confirm verification of the selected employer",null,ChatPresenter.chatObjects.size()+1);
                                    next=false;
                                }
                            }

                        }
                        else {
                            presenter.botMessage("Please wait while we confirm the vouching process of the above selected voucher",null,ChatPresenter.chatObjects.size()+1);
                            next=false;
                        }
                    }else if(CREDIT_RULES_STATUS==3){
                        presenter.botMessage("Thank you for completing the verification process. We will update you on your loan status shortly.",null,ChatPresenter.chatObjects.size()+1);
                        next=false;
                    }
                }

            }else if(isEmiSummarySave){
//                presenter.botMessage("Thank you!",null,ChatPresenter.chatObjects.size()+1);
//                ChatActivity.presenter.botMessage("Please proceed to the next step. Let’s complete a video call first.",ChatActivity.videoRecording,ChatPresenter.chatObjects.size()+1);
//                next =false;
                presenter.botMessage("Are you using the same mobile no. "+validateOTPResponse.getData().getPhoneNumber()+" on Whatsapp?",whatsApp,ChatPresenter.chatObjects.size()+1);
                next=false;

            }

//            if((validateOTPResponse.getData().getIsFaceAuth()==0 ||validateOTPResponse.getData().getIsFaceAuth()==1 ||validateOTPResponse.getData().getIsFaceAuth()==2)&&next){
//                if(validateOTPResponse.getData().getIsFaceAuth()==1){
//                    isFaceAuthSave=true;
//                    presenter.botMessage("Please proceed to the next step. Let’s complete a video call first.",ChatActivity.videoRecording,ChatPresenter.chatObjects.size()+1);
//                    presenter.botMessage("Thank you, for completeing the video recording process",null,ChatPresenter.chatObjects.size()+1);
//                }else {
//                    presenter.botMessage("Please proceed to the next step. Let’s complete a video call first.",ChatActivity.videoRecording,ChatPresenter.chatObjects.size()+1);
//                    next=false;
//                }
//            }else if(isLoanAgreementAccepted){
//                presenter.botMessage("Please proceed to the next step. Let’s complete a video call first.",ChatActivity.videoRecording,ChatPresenter.chatObjects.size()+1);
//                next=false;
//            }





            if(validateOTPResponse.getData().getWhatsappNumber()!=null && next){
                isWhatsAppSavd=true;
                presenter.botMessage("Are you using the same mobile no. "+validateOTPResponse.getData().getPhoneNumber()+" on Whatsapp?",whatsApp,ChatPresenter.chatObjects.size()+1);
                if(validateOTPResponse.getData().getWhatsappNumber().equals(validateOTPResponse.getData().getPhoneNumber())){
                    presenter.onEditTextActionDone("Yes, i'm using the same number",null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                    CUSTOMER_DETAILS= (Data) getJson(validateOTPResponse.getData(),"same_number").clone();

                }else {
                    presenter.onEditTextActionDone("No, i'm using a different number",null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);

                    presenter.botMessage("Please enter your whatsapp number",whatsAppInput,ChatPresenter.chatObjects.size()+1);
                    presenter.onEditTextActionDone(validateOTPResponse.getData().getWhatsappNumber(),null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                    CUSTOMER_DETAILS= (Data) getJson(validateOTPResponse.getData(),"same_number").clone();


                }

//                presenter.onEditTextActionDone("No, i'm using a different number",null,ChatPresenter.chatObjects.size()+1);
//                presenter.botMessage("Please enter your whatsapp number",whatsAppInput,ChatPresenter.chatObjects.size()+1);

            }else if(isLoanAgreementAccepted){

                presenter.botMessage("Are you using the same mobile no. "+validateOTPResponse.getData().getPhoneNumber()+" on Whatsapp?",whatsApp,ChatPresenter.chatObjects.size()+1);

                next=false;
            }




            if(validateOTPResponse.getData().getEmailAddress()!=null && next){
                presenter.botMessage("Please share your Email ID",eduEmailLayout,ChatPresenter.chatObjects.size()+1);
                presenter.onEditTextActionDone(validateOTPResponse.getData().getEmailAddress(),null,ChatPresenter.chatObjects.size()+1,CUSTOMER_DETAILS);
                CUSTOMER_DETAILS= (Data) getJson(validateOTPResponse.getData(),"share_email").clone();

                presenter.botMessage("Congratulations !",sendingNotification,ChatPresenter.chatObjects.size()+1);
                presenter.botMessage("Our team will reach out to collect the KYC documents from you during business hours. Post collection, your money will be disbursed",null,ChatPresenter.chatObjects.size()+1);

            }else if(isWhatsAppSavd){
                presenter.botMessage("Please share your Email ID",eduEmailLayout,ChatPresenter.chatObjects.size()+1);
                next=false;
            }







        }catch (Exception e){
            Log.i("PERSISTENCE",e.toString());
            e.printStackTrace();
        }finally {
            if( ApplicationData.loadChat!=null)
                ApplicationData.loadChat.dismiss();
        }


        docStatus.clear();
        Log.i("docss",new Gson().toJson(CUSTOMER_DETAILS));

    }

    private boolean isVoucherVerified(Voucher voucher) {

        if(voucher.getIsVoucherApproved()==1){
            return true;
        }
        return false;

    }


    private Data getJson(Data customerDetails, String key) throws CloneNotSupportedException {


        switch(key){
            case "education":
                Data data;
                CUSTOMER_DETAILS_PERSISTANCE=new Data();
                CUSTOMER_DETAILS_PERSISTANCE.setCustomerId(customerDetails.getCustomerId());
                CUSTOMER_DETAILS_PERSISTANCE.setPhoneNumber(customerDetails.getPhoneNumber());
                loanTypeClass.setLoanTypeName(customerDetails.getLoans().get(0).getLoanType().getLoanTypeName());
                loanTypeClass.setLoanTypeId(customerDetails.getLoans().get(0).getLoanType().getLoanTypeId());
                loanClass.setLoanType(loanTypeClass);
                loanClass.setApplicationNumber(customerDetails.getLoans().get(0).getApplicationNumber());
                loanClass.setCustomerApplicationId(customerDetails.getLoans().get(0).getCustomerApplicationId());


                for(int i=0;i<customerDetails.getLoans().get(0).getErrorList().size();i++){
//    errorList.setErrorMessage(customerDetails.getLoans().get(0).getErrorList().get(i).getErrorMessage());
//    errorList.setErrorName(customerDetails.getLoans().get(0).getErrorList().get(i).getErrorName());
//    errorList.setErrorStatus(customerDetails.getLoans().get(0).getErrorList().get(i).getErrorStatus());
                    errorList= (ErrorList) customerDetails.getLoans().get(0).getErrorList().get(i).clone(); //clone method is used to overcome the problem of latest object overriding the previous objects in arraylist


                    errorArray.add(errorList);
                }

                loanClass.setErrorList(errorArray);
                loanClass.setLoanFlags(customerDetails.getLoans().get(0).getLoanFlags());

                loansArray.add(loanClass);


                CUSTOMER_DETAILS_PERSISTANCE.setLoans(loansArray);


                UserAddress userAddress=new UserAddress();
                CUSTOMER_DETAILS_PERSISTANCE.setChildren(childArray);
                CUSTOMER_DETAILS_PERSISTANCE.setAddress(userAddress);
                CUSTOMER_DETAILS_PERSISTANCE.setDocuments(documentArray);
                CUSTOMER_DETAILS_PERSISTANCE.setVouchers(voucherArray);
                CUSTOMER_DETAILS_PERSISTANCE.setLanguage(customerDetails.getLanguage());



                return CUSTOMER_DETAILS_PERSISTANCE;



            case "which_school":
                CUSTOMER_DETAILS_PERSISTANCE.setSchool(customerDetails.getSchool());

                return CUSTOMER_DETAILS_PERSISTANCE;

            case "num_child":

                for(int i=0;i<customerDetails.getChildren().size();i++){
//                    children.setCustomerDependentId(customerDetails.getChildren().get(i).getCustomerDependentId());
//                    children.setFirstName(customerDetails.getChildren().get(i).getFirstName());
//                    children.setLastName(customerDetails.getChildren().get(i).getLastName());
//                    children.setInClass(customerDetails.getChildren().get(i).getInClass());

                    children= (Children) customerDetails.getChildren().get(i).clone();
                    schoolData= (SchoolData) customerDetails.getChildren().get(i).getSchool().clone();
                    classInformation= (ClassInformation) customerDetails.getChildren().get(i).getInClass().clone();
                    children.setSchool(schoolData);
                    children.setInClass(classInformation);

                    childArray.add(children);
                }

                CUSTOMER_DETAILS_PERSISTANCE.setChildren(childArray);
                CUSTOMER_DETAILS_PERSISTANCE.setNumberOfChildren(customerDetails.getNumberOfChildren());


                return CUSTOMER_DETAILS_PERSISTANCE;



            case "loan_amount":


                loanClass2= (Loan)loanClass.clone();
                loanClass2.setRequestAmount(customerDetails.getLoans().get(0).getRequestAmount());
                loanClass.setRequestAmount(customerDetails.getLoans().get(0).getRequestAmount());

                loansArray.set(0,loanClass2);

                CUSTOMER_DETAILS_PERSISTANCE.setLoans(loansArray);

                return CUSTOMER_DETAILS_PERSISTANCE;

            case "tenure":

//                 Loan loanClass3=new Loan();
//               // ArrayList < Loan > loansArray2 = new ArrayList<> ();
//                loanClass3= (Loan)loanClass.clone();
                loanClass.setTenure(customerDetails.getLoans().get(0).getTenure());

                loansArray.set(0,loanClass);
                CUSTOMER_DETAILS_PERSISTANCE.setLoans(loansArray);


                return CUSTOMER_DETAILS_PERSISTANCE;
//            case "loan_amount":
//
//
//               loansArray2 = new ArrayList<> ();
//               loanClass3=new Loan();
//
//                loanClass3= (Loan)loanClass.clone();
//                loanClass3.setRequestAmount(customerDetails.getLoans().get(0).getRequestAmount());
//          loansArray.remove(0);
//        //        CUSTOMER_DETAILS_PERSISTANCE.loans.remove(0);
//                loansArray2.add(loanClass3);
//                CUSTOMER_DETAILS_PERSISTANCE.setLoans(loansArray2);
//
//                return CUSTOMER_DETAILS_PERSISTANCE;
//
//            case "tenure":
//                ArrayList < Loan > loansArray3 = new ArrayList<> ();
//                 Loan loanClass4=new Loan();
////               // ArrayList < Loan > loansArray2 = new ArrayList<> ();
//
//
//                loanClass4= (Loan)loanClass3.clone();
//
//                loanClass4.setTenure(customerDetails.getLoans().get(0).getTenure());
//                loanClass4.setRequestAmount(customerDetails.getLoans().get(0).getRequestAmount());
//
//                loansArray3.add(loanClass4);
//
//                CUSTOMER_DETAILS_PERSISTANCE.setLoans(loansArray3);
//
//                loanClass.setRequestAmount(customerDetails.getLoans().get(0).getRequestAmount());
//                return CUSTOMER_DETAILS_PERSISTANCE;

            case "employment_type":
                CUSTOMER_DETAILS_PERSISTANCE.setEmploymentType(customerDetails.getEmploymentType());
                return CUSTOMER_DETAILS_PERSISTANCE;
            case "bussiness_name":
                CUSTOMER_DETAILS_PERSISTANCE.setBusinessName(customerDetails.getBusinessName());
                return CUSTOMER_DETAILS_PERSISTANCE;
            case "bussiness_type":
                CUSTOMER_DETAILS_PERSISTANCE.setBusinessType(customerDetails.getBusinessType());
                return CUSTOMER_DETAILS_PERSISTANCE;
            case "salary":
                CUSTOMER_DETAILS_PERSISTANCE.setSalaryRange(customerDetails.getSalaryRange());
                return CUSTOMER_DETAILS_PERSISTANCE;

            case "marital_status":
                CUSTOMER_DETAILS_PERSISTANCE.setMaritalStatus(customerDetails.getMaritalStatus());
                return CUSTOMER_DETAILS_PERSISTANCE;
            case "employee_id":
                CUSTOMER_DETAILS_PERSISTANCE.setEmployeeId(customerDetails.getEmployeeId());
                return CUSTOMER_DETAILS_PERSISTANCE;
            case "email_id":
                CUSTOMER_DETAILS_PERSISTANCE.setEmailAddress(customerDetails.getEmailAddress());
                return CUSTOMER_DETAILS_PERSISTANCE;
            case "personal_name":
                CUSTOMER_DETAILS_PERSISTANCE.setEmployerName(customerDetails.getEmployerName());
                CUSTOMER_DETAILS_PERSISTANCE.setFirstName(customerDetails.getFirstName());
                CUSTOMER_DETAILS_PERSISTANCE.setLastName(customerDetails.getLastName());
                return CUSTOMER_DETAILS_PERSISTANCE;
            case "working_years":
                CUSTOMER_DETAILS_PERSISTANCE.setWorkingYears(customerDetails.getWorkingYears());
                return CUSTOMER_DETAILS_PERSISTANCE;

            case "do_have_kids":
                CUSTOMER_DETAILS_PERSISTANCE.setIsSaveHaveKids(customerDetails.getIsSaveHaveKids());
                return CUSTOMER_DETAILS_PERSISTANCE;

            case "num_kids":
                CUSTOMER_DETAILS_PERSISTANCE.setNumberOfChildren(customerDetails.getNumberOfChildren());
                return CUSTOMER_DETAILS_PERSISTANCE;

            case "pan_card":
                DocumentData documentPan = new DocumentData();
                for(int i=0;i<customerDetails.getDocuments().size();i++){
                    if(customerDetails.getDocuments().get(i).getDocumentType().equals("PAN")){
                        documentPan.setDocumentType(customerDetails.getDocuments().get(i).getDocumentType());//.get(i).getClass().clone();
                        documentPan.setAddress(customerDetails.getDocuments().get(i).getAddress());
                        documentPan.setId(customerDetails.getDocuments().get(i).getId());
                        documentPan.setDateOfBirth(customerDetails.getDocuments().get(i).getDateOfBirth());
                        documentPan.setDocSide(customerDetails.getDocuments().get(i).getDocSide());
                        documentPan.setBackImage(customerDetails.getDocuments().get(i).getBackImage());
                        documentPan.setFrontImage(customerDetails.getDocuments().get(i).getFrontImage());
                        documentPan.setFrontImageURL(customerDetails.getDocuments().get(i).getFrontImageURL());
                        documentPan.setBackImageURL(customerDetails.getDocuments().get(i).getBackImageURL());

                        documentPan.setFirstName(customerDetails.getDocuments().get(i).getFirstName());
                        documentPan.setLastName(customerDetails.getDocuments().get(i).getLastName());
                        documentPan.setGender(customerDetails.getDocuments().get(i).getGender());
                        documentPan.setQuality(customerDetails.getDocuments().get(i).getQuality());
                        documentPan.setVerified(customerDetails.getDocuments().get(i).isVerified());

                        docArray.add(documentPan);
                        ArrayList<DocumentData> docArrayP=new ArrayList<>();
                        docArrayP= (ArrayList<DocumentData>) docArray.clone();
                        CUSTOMER_DETAILS_PERSISTANCE.setDocuments(docArrayP);
                        break;

                    }
                }




                return CUSTOMER_DETAILS_PERSISTANCE;
            case "aadhaar_card":
                DocumentData documentAdhaar = new DocumentData();
                for(int i=0;i<customerDetails.getDocuments().size();i++){
                    if(customerDetails.getDocuments().get(i).getDocumentType().equals("AADHAAR")){
                        documentAdhaar.setDocumentType(customerDetails.getDocuments().get(i).getDocumentType());//.get(i).getClass().clone();
                        documentAdhaar.setAddress(customerDetails.getDocuments().get(i).getAddress());
                        documentAdhaar.setId(customerDetails.getDocuments().get(i).getId());
                        documentAdhaar.setDateOfBirth(customerDetails.getDocuments().get(i).getDateOfBirth());
                        documentAdhaar.setDocSide(customerDetails.getDocuments().get(i).getDocSide());
                        documentAdhaar.setBackImage(customerDetails.getDocuments().get(i).getBackImage());
                        documentAdhaar.setFrontImage(customerDetails.getDocuments().get(i).getFrontImage());
                        documentAdhaar.setFrontImageURL(customerDetails.getDocuments().get(i).getFrontImageURL());
                        documentAdhaar.setBackImageURL(customerDetails.getDocuments().get(i).getBackImageURL());

                        documentAdhaar.setFirstName(customerDetails.getDocuments().get(i).getFirstName());
                        documentAdhaar.setLastName(customerDetails.getDocuments().get(i).getLastName());
                        documentAdhaar.setGender(customerDetails.getDocuments().get(i).getGender());
                        documentAdhaar.setQuality(customerDetails.getDocuments().get(i).getQuality());
                        documentAdhaar.setVerified(customerDetails.getDocuments().get(i).isVerified());

                        docArray.add(documentAdhaar);
                        ArrayList<DocumentData> docArrayA=new ArrayList<>(docArray);
                        CUSTOMER_DETAILS_PERSISTANCE.setDocuments(docArrayA);
                        break;

                    }
                }

//                CUSTOMER_DETAILS_PERSISTANCE.setDocuments(docArray);

                return CUSTOMER_DETAILS_PERSISTANCE;


            case "voter_id":
                DocumentData documentVoter = new DocumentData();
                for(int i=0;i<customerDetails.getDocuments().size();i++){
                    if(customerDetails.getDocuments().get(i).getDocumentType().equals("VOTER_ID")){
                        documentVoter.setDocumentType(customerDetails.getDocuments().get(i).getDocumentType());//.get(i).getClass().clone();
                        documentVoter.setAddress(customerDetails.getDocuments().get(i).getAddress());
                        documentVoter.setId(customerDetails.getDocuments().get(i).getId());
                        documentVoter.setDateOfBirth(customerDetails.getDocuments().get(i).getDateOfBirth());
                        documentVoter.setDocSide(customerDetails.getDocuments().get(i).getDocSide());
                        documentVoter.setBackImage(customerDetails.getDocuments().get(i).getBackImage());
                        documentVoter.setFrontImage(customerDetails.getDocuments().get(i).getFrontImage());
                        documentVoter.setFrontImageURL(customerDetails.getDocuments().get(i).getFrontImageURL());
                        documentVoter.setBackImageURL(customerDetails.getDocuments().get(i).getBackImageURL());

                        documentVoter.setFirstName(customerDetails.getDocuments().get(i).getFirstName());
                        documentVoter.setLastName(customerDetails.getDocuments().get(i).getLastName());
                        documentVoter.setGender(customerDetails.getDocuments().get(i).getGender());
                        documentVoter.setQuality(customerDetails.getDocuments().get(i).getQuality());
                        documentVoter.setVerified(customerDetails.getDocuments().get(i).isVerified());

                        docArray.add(documentVoter);
                        ArrayList<DocumentData> docArrayV=new ArrayList<>();
                        docArrayV= (ArrayList<DocumentData>) docArray.clone();

                        CUSTOMER_DETAILS_PERSISTANCE.setDocuments(docArrayV);
                        break;

                    }
                }

//                CUSTOMER_DETAILS_PERSISTANCE.setDocuments(docArray);

                return CUSTOMER_DETAILS_PERSISTANCE;

            case "same_address":
                CUSTOMER_DETAILS_PERSISTANCE.setIsCurrentPermanentAddressSame(customerDetails.getIsCurrentPermanentAddressSame());
                return CUSTOMER_DETAILS_PERSISTANCE;

            case "gas_bill":
                DocumentData documentGasBill = new DocumentData();
                for(int i=0;i<customerDetails.getDocuments().size();i++){
                    if(customerDetails.getDocuments().get(i).getDocumentType().equals("GAS_BILL")){
                        documentGasBill.setDocumentType(customerDetails.getDocuments().get(i).getDocumentType());//.get(i).getClass().clone();
                        documentGasBill.setAddress(customerDetails.getDocuments().get(i).getAddress());
                        documentGasBill.setId(customerDetails.getDocuments().get(i).getId());
                        documentGasBill.setDateOfBirth(customerDetails.getDocuments().get(i).getDateOfBirth());
                        documentGasBill.setDocSide(customerDetails.getDocuments().get(i).getDocSide());
                        documentGasBill.setBackImage(customerDetails.getDocuments().get(i).getBackImage());
                        documentGasBill.setFrontImage(customerDetails.getDocuments().get(i).getFrontImage());
                        documentGasBill.setFrontImageURL(customerDetails.getDocuments().get(i).getFrontImageURL());
                        documentGasBill.setBackImageURL(customerDetails.getDocuments().get(i).getBackImageURL());

                        documentGasBill.setFirstName(customerDetails.getDocuments().get(i).getFirstName());
                        documentGasBill.setLastName(customerDetails.getDocuments().get(i).getLastName());
                        documentGasBill.setGender(customerDetails.getDocuments().get(i).getGender());
                        documentGasBill.setQuality(customerDetails.getDocuments().get(i).getQuality());
                        documentGasBill.setVerified(customerDetails.getDocuments().get(i).isVerified());
                        docArray.add(documentGasBill);
                        ArrayList<DocumentData> docArrayG=new ArrayList<>();
                        docArrayG= (ArrayList<DocumentData>) docArray.clone();
                        CUSTOMER_DETAILS_PERSISTANCE.setDocuments(docArrayG);
                        break;


                    }
                }

//                CUSTOMER_DETAILS_PERSISTANCE.setDocuments(docArray);

                return CUSTOMER_DETAILS_PERSISTANCE;

            case "electricity_bill":
                DocumentData documentElecBill = new DocumentData();
                for(int i=0;i<customerDetails.getDocuments().size();i++){
                    if(customerDetails.getDocuments().get(i).getDocumentType().equals("ELECTRICITY_BILL")){
                        documentElecBill.setDocumentType(customerDetails.getDocuments().get(i).getDocumentType());//.get(i).getClass().clone();
                        documentElecBill.setAddress(customerDetails.getDocuments().get(i).getAddress());
                        documentElecBill.setId(customerDetails.getDocuments().get(i).getId());
                        documentElecBill.setDateOfBirth(customerDetails.getDocuments().get(i).getDateOfBirth());
                        documentElecBill.setDocSide(customerDetails.getDocuments().get(i).getDocSide());
                        documentElecBill.setBackImage(customerDetails.getDocuments().get(i).getBackImage());
                        documentElecBill.setFrontImage(customerDetails.getDocuments().get(i).getFrontImage());
                        documentElecBill.setFrontImageURL(customerDetails.getDocuments().get(i).getFrontImageURL());
                        documentElecBill.setBackImageURL(customerDetails.getDocuments().get(i).getBackImageURL());

                        documentElecBill.setFirstName(customerDetails.getDocuments().get(i).getFirstName());
                        documentElecBill.setLastName(customerDetails.getDocuments().get(i).getLastName());
                        documentElecBill.setGender(customerDetails.getDocuments().get(i).getGender());
                        documentElecBill.setQuality(customerDetails.getDocuments().get(i).getQuality());
                        documentElecBill.setVerified(customerDetails.getDocuments().get(i).isVerified());

                        docArray.add(documentElecBill);
                        ArrayList<DocumentData> docArrayE=new ArrayList<>(docArray);
                        CUSTOMER_DETAILS_PERSISTANCE.setDocuments(docArrayE);
                        break;

                    }
                }


                return CUSTOMER_DETAILS_PERSISTANCE;

            case "same_number":
                CUSTOMER_DETAILS_PERSISTANCE.setWhatsappNumber(customerDetails.getWhatsappNumber());
                return CUSTOMER_DETAILS_PERSISTANCE;





            default:
                return CUSTOMER_DETAILS_PERSISTANCE;

        }

    }
    private void setFlags(ArrayList<ErrorList> errorList) {

        for(int i=0;i<errorList.size();i++){
            switch (errorList.get(i).getErrorName()){

                case "PAN_UPLOAD_NUMBER":
                    PAN_UPLOAD_NUMBER=errorList.get(i).getErrorStatus();
                    break;
                case "CUSTOMER_AGE_RESTRICTION":
                    CUSTOMER_AGE_RESTRICTION=errorList.get(i).getErrorStatus();
                    break;
                case "PINCODE_VERIFICATION":
                    PIN_CODE_VERIFICATION=errorList.get(i).getErrorStatus();
                    break;
                case "BUREAU_PERMISSION_DENIED"  :
                    BUREAU_PERMISSION_DENIED=errorList.get(i).getErrorStatus();
                    break;
                case "CCS_PERMISSION_DENIED" :
                    CCS_PERMISSION_DENIED=errorList.get(i).getErrorStatus();
                    break;
                case "GOVT_VERIFICATION_PAN" :
                    GOVT_VERIFICATION_PAN=errorList.get(i).getErrorStatus();
                    break;
                case "STABILITY_OFFICE_PREMISES"  :
                    STABILITY_OFFICE_PREMISES=errorList.get(i).getErrorStatus();
                    break;
                case "SCHOOL_CHECK" :
                    SCHOOL_CHECK=errorList.get(i).getErrorStatus();
                    break;
                case "EMPLOYER_CHECK":
                    EMPLOYER_CHECK=errorList.get(i).getErrorStatus();
                    break;
                case "PAN_AADHAR_MATCH"  :
                    PAN_AADHAR_MATCH=errorList.get(i).getErrorStatus();
                    break;
                case "MINIMUM_SALARY":
                    MINIMUM_SALARY=errorList.get(i).getErrorStatus();
                    break;
                case "CREDIT_RULES_STATUS":
                    CREDIT_RULES_STATUS=errorList.get(i).getErrorStatus();
                    break;
                case "PAN_OCR_FAILED":
                    PAN_OCR_FAILED=errorList.get(i).getErrorStatus();
                    break;
                case "IS_PAN_EDITED":
                    IS_PAN_EDITED=errorList.get(i).getErrorStatus();
                    break;
                case "VOTER_OCR_FAILED":
                    VOTER_OCR_FAILED=errorList.get(i).getErrorStatus();
                    break;
                case "IS_VOTER_EDITED":
                    IS_VOTER_EDITED=errorList.get(i).getErrorStatus();
                    break;
                case "GOVT_VERIFICATION_VOTER":
                    GOVT_VERIFICATION_VOTER=errorList.get(i).getErrorStatus();
                    break;
                default:
                    IS_VOTER_EDITED=errorList.get(i).getErrorStatus();
                    break;



            }
        }


    }


    private File createImageFile() throws IOException {
        // String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        //  String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                "pic",
                ".jpg",
                storageDir
        );

        currentPhotoPath = image;
        return image;
    }

    public void childClick(View v, final int noOfChild, final TextView name, final TextView sname,  String className){

        firstname2.setText(name.getText().toString());
        surname2.setText(sname.getText().toString());

        if(noOfChild==1){

            if(TextUtils.isEmpty(name.getText())){
                Toast.makeText(getApplicationContext(), "Name can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }

            else if(selectChildClass==null || selectChildClass.getText().toString().equals("")){
                Toast.makeText(getApplicationContext(), "Please enter child's class name",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }

            else{
                firstname2.setText(name.getText().toString());
                surname2.setText(sname.getText().toString());
                new SaveChildDetails(NUMBER_OF_CHILDREN,firstname2.getText().toString(),surname2.getText().toString()).execute();
            }


        }
        else if(noOfChild==2){
            if(TextUtils.isEmpty(name.getText())){
                Toast.makeText(getApplicationContext(), "Name can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(selectChildClass==null ||selectChildClass.getText().toString().equals("")){
                Toast.makeText(getApplicationContext(), "Class can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else {

                firstname2.setText(name.getText().toString());
                surname2.setText(sname.getText().toString());
                new SaveChildDetails(NUMBER_OF_CHILDREN,firstname2.getText().toString(),surname2.getText().toString()).execute();
                addChild.setOnClickListener(view -> secondChild(noOfChild));
            }

        }
        else if(noOfChild==3){
            if(TextUtils.isEmpty(name.getText())){
                Toast.makeText(getApplicationContext(), "Name can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(selectChildClass==null || selectChildClass.getText().toString().equals("")){
                Toast.makeText(getApplicationContext(), "Class can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else {
                firstname2.setText(name.getText().toString());
                surname2.setText(sname.getText().toString());
                new SaveChildDetails(NUMBER_OF_CHILDREN,firstname2.getText().toString(),surname2.getText().toString()).execute();
                addChild.setOnClickListener(view -> secondChild(noOfChild));
            }

        }

        else if(noOfChild==4){
            if(TextUtils.isEmpty(name.getText())){
                Toast.makeText(getApplicationContext(), "Name can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(selectChildClass==null || selectChildClass.getText().toString().equals("")){
                Toast.makeText(getApplicationContext(), "Class can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else {
                firstname2.setText(name.getText().toString());
                surname2.setText(sname.getText().toString());
                new SaveChildDetails(NUMBER_OF_CHILDREN,firstname2.getText().toString(),surname2.getText().toString()).execute();
                addChild.setOnClickListener(view -> secondChild(noOfChild));
            }

        }
        else {
            if(TextUtils.isEmpty(name.getText())){
                Toast.makeText(getApplicationContext(), "Please enter child name",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(selectChildClass==null || selectChildClass.getText().toString().equals("")){
                Toast.makeText(getApplicationContext(), "Class can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else {
                firstname2.setText(name.getText().toString());
                surname2.setText(sname.getText().toString());
                new SaveChildDetails(NUMBER_OF_CHILDREN,firstname2.getText().toString(),surname2.getText().toString()).execute();
                addChild.setOnClickListener(view -> secondChild(noOfChild));
            }
        }
    }




    private void secondChild(final int noOfChild) {
        if(noOfChild==2) {
            if(TextUtils.isEmpty(firstname2.getText())){
                Toast.makeText(getApplicationContext(), "Please enter child name",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(selectChildClass==null || selectChildClass.getText().toString().equals("")){
                Toast.makeText(getApplicationContext(), "Class can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else{
                new SaveChildDetails(NUMBER_OF_CHILDREN,firstname2.getText().toString(),surname2.getText().toString()).execute();

            }

        }
        else if(noOfChild==3){
            if(TextUtils.isEmpty(firstname2.getText())){
                Toast.makeText(getApplicationContext(), "Please enter child name",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(selectChildClass==null|| selectChildClass.getText().toString().equals("")){
                Toast.makeText(getApplicationContext(), "Class can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else{
                new SaveChildDetails(NUMBER_OF_CHILDREN,firstname2.getText().toString(),surname2.getText().toString()).execute();
                addChild.setOnClickListener(view -> thirdChild(noOfChild));
            }

        }
        else if(noOfChild==4){
            if(TextUtils.isEmpty(firstname2.getText())){
                Toast.makeText(getApplicationContext(), "Please enter child name",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(selectChildClass==null||selectChildClass.getText().toString().equals("")){
                Toast.makeText(getApplicationContext(), "Class can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else{
                new SaveChildDetails(NUMBER_OF_CHILDREN,firstname2.getText().toString(),surname2.getText().toString()).execute();
                addChild.setOnClickListener(view -> { thirdChild(noOfChild); });
            }

        }
        else {
            if(TextUtils.isEmpty(firstname2.getText())){
                Toast.makeText(getApplicationContext(), "Please enter child name",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(selectChildClass==null || selectChildClass.getText().toString().equals("")){
                Toast.makeText(getApplicationContext(), "Class can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else{
                new SaveChildDetails(NUMBER_OF_CHILDREN,firstname2.getText().toString(),surname2.getText().toString()).execute();
                addChild.setOnClickListener(view -> { thirdChild(noOfChild); });
            }

        }
    }

    private void thirdChild(final int noOfChild) {
        if(noOfChild==3){

            if(TextUtils.isEmpty(firstname2.getText())){
                Toast.makeText(getApplicationContext(), "Please enter child name",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(selectChildClass==null || selectChildClass.getText().toString().equals("")){
                Toast.makeText(getApplicationContext(), "Class can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else{
                new SaveChildDetails(NUMBER_OF_CHILDREN,firstname2.getText().toString(),surname2.getText().toString()).execute();
            }

        }
        else if(noOfChild==4){
            if(TextUtils.isEmpty(firstname2.getText())){
                Toast.makeText(getApplicationContext(), "Please enter child name",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(selectChildClass==null || selectChildClass.getText().toString().equals("")){
                Toast.makeText(getApplicationContext(), "please enter child's class",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else {
                new SaveChildDetails(NUMBER_OF_CHILDREN,firstname2.getText().toString(),surname2.getText().toString()).execute();
                addChild.setOnClickListener(view -> fourthChild(noOfChild));
            }

        }
        else {
            if(TextUtils.isEmpty(firstname2.getText())){
                Toast.makeText(getApplicationContext(), "Name can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(selectChildClass==null || selectChildClass.getText().toString().equals("")){
                Toast.makeText(getApplicationContext(), "Class can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else {
                new SaveChildDetails(NUMBER_OF_CHILDREN,firstname2.getText().toString(),surname2.getText().toString()).execute();
                addChild.setOnClickListener(view -> fourthChild(noOfChild));
            }
        }
    }

    public void fourthChild(final int noOfChild){

        if(noOfChild==4){
            if(TextUtils.isEmpty(firstname2.getText())){
                Toast.makeText(getApplicationContext(), "Name can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(selectChildClass==null || selectChildClass.getText().toString().equals("")){
                Toast.makeText(getApplicationContext(), "Class can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else {
                new SaveChildDetails(NUMBER_OF_CHILDREN,firstname2.getText().toString(),surname2.getText().toString()).execute();
            }
        }else {
            if(TextUtils.isEmpty(firstname2.getText())){
                Toast.makeText(getApplicationContext(), "Name can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else if(selectChildClass==null || selectChildClass.getText().toString().equals("")){
                Toast.makeText(getApplicationContext(), "Class can not be empty!",
                        Toast.LENGTH_LONG).show();
                childLayout.setVisibility(View.VISIBLE);
            }else {
                new SaveChildDetails(NUMBER_OF_CHILDREN,firstname2.getText().toString(),surname2.getText().toString()).execute();
                addChild.setOnClickListener(view -> { fifthChild(); });
            }
        }
    }
    public void fifthChild(){
        if(TextUtils.isEmpty(firstname2.getText())){
            Toast.makeText(getApplicationContext(), "Name can not be empty!",
                    Toast.LENGTH_LONG).show();
            childLayout.setVisibility(View.VISIBLE);

        }else if(selectChildClass==null || selectChildClass.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), "Class can not be empty!",
                    Toast.LENGTH_LONG).show();
            childLayout.setVisibility(View.VISIBLE);
        }else {
            new SaveChildDetails(NUMBER_OF_CHILDREN,firstname2.getText().toString(),surname2.getText().toString()).execute();
        }

    }





    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {


            case(READ_CONTACT):

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    btnSelectVoucher.setVisibility(View.GONE);
                    Intent contactIntent=new Intent(ChatActivity.this, ContactActivity.class);
                    //   Log.i("voucherN",Voucher1);
                    contactIntent.putExtra("voucher1",Voucher1);
                    startActivityForResult(contactIntent,CONTACT_PICKER_REQUEST );

                } else {
                    ChatActivity.chatAdapter.notifyDataSetChanged();
                    Toast.makeText(ChatActivity.this, "We can not proceed without your permission", Toast.LENGTH_SHORT)
                            .show();
                }
                break;


            case(LOCATION_PERMISSION):

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1]==PackageManager.PERMISSION_GRANTED) {


                    final Intent locationService = new Intent(ChatActivity.this, LocationService.class);
                    ChatActivity.this.startService(locationService);
                    ChatActivity.this.bindService(locationService, serviceConnection, Context.BIND_AUTO_CREATE);
                    //new UseMyCurrentLocation().execute();
                    callForLocation();

                } else {
                    ChatActivity.chatAdapter.notifyDataSetChanged();
                    Toast.makeText(ChatActivity.this, "We can not proceed without your permission", Toast.LENGTH_SHORT)
                            .show();
                }
                break;


            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED &&grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED && grantResults[3] == PackageManager.PERMISSION_GRANTED && grantResults[4] == PackageManager.PERMISSION_GRANTED) {
                    llayoutAdditionalInfo.setVisibility(View.GONE);
                    tickLocation.setVisibility(View.VISIBLE);
                    tickContact.setVisibility(View.VISIBLE);
                    tickSMS.setVisibility(View.VISIBLE);
                    tickDevice.setVisibility(View.VISIBLE);

                    /*call service which will send contact data to the server*/
//                    final Intent contactService = new Intent(ChatActivity.this, SendContactDataService.class);
//                    contactService.putExtra("customerId",CUSTOMER_ID);
//                    ChatActivity.this.startService(contactService);

                    /*call service which will send sms data to the server*/
//                    final Intent smsService = new Intent(ChatActivity.this, SendSMSData.class);
//                    smsService.putExtra("customerId",CUSTOMER_ID);
//                    ChatActivity.this.startService(smsService);


                    /*call finbox service */
                    final Intent finboxService = new Intent(ChatActivity.this, FinboxService.class);
                    ChatActivity.this.startService(finboxService);

                    if(isCcsPermissionGranted){


                    }else {
                        if(DOC_TYPE.equals("AADHAAR") && isCurrentAddressSame)
                            presenter.botMessage("Can you provide us 2 people who can stand in for you ?", btnSelectVoucher, ChatPresenter.chatObjects.size() + 1);

                        else
                            presenter.botMessage("Can you provide your supporting document for address proof? You can give us your Gas Bill or Electricity Bill. Which one would you like to use?",electricityLayout,ChatPresenter.chatObjects.size()+1);
                        //presenter.botMessage("We are processing your application and this may take a few minutes. We will notify you when the eligibility check is complete",null,ChatPresenter.chatObjects.size()+1);

                    }
                    new SendErrorFlags("CCS_PERMISSION_DENIED","1").execute();



                } else {


                    if(ContextCompat.checkSelfPermission(ChatActivity.this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED){
                        tickContact.setVisibility(View.VISIBLE);
                    }
                    if(ContextCompat.checkSelfPermission(ChatActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                        tickLocation.setVisibility(View.VISIBLE);
                    }
//
                    if(ContextCompat.checkSelfPermission(ChatActivity.this, Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED){
                        tickSMS.setVisibility(View.VISIBLE);
                    }

                    if(ContextCompat.checkSelfPermission(ChatActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                        tickLocation.setVisibility(View.VISIBLE);
                    }

                    if(ContextCompat.checkSelfPermission(ChatActivity.this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED){
                        tickDevice.setVisibility(View.VISIBLE);
                    }


                    if(isCcsPermissionGranted){
                        chatAdapter.notifyDataSetChanged();
                        llayoutAdditionalInfo.setVisibility(View.VISIBLE);

                    }else {
                        new SendErrorFlags("CCS_PERMISSION_DENIED","2").execute();
                        chatAdapter.notifyDataSetChanged();
                    }

                }
                break;

            case  CAMERA_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)
                {
                    Intent takePictureIntent = new Intent(this, CameraActivity.class);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        File photoFile = null;
                        try {
                            photoFile = createImageFile();
                            File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                            final File file=new File(storageDir, "pic.jpg");
                            photoFile=file;
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                        if (photoFile != null) {
                            panURI = FileProvider.getUriForFile(this,
                                    "com.findeed.fileprovider",
                                    photoFile);
//                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, panURI);
                            //                           takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                            startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO_PAN);
                            takePictureIntent.putExtra("image_side","FRONT SIDE");
                            takePictureIntent.putExtra("doc_type","PAN");
                            startActivityForResult(takePictureIntent,REQUEST_TAKE_PHOTO_PAN);
                        }
                    }
                }
                else
                {
                    ChatActivity.chatAdapter.notifyDataSetChanged();
                }
                break;




            case CLICK_GAS_PERMISSION:

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)
                {
                    Intent takePictureIntent = new Intent(ChatActivity.this,CameraActivity.class);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        File photoFile = null;
                        try {
                            photoFile = createImageFile();
                            File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                            final File file=new File(storageDir, "pic.jpg");
                            photoFile=file;
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                        if (photoFile != null) {
                            gasUri = FileProvider.getUriForFile(ChatActivity.this,
                                    "com.findeed.fileprovider",
                                    photoFile);
                            //takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, gasUri);
                            takePictureIntent.putExtra("image_side","FRONT SIDE");
                            takePictureIntent.putExtra("doc_type","VOTER");
                            //takePictureIntent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                            startActivityForResult(takePictureIntent, CLICK_GAS);
                        }
                    }



                }else
                    ChatActivity.chatAdapter.notifyDataSetChanged();

                break;


            case CLICK_ELECTRICITY_PERMISSION:

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)
                {
                    Intent takePictureIntent = new Intent(ChatActivity.this,CameraActivity.class);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        File photoFile = null;
                        try {
                            photoFile = createImageFile();
                            File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                            final File file=new File(storageDir, "pic.jpg");
                            photoFile=file;                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                        if (photoFile != null) {
                            electricityUri = FileProvider.getUriForFile(ChatActivity.this,
                                    "com.findeed.fileprovider",
                                    photoFile);
                            takePictureIntent.putExtra("image_side","FRONT SIDE");
                            takePictureIntent.putExtra("doc_type","VOTER");
                            startActivityForResult(takePictureIntent, CLICK_ELECTRICITY);
                        }
                    }



                }else
                    ChatActivity.chatAdapter.notifyDataSetChanged();

                break;


            case  PAN_BACK_CLICK_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)
                {
                    Intent takePictureIntent = new Intent(ChatActivity.this, CameraActivity.class);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        File photoFile = null;
                        try {
                            photoFile = createImageFile();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                        if (photoFile != null) {
                            panBackUri = FileProvider.getUriForFile(this,
                                    "com.findeed.fileprovider",
                                    photoFile);
                            takePictureIntent.putExtra("image_side","FRONT SIDE");
                            takePictureIntent.putExtra("doc_type","VOTER");
                            startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO_PAN_BACK);
                        }
                    }
                }
                else
                {
                    ChatActivity.chatAdapter.notifyDataSetChanged();
                }
                break;





            case  CLICK_VOTER_PHOTO_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)
                {
                    // Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    Intent takePictureIntent = new Intent(ChatActivity.this,CameraActivity.class);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        File photoFile = null;
                        try {
                            photoFile = createImageFile();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                        if (photoFile != null) {
                            voterFrontURI = FileProvider.getUriForFile(this,
                                    "com.findeed.fileprovider",
                                    photoFile);
                            takePictureIntent.putExtra("image_side","FRONT SIDE");
                            takePictureIntent.putExtra("doc_type","VOTER");
                            startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO_VOTER_FRONT);
                        }
                    }
                }
                else
                {
                    ChatActivity.chatAdapter.notifyDataSetChanged();
                }
                break;
            default:
                break;
        }
    }





    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);



        if (requestCode == REQUEST_LOCATION) {

            if(resultCode  == RESULT_OK){

                if (ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                    final Intent locationService = new Intent(ChatActivity.this, LocationService.class);
                    ChatActivity.this.startService(locationService);
                    ChatActivity.this.bindService(locationService, serviceConnection, Context.BIND_AUTO_CREATE);
                    //new UseMyCurrentLocation().execute();
                    callForLocation();
                } else {
                    requestPermissions(new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                    }, LOCATION_PERMISSION);


                }

            }else {
                Toast.makeText(getApplicationContext(),"Location service is mandatory",Toast.LENGTH_SHORT).show();
                chatAdapter.notifyDataSetChanged();
            }
        }


        if(requestCode==TERMS_AND_CONDITIONS){
            if(TermsAndConditions.isBackPressed){
                Toast.makeText(getApplicationContext(),"You need to accept our terms and conditions in order to proceed further",Toast.LENGTH_SHORT).show();
                loanDisclosure.setVisibility(View.VISIBLE);
            }else {
                presenter.botMessage("Thank You, All your details have been submitted sucessfully, we will notify you when the loan is ready for disburse.",null,125458);

            }

        }


        if (requestCode == FACE_AUTH_CODE) {


            presenter.botMessage("Thank you, for completeing the video recording process",null,ChatPresenter.chatObjects.size()+1);
            presenter.botMessage("Are you using the same mobile no. "+validateOTPResponse.getData().getPhoneNumber()+" on Whatsapp?",whatsApp,ChatPresenter.chatObjects.size()+1);
            if (resultCode == FaceAuthResults.RESULT_OK) {
                if (data != null) {
                    new SaveLivelinessResult().execute();
                    FaceAuthResponse faceAuthResponse = data.getParcelableExtra("face_auth_result");
                    matchedValue= valueOf(faceAuthResponse.getMatchScore());
                    Toast.makeText(this.getApplicationContext(),"Matched face value is "+matchedValue,Toast.LENGTH_LONG).show();
                }
            } else {
                if (data != null) {
                    new SaveLivelinessResult().execute();
                    chatAdapter.notifyDataSetChanged();
                    Toast.makeText(chatActivity, "Could not find face to match with", Toast.LENGTH_SHORT).show();
                }
            }
        }
        else if(requestCode==CLICK_ELECTRICITY){
            if(resultCode==RESULT_OK){

                try {
                    ELECTRICITY_BILL = MediaStore.Images.Media.getBitmap(this.getContentResolver(), electricityUri);
                    presenter.sendVoterIdImage(ELECTRICITY_BILL,null,ChatPresenter.chatObjects.size()+1);
                    presenter.botMessage("Please continue for us to process your application",electricityContinue,ChatPresenter.chatObjects.size()+1);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }else {
                ChatActivity.chatAdapter.notifyDataSetChanged();
            }
        }

        else if(requestCode==PICK_ELECTRICITY){

            if(resultCode==RESULT_OK){
                if(data!=null){
                    final String action = data.getAction();
                    if (action == null) {

                        Uri imageUri = data.getData();

                        try {
                            ELECTRICITY_BILL  = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                            presenter.sendVoterIdImage(ELECTRICITY_BILL,null,ChatPresenter.chatObjects.size()+1);
                            presenter.botMessage("Please continue for us to process your application",electricityContinue,ChatPresenter.chatObjects.size()+1);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
            }else {
                chatAdapter.notifyDataSetChanged();
            }
        }



        else if(requestCode==CLICK_GAS){
            if(resultCode==RESULT_OK){

                try {
                    GAS_BILL=  MediaStore.Images.Media.getBitmap(this.getContentResolver(), gasUri);
                    presenter.sendVoterIdImage(GAS_BILL,null,ChatPresenter.chatObjects.size()+1);
                    presenter.botMessage("Please continue for us to process your application",gasContinue,ChatPresenter.chatObjects.size()+1);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }else {
                ChatActivity.chatAdapter.notifyDataSetChanged();
            }
        }


        else if(requestCode==PICK_GAS){

            if(resultCode==RESULT_OK){
                if(data!=null){
                    final String action = data.getAction();
                    if (action == null) {

                        Uri imageUri = data.getData();

                        try {
                            final Bitmap  bm = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                            GAS_BILL = bm;
                            presenter.sendVoterIdImage(GAS_BILL,null,ChatPresenter.chatObjects.size()+1);
                            presenter.botMessage("Please continue for us to process your application",gasContinue,ChatPresenter.chatObjects.size()+1);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
            }else {
                chatAdapter.notifyDataSetChanged();
            }
        }







        else if(requestCode==REQUEST_TAKE_PHOTO_VOTER_FRONT){
            if(resultCode==RESULT_OK){

                try {
                    VOTER_FRONT= MediaStore.Images.Media.getBitmap(this.getContentResolver(), voterFrontURI);
                    presenter.sendVoterIdImage(VOTER_FRONT,null,ChatPresenter.chatObjects.size()+1);
                    presenter.botMessage("Please upload backside image of the Voter's ID",clickPickForVoterBack,ChatPresenter.chatObjects.size()+1);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }else {
                ChatActivity.chatAdapter.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(),"Could not capture  image",Toast.LENGTH_SHORT).show();
            }
        }

        else if(requestCode==REQUEST_TAKE_PHOTO_VOTER_BACK){
            if(resultCode==RESULT_OK){
                final Bitmap  bm;
                try {
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    bm = MediaStore.Images.Media.getBitmap(this.getContentResolver(), voterBackURI);
//
                    VOTER_BACK=bm;
                    presenter.sendVoterIdImage(VOTER_BACK,null,ChatPresenter.chatObjects.size()+1);

                    presenter.botMessage("Please continue for us to process your Voter's ID",btncontinuevoterid,ChatPresenter.chatObjects.size()+1);
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }else {
                ChatActivity.chatAdapter.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(),"Could not capture  image",Toast.LENGTH_SHORT).show();
            }
        }

        else if(requestCode==REQUEST_TAKE_PHOTO_PAN_BACK){

            if(resultCode==RESULT_OK){
                final Bitmap  bm;
                try {
                    long size= currentPhotoPath.length();
                    //Bitmap compressedImageBitmap = new Compressor(this).compressToBitmap(currentPhotoPath);
                    // float a= compressedImageBitmap.getWidth()*compressedImageBitmap.getHeight();

                    bm = MediaStore.Images.Media.getBitmap(this.getContentResolver(), panBackUri);presenter.sendDocsImage(bm,null,ChatPresenter.chatObjects.size()+1);
//                    presenter.botMessage("Please continue for us to process your PAN",btnPanContinue,ChatPresenter.chatObjects.size()+1);
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }else {
                ChatActivity.chatAdapter.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(),"Could not capture  image",Toast.LENGTH_SHORT).show();
            }


        }

        else if(requestCode== REQUEST_TAKE_PHOTO_PAN){
            if(resultCode==RESULT_OK){
                //    Toast.makeText(this, "Sorry!!!, you can't use this app without granting permission", Toast.LENGTH_LONG).show();


                try {

//
                    lLayoutPanOption.setVisibility(View.GONE);
                    bitmap= MediaStore.Images.Media.getBitmap(this.getContentResolver(), panURI);
                    presenter.sendDocsImage(bitmap,null,ChatPresenter.chatObjects.size()+1);
                    presenter.botMessage("Please continue for us to process your PAN",btnPanContinue,ChatPresenter.chatObjects.size()+1);

                } catch (Exception e) {
                    e.printStackTrace();
                }



            }else if(resultCode==RESULT_CANCELED){
                chatAdapter.notifyDataSetChanged();
                //  Toast.makeText(getApplicationContext(),"Could not capture PAN image",Toast.LENGTH_SHORT).show();
            }

        }



        else if(requestCode==CONTACT_PICK){
            try{
                if(resultCode==RESULT_OK){

                    if(select2ndVoucher.getVisibility()==View.VISIBLE){
                        select2ndVoucher.setVisibility(View.GONE);
                    }

                    String contactName=data.getStringExtra("contact_name");
                    // Log.i("contName","aaa"+contactName);
                    String contactNumber=data.getStringExtra("contact_number");
                    ContactData cd = new ContactData();
                    cd.setVoucherImage(contactName.substring(0,1));
                    cd.setVoucherName(contactName);
                    cd.setVoucherNumber("Voucher 2");
                    cd.setVoucherStatus("Pending");
                    presenter.sendContacts(cd,null,ChatPresenter.chatObjects.size()+1);
                    ContactData school = new ContactData();
                    if(LOAN_TYPE==1){
                        school.setVoucherImage(CUSTOMER_DETAILS.getSchool().getSchoolName().substring(0,1));
                        school.setVoucherName(CUSTOMER_DETAILS.getSchool().getSchoolName());
                        school.setVoucherNumber("School");
                        school.setVoucherStatus("Pending");
                    }else {
                        school.setVoucherImage(CUSTOMER_DETAILS.getEmployerName().substring(0,1)); //company name is saved in schoolName where CustomerEmployer async task is being called
                        school.setVoucherName(CUSTOMER_DETAILS.getEmployerName());
                        school.setVoucherNumber("Employer");
                        school.setVoucherStatus("Pending");
                    }
                    presenter.sendContacts(school,null,ChatPresenter.chatObjects.size()+1);

                    presenter.botMessage("Thanks for providing details. Once your references complete the steps on their phone, will complete the reference checks and employee verification and get back to you. Pls note we are closed on Sat & Sun",null,ChatPresenter.chatObjects.size()+1);
                    new SendVoucherSMS("Your Friend",USER_NAME,contactNumber).execute();
                    new SaveCustomerVoucher(cd.getVoucherName(),contactNumber).execute();
                } else if(resultCode == RESULT_CANCELED){
                    chatAdapter.notifyDataSetChanged();
                    Toast.makeText(getApplicationContext(),"No contact selected",Toast.LENGTH_SHORT).show();
                }
            }catch(Exception e){
                e.printStackTrace();
                Log.i("CAUSE",e.getMessage());
            }
        }
        else if(requestCode == CONTACT_PICKER_REQUEST){
            if(resultCode == RESULT_OK) {
                //Intent intent = getIntent();
                // Bundle extras=getIntent().getExtras();
                String contactName=data.getStringExtra("contact_name");
                Log.i("contName","aaa"+contactName);
                String contactNumber=data.getStringExtra("contact_number");
                Voucher1=contactNumber;

                //results.addAll(MultiContactPicker.obtainResult(data));
                ContactData cd = new ContactData();
                cd.setVoucherImage(contactName.substring(0,1));
                cd.setVoucherName(contactName);
                new SendVoucherSMS("Your Friend",USER_NAME,contactNumber).execute();
                cd.setVoucherNumber("Voucher 1");
                cd.setVoucherStatus("Pending");
                new SaveCustomerVoucher(cd.getVoucherName(),contactNumber).execute();
                presenter.sendContacts(cd,null,ChatPresenter.chatObjects.size()+1);
                presenter.botMessage(" Kindly select your 2nd Voucher",select2ndVoucher,ChatPresenter.chatObjects.size()+1);

            } else if(resultCode == RESULT_CANCELED){
                chatAdapter.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(),"No contact selected",Toast.LENGTH_SHORT).show();
            }
        }

        else if(requestCode==PAN_PICK_PHOTO){

            if(resultCode==RESULT_OK){
                lLayoutPanOption.setVisibility(View.GONE);
                if(data!=null){
                    final String action = data.getAction();
                    if (action == null) {

                        Uri imageUri = data.getData();

                        try {
                            bitmap= MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                            presenter.sendDocsImage(bitmap,null,ChatPresenter.chatObjects.size()+1);
                            lLayoutPanOption.setVisibility(View.GONE);
//                            presenter.botMessage("Please upload backside image of the PAN card.",clickPickPanBack,ChatPresenter.chatObjects.size()+1);
                            presenter.botMessage("Please continue for us to process your PAN",btnPanContinue,ChatPresenter.chatObjects.size()+1);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
            }else {
                chatAdapter.notifyDataSetChanged();
            }



        }

        else if(requestCode==REQUEST_PICK_PHOTO_PAN_BACK){
            if(resultCode==RESULT_OK){
                final String action = data.getAction();
                if (action == null) {

                    Uri imageUri = data.getData();
                    llayoutoptionselectforvoterid.setVisibility(View.GONE);
                    try {
                        final Bitmap  bm = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                        presenter.sendDocsImage(bm,null,ChatPresenter.chatObjects.size()+1);
                        llayoutoptionselectforvoterid.setVisibility(View.GONE);
//                        presenter.botMessage("Please continue for us to process your PAN",btnPanContinue,ChatPresenter.chatObjects.size()+1);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }else {
                chatAdapter.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(), "Did  not choose the image", Toast.LENGTH_SHORT).show();
            }
        }


        else if(requestCode==TAKE_AADHAR_PHOTO){

            new ExtractProcessedInformation().execute();



        }


        else if(requestCode==REQUEST_PICK_PHOTO_VOTER_FRONT){
            if(resultCode==RESULT_OK){
                final String action = data.getAction();
                if (action == null) {

                    Uri imageUri = data.getData();
                    llayoutoptionselectforvoterid.setVisibility(View.GONE);
                    try {
                        VOTER_FRONT = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                        presenter.sendVoterIdImage(VOTER_FRONT,null,ChatPresenter.chatObjects.size()+1);
                        llayoutoptionselectforvoterid.setVisibility(View.GONE);
                        presenter.botMessage("Please upload backside image of the Voter's ID",clickPickForVoterBack,ChatPresenter.chatObjects.size()+1);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }else {
                    chatAdapter.notifyDataSetChanged();
                    Toast.makeText(getApplicationContext(), "Did  not choose the image", Toast.LENGTH_SHORT).show();
                }
            }
        }



        else if(requestCode==REQUEST_PICK_PHOTO_VOTER_BACK){
            if(resultCode==RESULT_OK){
                final String action = data.getAction();
                if (action == null) {
                    llayoutoptionselectforvoterid.setVisibility(View.GONE);
                    clickPickForVoterBack.setVisibility(View.GONE);

                    Uri imageUri = data.getData();

                    try {
                        VOTER_BACK=  MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                        presenter.sendVoterIdImage(VOTER_BACK,null,ChatPresenter.chatObjects.size()+1);
                        presenter.botMessage("Please continue for us to process your Voter's Id",btncontinuevoterid,ChatPresenter.chatObjects.size()+1);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }else {
                    chatAdapter.notifyDataSetChanged();
                    Toast.makeText(getApplicationContext(), "Did  not choose the image", Toast.LENGTH_SHORT).show();
                }
            }
        }


    }



    @Override
    public void notifyAdapterObjectAdded(int position) {
        this.chatAdapter.notifyItemInserted(position);
    }

    @Override
    public void scrollChatDown() {
        try{
            this.rvChatList.smoothScrollToPosition(presenter.getChatObjects().size() - 1);

        }catch(IllegalArgumentException e){
            e.printStackTrace();
        }
    }



    private EditText.OnEditorActionListener searchBoxListener = new EditText.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView tv, int actionId, KeyEvent keyEvent) {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                if (!TextUtils.isEmpty(tv.getText())) {
                    listv.setVisibility(View.VISIBLE);
                    presenter.onEditTextActionDone(tv.getText().toString(),null,ChatPresenter.chatObjects.size()+1);


//                    etSearchBox.getText().clear();
                    return true;
                }
            }
            return false;
        }
    };



    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    /*
     *
     *
     *
     *
     *
     *
     *
     * API'S
     *
     *
     *
     *
     * */


    public static boolean isNull(String str) {
        return str == null ? true : false;
    }
    public static boolean isNullOrBlank(String param) { if (isNull(param) || param.trim().length() == 0) { return true; }return false; }
    public boolean isAlpha(String name) { return name.matches("^[ a-zA-Z]*$"); }
    public boolean isValidPanId (String panId){ return panId.matches("^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$");}
    public boolean isValidPin (String pinCode){ return pinCode.matches("^([0-9]){6}?$"); }
    public boolean isValidVoterId (String voterId){ return voterId.matches("^([a-zA-Z]){3}([0-9]){7}?$") || voterId.matches("[a-zA-Z]{2}(\\/)[0-9]{2}(\\/)[0-9]{3}(\\/)[0-9]{6}");}
    public boolean findAndMatchPin(String address){

        try{
            Pattern zipPattern = Pattern.compile( "(\\d{6})" );
            Matcher zipMatcher = zipPattern.matcher(address);
            if (zipMatcher.find()) {
                String zip = zipMatcher.group(1);
                for(int i=0;i<pincodeList.getData().size();i++){
                    if(pincodeList.getData().get(i).getPincodeName().equals(zip)){
                        return true;
                    }
                }

            }else
                return false;
            return false;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }


    public boolean isPinAvailable(String address){
        Pattern zipPattern = Pattern.compile( "(\\d{6})" );
        Matcher zipMatcher = zipPattern.matcher(address);
        return zipMatcher.find();
    }








    /*5301*/


    @Override
    public void onFocusChange(View v, boolean hasFocus) {

        switch (v.getId()){

            case R.id.pan_number:
                if(!hasFocus){
                    if(isNullOrBlank(panIdEditText.getText().toString())){
                        panIdValidations.setVisibility(View.VISIBLE);
                        panIdEditText.setBackgroundResource(R.drawable.shape_panediterror);


                    }else {
                        if(!isValidPanId(panIdEditText.getText().toString())){
                            panIdValidations.setVisibility(View.VISIBLE);
                            panIdEditText.setBackgroundResource(R.drawable.shape_panediterror);

                        }else {
                            panIdValidations.setVisibility(View.INVISIBLE);

                            panIdEditText.setBackgroundResource(R.drawable.shape_panedittext);




                        }
                    }

                }else{

                    panIdValidations.setVisibility(View.INVISIBLE);



                }
                break;
            case R.id.pan_name:
                if(!hasFocus){
                    if(isNullOrBlank(panNameEditText.getText().toString())){

                        panNameValidations.setText("Please enter name");
                        panNameValidations.setVisibility(View.VISIBLE);
                        panNameEditText.setBackgroundResource(R.drawable.shape_panediterror);


                    }else {
                        if(!isAlpha(panNameEditText.getText().toString())) {
                            panNameValidations.setText("Please Enter Valid Name");
                            panNameValidations.setVisibility(View.VISIBLE);
                            panNameEditText.setBackgroundResource(R.drawable.shape_panediterror);

                            // PanId.setBackgroundResource(R.drawable.shape_panedittext);
                            //panName.getBackground().mutate().setColorFilter(ContextCompat.getColor(this,R.color.error), PorterDuff.Mode.SRC_ATOP);
                        }else {

                            panNameValidations.setVisibility(View.INVISIBLE);

                            panNameEditText.setBackgroundResource(R.drawable.shape_panedittext);
                        }
                    }

                }else{
                    panNameValidations.setVisibility(View.INVISIBLE);


                }
                break;

            case R.id.id_number:
                if(!hasFocus){
                    if(isNullOrBlank(voterIdEditText.getText().toString())){

                        voterIdError.setText("Please enter Voter Id");
                        voterIdError.setVisibility(View.VISIBLE);
                        voterIdEditText.setBackgroundResource(R.drawable.shape_panediterror);


                    }else {
                        if(!isValidVoterId(voterIdEditText.getText().toString())) {
                            voterIdError.setText("Please Enter Valid Voter Id");
                            voterIdError.setVisibility(View.VISIBLE);
                            voterIdEditText.setBackgroundResource(R.drawable.shape_panediterror);

                        }else {

                            voterIdError.setVisibility(View.INVISIBLE);

                            voterIdEditText.setBackgroundResource(R.drawable.shape_panedittext);


//
                        }
                    }

                }else{
                    //    panIdValidations.setVisibility(View.GONE);
                    voterIdError.setVisibility(View.INVISIBLE);


                }
                break;
            // voterNameEditText etname.setOnFocusChangeListener(this);
            //        etarea.setOnFocusChangeListener(this);
            //        etcity.setOnFocusChangeListener(this);
            case R.id.voter_name:
                if(!hasFocus){
                    if(isNullOrBlank(voterNameEditText.getText().toString())){


                        voterNameError.setText("Please enter name");
                        voterNameError.setVisibility(View.VISIBLE);
                        voterNameEditText.setBackgroundResource(R.drawable.shape_panediterror);


                    }else {
                        if(!isAlpha(voterNameEditText.getText().toString())) {
                            voterNameError.setText("Please enter valid name");
                            voterNameError.setVisibility(View.VISIBLE);
                            voterNameEditText.setBackgroundResource(R.drawable.shape_panediterror);

                        }else {

                            voterNameError.setVisibility(View.INVISIBLE);

                            voterNameEditText.setBackgroundResource(R.drawable.shape_panedittext);


//
                        }
                    }

                }else{
                    //    panIdValidations.setVisibility(View.GONE);
                    voterNameError.setVisibility(View.INVISIBLE);


                }
                break;

            case R.id.voteraddress:
                if(!hasFocus){
                    if(isNullOrBlank(voterAddressEditText.getText().toString())){


                        voterAddressError.setText("Please enter address");
                        voterAddressError.setVisibility(View.VISIBLE);
                        voterAddressEditText.setBackgroundResource(R.drawable.shape_panediterror);


                    }else if(!isPinAvailable(voterAddressEditText.getText().toString())){
                        voterAddressError.setText("Please enter pin code");
                        voterAddressError.setVisibility(View.VISIBLE);
                        voterAddressEditText.setBackgroundResource(R.drawable.shape_panediterror);

                    }



                    else {


                        voterAddressError.setVisibility(View.INVISIBLE);

                        voterAddressEditText.setBackgroundResource(R.drawable.shape_panedittext);


//

                    }

                }else{
                    //    panIdValidations.setVisibility(View.GONE);
                    voterAddressError.setVisibility(View.INVISIBLE);


                }
                break;
            case R.id.et_name:
                if(!hasFocus){
                    if(isNullOrBlank(etname.getText().toString())){


                        tvErrorSchoolName.setText("Please enter school name");
                        tvErrorSchoolName.setVisibility(View.VISIBLE);
                        etname.setBackgroundResource(R.drawable.shape_panediterror);


                    }else {
                        if(!isAlpha(etname.getText().toString())) {
                            tvErrorSchoolName.setText("Please enter valid school name");
                            tvErrorSchoolName.setVisibility(View.VISIBLE);
                            etname.setBackgroundResource(R.drawable.shape_panediterror);

                        }else {

                            tvErrorSchoolName.setVisibility(View.INVISIBLE);

                            etname.setBackgroundResource(R.drawable.shape_panedittext);

//
                        }
                    }

                }else{
                    //    panIdValidations.setVisibility(View.GONE);
                    tvErrorSchoolName.setVisibility(View.INVISIBLE);


                }
                break;
            case R.id.et_area:
                if(!hasFocus){
                    if(isNullOrBlank(etarea.getText().toString())){


                        tvErrorSchoolArea.setText("Please enter school area");
                        tvErrorSchoolArea.setVisibility(View.VISIBLE);
                        etarea.setBackgroundResource(R.drawable.shape_panediterror);


                    }else {
                        if(!isAlpha(etarea.getText().toString())) {
                            tvErrorSchoolArea.setText("Please enter valid school area");
                            tvErrorSchoolArea.setVisibility(View.VISIBLE);
                            etarea.setBackgroundResource(R.drawable.shape_panediterror);

                        }else {

                            tvErrorSchoolArea.setVisibility(View.INVISIBLE);

                            etarea.setBackgroundResource(R.drawable.shape_panedittext);


//
                        }
                    }

                }else{
                    //    panIdValidations.setVisibility(View.GONE);
                    tvErrorSchoolArea.setVisibility(View.INVISIBLE);


                }
                break;
            case R.id.et_city:
                if(!hasFocus){
                    if(isNullOrBlank(etcity.getText().toString())){


                        tvErrorSchoolCity.setText("Please enter city");
                        tvErrorSchoolCity.setVisibility(View.VISIBLE);
                        etcity.setBackgroundResource(R.drawable.shape_panediterror);


                    }else {
                        if(!isAlpha(etcity.getText().toString())) {
                            tvErrorSchoolCity.setText("Please enter valid city");
                            tvErrorSchoolCity.setVisibility(View.VISIBLE);
                            etcity.setBackgroundResource(R.drawable.shape_panediterror);

                        }else {

                            tvErrorSchoolCity.setVisibility(View.INVISIBLE);

                            etcity.setBackgroundResource(R.drawable.shape_panedittext);


//
                        }
                    }

                }else{
                    //    panIdValidations.setVisibility(View.GONE);
                    tvErrorSchoolCity.setVisibility(View.INVISIBLE);


                }
                break;
            case R.id.firstname2:
                if(!hasFocus){
                    if(isNullOrBlank(firstname2.getText().toString())){


                        childFirstNameError.setText("Please enter first name");
                        childFirstNameError.setVisibility(View.VISIBLE);
                        firstname2.setBackgroundResource(R.drawable.shape_panediterror);


                    }else {
                        if(!isAlpha(firstname2.getText().toString())) {
                            childFirstNameError.setText("please enter valid first name");
                            childFirstNameError.setVisibility(View.VISIBLE);
                            firstname2.setBackgroundResource(R.drawable.shape_panediterror);

                        }else {

                            childFirstNameError.setVisibility(View.INVISIBLE);

                            firstname2.setBackgroundResource(R.drawable.shape_panedittext);

//
                        }
                    }

                }else{
                    //    panIdValidations.setVisibility(View.GONE);
                    childFirstNameError.setVisibility(View.INVISIBLE);


                }
                break;
            case R.id.surname2:
                if(!hasFocus){

                    if( !isNullOrBlank(surname2.getText().toString()) && !isAlpha(surname2.getText().toString())) {
                        childSurnameError.setText("Please enter valid surname");
                        childSurnameError.setVisibility(View.VISIBLE);
                        surname2.setBackgroundResource(R.drawable.shape_panediterror);

                    }else {

                        childSurnameError.setVisibility(View.INVISIBLE);

                        surname2.setBackgroundResource(R.drawable.shape_panedittext);


//

                    }

                }else{
                    //    panIdValidations.setVisibility(View.GONE);
                    childSurnameError.setVisibility(View.INVISIBLE);


                }
                break;
            //whatIsYourBussiness
            case R.id.bussinessType_edittext:
                if(!hasFocus){
                    if(isNullOrBlank(whatIsYourBussiness.getText().toString())){


                        bussinessTypeError.setText("Please enter bussiness");
                        bussinessTypeError.setVisibility(View.VISIBLE);
                        whatIsYourBussiness.setBackgroundResource(R.drawable.shape_panediterror);


                    }else {
                        if(!isAlpha(whatIsYourBussiness.getText().toString())) {
                            bussinessTypeError.setText("please enter valid bussiness");
                            bussinessTypeError.setVisibility(View.VISIBLE);
                            whatIsYourBussiness.setBackgroundResource(R.drawable.shape_panediterror);

                        }else {

                            bussinessTypeError.setVisibility(View.INVISIBLE);

                            whatIsYourBussiness.setBackgroundResource(R.drawable.shape_panedittext);


//
                        }
                    }

                }else{
                    //    panIdValidations.setVisibility(View.GONE);
                    bussinessTypeError.setVisibility(View.INVISIBLE);


                }
                break;


            case R.id.address_line_one:
                if(!hasFocus){
                    if(isNullOrBlank(addressLineOne.getText().toString())){


                        currentAddressError.setText("Please enter address");
                        currentAddressError.setVisibility(View.VISIBLE);
                        addressLineOne.setBackgroundResource(R.drawable.shape_panediterror);


                    }else {


                        currentAddressError.setVisibility(View.INVISIBLE);

                        addressLineOne.setBackgroundResource(R.drawable.shape_panedittext);


//

                    }

                }else{
                    //    panIdValidations.setVisibility(View.GONE);
                    currentAddressError.setVisibility(View.INVISIBLE);


                }
                break;
            case R.id.locality:
                if(!hasFocus){
                    if(isNullOrBlank(locality.getText().toString())){


                        currentLocalityError.setText("Please enter locality");
                        currentLocalityError.setVisibility(View.VISIBLE);
                        locality.setBackgroundResource(R.drawable.shape_panediterror);


                    }else {


                        currentLocalityError.setVisibility(View.INVISIBLE);

                        locality.setBackgroundResource(R.drawable.shape_panedittext);


//

                    }

                }else{
                    //    panIdValidations.setVisibility(View.GONE);
                    currentLocalityError.setVisibility(View.INVISIBLE);


                }
                break;
            case R.id.city:
                if(!hasFocus){
                    if(isNullOrBlank(city.getText().toString())){


                        currentCityError.setText("Please enter city name");
                        currentCityError.setVisibility(View.VISIBLE);
                        city.setBackgroundResource(R.drawable.shape_panediterror);


                    }else {
                        if(!isAlpha(city.getText().toString())) {
                            currentCityError.setText("please enter valid city name");
                            currentCityError.setVisibility(View.VISIBLE);
                            city.setBackgroundResource(R.drawable.shape_panediterror);

                        }else {

                            currentCityError.setVisibility(View.INVISIBLE);

                            city.setBackgroundResource(R.drawable.shape_panedittext);


//
                        }
                    }

                }else{
                    //    panIdValidations.setVisibility(View.GONE);
                    currentCityError.setVisibility(View.INVISIBLE);


                }
                break;
            case R.id.pin_code:
                if(!hasFocus){
                    if(isNullOrBlank(pinCode.getText().toString())){


                        currentPinError.setText("Please enter PIN Code");
                        currentPinError.setVisibility(View.VISIBLE);
                        pinCode.setBackgroundResource(R.drawable.shape_panediterror);


                    }else {
                        if(!isValidPin(pinCode.getText().toString())) {
                            currentPinError.setText("Please enter valid PIN Code");
                            currentPinError.setVisibility(View.VISIBLE);
                            pinCode.setBackgroundResource(R.drawable.shape_panediterror);

                        }else {

                            currentPinError.setVisibility(View.INVISIBLE);

                            pinCode.setBackgroundResource(R.drawable.shape_panedittext);


//
                        }
                    }

                }else{
                    //    panIdValidations.setVisibility(View.GONE);
                    currentPinError.setVisibility(View.INVISIBLE);


                }
                break;


















            case R.id.et_name_emp:
                if(!hasFocus){
                    if(isNullOrBlank(employerName.getText().toString())){


                        employerNameError.setText("Please enter employer name");
                        employerNameError.setVisibility(View.VISIBLE);
                        pinCode.setBackgroundResource(R.drawable.shape_panediterror);


                    }else {


                        employerNameError.setVisibility(View.INVISIBLE);

                        employerName.setBackgroundResource(R.drawable.shape_panedittext);


//

                    }

                }else{
                    //    panIdValidations.setVisibility(View.GONE);
                    employerNameError.setVisibility(View.INVISIBLE);


                }
                break;
            case R.id.et_area_emp:
                if(!hasFocus){
                    if(isNullOrBlank(employerArea.getText().toString())){


                        employerAreaError.setText("Please enter employer area");
                        employerAreaError.setVisibility(View.VISIBLE);
                        employerArea.setBackgroundResource(R.drawable.shape_panediterror);


                    }else

                        employerAreaError.setVisibility(View.INVISIBLE);

                    employerArea.setBackgroundResource(R.drawable.shape_panedittext);


//



                }else{
                    //    panIdValidations.setVisibility(View.GONE);
                    employerAreaError.setVisibility(View.INVISIBLE);


                }
                break;
            case R.id.et_city_emp:
                if(!hasFocus){
                    if(isNullOrBlank(employerCity.getText().toString())){


                        employerCityError.setText("Please enter city name");
                        employerCityError.setVisibility(View.VISIBLE);
                        employerCity.setBackgroundResource(R.drawable.shape_panediterror);


                    }else {
                        if(!isAlpha(employerCity.getText().toString())) {
                            employerCityError.setText("Please enter valid city name");
                            employerCityError.setVisibility(View.VISIBLE);
                            employerCity.setBackgroundResource(R.drawable.shape_panediterror);

                        }else {
                            employerCityError.setVisibility(View.INVISIBLE);
                            employerCity.setBackgroundResource(R.drawable.shape_panedittext);


//
                        }
                    }

                }else{
                    //    panIdValidations.setVisibility(View.GONE);
                    employerCityError.setVisibility(View.INVISIBLE);


                }
                break;







            case R.id.et_first_name:
                if(!hasFocus){
                    if(isNullOrBlank(firstName.getText().toString())){


                        applicantFirstNameErr.setText("Please enter name");
                        applicantFirstNameErr.setVisibility(View.VISIBLE);
                        firstName.setBackgroundResource(R.drawable.shape_panediterror);


                    }else {
                        if(!isAlpha(firstName.getText().toString())) {
                            applicantFirstNameErr.setText("Please enter valid name");
                            applicantFirstNameErr.setVisibility(View.VISIBLE);
                            firstName.setBackgroundResource(R.drawable.shape_panediterror);

                        }else {

                            applicantFirstNameErr.setVisibility(View.INVISIBLE);

                            firstName.setBackgroundResource(R.drawable.shape_panedittext);


//
                        }
                    }

                }else{
                    //    panIdValidations.setVisibility(View.GONE);
                    applicantFirstNameErr.setVisibility(View.INVISIBLE);


                }
                break;
            case R.id.et_surname:
                if(!hasFocus){

                    if(!isNullOrBlank(lastName.getText().toString()) && !isAlpha(lastName.getText().toString())) {
                        applicantSurNameErr.setText("Please enter valid surname");
                        applicantSurNameErr.setVisibility(View.VISIBLE);
                        lastName.setBackgroundResource(R.drawable.shape_panediterror);

                    }else {

                        applicantSurNameErr.setVisibility(View.INVISIBLE);

                        lastName.setBackgroundResource(R.drawable.shape_panedittext);


//
                    }


                }else{
                    //    panIdValidations.setVisibility(View.GONE);
                    applicantSurNameErr.setVisibility(View.INVISIBLE);


                }
                break;


            default:
                throw new IllegalStateException("Unexpected value: " + v.getId());
        }




    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.do_not_have_email:
                eduEmailLayout.setVisibility(View.GONE);
                presenter.botMessage("Congratulations !",sendingNotification,ChatPresenter.chatObjects.size()+1);
                presenter.botMessage("Our team will reach out to collect the KYC documents from you during business hours. Post collection, your money will be disbursed",null,ChatPresenter.chatObjects.size()+1);
                break;

            case R.id.layout1:
                LOAN_TYPE=2;
                loanType.setVisibility(View.GONE);
                presenter.onEditTextActionDone("Personal Loan",null,ChatPresenter.chatObjects.size()+1,validateOTPResponse.getData());
                presenter.botMessage("Can you provide your employer information to avail the loan",searchEmployer,ChatPresenter.chatObjects.size()+1);
                new createCustomerLoan().execute();
                break;

            case R.id.pick_pan_back:
                try {
                    clickPickPanBack.setVisibility(View.GONE);
                    Intent galleryIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                    galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
                    galleryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, false);
                    galleryIntent.setType("image/*");
                    startActivityForResult(galleryIntent, REQUEST_PICK_PHOTO_PAN_BACK);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.click_pan_back:
                clickPickPanBack.setVisibility(View.GONE);
                if (ContextCompat.checkSelfPermission(this,Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    // Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    Intent takePictureIntent = new Intent(ChatActivity.this,CameraActivity.class);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        File photoFile = null;
                        try {
                            photoFile = createImageFile();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                        if (photoFile != null) {
                            panBackUri = FileProvider.getUriForFile(ChatActivity.this,
                                    "com.findeed.fileprovider",
                                    photoFile);
                            takePictureIntent.putExtra("back_side","FRONT SIDE");
                            takePictureIntent.putExtra("doc_type","PAN");
                            startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO_PAN_BACK);
                        }
                    }

                } else {
                    requestPermissions(new String[]{
                                    Manifest.permission.CAMERA,
                                    Manifest.permission.READ_EXTERNAL_STORAGE
                            }

                            , PAN_BACK_CLICK_PERMISSION);

                }
                break;





        }


    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        int id = menuItem.getItemId();

        if (id == R.id.my_loan) {
            Toast.makeText(chatActivity, "You Select MY Loan", Toast.LENGTH_SHORT).show();



        }

        if (id == R.id.loan_apply) {
            Toast.makeText(chatActivity, "You Select Apply for Loan", Toast.LENGTH_SHORT).show();



        } else if (id == R.id.vouch_someone) {

            // Toast.makeText(chatActivity, "You Select Vouch for someone", Toast.LENGTH_SHORT).show();

            new VouchForSomeOne(validateOTPResponse.getData().getPhoneNumber()).execute();

        } else if (id == R.id.notification) {

            Intent intent = new Intent(this, NotificationActivity.class);
            startActivity(intent);
        }

        else if (id == R.id.helpcontact) {
            Toast.makeText(chatActivity, "You Select Contact us", Toast.LENGTH_SHORT).show();
/*
            Intent intent = new Intent(ChatActivity.this, TestActivity.class);
            startActivity(intent);*/

        } else if (id == R.id.invitefriend) {

            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, "http://findeed.in/");
            sendIntent.setType("text/plain");

            Intent shareIntent = Intent.createChooser(sendIntent, null);
            startActivity(shareIntent);

        } else if (id == R.id.rate) {

            final Dialog dialog = new Dialog(ChatActivity.this);
            dialog.setContentView(R.layout.dialog_rating);
            RatingBar ratingBar = dialog.findViewById(R.id.ratingbar);
            TextView tvRating = dialog.findViewById(R.id.tv_rating);
            dialog.show();

            dialog.getWindow().setBackgroundDrawable(new
                    ColorDrawable(Color.TRANSPARENT));


            ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                    Toast.makeText(ChatActivity.this,
                            "Rating is :  "+ ratingBar.getRating(),
                            Toast.LENGTH_SHORT).show();
                    tvRating.setTextColor(ContextCompat.getColor(ChatActivity.ctx,R.color.dark_pink));
                }
            });


            tvRating.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });



        } else if (id == R.id.terms) {

            Intent i = new Intent(this, TermsActivity.class);
            this.startActivity(i);



        } else if (id == R.id.privacy){
            Intent i = new Intent(this, PrivacyActivity.class);
            this.startActivity(i);

        }
        dl.closeDrawer(GravityCompat.START,true);
        return false;
    }
    public class GetImageFromUrl extends AsyncTask<String, Void, Bitmap>{
        ProgressDialog progressDialog = new ProgressDialog(ChatActivity.this);

        ImageView imageView;
        String stringUrl;
        String DocType;
        String DocSide;
        public GetImageFromUrl(String URL, String DocType,String DocSide){
          this.stringUrl=URL;
          this.DocType=DocType;
          this.DocSide=DocSide;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


//            progressDialog.setCancelable(false);
//            progressDialog.show();
        }

        @Override
        protected Bitmap doInBackground(String... url) {

            bitmap = null;
            InputStream inputStream;
            try {
                inputStream = new java.net.URL(stringUrl).openStream();
                bitmap = BitmapFactory.decodeStream(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (DocType.equals("PAN") && DocSide.equals("FRONT")) {
                PanFrontBitmap=bitmap;
                return PanFrontBitmap;

                }
                else if (DocType.equals("PAN") && DocSide.equals("BACK")) {
                PanBackBitmap=bitmap;
                return PanBackBitmap;

                }
                else if (DocType.equals("VOTER") && DocSide.equals("FRONT")) {
                 VoterFrontBitmap=bitmap;
                 return VoterFrontBitmap;
                }
                else if (DocType.equals("VOTER") && DocSide.equals("BACK")) {
                VoterBackBitmap=bitmap;
                return VoterBackBitmap;

                }
                else if (DocType.equals("AADHAAR") && DocSide.equals("FRONT")) {

                    AadhaarFrontBitmap=bitmap;
                    return AadhaarFrontBitmap;
                }
                else if (DocType.equals("GAS_BILL") && DocSide.equals("FRONT")) {
                GasBillBitmap=bitmap;
                return GasBillBitmap;
                }
                else if (DocType.equals("ELECTRICITY_BILL") && DocSide.equals("FRONT")) {
                ElectricityBillBitmap=bitmap;
                return ElectricityBillBitmap;
                }
            return bitmap;
        }
        @Override
        protected void onPostExecute(Bitmap bitmap){
            super.onPostExecute(bitmap);
//            imageView.setImageBitmap(bitmap);
//            if(bitmap!=null) {
//                if (DocType.equals("PAN") && DocSide.equals("FRONT")) {
//                    presenter.sendDocsImage(bitmap, null, ChatPresenter.chatObjects.size() + 1);
//                    presenter.botMessage("Please upload backside image of the PAN card.", clickPickPanBack, ChatPresenter.chatObjects.size() + 1);
//
//                    return;
//                    // }
//                }
//                else if (DocType.equals("PAN") && DocSide.equals("BACK")) {
//
//                    return;//   progressDialog.dismiss();
//                }
//                else if (DocType.equals("VOTER") && DocSide.equals("FRONT")) {
//                   //progressDialog.dismiss();
//                }
//                else if (DocType.equals("VOTER") && DocSide.equals("BACK")) {
////                            Bitmap icon2 = BitmapFactory.decodeResource(getApplicationContext().getResources(),
////                                    R.drawable.dummy_pan_back);
//                    //   progressDialog.dismiss();
//
//                }
//                else if (DocType.equals("AADHAAR") && DocSide.equals("FRONT")) {
////                            Bitmap icon2 = BitmapFactory.decodeResource(getApplicationContext().getResources(),
////                                    R.drawable.dummy_pan_back);
//                    //   progressDialog.dismiss();
//                }
//                else if (DocType.equals("GAS_BILL") && DocSide.equals("FRONT")) {
////                            Bitmap icon2 = BitmapFactory.decodeResource(getApplicationContext().getResources(),
////                                    R.drawable.dummy_pan_back);
//                    //    progressDialog.dismiss();
//                }
//                else if (DocType.equals("ELECTRICITY_BILL") && DocSide.equals("FRONT")) {
////                            Bitmap icon2 = BitmapFactory.decodeResource(getApplicationContext().getResources(),
////                                    R.drawable.dummy_pan_back);
//                    //    progressDialog.dismiss();
//                }
            }
        }


    public class SaveCustomerEmployer extends AsyncTask<String, String ,String> {

        int companyId;

        public SaveCustomerEmployer(int companyId) {
            this.companyId = companyId;
            Log.i("EMP_ID",String.valueOf(companyId));
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            new ChatActivity.GetCustomerDetails().execute();
        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"customer/saveCustomerEmployerId/"+CUSTOMER_ID+"/"+companyId)
                    .build();
            try{
                Response response = client.newCall(request).execute();
                return response.body().string();
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }

    public class SaveHaveKid extends AsyncTask<String, String ,String> {

        String oneOrZero;

        public SaveHaveKid(String oneOrZero) {
            this.oneOrZero = oneOrZero;
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            new ChatActivity.GetCustomerDetails().execute();
        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url("http://157.245.102.194:8080/los-qa/customer/saveIsSaveHaveKids/"+CUSTOMER_ID+"/"+oneOrZero)
                    .build();
            try{
                Response response = client.newCall(request).execute();
                return response.body().string();
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }


    public class SaveIsEmiSummary extends AsyncTask<String, String ,String>{

        String oneOrZero;

        public SaveIsEmiSummary(String oneOrZero) {
            this.oneOrZero = oneOrZero;
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            new GetCustomerDetails().execute();
        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"customer/saveIsSaveEmiSummary/"+CUSTOMER_ID+"/"+oneOrZero)
                    .build();
            try{
                Response response = client.newCall(request).execute();
                return response.body().string();
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }



    public class IsCurrentAddressSame extends AsyncTask<String, String ,String>{

        String oneOrZero;

        public IsCurrentAddressSame(String oneOrZero) {
            this.oneOrZero = oneOrZero;
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            new GetCustomerDetails().execute();
        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"customer/saveIsCurrentPermanentAddressSame/"+CUSTOMER_ID+"/"+oneOrZero)
                    .build();
            try{
                Response response = client.newCall(request).execute();
                return response.body().string();
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }


    public class SaveBussinessType extends AsyncTask<String, String ,String>{

        String bType;

        public SaveBussinessType(String bType) {
            this.bType = bType;
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            new GetCustomerDetails().execute();
        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"customer/saveBusinessType/"+CUSTOMER_ID+"/"+bType)
                    .build();
            try{
                Response response = client.newCall(request).execute();
                return response.body().string();
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }

    public class SaveBussinessName extends AsyncTask<String, String ,String>{

        String bName;

        public SaveBussinessName(String bName) {
            this.bName = bName;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            new GetCustomerDetails().execute();
        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"customer/saveBusinessName/"+CUSTOMER_ID+"/"+bName)
                    .build();
            try{
                Response response = client.newCall(request).execute();
                return response.body().string();
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }
    public class StartOver extends AsyncTask<String, String ,String>{

        Intent intent;

        public StartOver(Intent intent) {
            this.intent = intent;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            finish();
            startActivity(intent);
            stopService(new Intent(ChatActivity.this, FinboxService.class));
        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"customer/startOver/"+CUSTOMER_ID+"/"+validateOTPResponse.getData().getPhoneNumber())
                    .build();
            try{
                Response response = client.newCall(request).execute();
                validateOTPResponse=gson.fromJson(response.body().string(),ValidateOTPResponse.class);
                Log.i("StartOver",gson.toJson(validateOTPResponse));
                CUSTOMER_ID=validateOTPResponse.getData().getCustomerId();
                return response.body().string();
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }

    public class GetPinCodes extends AsyncTask<String, String ,String>{


        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"customer/getPincodes")
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String resData=response.body().string();
                pincodeList = gson.fromJson(resData, PincodeList.class);
                return "Sucess";
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }

        }
    }
    private  class SendVoucherSMS extends AsyncTask<String,String ,String >{
        String  fName,lName,phNumber;

        public SendVoucherSMS(String fName, String lName, String phNumber) {
            this.fName = fName;
            this.lName = lName;
            this.phNumber = phNumber;
        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"otp/sendVoucherSMS/"+fName+"/"+lName+"/"+phNumber+"/"+"aHR0cDovL2ZpbmRlZWQuYXBwLmxpbmsvbWF5R0NJN2ZwMg==")
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String resData=response.body().string();
                Log.i("SMSRESPONSE",resData);
                return null;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }

        }
    }
    private class SendErrorFlags extends AsyncTask<String,String,String >{

        String errorName;
        String value;


        public SendErrorFlags(String errorName,String value) {
            this.errorName = errorName;
            this.value=value;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(errorName.equals("BUREAU_PERMISSION_DENIED"))
                new GetCustomerDetails().execute();
        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"customer/updateError/"+CUSTOMER_ID+"/"+APPLICATION_ID+"/"+errorName+"/"+value)
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String resData=response.body().string();
                return null;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }
    private class SaveWhatsappNumber extends  AsyncTask<String,String ,String >{

        String wNumber;
        public SaveWhatsappNumber(String phNo) {
            this.wNumber=phNo;
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            new GetCustomerDetails().execute();
        }

        @Override
        protected String doInBackground(String... strings) {


            try {
                SendWhatsappNumber number = new SendWhatsappNumber();
                number.setCustomerId(CUSTOMER_ID);
                number.setWhatsappNumber(wNumber);
                String jsonData= gson.toJson(number);

                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                OkHttpClient client = new OkHttpClient();
                RequestBody body = RequestBody.create(JSON, jsonData);
                Request request = new Request.Builder()
                        .url(END_POINT+"customer/saveWhatsappNumber")
                        .post(body)
                        .build();

                Response response = client.newCall(request).execute();
                String resData=response.body().string();
                Log.d("response",resData);
                return  resData;

            }
            catch (Exception e) {
                e.printStackTrace();
                return null;
            }

        }
    }



    private class SaveCustomerEmailAddress extends  AsyncTask<String,String ,String >{

        String email;
        public SaveCustomerEmailAddress(String email) {
            this.email=email;
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            new GetCustomerDetails().execute();
        }

        @Override
        protected String doInBackground(String... strings) {


            try {


                SaveEmailRequest mail = new SaveEmailRequest();
                mail.setCustomerId(CUSTOMER_ID);
                mail.setEmailAddress(email);
                String jsonData= gson.toJson(mail);

                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                OkHttpClient client = new OkHttpClient();
                RequestBody body = RequestBody.create(JSON, jsonData);
                Request request = new Request.Builder()
                        .url(END_POINT+"customer/saveCustomerEmail")
                        .post(body)
                        .build();

                Response response = client.newCall(request).execute();
                String resData=response.body().string();
                Log.d("response",resData);
                return  resData;

            }
            catch (Exception e) {
                e.printStackTrace();
                return null;
            }

        }
    }
    private class SendCurrentLocation extends AsyncTask<String,String ,String >{



        ProgressDialog pd = new ProgressDialog(ChatActivity.this);
        Bitmap staticMapImage;
        final String url_map = "https://maps.google.com/maps/api/staticmap?center=" + latitude + "," + longitude + "&zoom=17&markers=" + latitude + "," + longitude + "&size=250x250&sensor=false&key=AIzaSyD0dxGm6-bsvSvZv0Slk5pMKk7jFJq77Ms";


        @Override
        protected void onPreExecute() {
            pd.setMessage("Please wait...");
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(s!=null){
                if(pd.isShowing()){

                    pd.dismiss();
                    boolean pinCodeMatched=matchPinCode(currentPincode.getText().toString());
                    if(!pinCodeMatched){
                        useMyCurrentLocationLayout.setVisibility(View.GONE);
                        userLocation=currentCity.getText().toString()+","+currentPincode.getText().toString();
                        presenter.sendUserLocation(staticMapImage,useMyCurrentLocationLayout,ChatPresenter.chatObjects.size()+1);
                        presenter.botMessage("Sorry! We do not service the Pin code currently",null,ChatPresenter.chatObjects.size()+1);
                        new SendErrorFlags("PINCODE_VERIFICATION","2").execute();
                        new SaveUserAddress("").execute();

                    }else {

                        presenter.sendUserLocation(staticMapImage,useMyCurrentLocationLayout,ChatPresenter.chatObjects.size()+1);
                        userLocation=cityName+","+pin;
                        useMyCurrentLocationLayout.setVisibility(View.GONE);

                        if (ContextCompat.checkSelfPermission(ChatActivity.this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
                            /*Strat finbox in case user have started over the application*/
                            final Intent finboxService = new Intent(ChatActivity.this, FinboxService.class);
                            ChatActivity.this.startService(finboxService);

                            presenter.botMessage("Can you provide your supporting document for address proof? You can give us your Gas Bill or Electricity Bill. Which one would you like to use?",electricityLayout,ChatPresenter.chatObjects.size()+1);

                        }

                        else {
                            presenter.botMessage("We would require some additional information from you for smooth processing of your loan application", llayoutAdditionalInfo, ChatPresenter.chatObjects.size() + 1);
                        }
                        new SaveUserAddress("").execute();
                        new SendErrorFlags("PINCODE_VERIFICATION","1").execute();
                    }

                }
            }else {
                pd.dismiss();
//            Bitmap icon2 = BitmapFactory.decodeResource(getApplicationContext().getResources(),
//                    R.drawable.location_placeholder);
//            presenter.sendUserLocation(icon2,useMyCurrentLocationLayout,ChatPresenter.chatObjects.size()+1);
//            userLocation=cityName+","+pin;
                useMyCurrentLocationLayout.setVisibility(View.GONE);




                new GetAllLoanDetails().execute();
            }


        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                URL url = new URL(url_map);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                staticMapImage = BitmapFactory.decodeStream(input);
                return "Sucess";

            } catch (IOException e) {


                e.printStackTrace();
                return null;
            }
        }
    }
    private class SendSchoolLocation extends AsyncTask<String,String ,String >{



        ProgressDialog pd = new ProgressDialog(ChatActivity.this);
        Bitmap staticMapImage;
        final String url_map = "https://maps.google.com/maps/api/staticmap?center=" + 12.890670 + "," + 77.557977 + "&zoom=17&markers=" + 12.890670 + "," + 77.557977 + "&size=250x250&sensor=false&key=AIzaSyD0dxGm6-bsvSvZv0Slk5pMKk7jFJq77Ms";


        @Override
        protected void onPreExecute() {
            pd.setMessage("Please wait...");
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(s!=null) {
                pd.dismiss();
                hideKeyboard();
                lvlay.setVisibility(View.GONE);
                actvFrameLayout.setVisibility(View.GONE);
                btnActvClear.setVisibility(View.GONE);
                schoolName = arraylist.get(schoolPos).getSchoolname();
                sName=schoolName;
                presenter.setImage(staticMapImage,actvFrameLayout,ChatPresenter.chatObjects.size()+1,schoolName);
                presenter.botMessage("How many of your children study in " + schoolName+" ?",llayoutchildcount,ChatPresenter.chatObjects.size()+1);
                new SaveChildSchool().execute();
            }


        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                URL url = new URL(url_map);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                staticMapImage = BitmapFactory.decodeStream(input);
                return "Sucess";

            } catch (IOException e) {


                e.printStackTrace();
                return null;
            }
        }
    }
    private class SaveDocumentDetails extends AsyncTask<String ,String ,String>{
        String jsonData;



        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            switch (DOC_TYPE){
                case "PAN":
                    SimpleDateFormat format = new SimpleDateFormat("dd/mm/yyyy");
                    try {
                        Date date = format.parse(panDobEditText.getText().toString());
                        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Calcutta"));
                        cal.setTime(date);
                        USER_AGE=cal.get(Calendar.YEAR);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    savePan.dismiss();
                    boolean age = validateAge();
                    if(age) {
                        new SendErrorFlags("CUSTOMER_AGE_RESTRICTION", "1").execute();
                        lLayoutEditPanInfo.setVisibility(View.GONE);
                        lLayoutUpdatePanInfo.setVisibility(View.GONE);
                        if(GOVT_VERIFICATION_PAN==1 ){
                            presenter.onEditTextActionDone("PAN ID: " + panIdEditText.getText().toString() + "\n" + "Name: " + panNameEditText.getText().toString() + "\n" + "Date of birth:" + panDobEditText.getText().toString(), null, ChatPresenter.chatObjects.size() + 1, CUSTOMER_DETAILS);
                            presenter.botMessage("Awesome now we have your ID proof.", null, ChatPresenter.chatObjects.size() + 1);
                            presenter.botMessage("Can you provide your address proof? You can give us Your Aadhaar or Voter's ID card. Which one would you like to use?", llayoutoption, ChatPresenter.chatObjects.size() + 1);
                        }else {
                            presenter.onEditTextActionDone("PAN ID: " + panIdEditText.getText().toString() + "\n" + "Name: " + panNameEditText.getText().toString() + "\n" + "Date of birth:" + panDobEditText.getText().toString(), null, ChatPresenter.chatObjects.size() + 1, CUSTOMER_DETAILS);
                            presenter.botMessage("Looks like the PAN details you uploaded were not valid",null,ChatPresenter.chatObjects.size()+1);
                        }

                    }else {
                        new SendErrorFlags("CUSTOMER_AGE_RESTRICTION","2").execute();
                        lLayoutEditPanInfo.setVisibility(View.GONE);
                        lLayoutUpdatePanInfo.setVisibility(View.GONE);
                        presenter.onEditTextActionDone("PAN ID: " + panIdEditText.getText().toString() + "\n" + "Name: " + panNameEditText.getText().toString() + "\n" + "Date of birth:" + panDobEditText.getText().toString(), lLayoutUpdatePanInfo, ChatPresenter.chatObjects.size() + 1,CUSTOMER_DETAILS);
                        presenter.botMessage("Sorry we are unable to process your application. The applicant must be atleast  23 years old and cannot be older than 60 years. Please email us on contact@findeed.in if need further help.",null,ChatPresenter.chatObjects.size()+1);
                    }

                    new GetCustomerDetails().execute();
                    break;

                case "GAS_BILL":
                    if(billDialog.isShowing())
                        billDialog.dismiss();
                    presenter.botMessage("Can you provide us 2 people who can stand in for you ?", btnSelectVoucher, ChatPresenter.chatObjects.size() + 1);
                    new GetCustomerDetails().execute();
                    break;
                case "ELECTRICITY_BILL":
                    if(billDialog.isShowing())
                        billDialog.dismiss();
                    presenter.botMessage("Can you provide us 2 people who can stand in for you ?", btnSelectVoucher, ChatPresenter.chatObjects.size() + 1);
                    new GetCustomerDetails().execute();
                    break;


            }


        }

        @Override
        protected String doInBackground(String... strings) {
            try {


                SaveDocumentDetailsRequest documentDetailsRequest= new SaveDocumentDetailsRequest();
                SaveDocument sd =new SaveDocument();
                switch (DOC_TYPE){

                    case "EDITED_VOTER_ID":
                        documentDetailsRequest.setAddress(voterAddressEditText.getText().toString());
                        documentDetailsRequest.setId(CUSTOMER_ID);
                        documentDetailsRequest.setDateOfBirth(voterDobEditText.getText().toString());
                        documentDetailsRequest.setDocumentType("VOTER_ID");
                        documentDetailsRequest.setFirstName(voterNameEditText.getText().toString());
                        documentDetailsRequest.setLastName("");
                        documentDetailsRequest.setGender(sex);
                        documentDetailsRequest.setDocumentId(voterIdEditText.getText().toString());
                        jsonData=gson.toJson(documentDetailsRequest);
                        break;
                    case  "VOTER_ID":
                        documentDetailsRequest.setAddress(VOTER_ADDRESS);
                        documentDetailsRequest.setId(CUSTOMER_ID);
                        documentDetailsRequest.setDateOfBirth(VOTER_DOB);
                        documentDetailsRequest.setDocumentType("VOTER_ID");
                        documentDetailsRequest.setFirstName(VOTER_FIRST_NAME);
                        documentDetailsRequest.setLastName(VOTER_LAST_NAME);
                        documentDetailsRequest.setGender(VOTER_GENDER);
                        documentDetailsRequest.setDocumentId(VOTER_DOCUMENT_ID);
                        jsonData=gson.toJson(documentDetailsRequest);

                        break;

                    case "AADHAAR":

                        documentDetailsRequest.setAddress(AADHAR_ADDRESS);
                        documentDetailsRequest.setId(CUSTOMER_ID);
                        documentDetailsRequest.setFrontImage(USER_FACE);

                        documentDetailsRequest.setDateOfBirth(AADHAR_DOB);
                        documentDetailsRequest.setDocumentType("AADHAAR");
                        documentDetailsRequest.setFirstName(AADHAR_FIRST_NAME);
                        documentDetailsRequest.setLastName(AADHAR_LAST_NAME);
                        documentDetailsRequest.setGender(AADHAR_GENDER);
                        documentDetailsRequest.setDocumentId(AADHAR_DOC_ID);
                        jsonData=gson.toJson(documentDetailsRequest);
                        break;

                    case "PAN":
                        documentDetailsRequest.setAddress("");
                        documentDetailsRequest.setId(CUSTOMER_ID);
                        documentDetailsRequest.setDateOfBirth(panDobEditText.getText().toString());
                        documentDetailsRequest.setDocumentType("PAN");
                        documentDetailsRequest.setFirstName(panNameEditText.getText().toString());
                        documentDetailsRequest.setLastName("");
                        documentDetailsRequest.setGender("");
                        documentDetailsRequest.setDocumentId(panIdEditText.getText().toString());
                        jsonData=gson.toJson(documentDetailsRequest);
                        break;

                    case "GAS_BILL":
                        ByteArrayOutputStream gasOutputStream = new ByteArrayOutputStream();
                        GAS_BILL.compress(Bitmap.CompressFormat.JPEG, 100, gasOutputStream);
                        byte[] gasByteArray = gasOutputStream .toByteArray();
                        sd.setId(CUSTOMER_ID);
                        sd.setDocumentType("GAS_BILL");
                        sd.setDocSide("FRONT");
                       // sd.setFrontImage("");

                        sd.setFrontImage(Base64.encodeToString(gasByteArray,Base64.NO_WRAP));
                        sd.setBackImage("");
                        jsonData=gson.toJson(sd);
                        break;
                    case "ELECTRICITY_BILL":
                        ByteArrayOutputStream ele = new ByteArrayOutputStream();
                        ELECTRICITY_BILL.compress(Bitmap.CompressFormat.JPEG, 100, ele);
                        byte[] eleb = ele .toByteArray();
                        sd.setId(CUSTOMER_ID);
                        sd.setDocumentType("ELECTRICITY_BILL");
                        sd.setDocSide("FRONT");
                       // sd.setFrontImage("");
                        sd.setFrontImage(Base64.encodeToString(eleb,Base64.NO_WRAP));
                        sd.setBackImage("");
                        jsonData=gson.toJson(sd);
                        break;



                }


                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                OkHttpClient client = new OkHttpClient();
                RequestBody body = RequestBody.create(JSON, jsonData);
                Request request = new Request.Builder()
                        .url(END_POINT+"customer/saveDocumentDetails")
                        .post(body)
                        .build();

                Response response = client.newCall(request).execute();
                String resData=response.body().string();
                Log.d("response",resData);
                return  resData;


            } catch (IOException e) {
                e.printStackTrace();
                return null;

            }catch (RuntimeException r){
                r.printStackTrace();
                return null;
            }
        }
    }
    private class VerifyUserDocumentPAN extends  AsyncTask<String,String,String >{
        String pId,pname,PAN_DOB;

        public VerifyUserDocumentPAN(String pId, String pname, String pDob) {
            this.pId = pId;
            this.pname = pname;
            this.PAN_DOB = pDob;
        }

        @Override
        protected void onPostExecute(String s) {
            if(s!=null){
                if(s.equals("SUCCESS")){
                    new SendErrorFlags("GOVT_VERIFICATION_PAN","1").execute();
                    GOVT_VERIFICATION_PAN=1;

                }else if(s.equals("FAIL")){
                    new SendErrorFlags("GOVT_VERIFICATION_PAN","2").execute();
                    GOVT_VERIFICATION_PAN=2;
                }

            }
        }

        @Override
        protected String doInBackground(String... strings) {

            headers h = new headers();
            h.setClient_code(CLIENT_CODE);
            h.setSub_client_code("dummy");
            h.setChannel_code("ANDROID_SDK");
            h.setChannel_version("1.0.0");
            h.setTransmission_datetime("1428393481435");
            h.setOperation_mode("SELF");
            h.setRun_mode("TRIAL");
            h.setActor_type("OTHER");
            h.setUser_handle_type("EMAIL");
            h.setUser_handle_value("a@b.com");
            h.setFunction_code("VERIFY_PAN");
            h.setFunction_sub_code("DATA");
            h.setStan(getRequestId());

            PanDetails pd = new PanDetails();
            pd.setDob(PAN_DOB);
            pd.setName(pname);
            pd.setPan_number(pId);
            pd.setDocument("");
            pd.setPan_type("IND");

            PanRequestPayload prp= new PanRequestPayload();
            prp.setPan_details(pd);

            VerifyUserIdDocRequestPayload payload = new VerifyUserIdDocRequestPayload();
            payload.setHeaders(h);
            payload.setRequest(prp);

            try{

                String jsonData=gson.toJson(payload);
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                OkHttpClient client = new OkHttpClient();
                RequestBody body = RequestBody.create(JSON, jsonData);
                Request request = new Request.Builder()
                        .url(KHOSALA_URL+"service/api/2.0/verifyUserIdDoc")
                        .post(body)
                        .build();

                Response response = client.newCall(request).execute();
                Object o=gson.fromJson(response.body().string(),Object.class);
                Log.i("PAN_CHECK",gson.toJson(o));
                return ((LinkedTreeMap) o).get("verification_status").toString();


            }catch (Exception e){
                e.printStackTrace();
                return null;
            }

        }
    }


    private class VerifyUserDocumentVoter extends  AsyncTask<String,String,String >{
        String epicNumber;

        public VerifyUserDocumentVoter(String epicNumber) {
            this.epicNumber=epicNumber;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(s!=null){
                if(s.equals("SUCCESS")){
                    new SendErrorFlags("GOVT_VERIFICATION_VOTER","1").execute();
                    GOVT_VERIFICATION_VOTER=1;

                }else if(s.equals("FAIL")){
                    new SendErrorFlags("GOVT_VERIFICATION_VOTER","2").execute();
                    GOVT_VERIFICATION_VOTER=2;
                }

            }
        }

        @Override
        protected String doInBackground(String... strings) {

            headers h = new headers();
            h.setClient_code(CLIENT_CODE);
            h.setSub_client_code("dummy");
            h.setChannel_code("ANDROID_SDK");
            h.setChannel_version("1.0.0");
            h.setTransmission_datetime("1428393481435");
            h.setOperation_mode("SELF");
            h.setRun_mode("TRIAL");
            h.setActor_type("OTHER");
            h.setUser_handle_type("EMAIL");
            h.setUser_handle_value("a@b.com");
            h.setFunction_code("VERIFY_VOTER_ID");
            h.setFunction_sub_code("DATA");
            h.setStan(getRequestId());

            RequestVoterIdDetail rvd = new RequestVoterIdDetail();
            rvd.setEpic_number(epicNumber);
            rvd.setDocument("");

            VoterIdPayload vip = new VoterIdPayload();
            vip.setVoter_id_details(rvd);


            VerifyVoterIdRequestPayload payload = new VerifyVoterIdRequestPayload();
            payload.setHeaders(h);
            payload.setRequest(vip);

            try{

                String jsonData=gson.toJson(payload);
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                OkHttpClient client = new OkHttpClient();
                RequestBody body = RequestBody.create(JSON, jsonData);
                Request request = new Request.Builder()
                        .url(KHOSALA_URL+"service/api/2.0/verifyUserIdDoc")
                        .post(body)
                        .build();

                Response response = client.newCall(request).execute();
                Object o=gson.fromJson(response.body().string(),Object.class);
                Log.i("VOTER_CHECK",gson.toJson(o));
                return ((LinkedTreeMap) o).get("verification_status").toString();

            }catch (Exception e){
                e.printStackTrace();
                return null;
            }

        }
    }
    private class GetClassList extends AsyncTask<String,String,String >{

        ClassInSchool classInSchool;



        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"/org/listClasses/"+SCHOOL_ID)
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String resData=response.body().string();

                classInSchool = gson.fromJson(resData, ClassInSchool.class);
                return null;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }
    private class GetDocumentDetails extends AsyncTask<String,String,String >{

        GetDocumentDetails getDocumentDetails;
        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"customer/getDocumentDetails/"+CUSTOMER_ID+"/"+DOC_TYPE)
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String resData=response.body().string();

                getDocumentDetails = gson.fromJson(resData, GetDocumentDetails.class);
                return null;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }
    private class GetLoanStatus extends AsyncTask<String,String ,String >{

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"customer/customerLoanStatus/"+CUSTOMER_ID+"/"+APPLICATION_ID)
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String resData=response.body().string();
                return null;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }
    private class SaveLoanDetails extends AsyncTask<String,String ,String >{


        int loanValue;
        LoanRequest loanRequest;

        public SaveLoanDetails(int loanValue) {
            this.loanValue = loanValue;
        }

        @Override
        protected String doInBackground(String... strings) {
//            if(paymentFrequency.isChecked()){
//                frequency=1;
//            }else {
//                frequency=0;
//            }
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"customer/customerLoanRequest/"+APPLICATION_ID+"/"+loanValue+"/"+TENURE+"/"+"1")
                    .build();

            try{
                Log.i("URL",request.url().toString());
                Response response = client.newCall(request).execute();
                String resData=response.body().string();
                loanRequest = gson.fromJson(resData,LoanRequest.class);
                Log.i("EMI",resData);
                return null;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }
    private class GetVoucherDetails extends AsyncTask<String ,String ,String >{

        CustomerVoucher customerVoucher;
        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"customer/getCustomerVouchers/"+CUSTOMER_ID)
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String resData=response.body().string();

                customerVoucher = gson.fromJson(resData,CustomerVoucher.class);
                return null;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }
    private class SaveLivelinessResult extends AsyncTask<String,String ,String >{



        @Override
        protected String doInBackground(String... strings) {
            try {
                LivelinessReq livelinessReq= new LivelinessReq();
                livelinessReq.setCustomerId(CUSTOMER_ID);
                livelinessReq.setImage(USER_FACE);
                livelinessReq.setMatchValue(matchedValue);

                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                OkHttpClient client = new OkHttpClient();
                String jsonData=gson.toJson(livelinessReq);
                RequestBody body = RequestBody.create(JSON, jsonData);
                Request request = new Request.Builder()
                        .url(END_POINT+"customer/saveCustomerLiveliness")
                        .post(body)
                        .build();

                Response response = client.newCall(request).execute();
                String resData=response.body().string();
                return  resData;


            } catch (IOException e) {
                e.printStackTrace();
                return null;

            }catch (RuntimeException r){
                r.printStackTrace();
                return null;
            }
        }
    }
    private class GetCustomerLiveliness extends AsyncTask<String ,String ,String >{

        GetLivelinessResult getLivelinessResult;
        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"customer/getCustomerLiveliness/"+CUSTOMER_ID)
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String resData=response.body().string();

                getLivelinessResult = gson.fromJson(resData,GetLivelinessResult.class);
                return null;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }
    private class GetAllLoanDetails extends  AsyncTask<String,String,String >{
        customerAllLoanDetailsData caldd;




        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


            maxEligibleAmount= parseFloat(((LinkedTreeMap) caldd.data.get(0)).get("maxLoanEligible").toString());
            eligibileLoanBar.setMax(maxEligibleAmount);
            eligibileLoanBar.setProgress(maxEligibleAmount/2);
            seekBarUpperLimit.setText("\u20B9 "+String.valueOf(maxEligibleAmount));
            emiRateFindeed.setText("\u20B9 "+emiRate+"/Month");
            presenter.botMessage("Congratulations! You are eligible for a loan amount of upto Rs."+maxEligibleAmount,null,ChatPresenter.chatObjects.size()+1);
            presenter.botMessage("Here is a summary based on your earlier selection. You can change the required loan amount, tenure and time of payment to see your EMI.",frameLayout,ChatPresenter.chatObjects.size()+1);


        }

        @Override
        protected String doInBackground(String... strings) {

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"customer/customerAllLoanDetails/"+CUSTOMER_ID)
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String resData=response.body().string();

                caldd = gson.fromJson(resData, customerAllLoanDetailsData.class);
                return null;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }

        }
    }
    public static float calculateEmi(float p, float r, float t) {
        float emi;

        r = r / (12 * 100); // one month interest
        //  r=41.7/100;

        // emi=  (p * r/12) * [(1+r/12) ^t] / [(1+r/12) ^t-1];

        emi = (p * r/12 * (float)Math.pow(1 + r, t))
                / (float)(Math.pow(1 + r, t) - 1);

        return (emi);
    }


    public static float getEMI(float principleAmount,float tenure) {
        float intrest=(principleAmount*2*tenure)/100;
        return Math.round((principleAmount+intrest)/tenure);
    }



    private class SaveUserAddress extends AsyncTask<String,String,String >{

        String doc_type;

        public SaveUserAddress(String doc_type) {
            this.doc_type = doc_type;
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            new GetCustomerDetails().execute();
        }

        @Override
        protected String doInBackground(String... strings) {

            SaveCustomerAddressRequest saveCustomerAddressRequest= new SaveCustomerAddressRequest();


            try {
                if(isCurrentAddressSame){
                    if(doc_type.equals("AADHAAR")){
                        saveCustomerAddressRequest.setCustomerId(String.valueOf(CUSTOMER_ID));
                        saveCustomerAddressRequest.setAddressLine1(AADHAR_ADDRESS);
                        Log.i("AADHAAR_ADDRESS",AADHAR_ADDRESS);
                        saveCustomerAddressRequest.setAddressLine2("");
                        saveCustomerAddressRequest.setArea("");
                        saveCustomerAddressRequest.setCity("");
                        saveCustomerAddressRequest.setZipcode("");
                        saveCustomerAddressRequest.setLatitude(0.0);
                        saveCustomerAddressRequest.setLongitude(0.0);
                    }else if(doc_type.equals("VOTER_ID")) {
                        saveCustomerAddressRequest.setCustomerId(String.valueOf(CUSTOMER_ID));
                        saveCustomerAddressRequest.setAddressLine1(VOTER_ADDRESS);
                        saveCustomerAddressRequest.setAddressLine2("");
                        saveCustomerAddressRequest.setArea("");
                        saveCustomerAddressRequest.setCity("");
                        saveCustomerAddressRequest.setZipcode("");
                        saveCustomerAddressRequest.setLatitude(0.0);
                        saveCustomerAddressRequest.setLongitude(0.0);

                    }
                }else {
                    saveCustomerAddressRequest.setCustomerId(String.valueOf(CUSTOMER_ID));
                    saveCustomerAddressRequest.setAddressLine1(ADDRESS_LINE_1);
                    saveCustomerAddressRequest.setAddressLine2("");
                    saveCustomerAddressRequest.setArea(LOCALITY);
                    saveCustomerAddressRequest.setCity(CITY);
                    saveCustomerAddressRequest.setZipcode(PINCODE);
                    saveCustomerAddressRequest.setLatitude(latitude);
                    saveCustomerAddressRequest.setLongitude(longitude);
                }



                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                OkHttpClient client = new OkHttpClient();
                String jsonData=gson.toJson(saveCustomerAddressRequest);
                Log.i("CAddress",jsonData);
                RequestBody body = RequestBody.create(JSON, jsonData);
                Request request = new Request.Builder()
                        .url(END_POINT+"customer/saveAddress")
                        .post(body)
                        .build();

                Response response = client.newCall(request).execute();
                String resData=response.body().string();
                Log.i("cAddressRes",resData);
                return  resData;


            } catch (IOException e) {
                e.printStackTrace();
                return null;

            }catch (RuntimeException r){
                r.printStackTrace();
                return null;
            }
        }
    }




    public class createCustomerLoan extends AsyncTask<String,String,String>{


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            new GetCustomerDetails().execute();
        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"customer/createCustomerLoan/"+CUSTOMER_ID+"/"+LOAN_TYPE)
                    .get()
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String resData=response.body().string();
                CreateLoanData cld = gson.fromJson(resData,CreateLoanData.class);
                APPLICATION_ID=cld.getData();
                return resData;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }
    public  class  SaveCustomerName extends AsyncTask<String ,String ,String >{
        String fName ,lname;
        int cId;
        String url;

        public SaveCustomerName(String fName, String lname, int cId) {
            this.fName = fName;
            this.lname = lname;
            this.cId = cId;
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            new GetCustomerDetails().execute();
        }

        @Override
        protected String doInBackground(String... strings) {

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"customer/saveCustomerName/"+cId+"/"+fName+"/"+lname)
                    .get()
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String resData=response.body().string();
                return resData;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }

    }
    public class SaveEmployeeId extends AsyncTask<String ,String ,String >{

        String eId;
        int cId;

        public SaveEmployeeId(String eId, int cId) {
            this.eId = eId;
            this.cId = cId;
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            new GetCustomerDetails().execute();
        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"customer/saveCustomerEmployeeId/"+cId+"/"+eId)
                    .get()
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String resData=response.body().string();
                return resData;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }
    public class SaveWorkingYear extends AsyncTask<String ,String ,String >{

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            new GetCustomerDetails().execute();
        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"customer/saveCustomerWorkingYears/"+CUSTOMER_ID+"/"+EMPLOYMENT_AGE)
                    .get()
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String resData=response.body().string();
                return resData;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }
    public class SaveMaritalStatus extends AsyncTask<String ,String ,String >{


        String status;

        public SaveMaritalStatus(String status) {
            this.status = status;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            MARITAL_STATUS="";
        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"customer/saveCustomerMaritalStatus/"+CUSTOMER_ID+"/"+status)
                    .get()
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String resData=response.body().string();
                return resData;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }
    public class GetSchoolList extends AsyncTask<String, String ,String>{
        ArrayList<SchoolData> sd;


        @Override
        protected void onPostExecute(String s) {
            Map<String, String> map = new HashMap<>();
            for(int i=0;i<sd.size();i++){

                map.put(sd.get(i).getSchoolName(),sd.get(i).getAddressLine1()+","+sd.get(i).getZipcode());
                SchoolId.put(sd.get(i).getSchoolName(),sd.get(i).getSchoolId());
            }


//            map.put("Amity Public School","Bangalore, 664521");
//            map.put("Abingdon High School","Pune, 110235");
//            map.put("Albion High School","Petersburg,548975");
//            map.put("Bellmont High School","Delhi, 110025");
//            map.put("Bowen High School","Mangol, 845789");
//            map.put("Burlington High School","Sparta, 214578");
//            map.put("Fairbury-Cropsey HS","Guana, 5894789");
//            map.put("Flat Rock High School","Vanta, 5484566");
//            map.put("Fairy Tale High School","Gotham, 5484566");
//            map.put("Royal High School","Los Angeles, 113311");
//            map.put("Happy public school","Mumbai, 445874");
//            map.put("Dayanand Public School","Lisbon, 234798");
//            map.put("Delhi Public School","Hyderabad, 512369");
//            map.put("Blue Bells Public School","Pune, 587469");
//            map.put("Zeaneth Public School","Vancouver, 125485");


            listv = findViewById(R.id.listview);
            for (Map.Entry<String, String> entry : map.entrySet()) {
                arraylist.add(new SchoolNames(entry.getKey(), entry.getValue()));

            }

            final ListViewAdapter adapter = new ListViewAdapter(ctx, arraylist);
            listv.setAdapter(adapter);



            actv.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


                }
                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    lvlay.setVisibility(View.VISIBLE);
                    btnActvClear.setVisibility(View.VISIBLE);
                    adapter.filter(charSequence.toString());
                }
                @Override
                public void afterTextChanged(Editable editable) {

                  /*  if(editable.length() > 2) {
                        if (!actv.isPopupShowing()) {
                            Toast.makeText(getApplicationContext(),"No results", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }*/

                }
            });

            listv.setOnItemClickListener((parent, view, position, id) -> {

                schoolPos = position;
                SCHOOL_ID=SchoolId.get(arraylist.get(position).getSchoolname()).intValue();
                new SendSchoolLocation().execute();
            });



            btnActvClear.setOnClickListener(view -> actv.setText(""));
        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"org/getSchools")
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String resData=response.body().string();

                com.findeed.API.GetSchoolList getList = gson.fromJson(resData, com.findeed.API.GetSchoolList.class);
                sd=getList.getSchoolData();
                return null;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }

        }
    }
    public class GetCompanyList extends AsyncTask<String, String ,String>{


        @Override
        protected void onPostExecute(String s) {

            companyList =new ArrayList<>();
            for(int i=0;i<companyNameList.getData().size();i++){

                companyList.add(i,new CompaniesNames(companyNameList.getData().get(i).getEmployerName(),companyNameList.getData().get(i).getAddressLine1()));
                companyId.put(companyNameList.getData().get(i).getEmployerName(),companyNameList.getData().get(i).getEmployerId());
            }
            listViewCompaniesAdapter = new ListViewCompaniesAdapter(ChatActivity.this,companyList);
            companyListView.setAdapter(listViewCompaniesAdapter);

        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"org/listEmployers")
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String resData=response.body().string();

                companyNameList = gson.fromJson(resData, com.findeed.API.CompanyNameData.class);
                return "Sucess";
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }

        }
    }
    public class AddEmployer extends AsyncTask<String,String,String > {
        String name,area,city;

        public AddEmployer(String name, String city, String area) {
            this.name = name;
            this.city = city;
            this.area = area;

        }

        @Override
        protected void onPostExecute(String s) {


            ChatPresenter.chatObjects.subList(ChatPresenter.chatObjects.size()-1,ChatPresenter.chatObjects.size()).clear();
            chatAdapter.notifyDataSetChanged();
            presenter.botMessage("We have received your request to add employer. We will get in touch when the employer is added to the list.",null,ChatPresenter.chatObjects.size()+1);


        }

        @Override
        protected String doInBackground(String... strings) {
            AddNewEmployerRequest aner = new AddNewEmployerRequest();
            aner.setEmployerName(name);
            aner.setArea(area);
            aner.setCity(city);
            aner.setAddressLine1("");
            aner.setAddressLine2("");
            aner.setContactName("");
            aner.setZipcode("");
            String jsonData=gson.toJson(aner);
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            OkHttpClient client = new OkHttpClient();
            RequestBody body = RequestBody.create(JSON, jsonData);
            Request request = new Request.Builder()
                    .url(END_POINT+"org/createEmployer")
                    .post(body)
                    .build();
            try{
                Response response = client.newCall(request).execute();
                String resData=response.body().string();

                return resData;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }

    }
    public class CreateSchool extends AsyncTask<String,String,String > {


        @Override
        protected void onPostExecute(String s) {

            Bitmap icon = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                    R.drawable.school_static_image);
            schoolName=etname.getText().toString();
            presenter.setImage(icon,llAddSchoolRequest,ChatPresenter.chatObjects.size()+1,schoolName);
            lvlay.setVisibility(View.GONE);
            presenter.botMessage("We have received your request. We will get back to you when the school is listed with us.",null,-123);
            // presenter.botMessage("How many of your children study in " + etname.getText().toString(),llayoutchildcount,ChatPresenter.chatObjects.size()+1);


        }

        @Override
        protected String doInBackground(String... strings) {
            AddNewSchool as = new AddNewSchool();
            as.setSchoolName(etname.getText().toString());
            as.setArea(etarea.getText().toString());
            as.setCity(etcity.getText().toString());
            String jsonData=gson.toJson(as);
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            OkHttpClient client = new OkHttpClient();
            RequestBody body = RequestBody.create(JSON, jsonData);
            Request request = new Request.Builder()
                    .url(END_POINT+"org/createSchool")
                    .post(body)
                    .build();
            try{
                Response response = client.newCall(request).execute();
                String resData=response.body().string();

                return resData;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }

    }
    public  class  NumberOfChildren extends AsyncTask<String ,String, String >{

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            new GetCustomerDetails().execute();
        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"customer/numberOfChildren/"+CUSTOMER_ID+"/"+NUMBER_OF_CHILDREN)
                    .get()
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String resData=response.body().string();
                //   NumberOfChildren nc =gson.fromJson(resData,   NumberOfChildren.class);
                return resData;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }

        }
    }
    public class LoanRequirement extends AsyncTask<String, String ,String >{

        String amount;

        public LoanRequirement(String amount) {
            this.amount = amount;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            new GetCustomerDetails().execute();
        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"customer/loanAmount/"+APPLICATION_ID+"/"+amount)
                    .build();
            try{
                Response response = client.newCall(request).execute();
                String resData=response.body().string();
                LoanAmount la = gson.fromJson(resData,LoanAmount.class);
                return resData;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }

        }
    }
    public class EmploymentType extends AsyncTask<String, String ,String >{



        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            new GetCustomerDetails().execute();
        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"customer/employmentType/"+CUSTOMER_ID+"/"+EMPLOYMENT_TYPE)
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String resData=response.body().string();
                return resData;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }
    private class SaveLoanTenure extends AsyncTask<String,String ,String >{


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            tempTenure=TENURE;
            // new GetEmiRate(tempTenure,selectedLoanAmount).execute();
            TENURE=0;
            showTenure.setText("Select Loan Tenure");
            LoanTenure=null;
            new GetCustomerDetails().execute();

        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"customer/saveCustomerLoanTenure/"+APPLICATION_ID+"/"+TENURE)
                    .build();

            try{
                Response response = client.newCall(request).execute();
                Log.d("APPLICATION ID",APPLICATION_ID.toString());
                String resData=response.body().string();
                return resData;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }
    class GetEmiRate extends AsyncTask<String,String ,String > {
        int t,p;
        public GetEmiRate(int tenure, int selectedLoanAmount) {
            this.t=tenure;
            this.p=selectedLoanAmount;
        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    /*loanId,principelAmount,tenure,frequency*/
                    .url(END_POINT+"customer/customerLoanRequest/"+APPLICATION_ID+"/"+p+"/"+t+"/"+"1")
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String resData=response.body().string();
                LoanRequest lr = gson.fromJson(resData,LoanRequest.class);
                EMI_RATE=Math.round(valueOf(lr.getData().getEmiRate()));
                return resData;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }
    public class SaveChildSchool extends AsyncTask<String,String,String>{


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            new GetCustomerDetails().execute();
        }

        @Override
        protected String doInBackground(String... strings) {

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"customer/childSchool/"+CUSTOMER_ID+"/"+SCHOOL_ID)
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String resData=response.body().string();
                SaveChildSchoolResponse saveChildSchoolResponse =gson.fromJson(resData,SaveChildSchoolResponse.class);

                return resData;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }
    public class SaveChildDetails extends AsyncTask<String, String, String >{

        int classNumber;
        int noc;
        String fName,lName;
        SaveChildResponse saveChildResponse;




        public SaveChildDetails(int numberOfChildren,String fName,String lName) {
            this.noc = numberOfChildren;
            this.fName=fName;
            this.lName=lName;

        }



        @Override
        protected void onPostExecute(String s) {
            try{
                if(s!=null && saveChildResponse!=null){
                    mine++;
                    presenter.addChildDetail(saveChildResponse.getData().getFirstName()+ " "+saveChildResponse.getData().getLastName(),null,ChatPresenter.chatObjects.size()+1,true,saveChildResponse.getData().getCustomerDependentId());
                    if(saveChildResponse.getData().getInClass()==null){
                        presenter.addChildDetail(className,null,ChatPresenter.chatObjects.size()+1,false,0);

                    }else
                        presenter.addChildDetail(saveChildResponse.getData().getInClass(),null,ChatPresenter.chatObjects.size()+1,false,0);
                    if(mine==noc){
                        childLayout.setVisibility(View.GONE);
                        hideKeyboard();
                        presenter.botMessage("How much loan do you require ?",llayoutseekbar,ChatPresenter.chatObjects.size()+1);
                        mine=0;
                    }else {
                        switch (mine){

                            case 1:
                                addChild.setBackgroundResource(R.drawable.shape_btn_addchild);
                                firstname2.setText("");
                                surname2.setText("");
                                selectChildClass.setText("");
                                childHeader.setText("Second Child");
                                firstname2.requestFocus();

                                presenter.botMessage("Can you provide me the details of second child?",childLayout,ChatPresenter.chatObjects.size()+1);
                                break;
                            case 2:
                                addChild.setBackgroundResource(R.drawable.shape_btn_addchild);
                                firstname2.setText("");
                                surname2.setText("");
                                selectChildClass.setText("");
                                childHeader.setText("Third Child");
                                firstname2.requestFocus();
                                presenter.botMessage("Can you provide me the details of third child?",childLayout,ChatPresenter.chatObjects.size()+1);
                                break;
                            case 3:
                                addChild.setBackgroundResource(R.drawable.shape_btn_addchild);
                                firstname2.setText("");
                                surname2.setText("");
                                selectChildClass.setText("");
                                childHeader.setText("Fourth Child");
                                firstname2.requestFocus();
                                presenter.botMessage("Can you provide me the details of fourth child?",childLayout,ChatPresenter.chatObjects.size()+1);
                                break;
                            case 4:
                                addChild.setBackgroundResource(R.drawable.shape_btn_addchild);
                                firstname2.setText("");
                                surname2.setText("");
                                selectChildClass.setText("");
                                childHeader.setText("Fifth Child");
                                firstname2.requestFocus();
                                presenter.botMessage("Can you provide me the details of fifth child?",childLayout,ChatPresenter.chatObjects.size()+1);
                                break;
                        }
                    }
                    new GetCustomerDetails().execute();
                }
                else {
                    Toast.makeText(ChatActivity.this, "Couldn't save child detail something went wrong!", Toast.LENGTH_SHORT).show();
                }


            }catch (Exception e){
                e.printStackTrace();
            }



        }

        @Override
        protected String doInBackground(String... strings) {

            if(className.equals("10th Standard")){
                classNumber=Integer.parseInt(className.substring(0,2));
            }else {
                classNumber= Integer.parseInt(className.substring(0,1));
            }
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"customer/saveChildDetails/"+CUSTOMER_ID+"/"+0+"/"+fName+"/"+lName+"/"+SCHOOL_ID+"/"+classNumber)
                    .get()
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String resData=response.body().string();
                saveChildResponse =gson.fromJson(resData,SaveChildResponse.class);
                return resData;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }
    public  class GetDocumentData extends AsyncTask<String,String ,String >{


        SaveDocumentResponse sdr;


        @Override
        protected void onPostExecute(String s) {
            lLayoutProcessingPan.setVisibility(View.GONE);
            llayoutprocessingvoterid.setVisibility(View.GONE);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            Log.i("SSSSSS",s);
            if(s.equals("Passed")){
                if(DOC_TYPE.equals("PAN")){
                    new SendErrorFlags("PAN_OCR_FAILED","2").execute();
                    panNumber=sdr.getData().getDocumentId();
                    panName=sdr.getData().getFirstName();
                    panDob=sdr.getData().getDateOfBirth();
                    PAN_DOB=panDob;
                    if(isNullOrBlank(panName) || isNullOrBlank(panDob) || isNullOrBlank(panNumber)){
                        panIdEditText.setText(panNumber);
                        panNameEditText.setText(panName);
                        panDobEditText.setText(panDob);
                        panCross.setVisibility(View.INVISIBLE);
                        presenter.botMessage("We were not able to read data from the image you provided, Please fill your PAN details manually",lLayoutUpdatePanInfo,ChatPresenter.chatObjects.size()+1);

                    }else {
                        SimpleDateFormat format = new SimpleDateFormat("dd/mm/yyyy");
                        try {
                            Date date = format.parse(PAN_DOB);
                            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Calcutta"));
                            cal.setTime(date);
                            USER_AGE=cal.get(Calendar.YEAR);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        new VerifyUserDocumentPAN(panNumber,panName,panDob).execute();
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        presenter.botMessage("Here are your details in the PAN card",null,ChatPresenter.chatObjects.size()+1);
                        presenter.botMessage("PAN ID: "+panNumber+"\n"+"Name: "+panName+"\n"+"Date of Birth: "+panDob,lLayoutEditPanInfo,ChatPresenter.chatObjects.size()+1);
                        if(LOAN_TYPE==1){
                            USER_NAME=panName;
                            new SaveCustomerName(USER_NAME, "", CUSTOMER_ID).execute();
                        }
                    }


                }
                else {
                    new SendErrorFlags("VOTER_OCR_FAILED","2").execute();
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    presenter.botMessage("Here are your details in the Voter's ID ",null,ChatPresenter.chatObjects.size()+1);
                    VOTER_ADDRESS=sdr.getData().getAddress();
                    VOTER_DOCUMENT_ID=sdr.getData().getDocumentId();
                    VOTER_FIRST_NAME=sdr.getData().getFirstName();
                    VOTER_LAST_NAME=sdr.getData().getLastName();
                    VOTER_DOB=sdr.getData().getDateOfBirth();
                    VOTER_GENDER=sdr.getData().getGender();
                    VOTER_DOB=null;
                    if(isNullOrBlank(VOTER_ADDRESS)||isNullOrBlank(VOTER_DOCUMENT_ID) || isNullOrBlank(VOTER_FIRST_NAME) ||isNullOrBlank(VOTER_DOB)){
                       voterIdEditText.setText(VOTER_DOCUMENT_ID);
                       voterNameEditText.setText(VOTER_FIRST_NAME);
                       voterDobEditText.setText(VOTER_DOB);
                       voterAddressEditText.setText(VOTER_ADDRESS);
                       clearVoterEdit.setVisibility(View.INVISIBLE);
                        presenter.botMessage("We were not able to read data from the image you provided, Please fill your Voter's Id details manually",llUpdateVoterInfo,ChatPresenter.chatObjects.size()+1);

                    }else {

                        new VerifyUserDocumentVoter(VOTER_DOCUMENT_ID).execute();
                        presenter.botMessage("Full Name: "+VOTER_FIRST_NAME+"\n"+"Gender-"+VOTER_GENDER+"\n"+"Date of Birth - "+VOTER_DOB+"\n"+"Address -"+VOTER_ADDRESS,llayoutvoterdetailedit,ChatPresenter.chatObjects.size()+1);
                        if(VOTER_ADDRESS!=null && !VOTER_ADDRESS.equals("-"))
                            isPinMatch=findAndMatchPin(VOTER_ADDRESS);
                    }


                }


                new GetCustomerDetails().execute();

            }else {
                if(DOC_TYPE.equals("PAN")){
                    panCross.setVisibility(View.GONE);
                    new SendErrorFlags("PAN_OCR_FAILED","1").execute();
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    Toast.makeText(ctx,"Document OCR failed!",Toast.LENGTH_LONG).show();
                    presenter.botMessage("We were not able to read data from the image you provided, Please fill your PAN details manually",lLayoutUpdatePanInfo,ChatPresenter.chatObjects.size()+1);

                }else {
                    new SendErrorFlags("VOTER_OCR_FAILED","1").execute();
                    clearVoterEdit.setVisibility(View.GONE);
                    llayoutprocessingvoterid.setVisibility(View.GONE);
                    Toast.makeText(ctx,"Document OCR failed!",Toast.LENGTH_LONG).show();
                    presenter.botMessage("We were not able to read data from the image you provided, Please fill your Voter's Id details manually",llUpdateVoterInfo,ChatPresenter.chatObjects.size()+1);
                }
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try{
                SaveDocument sd =new SaveDocument();

                switch (DOC_TYPE) {
                    case "PAN":
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                        byte[] panByteArray = byteArrayOutputStream .toByteArray();
                        sd.setId(CUSTOMER_ID);
                        sd.setDocumentType("PAN");
                        sd.setDocSide("FRONT");
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                       // ByteArrayInputStream bis=new ByteArrayInputStream(panByteArray);
                        sd.setFrontImage(Base64.encodeToString(panByteArray,Base64.NO_WRAP));
                        sd.setBackImage("");
                        break;
                    case "VOTER_ID":
                        ByteArrayOutputStream voterOutputStream = new ByteArrayOutputStream();
                        VOTER_FRONT.compress(Bitmap.CompressFormat.JPEG, 100, voterOutputStream);
                        byte[] byteArray = voterOutputStream .toByteArray();
                        ByteArrayOutputStream byteArrayOutputStream1 = new ByteArrayOutputStream();
                        VOTER_BACK.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream1);
                        byte[] byteArray1 = byteArrayOutputStream1 .toByteArray();
                        sd.setId(CUSTOMER_ID);
                        sd.setDocumentType("VOTER_ID");
                        sd.setDocSide("BOTH");
                        sd.setFrontImage(Base64.encodeToString(byteArray,Base64.NO_WRAP));
                        sd.setBackImage(Base64.encodeToString(byteArray1,Base64.NO_WRAP));
                        break;
                }


                String jsonData= gson.toJson(sd);
//                int maxLogSize = 1000;
//                for(int i = 0; i <= jsonData.length() / maxLogSize; i++) {
//                    int start = i * maxLogSize;
//                    int end = (i+1) * maxLogSize;
//                    end = end > jsonData.length() ? jsonData.length() : end;
//                    Log.v("OCR_PAYLOAD", jsonData.substring(start, end));
//                }
//                //Log.i("OCR_PAYLOAD",jsonData);
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                RequestBody body = RequestBody.create(JSON,jsonData);
                OkHttpClient client =new OkHttpClient.Builder()
                        .connectTimeout(10, TimeUnit.SECONDS)
                        .writeTimeout(10, TimeUnit.SECONDS)
                        .readTimeout(30, TimeUnit.SECONDS)
                        .build();
                Request request = new Request.Builder()
                        .url(END_POINT+"customer/saveDocument")
                        .post(body)
                        .build();


                Response response = client.newCall(request).execute();
                String resData=response.body().string();
                Log.i("OCRDATA",resData);
                sdr =gson.fromJson(resData,SaveDocumentResponse.class);
                if(sdr.getStatusCode().equals("500") || sdr.getData().getDocumentId()==null){
                    return "Fail";
                }else{
                    return "Passed";
                }

            }catch (Exception e){
                e.printStackTrace();
                return "Fail";
            }

        }
    }
    private class SaveCustomerSalaryRange extends AsyncTask<String, String,String>{
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            new GetCustomerDetails().execute();
        }

        @Override
        protected String doInBackground(String... strings) {

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"customer/saveSalaryRange/"+CUSTOMER_ID+"/"+selectedValue)
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String resData=response.body().string();
                return resData;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }

        }
    }
    private class SaveCustomerVoucher extends AsyncTask<String, String,String>{

        String voucherName,voucherNumber;

        public SaveCustomerVoucher(String voucherName, String voucherNumber) {
            this.voucherName = voucherName;
            this.voucherNumber = voucherNumber;
        }

        @Override
        protected String doInBackground(String... strings) {

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"customer/customerVoucher/"+CUSTOMER_ID+"/"+voucherName+"/"+voucherNumber)
                    .build();
            try{
                Response response = client.newCall(request).execute();
                String resData=response.body().string();
                return resData;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }

        }
    }
    private class ExtractProcessedInformation extends AsyncTask<String,String ,String >{
        String userInfo;



        @Override
        protected void onPostExecute(String s) {

            if(s==null){
                ChatPresenter.chatObjects.subList(ChatPresenter.chatObjects.size()-2,ChatPresenter.chatObjects.size()).clear();
                ChatActivity.chatAdapter.notifyDataSetChanged();
                return;
            }else {
                presenter.botMessage("Thank you!,",null,ChatPresenter.chatObjects.size()+1);
                presenter.botMessage("Here are your details in aadhaar",null,ChatPresenter.chatObjects.size()+1);
                byte[] decodedString = Base64.decode(USER_FACE, Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                presenter.setBotMessageImage(decodedByte,null,ChatPresenter.chatObjects.size()+1);
                presenter.botMessage(userInfo,null,ChatPresenter.chatObjects.size()+1);
                boolean isAgeSame;
                Log.i("PAN_AADHAR_DOB",AADHAR_DOB+PAN_DOB);
                isAgeSame=matchAge(AADHAR_DOB,PAN_DOB);
                if(isAgeSame)
                    new SendErrorFlags("PAN_AADHAR_MATCH","1").execute();
                else
                    new SendErrorFlags("PAN_AADHAR_MATCH","2").execute();
                if(isAgeSame){
                    presenter.botMessage("Thank you  for providing the ID and Address proofs.", null, ChatPresenter.chatObjects.size() + 1);
                    presenter.botMessage("Please click \"I Agree\" to allow Findeed to share the details with the lending partners and check your credit information with the credit bureau.", llayoutagreeorcancel, ChatPresenter.chatObjects.size() + 1);

                }else {

                    presenter.botMessage("The date of birth you provided on the PAN did not match the address proof. Please check your details again and resubmit",null,ChatPresenter.chatObjects.size()+1);
                }
                DOC_TYPE="AADHAAR";
                new SaveDocumentDetails().execute();

            }


        }

        @Override
        protected String doInBackground(String... strings) {
            if(WebViewActivity.USER_ID==null || WebViewActivity.USER_ID.equals("")){
                return null;

            }else {
                String seq= CLIENT_CODE+"|"+WebViewActivity.USER_ID+"|"+ApplicationData.API_KEY+"|"+ApplicationData.SALT;
                String h=sha256(seq);
                MsiteFetchDataRequest msiteFetchDataRequest= new MsiteFetchDataRequest();
                headers h1 = new headers();
                h1.setClient_code(ApplicationData.CLIENT_CODE);
                h1.setSub_client_code(ApplicationData.CLIENT_CODE);
                h1.setActor_type("NA");
                h1.setChannel_code("MSITE");
                h1.setStan(WebViewActivity.STAN);
                h1.setRun_mode("REAL");
                h1.setFunction_code("REVISED");
                h1.setFunction_sub_code("DEFAULT");
                MsiteFetchRequest msiteFetchRequest = new MsiteFetchRequest();
                msiteFetchRequest.setApi_key(ApplicationData.API_KEY);
                msiteFetchRequest.setUser_id(WebViewActivity.USER_ID);
                msiteFetchRequest.setHash(h);
                msiteFetchDataRequest.setHeaders(h1);
                msiteFetchDataRequest.setRequest(msiteFetchRequest);

                try {
                    String jsonData = gson.toJson(msiteFetchDataRequest);
                    MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                    RequestBody body = RequestBody.create(JSON, jsonData);
                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .build();

                    Request request = new Request.Builder()
                            .url(KHOSALA_URL+"video-id-kyc/api/1.0/fetchKYCInfo")
                            .post(body)
                            .build();


                    Response response = client.newCall(request).execute();
                    String resData=response.body().string();
                    ResponseData responseData=gson.fromJson(resData,ResponseData.class);
                    String decoded = new String(Base64.decode(responseData.getResponse_data().getKyc_info(),0));
                    KYCInfo kyc_info =gson.fromJson(decoded,KYCInfo.class);
                    Log.i("AADHAAR_KYC_DATA",gson.toJson(kyc_info));
                    userInfo= "Aadhar Number: "+kyc_info.getOriginal_kyc_info().getDocument_id()+"\n"+"Name: "+kyc_info.getOriginal_kyc_info().getName()+"\n"+"Date of Birth:"+kyc_info.getOriginal_kyc_info().getDob()+"\n"+"Address:"+kyc_info.getOriginal_kyc_info().getAddress();
                    AADHAR_ADDRESS=kyc_info.getOriginal_kyc_info().getAddress();
                    AADHAR_DOC_ID=kyc_info.getOriginal_kyc_info().getDocument_id();
                    AADHAR_FIRST_NAME=kyc_info.getOriginal_kyc_info().getName();
                    AADHAR_LAST_NAME=AADHAR_FIRST_NAME.split(" ")[1];
                    AADHAR_DOB=kyc_info.getOriginal_kyc_info().getDob();
                    AADHAR_GENDER=kyc_info.getOriginal_kyc_info().getGender();
                    USER_FACE=kyc_info.getPhoto().getDocument_image().replaceAll("\n","");
                    if(AADHAR_ADDRESS!=null)
                        isPinMatch=findAndMatchPin(AADHAR_ADDRESS);
                    Log.i("pinMatchAadharExtracted",String.valueOf(isPinMatch));

                    return  "Sucess";
                }catch (Exception e){
                    e.printStackTrace();
                    return null;

                }

            }

        }
    }

    private class fetchSchoolListClass extends  AsyncTask<String,String ,String >{

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url("http://157.245.102.194:8080/los-qa/org/listClasses/1")
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String resData=response.body().string();
                fetchSchoolListClass fetchSchoolList = gson.fromJson(resData, fetchSchoolListClass.class);
                return null;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }

        }
    }

    private TextWatcher fillDetail = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {


        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            String firstName = firstname2.getText().toString().trim();
            String surName = surname2.getText().toString().trim();
            String abc = className;

            addChild.setEnabled((!firstName.isEmpty() && !surName.isEmpty()) && !abc.isEmpty());
            if(!firstName.isEmpty() && !surName.isEmpty() && !abc.isEmpty())
            {
                addChild.setBackgroundColor(Color.BLACK);
                addChild.setBackgroundResource(R.drawable.shape_btn_addchild_black);

            }
            else if(firstName.isEmpty() || surName.isEmpty() || abc.isEmpty())
            {
                addChild.setEnabled(false);
                addChild.setBackgroundColor(ContextCompat.getColor(ChatActivity.ctx,R.color.silverDark));
                addChild.setBackgroundResource(R.drawable.btn_gradient_style);
            }


        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };


    public  class TriggerCreditRule extends AsyncTask<String,String ,String >{

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"loan/runEngine/"+validateOTPResponse.getData().getPhoneNumber())
                    .build();
            try{
                Response response = client.newCall(request).execute();
                String r=response.body().string();
                Log.i("RunEngine",r);
                return r;

            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }
    public  class GetUpdatedData extends AsyncTask<String,String ,String >{

        Intent intent;

        public GetUpdatedData(Intent intent) {
            this.intent = intent;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            finish();
            startActivity(intent);


        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(ApplicationData.END_POINT+"customer/getCustomerDetails/"+validateOTPResponse.getData().getPhoneNumber())
                    .build();
            try{
                Response response = client.newCall(request).execute();
                ValidateOTPResponse vr=new Gson().fromJson(response.body().string(),ValidateOTPResponse.class);
                validateOTPResponse=vr;
                return response.body().string();

            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }

    public static   class GetCustomerDetails extends AsyncTask<String,String ,String >{



        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(ApplicationData.END_POINT+"customer/getCustomerDetails/"+validateOTPResponse.getData().getPhoneNumber())
                    .build();
            try{
                Response response = client.newCall(request).execute();
                ValidateOTPResponse vr=new Gson().fromJson(response.body().string(),ValidateOTPResponse.class);
                CUSTOMER_DETAILS=vr.getData();
                CUSTOMER_DETAILS.setSchool(vr.getData().getSchool());
                return response.body().string();

            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }


    @Override
    public void onBackPressed() {
        if(doubleBackToExitPressedOnce){
            // super.onBackPressed();
            Intent a = new Intent(Intent.ACTION_MAIN);
            a.addCategory(Intent.CATEGORY_HOME);
            finish();
            int pid = android.os.Process.myPid();
            android.os.Process.killProcess(pid);
            startActivity(a);
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please press BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 500);

    }


    class VouchForSomeOne extends AsyncTask<String,String ,String >{

        String phone;
        ProgressDialog vouch;

        public VouchForSomeOne(String phone) {
            this.phone = phone;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            vouch = new ProgressDialog(ChatActivity.this);
            vouch.setTitle("Please Wait...");
            vouch.setCancelable(false);
            vouch.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(s!=null){
                vouch.dismiss();
                if(ApplicationData.voucherResponseData.getData().size()==0){
                    Toast.makeText(ChatActivity.this, "You are not a voucher", Toast.LENGTH_SHORT).show();
                }else {
                    Intent intent = new Intent(ChatActivity.this,VoucherChatActivity.class);
                    ChatActivity.chatAdapter=null;
                    finish();
                    startActivity(intent);
                }

            }

        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(END_POINT+"voucher/getVoucherDetails/"+phone)
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String a=response.body().string();
                ApplicationData.voucherResponseData=gson.fromJson(a, VoucherResponseData.class);
                return ApplicationData.voucherResponseData.getMessage() ;

            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }







    private ServiceConnection serviceConnection = new ServiceConnection() {
        LocationService locationService;
        public void onServiceConnected(ComponentName className, IBinder service) {
            // This is called when the connection with the service has been
            // established, giving us the service object we can use to
            // interact with the service.  Because we have bound to a explicit
            // service that we know is running in our own process, we can
            // cast its IBinder to a concrete class and directly access it.
            String name = className.getClassName();

            if (name.endsWith("LocationService")) {
                locationService = ((LocationService.LocationServiceBinder) service).getService();

                locationService.startUpdatingLocation();
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            // Because it is running in our same process, we should never
            // see this happen.
            if (className.getClassName().equals("LocationService")) {
                locationService.stopUpdatingLocation();
                locationService = null;
            }
        }
    };


    @Override
    protected void onDestroy() {
        companyLayout.setVisibility(View.GONE);
        getIntent().setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        super.onDestroy();

    }


    @Override
    protected void onStart() {
        super.onStart();

    }


    @Override
    public boolean onTrackballEvent(MotionEvent event) {
        return super.onTrackballEvent(event);
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        companyLayout.setVisibility(View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }


    @Override
    protected void onPause() {
        super.onPause();

        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        activityManager.getMemoryInfo(mi);
        double availableMegs = mi.availMem / 0x100000L;
        double percentAvail = mi.availMem / (double)mi.totalMem * 100.0;
        Log.d("Total memory usase",String.valueOf(percentAvail));


    }

}
