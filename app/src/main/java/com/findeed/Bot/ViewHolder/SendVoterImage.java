package com.findeed.Bot.ViewHolder;

import android.view.View;

public class SendVoterImage extends ChatObject {

    View viewType;
    int pos;

    public View getViewType() {
        return viewType;
    }

    public void setViewType(View viewType) {
        this.viewType = viewType;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    @Override
    public int getType() {
        return ChatObject.VOTER_IMAGE;
    }
}
