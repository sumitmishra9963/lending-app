package com.findeed.Bot.ViewHolder;

import android.view.View;
import android.widget.ImageView;

import com.findeed.ApplicationData;
import com.findeed.Bot.ChatActivity;
import com.findeed.Bot.ChatPresenter;
import com.findeed.R;
import com.findeed.Voucher.VoucherChatActivity;

public class SendVoterImageVH extends BaseViewHolder {
    ImageView voterIdImage;
    ChatObject currentObject;
    public SendVoterImageVH(View itemView) {
        super(itemView);

        this.voterIdImage=itemView.findViewById(R.id.voter_id_image);
        listner(itemView);

    }



    @Override
    public void onBindView(ChatObject object) {
        voterIdImage.setImageBitmap(object.getVoterImage());
        this.currentObject=object;


    }

    private void listner(View itemView) {
        ImageView docEditButton = itemView.findViewById(R.id.edit_voterdoc);
        docEditButton.setOnClickListener(v -> {
            SendVoterImage botDocImage = (SendVoterImage) currentObject;
            int pos=botDocImage.getPos();
            if(ChatResponseVH.currentActiveView!=null)
                ChatResponseVH.currentActiveView.setVisibility(View.GONE);
            ChatPresenter.chatObjects.subList(pos-1,ChatPresenter.chatObjects.size()).clear();
            if(ApplicationData.isItVoucher){
                VoucherChatActivity.voucherAdapter.notifyDataSetChanged();
            }else {
                ChatActivity.chatAdapter.notifyDataSetChanged();

            }
        });


    }
}
