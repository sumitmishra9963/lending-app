package com.findeed.Bot.ViewHolder;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.findeed.ApplicationData;
import com.findeed.Bot.ChatActivity;
import com.findeed.Bot.ChatPresenter;
import com.findeed.Bot.SaveUserName;
import com.findeed.R;

import java.util.Objects;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class SaveUserNameVH extends BaseViewHolder {
    private Dialog bottomSheetDialog;
    SaveUserName user=null;
    int surNamePosition;

    TextView userName;
    ImageView edituserName;
    public SaveUserNameVH(View itemView) {
        super(itemView);
        this.userName=itemView.findViewById(R.id.user_name_personal);
        this.edituserName=itemView.findViewById(R.id.user_name_edit_personal);
        editListner();
    }



    @Override
    public void onBindView(ChatObject object) {

        userName.setText(object.getUserName());

    }


    private void editListner() {

        edituserName.setOnClickListener(v -> {


            String clickedText=userName.getText().toString();
            for(int i = 3; i< ChatPresenter.chatObjects.size(); i++){
                ChatObject temp= ChatPresenter.chatObjects.get(i);
                try{
                    if(temp.getUserName().equals(clickedText)){
                         user= (SaveUserName) ChatPresenter.chatObjects.get(i);
                         surNamePosition=i;
                        break;
                    }else
                        continue;
                }catch (NullPointerException e){
                    e.printStackTrace();
                }
            }

            try{
                assert user != null;
                if(user.isItFirstName()){
                     bottomSheetDialog = new Dialog(ChatActivity.chatActivity);
                     Objects.requireNonNull(bottomSheetDialog.getWindow()).setBackgroundDrawable(new
                            ColorDrawable(Color.TRANSPARENT));

                    bottomSheetDialog.setContentView(R.layout.user_name_input_edit);
                    bottomSheetDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                    Window window = bottomSheetDialog.getWindow();
                    window.setGravity(Gravity.CENTER);
                    bottomSheetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    EditText userFirstName=bottomSheetDialog.findViewById(R.id.user_first_name);
                    EditText userSurName=bottomSheetDialog.findViewById(R.id.user_sur_name);
                    Button updateDetails=bottomSheetDialog.findViewById(R.id.update_user_detail);

                    String [] userName=user.getUserName().split(":");
                    userFirstName.setText(userName[1]);
                    SaveUserName surName= (SaveUserName) ChatPresenter.chatObjects.get(surNamePosition+1);
                    String[] sName=surName.getUserName().split(":");
                    if(sName.length>=2)
                        userSurName.setText(sName[1]);

                    bottomSheetDialog.show();
                    updateDetails.setOnClickListener(v1 -> {

                        if(isNullOrBlank(userFirstName.getText().toString())){
                            Toast.makeText(ChatActivity.ctx, "Enter first name! ", Toast.LENGTH_SHORT).show();
                        }else {

                            if(!isAlpha(userFirstName.getText().toString())){
                                Toast.makeText(ChatActivity.ctx, "Please enter valid name", Toast.LENGTH_SHORT).show();

                            }else {

                                user.setUserName("First Name:"+userFirstName.getText().toString());
                                surName.setUserName("Surname:"+userSurName.getText().toString());
                                ChatActivity.chatAdapter.notifyDataSetChanged();
                                bottomSheetDialog.dismiss();
                                new UpdateUserName(userFirstName.getText().toString(),userSurName.getText().toString(),user.getCustomerId()).execute();

                            }
                        }
                    });


                }
            }catch (Exception e){
                e.printStackTrace();
            }




        });


    }
    public static boolean isNull(String str) {
        return str == null ? true : false;
    }
    public static boolean isNullOrBlank(String param) { if (isNull(param) || param.trim().length() == 0) { return true; }return false; }
    public boolean isAlpha(String name) { return name.matches("^[ a-zA-Z]*$"); }




    public  class  UpdateUserName extends AsyncTask<String ,String ,String > {
        String fName ,lname;
        int cId;


        public UpdateUserName(String fName, String lname, int cId) {
            this.fName = fName;
            this.lname = lname;
            this.cId = cId;
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            new ChatActivity.GetCustomerDetails().execute();
        }

        @Override
        protected String doInBackground(String... strings) {

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(ApplicationData.END_POINT+"customer/saveCustomerName/"+cId+"/"+fName+"/"+lname)
                    .get()
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String resData=response.body().string();
                return resData;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }

    }
}
