package com.findeed.Bot.ViewHolder;

import android.view.View;
import android.widget.ImageView;

import com.findeed.ApplicationData;
import com.findeed.Bot.ChatActivity;
import com.findeed.Bot.ChatPresenter;
import com.findeed.R;
import com.findeed.Voucher.VoucherChatActivity;

public class BotDocImageVH extends BaseViewHolder {
    ImageView docImage;
    ChatObject currentObject;


    public BotDocImageVH(View itemView) {
        super(itemView);
        this.docImage=itemView.findViewById(R.id.inputImageWithoutText);
        listner(itemView);

    }



    @Override
    public void onBindView(ChatObject object) {
        this.docImage.setImageBitmap(object.getDocBitMap());
        this.currentObject=object;


    }

    private void listner(View docView) {

    ImageView docEditButton = docView.findViewById(R.id.edit_document);
    docEditButton.setOnClickListener(v -> {
        try{
            BotDocImage botDocImage = (BotDocImage) currentObject;
            int pos=botDocImage.getPos();
            if(ChatResponseVH.currentActiveView!=null)
                    ChatResponseVH.currentActiveView.setVisibility(View.GONE);
            ChatPresenter.chatObjects.subList(pos-1,ChatPresenter.chatObjects.size()).clear();
            if(ApplicationData.isItVoucher){
                VoucherChatActivity.voucherAdapter.notifyDataSetChanged();
            }else {
                ChatActivity.chatAdapter.notifyDataSetChanged();
            }
        }catch (Exception e){
            e.printStackTrace();
        }




    });



    }
}
