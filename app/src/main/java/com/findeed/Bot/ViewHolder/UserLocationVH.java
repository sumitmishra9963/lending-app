package com.findeed.Bot.ViewHolder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.findeed.ApplicationData;
import com.findeed.Bot.ChatActivity;
import com.findeed.Bot.ChatPresenter;
import com.findeed.Bot.UserLocation;
import com.findeed.R;
import com.findeed.Voucher.VoucherChatActivity;

public class UserLocationVH extends BaseViewHolder {
    TextView userLocation;
    ImageView staticLocation;
   View locationView;
   UserLocation ul;



    public UserLocationVH(View itemView) {
        super(itemView);
        this.userLocation=itemView.findViewById(R.id.user_location);
        this.staticLocation=itemView.findViewById(R.id.static_location_image);
        this.locationView=itemView;
        clickListener();

    }


    @Override
    public void onBindView(ChatObject object) {

        if(ChatActivity.userLocation==null){
            userLocation.setText(VoucherChatActivity.userLocation);
        }else
        userLocation.setText(ChatActivity.userLocation);
        staticLocation.setImageBitmap(object.getLocationMap());
        ul = (UserLocation) object;

    }

    private void clickListener() {
    ImageView editButton = locationView.findViewById(R.id.location_edit_button);
    editButton.setOnClickListener(v -> {

        if(ChatResponseVH.currentActiveView!=null){
            ChatResponseVH.currentActiveView.setVisibility(View.GONE);
        }

        int t= ul.getPos();
        ChatPresenter.chatObjects.subList(ul.getPos()-2,ChatPresenter.chatObjects.size()).clear();
        if(ApplicationData.isItVoucher){
            VoucherChatActivity.voucherAdapter.notifyDataSetChanged();
        }else {
            ChatActivity.chatAdapter.notifyDataSetChanged();

        }
    });


    }
}
