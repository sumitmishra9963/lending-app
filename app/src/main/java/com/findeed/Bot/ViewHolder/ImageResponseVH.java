package com.findeed.Bot.ViewHolder;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.findeed.ApplicationData;
import com.findeed.Bot.ChatActivity;
import com.findeed.Bot.ChatInput;
import com.findeed.Bot.ChatPresenter;
import com.findeed.R;
import com.findeed.Voucher.VoucherChatActivity;

public class ImageResponseVH extends BaseViewHolder {
    private ImageView imageView;
    TextView sName;
    View imageInputView;
    ImageResponse ir ;


    public ImageResponseVH(View itemView) {
        super(itemView);
        this.imageView=itemView.findViewById(R.id.inputImage);
        this.sName=itemView.findViewById(R.id.school_name);
        this.imageInputView=itemView;
        listner();
    }


    @Override
    public void onBindView(ChatObject object) {


        ir= (ImageResponse) object;
        sName.setText(ir.getName());
        this.imageView.setImageBitmap(object.getImage());



    }
    public void listner(){

        ImageView  userMessageEdit=imageInputView.findViewById(R.id.school_edit_button);
        userMessageEdit.setOnClickListener(view -> {
           if(ChatResponseVH.currentActiveView!=null){
               ChatResponseVH.currentActiveView.setVisibility(View.GONE);
           }
            ir.getViewType().setVisibility(View.VISIBLE);
            ChatPresenter.chatObjects.subList(ir.getPos()-1,ChatPresenter.chatObjects.size()).clear();
            if(ApplicationData.isItVoucher){
                VoucherChatActivity.voucherAdapter.notifyDataSetChanged();
            }else {
                ChatActivity.chatAdapter.notifyDataSetChanged();

            }


        });


    }
}
