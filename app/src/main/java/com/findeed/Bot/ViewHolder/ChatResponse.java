package com.findeed.Bot.ViewHolder;

import android.view.View;

import com.findeed.Bot.ViewHolder.ChatObject;

public class ChatResponse extends ChatObject {

    public View getViewType() {
        return viewType;
    }

    public void setViewType(View viewType) {
        this.viewType = viewType;
    }



    View viewType;
    int pos;

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    @Override
    public int getType() {

        return ChatObject.RESPONSE_OBJECT;
    }
}
