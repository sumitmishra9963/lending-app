package com.findeed.Bot.ViewHolder;


import android.view.View;

public class BotImageResponse extends ChatObject {

    int pos;
    View viewType;

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public View getViewType() {
        return viewType;
    }

    public void setViewType(View viewType) {
        this.viewType = viewType;
    }

    @Override
    public int getType() {
        return ChatObject.BOT_MESSAGE;
    }
}
