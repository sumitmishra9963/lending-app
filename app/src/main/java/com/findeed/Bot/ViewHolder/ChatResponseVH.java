package com.findeed.Bot.ViewHolder;



import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.findeed.Bot.ChatActivity;
import com.findeed.Bot.ChatPresenter;
import com.findeed.R;



public class ChatResponseVH extends BaseViewHolder  {
    public static Handler handler = new Handler();
    public static View currentActiveView;
    private TextView tvResponseText;
    private  LinearLayout bubbleLayout;
    LinearLayout responseView;


    public ChatResponseVH(View itemView) {
        super(itemView);
        this.tvResponseText =  itemView.findViewById(R.id.tv_response_text);
       // bubbleLayout= itemView.findViewById(R.id.chat_bubble);
        responseView=itemView.findViewById(R.id.msg);
    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindView(final ChatObject object) {




//        responseView.postDelayed(() -> {

//            bubbleLayout.setVisibility(View.GONE);
//            responseView.setVisibility(View.VISIBLE);
            ChatResponse cr = (ChatResponse) object;
            if(cr.getViewType()!=null){
                if(cr.getViewType()!=null && cr.getPos()==ChatPresenter.chatObjects.size()){
                    cr.getViewType().setVisibility(View.VISIBLE);
                    currentActiveView=cr.getViewType();
                }
            }

//        },1500);



        this.tvResponseText.setText(object.getText());

    }
}
