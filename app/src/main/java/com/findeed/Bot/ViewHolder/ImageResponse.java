package com.findeed.Bot.ViewHolder;

import android.view.View;

import com.findeed.API.Data;

public class ImageResponse extends ChatObject {
    View viewType;
    int pos;
    String name;
    Data CustomerData;

    public void setCustomerData(Data customerData) {
        CustomerData = customerData;
    }

    public Data getCustomerData() {
        return CustomerData;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public View getViewType() {
        return viewType;
    }

    public void setViewType(View viewType) {
        this.viewType = viewType;
    }

    @Override
    public int getType() {
        return ChatObject.IMAGE_OBJECT;
    }


}
