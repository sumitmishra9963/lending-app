package com.findeed.Bot.ViewHolder;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.findeed.Bot.AddChild;
import com.findeed.Bot.ChatActivity;
import com.findeed.Bot.ChatPresenter;
import com.findeed.R;

import java.util.Objects;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class AddChildVH extends BaseViewHolder {
    private TextView message;
    private View currentView;
    private AddChild addChild;
    private Dialog bottomSheetDialog;
    private BottomSheetDialog b2;
    private String END_POINT = "http://157.245.102.194:8080/los-build/";
    private boolean isClassClicked=false;

    private RelativeLayout rlFirstStand;
    private RelativeLayout rlSecondStand;
    private RelativeLayout rlThirdStand;
    private RelativeLayout rlFourStand;
    private RelativeLayout rlFifthStand;
    private RelativeLayout rlSixStand;
    private RelativeLayout rlseventhstand;
    private RelativeLayout rleightstand;
    private RelativeLayout rlninthstand;
    private RelativeLayout rltenstand;
    private TextView firstStand;
    private TextView secondStand;
    private TextView thirdStand;
    private TextView fourthStand;
    private TextView fifthStand;
    private TextView sixStand;
    private TextView sevenStand;
    private TextView eightStand;
    private TextView nineStand;
    private TextView tenStand;
    private TextView selectClass;
    private String  className;
    private ImageView childClear;
    private ImageView okFirst;
    private ImageView okSecond;
    private ImageView okThird;
    private ImageView okFour;
    private ImageView okFifth;
    private ImageView okSix;
    private ImageView okSeven;
    private ImageView okEight;
    private ImageView okNine;
    private ImageView okTen;
    public AddChildVH(View itemView) {
        super(itemView);
        this.message=itemView.findViewById(R.id.child_data_input_text);
        currentView=itemView;
        clickListener();
    }



    @Override
    public void onBindView(ChatObject object) {

        message.setText(object.getChildDetail());

    }
    @SuppressLint("ResourceAsColor")
    private void clickListener() {
        try{

            ImageView editButton = currentView.findViewById(R.id.child_detail_edit_button);
            editButton.setOnClickListener(v -> {
                String clickedText=message.getText().toString();
                for(int i=3; i< ChatPresenter.chatObjects.size();i++){
                    ChatObject temp=ChatPresenter.chatObjects.get(i);
                    try{
                        if(temp.childDetail.equals(clickedText)){
                            addChild= (AddChild) ChatPresenter.chatObjects.get(i);
                            break;
                        }
                    }catch (NullPointerException e){
                        e.printStackTrace();
                    }
                }
                if(addChild.isItName()){
                    bottomSheetDialog=new Dialog(ChatActivity.chatActivity);
                    Objects.requireNonNull(bottomSheetDialog.getWindow()).setBackgroundDrawable(new
                            ColorDrawable(Color.TRANSPARENT));

                    bottomSheetDialog.setContentView(R.layout.edit_child_detail_custom_dialog);
                    bottomSheetDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                    Window window = bottomSheetDialog.getWindow();
                    window.setGravity(Gravity.CENTER);
                    bottomSheetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    EditText fName=bottomSheetDialog.findViewById(R.id.f_name);
                    EditText lName=bottomSheetDialog.findViewById(R.id.l_name);
                    TextView  cName=bottomSheetDialog.findViewById(R.id.c_name);
                    Button updateChildDetail=bottomSheetDialog.findViewById(R.id.update_child_detail);


                    cName.setOnClickListener(v1 -> {

                        b2 = new BottomSheetDialog(ChatActivity.chatActivity);
                        // testdialog.setContentView(R.layout.custom_bottomdialog_childselect);


                        bottomSheetDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                        View viewvv = View.inflate(ChatActivity.chatActivity, R.layout.custom_bottomdialog_childselect, null);
                        b2.setContentView(viewvv);
                        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(((View) viewvv.getParent()));
                        // bottomSheetBehavior.setPeekHeight(1800);

                        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                        rlFirstStand = b2.findViewById(R.id.rl_firststandard);
                        rlSecondStand = b2.findViewById(R.id.rl_secondstandard);
                        rlThirdStand = b2.findViewById(R.id.rl_thirdstandard);
                        rlFourStand = b2.findViewById(R.id.rl_forthstandard);
                        rlFifthStand = b2.findViewById(R.id.rl_fifthstandard);
                        rlSixStand= b2.findViewById(R.id.rl_sixstandard);
                        rlseventhstand = b2.findViewById(R.id.rl_sevenstandard);
                        rleightstand = b2.findViewById(R.id.rl_eightstandard);
                        rlninthstand = b2.findViewById(R.id.rl_ninthstandard);
                        rltenstand = b2.findViewById(R.id.rl_tenstandard);
                        firstStand = b2.findViewById(R.id.txt_FirstStandard);
                        secondStand = b2.findViewById(R.id.txt_SecondtStandard);
                        thirdStand = b2.findViewById(R.id.txt_ThirdStandard);
                        fourthStand = b2.findViewById(R.id.txt_FourthStandard);
                        fifthStand = b2.findViewById(R.id.txt_FifthStandard);
                        sixStand = b2.findViewById(R.id.txt_SixStandard);
                        sevenStand = b2.findViewById(R.id.txt_SevenStandard);
                        eightStand = b2.findViewById(R.id.txt_EightStandard);
                        nineStand = b2.findViewById(R.id.txt_NineStandard);
                        tenStand= b2.findViewById(R.id.txt_tenStandard);
                        selectClass = b2.findViewById(R.id.select_class);


                        rltenstand.setOnClickListener(v2 -> {
                            className="10th Standard";
                            okTen.setVisibility(View.VISIBLE);
                            okFirst.setVisibility(View.INVISIBLE);
                            okSecond.setVisibility(View.INVISIBLE);
                            okThird.setVisibility(View.INVISIBLE);
                            okFour.setVisibility(View.INVISIBLE);
                            okFifth.setVisibility(View.INVISIBLE);
                            okSix.setVisibility(View.INVISIBLE);
                            okSeven.setVisibility(View.INVISIBLE);
                            okEight.setVisibility(View.INVISIBLE);
                            okNine.setVisibility(View.INVISIBLE);

                            selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);

                        });

                        rlninthstand.setOnClickListener(v22 -> {
                            className="9th Standard";
                            okTen.setVisibility(View.INVISIBLE);
                            okFirst.setVisibility(View.INVISIBLE);
                            okSecond.setVisibility(View.INVISIBLE);
                            okThird.setVisibility(View.INVISIBLE);
                            okFour.setVisibility(View.INVISIBLE);
                            okFifth.setVisibility(View.INVISIBLE);
                            okSix.setVisibility(View.INVISIBLE);
                            okSeven.setVisibility(View.INVISIBLE);
                            okEight.setVisibility(View.INVISIBLE);
                            okNine.setVisibility(View.VISIBLE);

                            selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);

                        });

                        rleightstand.setOnClickListener(v23 -> {
                            className="8th Standard";
                            okTen.setVisibility(View.INVISIBLE);
                            okFirst.setVisibility(View.INVISIBLE);
                            okSecond.setVisibility(View.INVISIBLE);
                            okThird.setVisibility(View.INVISIBLE);
                            okFour.setVisibility(View.INVISIBLE);
                            okFifth.setVisibility(View.INVISIBLE);
                            okSix.setVisibility(View.INVISIBLE);
                            okSeven.setVisibility(View.INVISIBLE);
                            okEight.setVisibility(View.VISIBLE);
                            okNine.setVisibility(View.INVISIBLE);


                            selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);

                        });

                        rlseventhstand.setOnClickListener(v24 -> {
                            className="7th Standard";
                            okTen.setVisibility(View.INVISIBLE);
                            okFirst.setVisibility(View.INVISIBLE);
                            okSecond.setVisibility(View.INVISIBLE);
                            okThird.setVisibility(View.INVISIBLE);
                            okFour.setVisibility(View.INVISIBLE);
                            okFifth.setVisibility(View.INVISIBLE);
                            okSix.setVisibility(View.INVISIBLE);
                            okSeven.setVisibility(View.VISIBLE);
                            okEight.setVisibility(View.INVISIBLE);
                            okNine.setVisibility(View.INVISIBLE);


                            selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);

                        });

                        rlSixStand.setOnClickListener(v25 -> {
                            className="6th Standard";
                            okTen.setVisibility(View.INVISIBLE);
                            okFirst.setVisibility(View.INVISIBLE);
                            okSecond.setVisibility(View.INVISIBLE);
                            okThird.setVisibility(View.INVISIBLE);
                            okFour.setVisibility(View.INVISIBLE);
                            okFifth.setVisibility(View.INVISIBLE);
                            okSix.setVisibility(View.VISIBLE);
                            okSeven.setVisibility(View.INVISIBLE);
                            okEight.setVisibility(View.INVISIBLE);
                            okNine.setVisibility(View.INVISIBLE);


                            selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);

                        });

                        rlFifthStand.setOnClickListener(v26 -> {
                            className="5th Standard";
                            okTen.setVisibility(View.INVISIBLE);
                            okFirst.setVisibility(View.INVISIBLE);
                            okSecond.setVisibility(View.INVISIBLE);
                            okThird.setVisibility(View.INVISIBLE);
                            okFour.setVisibility(View.INVISIBLE);
                            okFifth.setVisibility(View.VISIBLE);
                            okSix.setVisibility(View.INVISIBLE);
                            okSeven.setVisibility(View.INVISIBLE);
                            okEight.setVisibility(View.INVISIBLE);
                            okNine.setVisibility(View.INVISIBLE);


                            selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);

                        });

                        rlFourStand.setOnClickListener(v27 -> {
                            className="4th Standard";
                            okTen.setVisibility(View.INVISIBLE);
                            okFirst.setVisibility(View.INVISIBLE);
                            okSecond.setVisibility(View.INVISIBLE);
                            okThird.setVisibility(View.INVISIBLE);
                            okFour.setVisibility(View.VISIBLE);
                            okFifth.setVisibility(View.INVISIBLE);
                            okSix.setVisibility(View.INVISIBLE);
                            okSeven.setVisibility(View.INVISIBLE);
                            okEight.setVisibility(View.INVISIBLE);
                            okNine.setVisibility(View.INVISIBLE);


                            selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);

                        });

                        rlThirdStand.setOnClickListener(v28 -> {
                            className = "3rd Standard";
                            okThird.setVisibility(View.VISIBLE);
                            // okTen.setVisibility(View.INVISIBLE);
                            okFirst.setVisibility(View.INVISIBLE);
                            okSecond.setVisibility(View.INVISIBLE);
                            okFour.setVisibility(View.INVISIBLE);
                            okFifth.setVisibility(View.INVISIBLE);
                            okSix.setVisibility(View.INVISIBLE);
                            okSeven.setVisibility(View.INVISIBLE);
                            okEight.setVisibility(View.INVISIBLE);
                            okNine.setVisibility(View.INVISIBLE);

                            selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);
                        });

                        rlSecondStand.setOnClickListener(v29 -> {
                            className = "2nd Standard";
                            okTen.setVisibility(View.INVISIBLE);
                            okFirst.setVisibility(View.INVISIBLE);
                            okSecond.setVisibility(View.VISIBLE);
                            okThird.setVisibility(View.INVISIBLE);
                            okFour.setVisibility(View.INVISIBLE);
                            okFifth.setVisibility(View.INVISIBLE);
                            okSix.setVisibility(View.INVISIBLE);
                            okSeven.setVisibility(View.INVISIBLE);
                            okEight.setVisibility(View.INVISIBLE);
                            okNine.setVisibility(View.INVISIBLE);

                            selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);

                        });

                        rlFirstStand.setOnClickListener(v210 -> {

                            className = "1st Standard";
                            okFirst.setVisibility(View.VISIBLE);
                            okSecond.setVisibility(View.INVISIBLE);
                            okThird.setVisibility(View.INVISIBLE);
                            okFour.setVisibility(View.INVISIBLE);
                            okFifth.setVisibility(View.INVISIBLE);
                            okSix.setVisibility(View.INVISIBLE);
                            okSeven.setVisibility(View.INVISIBLE);
                            okEight.setVisibility(View.INVISIBLE);
                            okNine.setVisibility(View.INVISIBLE);
                            okTen.setVisibility(View.INVISIBLE);


                            selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);

                        });
                        childClear = b2.findViewById(R.id.btnChildClear);
                        okFirst = b2.findViewById(R.id.okfirst);
                        okSecond = b2.findViewById(R.id.oksecond);
                        okThird = b2.findViewById(R.id.okthird);
                        okFour = b2.findViewById(R.id.okfourth);
                        okFifth = b2.findViewById(R.id.okfifth);
                        okSix = b2.findViewById(R.id.oksix);
                        okSeven = b2.findViewById(R.id.okseven);
                        okEight = b2.findViewById(R.id.okeight);
                        okNine = b2.findViewById(R.id.oknine);
                        okTen = b2.findViewById(R.id.okten);

                        Window window2 = b2.getWindow();
                        assert window2 != null;
                        window2.setGravity(Gravity.CENTER);


                        b2.show();

                        childClear.setOnClickListener(v12 -> b2.dismiss());

                        firstStand.setOnClickListener(v14 -> {
                            className = firstStand.getText().toString();
                            okFirst.setVisibility(View.VISIBLE);
                            okSecond.setVisibility(View.INVISIBLE);
                            okThird.setVisibility(View.INVISIBLE);
                            okFour.setVisibility(View.INVISIBLE);
                            okFifth.setVisibility(View.INVISIBLE);
                            okSix.setVisibility(View.INVISIBLE);
                            okSeven.setVisibility(View.INVISIBLE);
                            okEight.setVisibility(View.INVISIBLE);
                            okNine.setVisibility(View.INVISIBLE);
                            okTen.setVisibility(View.INVISIBLE);


                            selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);

                        });

                        secondStand.setOnClickListener(v16 -> {
                            className = secondStand.getText().toString();
                            okFirst.setVisibility(View.INVISIBLE);
                            okSecond.setVisibility(View.VISIBLE);
                            okThird.setVisibility(View.INVISIBLE);
                            okFour.setVisibility(View.INVISIBLE);
                            okFifth.setVisibility(View.INVISIBLE);
                            okSix.setVisibility(View.INVISIBLE);
                            okSeven.setVisibility(View.INVISIBLE);
                            okEight.setVisibility(View.INVISIBLE);
                            okNine.setVisibility(View.INVISIBLE);
                            okTen.setVisibility(View.INVISIBLE);


                            selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);
                        });
                        thirdStand.setOnClickListener(v18 -> {
                            className = thirdStand.getText().toString();

                            okFirst.setVisibility(View.INVISIBLE);
                            okSecond.setVisibility(View.INVISIBLE);
                            okThird.setVisibility(View.VISIBLE);
                            okFour.setVisibility(View.INVISIBLE);
                            okFifth.setVisibility(View.INVISIBLE);
                            okSix.setVisibility(View.INVISIBLE);
                            okSeven.setVisibility(View.INVISIBLE);
                            okEight.setVisibility(View.INVISIBLE);
                            okNine.setVisibility(View.INVISIBLE);
                            okTen.setVisibility(View.INVISIBLE);

                            selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);
                        });

                        fourthStand.setOnClickListener(v110 -> {
                            className = fourthStand.getText().toString();

                            okFirst.setVisibility(View.INVISIBLE);
                            okSecond.setVisibility(View.INVISIBLE);
                            okThird.setVisibility(View.INVISIBLE);
                            okFour.setVisibility(View.VISIBLE);
                            okFifth.setVisibility(View.INVISIBLE);
                            okSix.setVisibility(View.INVISIBLE);
                            okSeven.setVisibility(View.INVISIBLE);
                            okEight.setVisibility(View.INVISIBLE);
                            okNine.setVisibility(View.INVISIBLE);
                            okTen.setVisibility(View.INVISIBLE);


                            selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);

                        });

                        fifthStand.setOnClickListener(v19 -> {
                            className = fifthStand.getText().toString();

                            okFirst.setVisibility(View.INVISIBLE);
                            okSecond.setVisibility(View.INVISIBLE);
                            okThird.setVisibility(View.INVISIBLE);
                            okFour.setVisibility(View.INVISIBLE);
                            okFifth.setVisibility(View.VISIBLE);
                            okSix.setVisibility(View.INVISIBLE);
                            okSeven.setVisibility(View.INVISIBLE);
                            okEight.setVisibility(View.INVISIBLE);
                            okNine.setVisibility(View.INVISIBLE);
                            okTen.setVisibility(View.INVISIBLE);

                            selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);
                        });

                        sixStand.setOnClickListener(v19 -> {
                            className = sixStand.getText().toString();
                            okFirst.setVisibility(View.INVISIBLE);
                            okSecond.setVisibility(View.INVISIBLE);
                            okThird.setVisibility(View.INVISIBLE);
                            okFour.setVisibility(View.INVISIBLE);
                            okFifth.setVisibility(View.INVISIBLE);
                            okSix.setVisibility(View.VISIBLE);
                            okSeven.setVisibility(View.INVISIBLE);
                            okEight.setVisibility(View.INVISIBLE);
                            okNine.setVisibility(View.INVISIBLE);
                            okTen.setVisibility(View.INVISIBLE);


                            selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);
                        });
                        sevenStand.setOnClickListener(v19 -> {
                            className = sevenStand.getText().toString();
                            okFirst.setVisibility(View.INVISIBLE);
                            okSecond.setVisibility(View.INVISIBLE);
                            okThird.setVisibility(View.INVISIBLE);
                            okFour.setVisibility(View.INVISIBLE);
                            okFifth.setVisibility(View.INVISIBLE);
                            okSix.setVisibility(View.INVISIBLE);
                            okSeven.setVisibility(View.VISIBLE);
                            okEight.setVisibility(View.INVISIBLE);
                            okNine.setVisibility(View.INVISIBLE);
                            okTen.setVisibility(View.INVISIBLE);


                            selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);
                        });
                        eightStand.setOnClickListener(v19 -> {
                            className = eightStand.getText().toString();
                            okFirst.setVisibility(View.INVISIBLE);
                            okSecond.setVisibility(View.INVISIBLE);
                            okThird.setVisibility(View.INVISIBLE);
                            okFour.setVisibility(View.INVISIBLE);
                            okFifth.setVisibility(View.INVISIBLE);
                            okSix.setVisibility(View.INVISIBLE);
                            okSeven.setVisibility(View.INVISIBLE);
                            okEight.setVisibility(View.VISIBLE);
                            okNine.setVisibility(View.INVISIBLE);
                            okTen.setVisibility(View.INVISIBLE);

                            selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);
                        });
                        nineStand.setOnClickListener(v19 -> {
                            className = nineStand.getText().toString();
                            okFirst.setVisibility(View.INVISIBLE);
                            okSecond.setVisibility(View.INVISIBLE);
                            okThird.setVisibility(View.INVISIBLE);
                            okFour.setVisibility(View.INVISIBLE);
                            okFifth.setVisibility(View.INVISIBLE);
                            okSix.setVisibility(View.INVISIBLE);
                            okSeven.setVisibility(View.INVISIBLE);
                            okEight.setVisibility(View.INVISIBLE);
                            okNine.setVisibility(View.VISIBLE);
                            okTen.setVisibility(View.INVISIBLE);

                            selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);
                        });
                        tenStand.setOnClickListener(v19 -> {
                            className = tenStand.getText().toString();
                            okFirst.setVisibility(View.INVISIBLE);
                            okSecond.setVisibility(View.INVISIBLE);
                            okThird.setVisibility(View.INVISIBLE);
                            okFour.setVisibility(View.INVISIBLE);
                            okFifth.setVisibility(View.INVISIBLE);
                            okSix.setVisibility(View.INVISIBLE);
                            okSeven.setVisibility(View.INVISIBLE);
                            okEight.setVisibility(View.INVISIBLE);
                            okNine.setVisibility(View.INVISIBLE);
                            okTen.setVisibility(View.VISIBLE);

                            selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);
                        });

                        fourthStand.setOnClickListener(v110 -> {
                            className = fourthStand.getText().toString();

                            okFirst.setVisibility(View.INVISIBLE);
                            okSecond.setVisibility(View.INVISIBLE);
                            okThird.setVisibility(View.INVISIBLE);
                            okFour.setVisibility(View.VISIBLE);
                            okFifth.setVisibility(View.INVISIBLE);
                            okSix.setVisibility(View.INVISIBLE);
                            okSeven.setVisibility(View.INVISIBLE);
                            okEight.setVisibility(View.INVISIBLE);
                            okNine.setVisibility(View.INVISIBLE);
                            okTen.setVisibility(View.INVISIBLE);


                            selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);

                        });

                        fifthStand.setOnClickListener(v19 -> {
                            className = fifthStand.getText().toString();

                            okFirst.setVisibility(View.INVISIBLE);
                            okSecond.setVisibility(View.INVISIBLE);
                            okThird.setVisibility(View.INVISIBLE);
                            okFour.setVisibility(View.INVISIBLE);
                            okFifth.setVisibility(View.VISIBLE);
                            okSix.setVisibility(View.INVISIBLE);
                            okSeven.setVisibility(View.INVISIBLE);
                            okEight.setVisibility(View.INVISIBLE);
                            okNine.setVisibility(View.INVISIBLE);
                            okTen.setVisibility(View.INVISIBLE);


                            selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);
                        });

                        sixStand.setOnClickListener(v19 -> {
                            className = sixStand.getText().toString();
                            okFirst.setVisibility(View.INVISIBLE);
                            okSecond.setVisibility(View.INVISIBLE);
                            okThird.setVisibility(View.INVISIBLE);
                            okFour.setVisibility(View.INVISIBLE);
                            okFifth.setVisibility(View.INVISIBLE);
                            okSix.setVisibility(View.VISIBLE);
                            okSeven.setVisibility(View.INVISIBLE);
                            okEight.setVisibility(View.INVISIBLE);
                            okNine.setVisibility(View.INVISIBLE);
                            okTen.setVisibility(View.INVISIBLE);

                            selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);
                        });
                        sevenStand.setOnClickListener(v19 -> {
                            className = sevenStand.getText().toString();
                            okFirst.setVisibility(View.INVISIBLE);
                            okSecond.setVisibility(View.INVISIBLE);
                            okThird.setVisibility(View.INVISIBLE);
                            okFour.setVisibility(View.INVISIBLE);
                            okFifth.setVisibility(View.INVISIBLE);
                            okSix.setVisibility(View.INVISIBLE);
                            okSeven.setVisibility(View.VISIBLE);
                            okEight.setVisibility(View.INVISIBLE);
                            okNine.setVisibility(View.INVISIBLE);
                            okTen.setVisibility(View.INVISIBLE);


                            selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);
                        });
                        eightStand.setOnClickListener(v19 -> {
                            className = eightStand.getText().toString();
                            okFirst.setVisibility(View.INVISIBLE);
                            okSecond.setVisibility(View.INVISIBLE);
                            okThird.setVisibility(View.INVISIBLE);
                            okFour.setVisibility(View.INVISIBLE);
                            okFifth.setVisibility(View.INVISIBLE);
                            okSix.setVisibility(View.INVISIBLE);
                            okSeven.setVisibility(View.INVISIBLE);
                            okEight.setVisibility(View.VISIBLE);
                            okNine.setVisibility(View.INVISIBLE);
                            okTen.setVisibility(View.INVISIBLE);


                            selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);
                        });
                        nineStand.setOnClickListener(v19 -> {
                            className = nineStand.getText().toString();
                            okFirst.setVisibility(View.INVISIBLE);
                            okSecond.setVisibility(View.INVISIBLE);
                            okThird.setVisibility(View.INVISIBLE);
                            okFour.setVisibility(View.INVISIBLE);
                            okFifth.setVisibility(View.INVISIBLE);
                            okSix.setVisibility(View.INVISIBLE);
                            okSeven.setVisibility(View.INVISIBLE);
                            okEight.setVisibility(View.INVISIBLE);
                            okNine.setVisibility(View.VISIBLE);
                            okTen.setVisibility(View.INVISIBLE);

                            selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);


                        });
                        tenStand.setOnClickListener(v19 -> {
                            className = tenStand.getText().toString();
                            okFirst.setVisibility(View.INVISIBLE);
                            okSecond.setVisibility(View.INVISIBLE);
                            okThird.setVisibility(View.INVISIBLE);
                            okFour.setVisibility(View.INVISIBLE);
                            okFifth.setVisibility(View.INVISIBLE);
                            okSix.setVisibility(View.INVISIBLE);
                            okSeven.setVisibility(View.INVISIBLE);
                            okEight.setVisibility(View.INVISIBLE);
                            okNine.setVisibility(View.INVISIBLE);
                            okTen.setVisibility(View.VISIBLE);

                            selectClass.setBackgroundResource(R.drawable.shape_btn_addloandark);
                        });

                        selectClass.setOnClickListener(view -> {
                            updateChildDetail.setBackgroundResource(R.drawable.shape_btn_addchilddark);
                            cName.setText(className);
                            isClassClicked=true;

                            b2.dismiss();
                        });


                    });

                    updateChildDetail.requestFocus();
                    String [] childName=addChild.getChildDetail().split(" ");
                    if(childName.length==1){
                        fName.setText(childName[0]);
                        lName.setText("");
                    }else {
                        fName.setText(childName[0]);
                        lName.setText(childName[1]);
                    }



                    cName.setText(ChatPresenter.chatObjects.get(addChild.getPos()).getChildDetail());
                    bottomSheetDialog.show();
                    updateChildDetail.setOnClickListener(v1 -> {

                        if(TextUtils.isEmpty(fName.getText().toString())){
                            Toast.makeText(ChatActivity.ctx, "Please enter child name", Toast.LENGTH_SHORT).show();

                        }else {
                            updateChildDetail.setBackgroundResource(R.drawable.shape_btn_addchilddark);
                            addChild.setChildDetail(fName.getText().toString()+" "+lName.getText().toString());
                            if(isClassClicked){
                                ChatPresenter.chatObjects.get(addChild.getPos()).setChildDetail(className);
                            }else {
                                ChatPresenter.chatObjects.get(addChild.getPos()).setChildDetail(ChatPresenter.chatObjects.get(addChild.getPos()).getChildDetail());
                            }
                            ChatActivity.chatAdapter.notifyDataSetChanged();
                            bottomSheetDialog.dismiss();
                            new UpdateChildDetail(fName.getText().toString(),lName.getText().toString(),ChatPresenter.chatObjects.get(addChild.getPos()).getChildDetail(),addChild.getDependentId()).execute();
                        }


                    });
                }else {

                }


            });



        }catch (Exception e){
            e.printStackTrace();

        }



    }


    private class UpdateChildDetail extends AsyncTask<String,String ,String >{
        String fName, lName,cName;
        int dId,classNumber ;

        public UpdateChildDetail(String fName,String lName,String  cName,int dId) {
            this.fName=fName;
            this.lName=lName;
            this.cName=cName;
            this.dId=dId;
        }



        @Override
        protected String doInBackground(String... strings) {
            if(cName.equals("10th Standard")){
                classNumber=Integer.parseInt(cName.substring(0,2));
            }else {
                classNumber= Integer.parseInt(cName.substring(0,1));
            }
            OkHttpClient client = new OkHttpClient();
            if(lName.equals(" ") || lName.equals("")){
                lName="x";
            }
            Request request = new Request.Builder()
                    .url(END_POINT+"customer/saveChildDetails/"+ChatActivity.CUSTOMER_ID+"/"+dId+"/"+fName+"/"+lName+"/"+ChatActivity.SCHOOL_ID+"/"+classNumber)
                    .build();

            try{
                Response response = client.newCall(request).execute();
                return response.body().string();
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }
}
