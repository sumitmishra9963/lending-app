package com.findeed.Bot.ViewHolder;

import android.graphics.Bitmap;

import com.github.pavlospt.roundedletterview.RoundedLetterView;

public class ContactData {

    String voucherImage;
    String voucherNumber;
    String voucherName;
    String voucherStatus;
    String statusImage;
    Bitmap voucherStatusImage;

    public Bitmap getVoucherStatusImage() {
        return voucherStatusImage;
    }

    public void setVoucherStatusImage(Bitmap voucherStatusImage) {
        this.voucherStatusImage = voucherStatusImage;
    }

    public String getVoucherImage() {
        return voucherImage;
    }

    public void setVoucherImage(String voucherImage) {
        this.voucherImage = voucherImage;
    }

    public String getVoucherNumber() {
        return voucherNumber;
    }

    public void setVoucherNumber(String voucherNumber) {
        this.voucherNumber = voucherNumber;
    }

    public String getVoucherName() {
        return voucherName;
    }

    public void setVoucherName(String voucherName) {
        this.voucherName = voucherName;
    }

    public String getVoucherStatus() {
        return voucherStatus;
    }

    public void setVoucherStatus(String voucherStatus) {
        this.voucherStatus = voucherStatus;
    }

    public String getStatusImage() {
        return statusImage;
    }

    public void setStatusImage(String statusImage) {
        this.statusImage = statusImage;
    }
}
