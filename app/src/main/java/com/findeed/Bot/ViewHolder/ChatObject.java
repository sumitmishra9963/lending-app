package com.findeed.Bot.ViewHolder;

import android.graphics.Bitmap;
import android.media.Image;
import android.support.annotation.NonNull;

import com.findeed.Bot.ChatContract;

import java.security.PublicKey;

public abstract class ChatObject {

    public static final int INPUT_OBJECT = 0;
    public static final int RESPONSE_OBJECT = 1;
    public static final int IMAGE_OBJECT=2;
    public static final int BOT_MESSAGE=3;
    public  static  final  int DOC_IMAGE=4;
    public static final int SEND_CONTACT=5;
    public  static  final  int SEND_LOCATION=6;
    public static  final int  ADD_CHILD=7;
    public  static  final int VOTER_IMAGE=8;
    public static final int USER_NAME=9;


    private String text;
    Bitmap bitmap;
    Bitmap botBitMap;
    String userName;

    public void setUserName(String userName) {
        this.userName = userName;
    }

    Bitmap docBitMap;
    Bitmap locationMap;
    ContactData voucher;
    public Bitmap getVoterImage() {
        return voterImage;
    }

    public void setVoterImage(Bitmap voterImage) {
        this.voterImage = voterImage;
    }

    Bitmap voterImage;

    public String getUserName() {
        return userName;
    }

    String childDetail;

    public String getChildDetail() {
        return childDetail;
    }

    public void setChildDetail(String childDetail) {
        this.childDetail = childDetail;
    }

    public Bitmap getLocationMap() { return locationMap; }

    public void setLocationMap(Bitmap locationMap) { this.locationMap = locationMap; }
    @NonNull
    public String getText() {
        return text;
    }

    @NonNull Bitmap getImage(){return  bitmap;}

    public void setText(@NonNull String text) {
        this.text = text;
    }

    public void setImage(@NonNull Bitmap bitmap){this.bitmap=bitmap;}

    public void setBotBitMap(@NonNull Bitmap bitmap){this.botBitMap=bitmap;}

    @NonNull Bitmap getBotBitMap(){return  this.botBitMap;}

    public Bitmap getDocBitMap() { return docBitMap; }

    public void setDocBitMap(Bitmap docBitMap) { this.docBitMap = docBitMap; }

    public void setVoucher(ContactData voucher) { this.voucher = voucher; }

    public ContactData getVoucher() { return voucher; }


    public abstract int getType();
}
