package com.findeed.Bot.ViewHolder;

import android.view.View;
import android.widget.ImageView;

import com.findeed.Bot.ChatActivity;
import com.findeed.R;

public class BotImageResponseVH extends BaseViewHolder {
    ImageView imageView;
    public BotImageResponseVH(View itemView) {
        super(itemView);
        this.imageView=itemView.findViewById(R.id.botImageResponse);
    }

    @Override
    public void onBindView(ChatObject object) {

        this.imageView.setImageBitmap(object.getBotBitMap());

    }
}
