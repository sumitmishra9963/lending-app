package com.findeed.Bot.ViewHolder;


import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.findeed.API.Customer;
import com.findeed.API.Data;
import com.findeed.API.SchoolData;
import com.findeed.API.VData;
import com.findeed.API.ValidateOTPResponse;
import com.findeed.API.VoucherResponseData;
import com.findeed.ApplicationData;
import com.findeed.Bot.ChatActivity;
import com.findeed.Bot.ChatInput;
import com.findeed.Bot.ChatPresenter;
import com.findeed.R;
import com.findeed.Voucher.VoucherChatActivity;
import com.google.gson.Gson;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.findeed.ApplicationData.validateOTPResponse;
import static com.findeed.ApplicationData.voucherResponseData;


public class ChatInputVH extends BaseViewHolder {

    public ChatInput ci = null;
    private TextView tvInputText;
    View chatInputView;

    public ChatInputVH(View itemView) {
        super(itemView);
        this.tvInputText =  itemView.findViewById(R.id.tv_input_text);
        this.chatInputView=itemView;
        intializeListeners();
    }



    @Override
    public void onBindView(ChatObject object) {

            this.tvInputText.setText(object.getText());


    }


    public void intializeListeners(){

        ImageView  userMessageEdit=chatInputView.findViewById(R.id.user_message_edit_button);
        userMessageEdit.setOnClickListener(v -> {
            String currentText=tvInputText.getText().toString();
            for(int i=1;i<ChatPresenter.chatObjects.size();i++){
                ChatObject temp=ChatPresenter.chatObjects.get(i);

                try{
                    if (ChatActivity.chatAdapter == null) {

                        if (temp.getText().equals(currentText)) {
                            ci = (ChatInput) ChatPresenter.chatObjects.get(i);
                            Log.i("chatobject", "*  " + temp.getText() + " " + new Gson().toJson(ci.getVoucherData()));

                            new SendPurgedDataVoucher(ci.getVoucherData()).execute();
                        }
                    }else {
                            if (temp.getText().equals(currentText)) {
                                ci = (ChatInput) ChatPresenter.chatObjects.get(i);
                                if (ci.getText().contains("Rs")) {
                                    ci.getCustomerDetails().getLoans().get(0).setRequestAmount("0.00");
                                    ci.getCustomerDetails().getLoans().get(0).setTenure("0");
                                }
                                if (ci.getText().contains("Months")) {
                                    ci.getCustomerDetails().getLoans().get(0).setTenure("0");
                                }
                                new SendPurgedData(ci.getCustomerDetails()).execute();
                                Log.i("chatobject", "*  " + temp.getText() + " " + new Gson().toJson(ci.getCustomerDetails()));

                                break;
                            }
                        }

                }catch (NullPointerException e){
                    e.printStackTrace();
                }
            }

            try{
                if(ChatResponseVH.currentActiveView!=null){
                    ChatResponseVH.currentActiveView.setVisibility(View.GONE);
                }

                if(ci.getViewType()==ChatActivity.lvlay && ChatActivity.lvlay!=null){
                    ci.getViewType().setVisibility(View.GONE);
                    ChatActivity.btnActvClear.setVisibility(View.GONE);
                }

                if(ChatActivity.companyLayout!=null){
                    if(ChatActivity.companyLayout.getVisibility()==View.VISIBLE){
                        ChatActivity.companyLayout.setVisibility(View.GONE);
                    }
                }


                ChatPresenter.chatObjects.subList(ci.getPos()-1,ChatPresenter.chatObjects.size()).clear();

                /* Check which adapter is null*/
                if(ApplicationData.isItVoucher){
                    VoucherChatActivity.voucherAdapter.notifyDataSetChanged();
                }else {
                    ChatActivity.chatAdapter.notifyDataSetChanged();

                }

            }catch (Exception e){
                e.printStackTrace();
            }


           // ChatActivity.chatAdapter.notifyDataSetChanged();

        });




}

class SendPurgedDataVoucher extends AsyncTask<String,String ,String >{

        VData input;


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(s!=null){
                new GetVoucherDetails().execute();

            }

        }

        public SendPurgedDataVoucher(VData input) {
            this.input = input;
        }

        @Override
        protected String doInBackground(String... strings) {


            String json=new Gson().toJson(input);
            Log.i("PurgedData",json);
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            RequestBody body = RequestBody.create(JSON, json);
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url("http://157.245.102.194:8080/los-qa/voucher/editPurgeVoucher")
                    .post(body)
                    .build();
            try{
                Response response = client.newCall(request).execute();
                return response.body().string();
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }


        }
    }

class SendPurgedData extends AsyncTask<String,String ,String >{

        Data customerData;


    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if(s!=null){
            new GetCustomerDetails().execute();

        }

    }

    public SendPurgedData(Data customerData) {
        this.customerData = customerData;
    }

    @Override
    protected String doInBackground(String... strings) {


        String json = new Gson().toJson(customerData);
        Log.i("PurgedData",json);
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody body =  RequestBody.create(json, JSON);
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url("http://157.245.102.194:8080/los-qa/customer/editPurge")
                .post(body)
                .build();
        try{
            Response response = client.newCall(request).execute();
            String  a= response.body().string();
            Log.i("EditPurgeResponse",a);
            return a;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }


    }
}

    private class GetCustomerDetails extends AsyncTask<String,String ,String >
    {



        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url("http://157.245.102.194:8080/los-qa/customer/getCustomerDetails/"+validateOTPResponse.getData().getPhoneNumber())
                    .build();
            try{
                Response response = client.newCall(request).execute();
                ValidateOTPResponse vr=new Gson().fromJson(response.body().string(),ValidateOTPResponse.class);
                ChatActivity.CUSTOMER_DETAILS=vr.getData();
                ChatActivity.CUSTOMER_DETAILS.school = (SchoolData) vr.getData().getSchool().clone();
                return response.body().string();

            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }


    private class GetVoucherDetails extends AsyncTask<String,String ,String >{
        Gson gson=new Gson();
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (s != null) {

            }
        }

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url("http://157.245.102.194:8080/los-qa/voucher/getVoucherDetails/"+voucherResponseData.getData().get(0).getPhoneNumber())
                    .build();

            try{
                Response response = client.newCall(request).execute();
                String a=response.body().string();
                VoucherChatActivity.VOUCHER_DATA=gson.fromJson(a,VoucherResponseData.class);
                return a;

            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }

}
