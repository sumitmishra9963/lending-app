package com.findeed.Bot.ViewHolder;

import android.media.Image;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.findeed.R;
import com.github.pavlospt.roundedletterview.RoundedLetterView;

public class SendVouchersVH extends BaseViewHolder {


    RoundedLetterView contactImage;
    TextView voucherNumber;
    TextView voucherName;
    TextView voucherStatus;
    ImageView voucherStatusImage;
    public SendVouchersVH(View itemView) {
        super(itemView);

        this.contactImage=itemView.findViewById(R.id.contact_image);
        this.voucherName=itemView.findViewById(R.id.voucher_name);
        this.voucherNumber=itemView.findViewById(R.id.voucher_number);
        this.voucherStatus=itemView.findViewById(R.id.voucher_status);
        this.voucherStatusImage=itemView.findViewById(R.id.voucher_status_image);
    }

    @Override
    public void onBindView(ChatObject object) {

        ContactData cd = object.getVoucher();
        this.contactImage.setTitleText(cd.getVoucherImage());
        this.voucherNumber.setText(cd.getVoucherNumber());
        this.voucherName.setText(cd.getVoucherName());
        this.voucherStatus.setText(cd.getVoucherStatus());
        this.voucherStatusImage.setImageBitmap(cd.getVoucherStatusImage());

    }
}
