package com.findeed.Bot;

import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;

import com.findeed.API.Data;
import com.findeed.API.VData;
import com.findeed.Bot.ViewHolder.BotDocImage;
import com.findeed.Bot.ViewHolder.BotImageResponse;
import com.findeed.Bot.ViewHolder.ChatObject;
import com.findeed.Bot.ViewHolder.ChatResponse;
import com.findeed.Bot.ViewHolder.ContactData;
import com.findeed.Bot.ViewHolder.ImageResponse;
import com.findeed.Bot.ViewHolder.SendVoterImage;
import com.google.gson.Gson;

import java.util.ArrayList;

public class ChatPresenter implements ChatContract.Presenter {

    public static ArrayList<ChatObject> chatObjects;
    private  ChatContract.View view;
    public static int objPos;
    ChatInput inputObject;

    public ChatPresenter() {
        // Create the ArrayList for the chat objects
        chatObjects = new ArrayList<>();

    }

    @Override
    public void attachView(ChatContract.View view) {
        this.view = view;
    }

    @Override
    public ArrayList<ChatObject> getChatObjects() {
        return chatObjects;
    }

    @Override
    public void onEditTextActionDone(String inputText, View viewType,int pos) {
        Log.i("chatObject", pos+" "+inputText);
       inputObject = new ChatInput();
        inputObject.setText(inputText);
        inputObject.setViewType(viewType);
        inputObject.setPos(pos);
        objPos=pos;
        chatObjects.add(inputObject);
        view.notifyAdapterObjectAdded(chatObjects.size() - 1);
        view.scrollChatDown();
    }

    ChatInput ci;
    @Override
    public void onEditTextActionDone(String inputText, View viewType, int pos,Data customerData) {
        Log.i("chatObject", pos+" "+inputText+"   "+new Gson().toJson(customerData));
       inputObject = new ChatInput();
        inputObject.setText(inputText);
        inputObject.setViewType(viewType);
        inputObject.setPos(pos);
            inputObject.setCustomerDetails(customerData);

        objPos=pos;
        chatObjects.add(inputObject);



        view.notifyAdapterObjectAdded(chatObjects.size() - 1);
        view.scrollChatDown();

    }

    @Override
    public void onEditTextActionDone(String inputText, View viewType, int pos, VData voucherData) {
        Log.i("chatObject", inputText+" "+new Gson().toJson(voucherData));
        ChatInput inputObject = new ChatInput();
        inputObject.setText(inputText);
        inputObject.setViewType(viewType);
        inputObject.setPos(pos);
        inputObject.setVoucherData(voucherData);
        objPos=pos;
        chatObjects.add(inputObject);
        view.notifyAdapterObjectAdded(chatObjects.size() - 1);
        view.scrollChatDown();
    }


    @Override
    public void botMessage(String inputText, View viewType,int pos) {
        Log.i("chatObject", pos+" "+inputText);
        ChatResponse botReply =new ChatResponse();
        botReply.setText(inputText);
        botReply.setViewType(viewType);
        botReply.setPos(pos);
        chatObjects.add(botReply);
        view.notifyAdapterObjectAdded(chatObjects.size()-1);
        view.scrollChatDown();
    }

    @Override
    public void setImage(Bitmap bitmap, View viewType, int position, String name) {

        ImageResponse imageResponse = new ImageResponse();
        imageResponse.setImage(bitmap);
        imageResponse.setViewType(viewType);
        imageResponse.setPos(position);
        imageResponse.setName(name);
       // imageResponse.setCustomerData(customerDetails);
        objPos=position;
        chatObjects.add(imageResponse);

        view.notifyAdapterObjectAdded(chatObjects.size()-1);
        view.scrollChatDown();

    }

    @Override
    public void setBotMessageImage(Bitmap bitmap,android.view.View viewType,int pos) {
        BotImageResponse bir = new BotImageResponse();
        bir.setBotBitMap(bitmap);
        bir.setViewType(viewType);
        bir.setPos(pos);
        objPos=pos;
        chatObjects.add(bir);
        view.notifyAdapterObjectAdded(chatObjects.size()-1);
        view.scrollChatDown();

    }

    @Override
    public void sendDocsImage(Bitmap bitmap,android.view.View viewType,int pos) {
        BotDocImage bdi=new BotDocImage();
        bdi.setDocBitMap(bitmap);
        bdi.setViewType(viewType);
        bdi.setPos(pos);
        objPos=pos;
        chatObjects.add(bdi);
        view.notifyAdapterObjectAdded(chatObjects.size()-1);
        view.scrollChatDown();

    }

    @Override
    public void sendContacts(ContactData voucher,android.view.View viewType,int pos) {
        SendVouchers sv = new SendVouchers();
        sv.setVoucher(voucher);
        sv.setPos(pos);
        sv.setViewType(viewType);
        chatObjects.add(sv);
        view.notifyAdapterObjectAdded(chatObjects.size()-1);
        view.scrollChatDown();
    }

    @Override
    public void sendUserLocation(Bitmap bitmap,android.view.View viewType,int position) {
        UserLocation userLocation = new UserLocation();
        userLocation.setLocationMap(bitmap);
        userLocation.setViewType(viewType);
        userLocation.setPos(position);
        objPos=position;
        chatObjects.add(userLocation);
        view.notifyAdapterObjectAdded(chatObjects.size()-1);
        view.scrollChatDown();
    }


    @Override
    public void addChildDetail(String inputtext,android.view.View viewType,int pos,boolean isItName,int dependentId) {
        Log.i("chatObject", pos+" "+inputtext);
        AddChild addChild = new AddChild();
        addChild.setChildDetail(inputtext);
        addChild.setPos(pos);
        objPos=pos;
        addChild.setViewType(viewType);
        addChild.setItName(isItName);
        addChild.setDependentId(dependentId);
        chatObjects.add(addChild);
        view.notifyAdapterObjectAdded(chatObjects.size()-1);
        view.scrollChatDown();

    }

    @Override
    public void sendVoterIdImage(Bitmap bitmap, View viewType, int pos) {
        SendVoterImage sendVoterImage = new SendVoterImage();
        sendVoterImage.setVoterImage(bitmap);
        sendVoterImage.setPos(pos);
        objPos=pos;
        sendVoterImage.setViewType(viewType);
        chatObjects.add(sendVoterImage);
        view.notifyAdapterObjectAdded(chatObjects.size()-1);
        view.scrollChatDown();


    }

    @Override
    public void addUserName(String inputtext, View viewType, int pos, boolean isItFirstName, int customerId) {
        SaveUserName saveUserName = new SaveUserName();
        saveUserName.setCustomerId(customerId);
        saveUserName.setItFirstName(isItFirstName);
        saveUserName.setPos(pos);
        saveUserName.setViewType(viewType);
        saveUserName.setUserName(inputtext);
        chatObjects.add(saveUserName);
        view.notifyAdapterObjectAdded(chatObjects.size()-1);
        view.scrollChatDown();

    }


}
