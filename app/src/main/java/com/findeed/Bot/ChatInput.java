package com.findeed.Bot;

import android.view.View;

import com.findeed.API.Data;
import com.findeed.API.VData;
import com.findeed.API.VoucherResponseData;
import com.findeed.Bot.ViewHolder.ChatObject;

public class ChatInput extends ChatObject {

    View viewType;
    int pos;

    Data customerDetails;
    VData voucherData;

    public VData getVoucherData() {
        return voucherData;
    }

    public void setVoucherData(VData voucherData) {
        this.voucherData = voucherData;
    }

    public Data getCustomerDetails() {
        return customerDetails;
    }

    public void setCustomerDetails(Data customerDetails) {
        this.customerDetails = customerDetails;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }


    public View getViewType() {
        return viewType;
    }

    public void setViewType(View viewType) {
        this.viewType = viewType;
    }

    @Override
    public int getType() {

        return ChatObject.INPUT_OBJECT;
    }

}
