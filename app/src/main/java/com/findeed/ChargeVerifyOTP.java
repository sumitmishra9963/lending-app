package com.findeed;
import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

    import com.findeed.Bot.ChatActivity;

    import java.util.concurrent.TimeUnit;

    public class ChargeVerifyOTP extends AppCompatActivity {

        TextView textCountTime, editPhoneNumber;
        EditText edittext1, edittext2, edittext3, edittext4;
        Button verifyProceed;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_charge_verify_otp);

            textCountTime = findViewById(R.id.txt_Count_Time);
            editPhoneNumber = findViewById(R.id.edit_PhoneNumber);
            edittext1 = findViewById(R.id.edttxt1);
            edittext2 = findViewById(R.id.edttxt2);
            edittext3 = findViewById(R.id.edttxt3);
            edittext4 = findViewById(R.id.edttxt4);
            verifyProceed = findViewById(R.id.btn_Verify_Proceed);

            edittext1.addTextChangedListener(otpTextWatcherVerify);
            edittext2.addTextChangedListener(otpTextWatcherVerify);
            edittext3.addTextChangedListener(otpTextWatcherVerify);
            edittext4.addTextChangedListener(otpTextWatcherVerify);

            new CountDownTimer(30000, 1000) {

                public void onTick(long millisUntilFinished) {
                    textCountTime.setText("seconds remaining: " + millisUntilFinished / 1000);
                }

                public void onFinish() {
                    textCountTime.setText("Resend OTP");
                }
            }.start();


            editPhoneNumber.setOnClickListener(v -> {
                Intent intent = new Intent(getApplicationContext(), PhoneRegisterActivity.class);
                startActivity(intent);
            });

            verifyProceed.setOnClickListener(v -> {
                Intent intentchat = new Intent(ChargeVerifyOTP.this, ChatActivity.class);
                startActivity(intentchat);
            });

            edittext1.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    int textlength1 = edittext1.getText().length();

                    if (textlength1 >= 1) {
                        edittext2.requestFocus();
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            edittext2.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    int textlength2 = edittext2.getText().length();

                    if (textlength2 >= 1) {
                        edittext3.requestFocus();
                    }

                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            edittext3.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    int textlength3 = edittext3.getText().length();

                    if (textlength3 >= 1) {
                        edittext4.requestFocus();
                    }

                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }

        private TextWatcher otpTextWatcherVerify = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String editOneInput1 = edittext1.getText().toString().trim();
                String editTwoInput2 = edittext2.getText().toString().trim();
                String editThirdInput3 = edittext3.getText().toString().trim();
                String editFourInput4 = edittext4.getText().toString().trim();

                verifyProceed.setEnabled(!editOneInput1.isEmpty() && !editTwoInput2.isEmpty() && !editThirdInput3.isEmpty() && !editFourInput4.isEmpty());



            }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

}
